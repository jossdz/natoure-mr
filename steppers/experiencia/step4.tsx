import React, { useEffect, useRef, useState } from 'react'
import mapboxgl from 'mapbox-gl'
import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder'

import { Button, Disclaimer, Input, Label } from '../../styles/forms'
import Map, { MapWrapper } from '../../styles/forms/Map'

mapboxgl.accessToken =
  'pk.eyJ1IjoibWx6eiIsImEiOiJjandrNmVzNzUwNWZjNGFqdGcwNmJ2ZWhpIn0.ybY6wnAtJwj-Tq0c46sW6A'

type Props = {
  onStepChange: (data, step) => void
  data?: {
    addressDetails: string
    address: {
      coords: number[]
      address: string
    }
  }
}

type Field = {
  value: any
  name: string
  valid: boolean
  optional: boolean
}

type Form = {
  address?: Field
}

const Step4 = ({ onStepChange, data }: Props): JSX.Element => {
  const [form, setForm] = useState<Form>({})
  const [validForm, setValidForm] = useState(false)
  const [value, setValue] = useState({})

  const node = useRef(null)
  const addressInput = useRef(null)

  const handleChange = (e) => {
    setForm((prevState) => {
      return {
        ...prevState,
        [e.name]: e,
      }
    })
  }

  const handleClick = () => {
    const data = Object.keys(form).reduce((acc, key) => {
      if (form[key].valid) {
        return { ...acc, [key]: form[key].value }
      } else {
        return { ...acc }
      }
    }, {})
    onStepChange(data, 5)
  }

  useEffect(() => {
    const fixedLocation = [-99.1412, 19.4352]

    const map = new mapboxgl.Map({
      container: node.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: fixedLocation,
      zoom: 5,
    })

    const geocoder = new MapboxGeocoder({
      accessToken: mapboxgl.accessToken,
      mapboxgl: mapboxgl,
      marker: false,
      language: 'es',
      placeholder: 'Ingresa la dirección',
    })

    addressInput.current.appendChild(geocoder.onAdd(map))

    geocoder.on('result', ({ result }) => {
      const marker = new mapboxgl.Marker({ draggable: true }).setLngLat(result.center).addTo(map)
      marker.on('dragend', (e) => {
        const lngLat = e.target.getLngLat()
        setValue({ coords: [lngLat.lng, lngLat.lat], address: result.place_name })
      })

      setValue({ coords: result.center, address: result.place_name })
    })
  }, [])

  useEffect(
    () =>
      handleChange({
        name: 'address',
        value: value,
        valid: Object.keys(value).length > 0,
        optional: false,
      }),
    [value]
  )

  useEffect(() => {
    const requiredFields = Object.values(form).filter((item) => !item.optional)
    const isValid = requiredFields.every((e) => e.valid)
    setValidForm(isValid)
  }, [form])

  useEffect(() => {
    data &&
      'address' in data &&
      setForm((prevState) => {
        return {
          ...prevState,
          address: {
            name: 'address',
            value: data.address,
            valid: true,
            optional: false,
          },
        }
      })
  }, [data])

  return (
    <React.Fragment>
      <Label title="Señala el lugar en donde recogerás al viajero" />
      <MapWrapper>
        <div id="geocoder" className="geocoder" ref={addressInput}></div>
        <Map ref={node} />
      </MapWrapper>
      <Label title="Describe cómo llegar al punto de encuentro" />
      <Disclaimer
        style={{ marginTop: 12, marginBottom: 24 }}
        text="Recuerda que muchos de los participantes en tu experiencia no conocen el lugar, sé lo más claro posible con las indicaciones. Menciona avenidas principales o lugares conocidos que los ayuden a ubicarse mejor."
      />
      <Input
        name="addressDetails"
        placeholder="Máximo 1000 caracteres"
        textarea
        onChange={handleChange}
        defaultValue={data && data.addressDetails}
      />
      <Button unable={!validForm} onClick={handleClick} />
    </React.Fragment>
  )
}

export default Step4
