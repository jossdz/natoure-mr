import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import Dinero from 'dinero.js'
import { BsReplyFill } from 'react-icons/bs'
import {
  Button,
  Chips,
  Disclaimer,
  GreenTab,
  Input,
  Label,
  Select,
  SmallText,
  Switch,
} from '../../styles/forms'

import { includes, notIncludes, clothing, languages, months, currency, dificulty } from '../data'

const GreyBox = styled.div`
  background-color: var(--light-grey);
  border-radius: 8px;
  width: 100%;
  max-width: calc(85vw - 32px);
  margin: 0 auto 64px auto;
  padding: 24px 16px;
  text-align: center;
  @media (min-width: 426px) {
    width: auto;
  }
`

type Props = {
  onStepChange: (data, step) => void
  data?: {
    experienceIncludes: string[]
    experienceNotIncludes: string[]
    sugestedClothing?: string[]
    experienceLanguages?: string[]
    bestSeason: string
    bestMonths?: string[]
    ageRangeFirst: string
    ageRangeSecond: string
    dificultLevel: string
    maxPersons: string
    personPayment: string
    currency1: string
    personCost: string
    currency2: string
    canPay: string
  }
}

type Field = {
  value: string
  name: string
  valid: boolean
  optional: boolean
}

type Form = {
  ageRangeFirst?: Field
  personPayment?: Field
  currency1?: Field
}

const Step2 = ({ onStepChange, data }: Props): JSX.Element => {
  const [form, setForm] = useState<Form>({})
  const [validForm, setValidForm] = useState(false)
  const [ageRangeFirst, setAgeRangeFirst] = useState('')
  const [ageRangeSecond, setAgeRangeSecond] = useState('')
  const [canPay, setCanPay] = useState(false)
  const [bestSeason, setBestSeason] = useState(false)
  const [amounts, setAmounts] = useState([])
  const [showCost, setShowCost] = useState(false)
  const [cost, setCost] = useState('')

  const handleChange = (e) => {
    if (e.name === 'personPayment' && e.value.length !== 0) setShowCost(true)

    if (e.name === 'canPay' && e.value === 'Sí') setCanPay(true)
    else setCanPay(false)

    if (e.name === 'bestSeason' && e.value === 'Sí') setBestSeason(true)
    else if (e.name === 'bestSeason' && e.value === 'No') setBestSeason(false)

    setForm((prevState) => {
      return {
        ...prevState,
        [e.name]: e,
      }
    })
  }

  const handleClick = () => {
    const data = Object.keys(form).reduce((acc, key) => {
      if (form[key].valid) {
        return { ...acc, [key]: form[key].value }
      } else {
        return { ...acc }
      }
    }, {})
    onStepChange(data, 3)
  }

  useEffect(() => {
    if (bestSeason === false) {
      if ('bestMonths' in form) delete form['bestMonths']
    }
  }, [bestSeason])

  useEffect(() => {
    const requiredFields = Object.values(form).filter((item) => !item.optional)
    const isValid = requiredFields.every((e) => e.valid)
    setValidForm(isValid)
  }, [form])

  useEffect(() => {
    handleChange({ name: 'ageRange', value: '', valid: false, optional: false })
    if (
      ageRangeFirst.length !== 0 &&
      ageRangeSecond.length !== 0 &&
      Number(ageRangeFirst) < Number(ageRangeSecond)
    )
      handleChange({
        name: 'ageRange',
        value: `De ${ageRangeFirst} a ${ageRangeSecond} años`,
        valid: true,
        optional: false,
      })
  }, [ageRangeFirst, ageRangeSecond])

  useEffect(() => {
    if (canPay) {
      const baseCost = Number(form.personPayment.value) * (5 / 8)
      setAmounts([baseCost, baseCost * 1.9, baseCost * 2.7, baseCost * 3.4, baseCost * 4])
    }
  }, [canPay])

  useEffect(() => {
    if (showCost) {
      const baseCost = Math.round(Number(form.personPayment.value) * 1.34)
      setCost(baseCost.toString())
    }
  }, [showCost, form])

  return (
    <React.Fragment>
      <Label title="¿Qué incluye esta experiencia?" />
      <Chips
        name="experienceIncludes"
        options={includes}
        onChange={handleChange}
        defaultValue={data && data.experienceIncludes}
      />
      <Label title="¿Qué no incluye esta experiencia?" />
      <Chips
        name="experienceNotIncludes"
        options={notIncludes}
        onChange={handleChange}
        defaultValue={data && data.experienceNotIncludes}
      />
      <Label optional title="Ropa sugerida para el viajero" />
      <Chips
        name="sugestedClothing"
        options={clothing}
        onChange={handleChange}
        optional
        defaultValue={data && data.sugestedClothing}
      />
      <Label optional title="Idiomas en que se da la experiencia" />
      <Chips
        name="experienceLanguages"
        options={languages}
        onChange={handleChange}
        optional
        defaultValue={data && data.experienceLanguages}
      />
      <Label title="Esta actividad se realiza mejor en cierta temporada del año" />
      <Switch
        name="bestSeason"
        first="No"
        last="Sí"
        onChange={handleChange}
        defaultValue={data && data.bestSeason}
      />
      {bestSeason && (
        <>
          <Label title="¿Cuáles son los mejores meses para realizar esta actividad?" />
          <Chips
            name="bestMonths"
            options={months}
            onChange={handleChange}
            defaultValue={data && data.bestMonths}
          />
        </>
      )}
      <Label title="¿Para qué rango de edad se recomienda esta experiencia?" />
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          width: '100%',
          maxWidth: '85vw',
          margin: '0 auto 64px auto',
        }}
      >
        <SmallText style={{ width: 'max-content', margin: '0 12px 0 0' }} black title="De" />
        <Input
          number
          style={{ width: 40 }}
          adjust
          name="ageRangeFirst"
          placeholder="12"
          onChange={({ value }) => setAgeRangeFirst(value)}
          defaultValue={data && data.ageRangeFirst}
        />
        <SmallText style={{ width: 'max-content', margin: '0 12px' }} black title="a" />
        <Input
          min={Number(ageRangeFirst)}
          number
          style={{ width: 40 }}
          adjust
          name="ageRangeSecond"
          placeholder="40"
          onChange={({ value }) => setAgeRangeSecond(value)}
          defaultValue={data && data.ageRangeSecond}
        />
        <SmallText style={{ width: 'max-content', margin: '0 12px' }} black title="años" />
      </div>
      <Label title="Nivel de dificultad de la experiencia" />
      <Select
        name="dificultLevel"
        options={dificulty}
        onChange={handleChange}
        defaultValue={data && data.dificultLevel}
      />
      <Label title="Número máximo de participantes" />
      <div
        style={{
          display: 'flex',
          width: '100%',
          maxWidth: '85vw',
          margin: '0 auto 64px auto',
          alignItems: 'center',
        }}
      >
        <Input
          number
          style={{ width: 40 }}
          adjust
          name="maxPersons"
          placeholder="4"
          onChange={handleChange}
          defaultValue={data && data.maxPersons}
        />
        <SmallText black title="Personas" style={{ margin: '0 12px' }} />
      </div>
      <GreenTab style={{ marginBottom: 32 }} title="Precio por persona" />
      <SmallText black title="Monto neto que quieres recibir sin IVA" />
      <div style={{ display: 'flex', width: '100%', maxWidth: '85vw', margin: '0 auto' }}>
        <Input
          style={{ width: 48, margin: '0 18px 64px 0' }}
          adjust
          name="personPayment"
          placeholder="$1,200"
          onChange={handleChange}
          defaultValue={data && data.personPayment}
        />
        <Select
          style={{ width: '100%' }}
          name="currency1"
          options={currency}
          onChange={handleChange}
          defaultValue={data && data.currency1}
        />
      </div>
      {showCost && (
        <React.Fragment>
          <GreenTab
            style={{ marginBottom: 32 }}
            title="Precio con el cual se publicará tu experiencia"
          />
          <SmallText black title="Precio + IVA + Comisión Natoure" />
          <div style={{ display: 'flex', width: '100%', maxWidth: '85vw', margin: '0 auto' }}>
            <Input
              style={{ width: 48, margin: '0 18px 64px 0' }}
              adjust
              name="personCost"
              placeholder="$1,600"
              onChange={handleChange}
              defaultValue={cost}
              disabled
            />
            <Select
              style={{ width: '100%' }}
              name="currency2"
              options={currency}
              onChange={handleChange}
              defaultValue={form.currency1?.value}
              disabled
            />
          </div>
        </React.Fragment>
      )}
      <Label title="¿Tu precio podría cubrir el costo de más de un viajero?" />
      <Switch
        name="canPay"
        first="No"
        last="Sí"
        onChange={handleChange}
        defaultValue={data && data.canPay}
      />
      {canPay && (
        <React.Fragment>
          <Disclaimer
            style={{ marginBottom: 64 }}
            text="Al seleccionar esta opción tu experiencia entrará en promoción para hacerla más atractiva para que los viajeros se organicen en grupos de hasta 5 personas, esto nace al analizar todos los datos de ventas que hemos tenido y dado que la probabilidad nos indica que en promedio viajan 1.69 personas por cada reserva hecha, de esta manera la promoción funciona de la siguiente manera:"
          />
          <GreyBox>
            {amounts.map((item, index) => (
              <div key={index}>
                <p>
                  Si viajan <strong>{index + 1} personas</strong> recibirás:
                </p>
                <div
                  style={{
                    padding: '8px 16px',
                    backgroundColor: 'white',
                    borderRadius: 8,
                    width: 'max-content',
                    margin: '0 auto',
                  }}
                >
                  {Dinero({ amount: item * 100 }).toFormat('$0,0.00')}
                </div>
              </div>
            ))}
          </GreyBox>
          <div
            style={{ display: 'flex', width: '100%', maxWidth: '85vw', margin: '0 auto 64px auto' }}
          >
            <img src="/icons/promo.jpg" alt="promo" />
            <div style={{ width: '50%', display: 'flex', flexDirection: 'column', marginLeft: 16 }}>
              <BsReplyFill
                style={{
                  color: 'var(--green-natoure)',
                  fontSize: 80,
                  marginLeft: -16,
                  transform: 'rotate(360deg) scaleX(-1)',
                }}
              />
              <SmallText
                black
                style={{ width: '100%' }}
                title="Tu experiencia aparecerá con esta etiqueta cuando esté en promoción"
              />
            </div>
          </div>
        </React.Fragment>
      )}
      <Button unable={!validForm} onClick={handleClick} />
    </React.Fragment>
  )
}

export default Step2
