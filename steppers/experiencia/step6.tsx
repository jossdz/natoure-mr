import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { RiLeafFill } from 'react-icons/ri'

import { Button, Disclaimer, GreenTab, Label, Upload } from '../../styles/forms'

import { consejos } from '../data'

type Props = {
  onStepChange: (data, step) => void
  data?: {
    mainPhoto: string[]
    photos: string[]
  }
}

const Img = styled.img`
  width: 100%;
  max-width: 85vw;
  display: block;
  margin: 0 auto 64px auto;
  @media (min-width: 768px) {
    max-width: 425px;
  }
`

type Field = {
  value: string
  name: string
  valid: boolean
  optional: boolean
}

type Form = {
  mainPhoto?: Field
}

const Step6 = ({ onStepChange, data }: Props): JSX.Element => {
  const [form, setForm] = useState<Form>({})
  const [validForm, setValidForm] = useState(false)

  const handleChange = (e) => {
    setForm((prevState) => {
      return {
        ...prevState,
        [e.name]: e,
      }
    })
  }

  const handleClick = () => {
    const data = Object.keys(form).reduce((acc, key) => {
      if (form[key].valid) {
        return { ...acc, [key]: form[key].value }
      } else {
        return { ...acc }
      }
    }, {})
    onStepChange(data, 7)
  }

  useEffect(() => {
    const requiredFields = Object.values(form).filter((item) => !item.optional)
    const isValid = requiredFields.every((e) => e.valid)
    setValidForm(isValid)
  }, [form])

  return (
    <>
      <Img src="/icons/tips.png" alt="tips" />
      <GreenTab title="Video o foto de presentación" />
      <Disclaimer
        style={{ marginBottom: 48 }}
        text='Esta foto o video será lo primero que el viajero vea al entrar al perfil de tu experiencia, debe ser una imagen "increíble" que lo enganche y lo haga sentir que tu experiencia es imperdible.'
      />
      <Label title="Sube tu archivo" />
      <Upload
        onlyPhoto
        name="mainPhoto"
        onChange={handleChange}
        defaultValue={data && data.mainPhoto}
      />
      <Label title="Sube más fotos para mostrar en tu galería" />
      <Upload name="photos" onChange={handleChange} defaultValue={data && data.photos} />
      <Img src="/icons/preview1.png" alt="preview" />
      <Disclaimer
        style={{ marginTop: 48, marginBottom: 64 }}
        text="Tu galeria de fotos es la forma perfecta para que el viajero pueda ver un poco de lo que él vivirá durante tu experiencia. Sube tantas fotos o videos como consideres necesario."
      />
      <div style={{ width: '100%', maxWidth: '85vw', margin: '0 auto 64px auto' }}>
        <div style={{ color: '#06bc68', marginBottom: 32 }}>Consejos:</div>
        {consejos.map((item, index) => (
          <div key={index} style={{ marginBottom: 32 }}>
            <RiLeafFill style={{ position: 'absolute', color: '#06bc68' }} />
            <div style={{ marginLeft: 24 }}>{item}</div>
          </div>
        ))}
      </div>
      <Button unable={!validForm} onClick={handleClick} />
    </>
  )
}

export default Step6
