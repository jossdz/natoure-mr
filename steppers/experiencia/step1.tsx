import React, { useEffect, useState } from 'react'

import {
  Button,
  Checkbox,
  Chips,
  Disclaimer,
  GreenTab,
  Input,
  Label,
  SectionText,
  Select,
  Switch,
  Upload,
} from '../../styles/forms'

import { macros } from '../data'

type Props = {
  onStepChange: (data, step) => void
  data?: {
    experienceName: string
    experienceResume: string
    experienceMacros: string[]
    experienceActivities: string[]
    experienceRecurrent: string
    experienceTime: string
    experienceDays?: {
      name: string
      description: string
      photos: string[]
    }
    daysHours: string
  }
}

type Field = {
  value: string
  name: string
  valid: boolean
  optional: boolean
}

type Form = {
  daysHours?: Field
  experienceTime?: Field
}

const Step1 = ({ onStepChange, data }: Props): JSX.Element => {
  const [form, setForm] = useState<Form>({})
  const [validForm, setValidForm] = useState(false)
  const [renderDays, setRenderDays] = useState(false)
  const [days, setDays] = useState(0)
  const [renderActivities, setRenderActivities] = useState(false)
  const [activities, setActivities] = useState([])

  const handleChange = (e) => {
    if (e.name === 'daysHours' && e.value === 'Días') setRenderDays(true)
    else if (e.name === 'daysHours' && e.value === 'Horas') setRenderDays(false)
    else if (e.name === 'experienceMacros' && e.value.length > 0) {
      setRenderActivities(true)
      setActivities(
        e.value
          .reduce((acc: string[], key: string) => {
            return [...acc, ...macros[key]]
          }, [])
          .sort((a: string, b: string) => a.localeCompare(b))
      )
    }
    setForm((prevState) => {
      return {
        ...prevState,
        [e.name]: e,
      }
    })
  }

  const handleClick = () => {
    const data = Object.keys(form).reduce((acc, key) => {
      if (form[key].valid) {
        return { ...acc, [key]: form[key].value }
      } else {
        return { ...acc }
      }
    }, {})

    const experienceDayNames = []
    const experienceDayDescriptions = []
    const experienceDayPhotos = []

    Object.keys(data).map((item) => {
      if (item.includes('experienceDayName')) {
        experienceDayNames.push(data[item])
        delete data[item]
      }
      if (item.includes('experienceDayDescription')) {
        experienceDayDescriptions.push(data[item])
        delete data[item]
      }
      if (item.includes('experienceDayPhotos')) {
        experienceDayPhotos.push(data[item])
        delete data[item]
      }
    })

    const experienceDays = [...Array(days)].map((item, index) => {
      return {
        name: experienceDayNames[index],
        description: experienceDayDescriptions[index],
        photos: experienceDayPhotos[index],
      }
    })

    const obj = renderDays ? { experienceDays, ...data } : data

    onStepChange(obj, 2)
  }

  useEffect(() => {
    const deleted = ['experienceDayName', 'experienceDayDescription', 'experienceDayPhotos']
    if (renderDays === false) {
      ;[...Array(days)].map((item, index) => {
        deleted.map((item) => {
          if (`${item}${index + 1}` in form) delete form[`${item}${index + 1}`]
        })
      })
    }
  }, [renderDays])

  useEffect(() => {
    const requiredFields = Object.values(form).filter((item) => !item.optional)
    const isValid = requiredFields.every((e) => e.valid)
    setValidForm(isValid)
    if ('experienceTime' in form) setDays(Number(form.experienceTime?.value))
  }, [form])

  useEffect(() => {
    if (data && 'daysHours' in data && data.daysHours === 'Días' && 'experienceTime' in data) {
      setDays(Number(data.experienceTime))
      setRenderDays(true)
    }
  }, [data])

  return (
    <React.Fragment>
      <SectionText title="Rellena los siguientes campos con información acerca de tu experiencia, así los viajeros podrán saber más." />
      <Disclaimer
        bold="Nota importante:"
        text="Los datos personales recabados estarán protegidos, por favor revisa nuestra "
        link="politica de privacidad."
        href="/politica"
        style={{ marginTop: 48, marginBottom: 48 }}
      />
      <Label title="Nombre de la experiencia" />
      <Input
        name="experienceName"
        placeholder="Ingresa el nombre"
        onChange={handleChange}
        defaultValue={data && data.experienceName}
      />
      <Label title="Resumen de la experiencia" />
      <Disclaimer
        style={{ marginTop: 12, marginBottom: 24 }}
        text="Escribe de forma breve lo que harán los participantes de tu experiencia, los objetivos de la misma y que ofrece esta experiencia que la hace única."
      />
      <Input
        name="experienceResume"
        placeholder="Máximo 1000 caracteres"
        textarea
        onChange={handleChange}
        defaultValue={data && data.experienceResume}
      />
      <Label title="¿A qué categoría pertenece tu experiencia?" />
      <Disclaimer
        style={{ marginTop: 12, marginBottom: 24 }}
        text={
          <ul style={{ paddingInlineStart: '20px' }}>
            <li>
              Naturaleza: Ecoturismo, turismo ecológico. Vive experiencias para desconectarte de las
              ciudades y conectar con la naturaleza.
            </li>
            <li>
              Rural: Conocido tambiñen como Turismo Comunitario en Sudamérica, es turismo que se
              practica de la mano de anfitriones de pueblos indígenas y comunidades locales.
            </li>
            <li>Aventura: Experiencias inolvidables para vibrar y recargarse de adrenalina.</li>
            <li>
              BioCultural: Turismo que involucra visitas a ruinas, monumentos históricos,
              gastronomía, asrtesanías.
            </li>
            <li>
              Regenerativo: Turismo para restaurar y regenar nuestro mundo, enfocado a restauración
              de lugares y actividaes para sanar la naturaleza
            </li>
            <li>
              Agroturismo: Turismo enfocado en conocer los medios de producción agrícola en todas
              sus formas.
            </li>
            <li>
              Rutas gastronómicas: Experiencias turísticas para disfrutar la riqueza gastronómica de
              las regiones, turismo gordo, turismo del bueno.
            </li>
            <li>
              Ciencia y conservación: El turismo científico es una actividad donde visitantes
              participan de la generación y difusión de conocimientos científicos, llevados por
              centros de investigación y desarrollo.
            </li>
          </ul>
        }
      />
      <Checkbox
        name="experienceMacros"
        options={Object.keys(macros)}
        onChange={handleChange}
        defaultValue={data && data.experienceMacros}
      />
      {renderActivities && (
        <React.Fragment>
          <Label title="¿Qué actividades se realizan en esta experiencia?" />
          <Chips
            name="experienceActivities"
            options={activities}
            onChange={handleChange}
            defaultValue={data && data.experienceActivities}
          />
        </React.Fragment>
      )}
      <Label title="¿Esta es una experiencia recurrente o se da una sola vez?" />
      <Select
        name="experienceRecurrent"
        options={['Recurrente', 'Una sola vez']}
        onChange={handleChange}
        defaultValue={data && data.experienceRecurrent}
      />
      <Label title="¿Cuál es el periodo de duración de tu experiencia?" />
      <div style={{ display: 'flex', width: '100%', maxWidth: '85vw', margin: '0 auto' }}>
        <Input
          number
          style={{ width: 24, margin: '0 24px 64px 0' }}
          adjust
          name="experienceTime"
          placeholder="4"
          onChange={handleChange}
          defaultValue={data && data.experienceTime}
        />
        <Switch
          name="daysHours"
          first="Horas"
          last="Días"
          onChange={handleChange}
          defaultValue={data && data.daysHours}
        />
      </div>
      {renderDays && (
        <React.Fragment>
          <Disclaimer
            style={{ marginBottom: 48 }}
            text="Ponle un nombre a cada día de tu experiencia y describe brevemente las actividades que se realizarán cada día"
          />
          {[...Array(days)].map((item, index) => (
            <div key={index}>
              <GreenTab title={`Día ${index + 1}`} />
              <Label title="Título del día" />
              <Input
                name={`experienceDayName${index + 1}`}
                placeholder="Ingresa el nombre"
                onChange={handleChange}
                defaultValue={data && 'experienceDays' in data && data.experienceDays[index].name}
              />
              <Label title="Descripción del día" />
              <Input
                name={`experienceDayDescription${index + 1}`}
                placeholder="Máximo 1000 caracteres"
                textarea
                onChange={handleChange}
                defaultValue={
                  data && 'experienceDays' in data && data.experienceDays[index].description
                }
              />
              <Label title="Sube algunas fotos o videos" />
              <Upload
                name={`experienceDayPhotos${index + 1}`}
                onChange={handleChange}
                defaultValue={data && 'experienceDays' in data && data.experienceDays[index].photos}
              />
            </div>
          ))}
        </React.Fragment>
      )}
      <Button style={{ marginBottom: 96 }} unable={!validForm} onClick={handleClick} />
    </React.Fragment>
  )
}

export default Step1
