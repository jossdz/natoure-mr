import React, { useEffect, useState } from 'react'

import { Button, Checkbox, SectionText } from '../../styles/forms'

type Props = {
  onStepChange: (data, step) => void
  onSubmit: () => void
}

type Field = {
  value: string
  name: string
  valid: boolean
  optional: boolean
}

type Form = {
  confirmation?: Field
}

const Step7 = ({ onStepChange, onSubmit }: Props): JSX.Element => {
  const [form, setForm] = useState<Form>({})
  const [validForm, setValidForm] = useState(false)

  const handleChange = (e) => {
    setForm((prevState) => {
      return {
        ...prevState,
        [e.name]: e,
      }
    })
    const data = e.valid ? { [e.name]: e.value } : {}
    onStepChange(data, 7)
  }

  useEffect(() => {
    const requiredFields = Object.values(form).filter((item) => !item.optional)
    const isValid = requiredFields.every((e) => e.valid)
    setValidForm(isValid)
  }, [form])

  return (
    <>
      <SectionText title="Si estás listo para publicar, confirma las condiciones de abajo y presiona el botón de Publicar para finalizar." />
      <Checkbox
        name="confirmation"
        options={[
          'Confirmo que soy propietario y/o tengo las licencias de uso, así como que mi experiencia cumple con las legislaciones y leyes locales.',
        ]}
        onChange={handleChange}
        disclaimer={false}
      />
      <Button unable={!validForm} onClick={onSubmit} text="Publicar" />
    </>
  )
}

export default Step7
