import React, { useEffect, useState } from 'react'

import { Article, Button, Checkbox, GreenTab, Label, Switch } from '../../styles/forms'

import { extraAttr, disabilities } from '../data'

type Article = {
  cientificName: string
  commonName: string
  photos: string[]
  priority: string
}

type Props = {
  onStepChange: (data, step) => void
  data?: {
    alcoholic: string
    extraAttr: string[]
    disabledPeople: string
    disabilities?: string[]
    flora: Article[]
    fauna: Article[]
  }
}

type Field = {
  value: string
  name: string
  valid: boolean
  optional: boolean
}

type Form = {
  extraAttr?: Field
}

const Step3 = ({ onStepChange, data }: Props): JSX.Element => {
  const [form, setForm] = useState<Form>({})
  const [validForm, setValidForm] = useState(false)
  const [canDisabled, setCanDisabled] = useState(false)
  const [flora, setFlora] = useState(0)
  const [fauna, setFauna] = useState(0)

  const handleChange = (e) => {
    if (e.name === 'disabledPeople' && e.value === 'Sí') setCanDisabled(true)
    else if (e.name === 'disabledPeople' && e.value === 'No') setCanDisabled(false)
    if (e.name.includes('flora')) setFlora(flora + 1)
    if (e.name.includes('fauna')) setFauna(fauna + 1)
    setForm((prevState) => {
      return {
        ...prevState,
        [e.name]: e,
      }
    })
  }

  const handleClick = () => {
    const data = Object.keys(form).reduce((acc, key) => {
      if (form[key].valid) {
        return { ...acc, [key]: form[key].value }
      } else {
        return { ...acc }
      }
    }, {})

    const fauna = []
    const flora = []

    Object.keys(data).map((item) => {
      if (item.includes('fauna')) {
        fauna.push(data[item])
        delete data[item]
      }
      if (item.includes('flora')) {
        flora.push(data[item])
        delete data[item]
      }
    })

    const obj =
      fauna.length === 0 && flora.length !== 0
        ? { flora, ...data }
        : flora.length === 0 && fauna.length !== 0
        ? { fauna, ...data }
        : fauna.length === 0 && flora.length === 0
        ? { ...data }
        : { fauna, flora, ...data }

    onStepChange(obj, 4)
  }

  useEffect(() => {
    if (canDisabled === false) {
      if ('disabilities' in form) delete form['disabilities']
    }
  }, [canDisabled])

  useEffect(() => {
    const requiredFields = Object.values(form).filter((item) => !item.optional)
    const isValid = requiredFields.every((e) => e.valid)
    setValidForm(isValid)
  }, [form])

  useEffect(() => {
    if (data) {
      if ('flora' in data)
        setForm((prevState) => {
          return {
            ...prevState,
            flora: { name: 'flora', value: data.flora, optional: false, valid: true },
          }
        })
      if ('fauna' in data)
        setForm((prevState) => {
          return {
            ...prevState,
            fauna: { name: 'fauna', value: data.fauna, optional: false, valid: true },
          }
        })
    }
  }, [data])

  return (
    <>
      <Label title="¿En esta experiencia se consumen bebidas alcohólicas?" />
      <Switch
        name="alcoholic"
        first="No"
        last="Sí"
        onChange={handleChange}
        defaultValue={data && data.alcoholic}
      />
      <Label title="Atributos extra de la experiencia" />
      <Checkbox
        name="extraAttr"
        options={extraAttr}
        onChange={handleChange}
        defaultValue={data && data.extraAttr}
      />
      <Label title="¿En esta experiencia pueden participar personas con discapacidad?" />
      <Switch
        name="disabledPeople"
        first="No"
        last="Sí"
        onChange={handleChange}
        defaultValue={data && data.disabledPeople}
      />
      {canDisabled && (
        <>
          <Label title="¿Con qué tipo de discapacidad?" />
          <Checkbox
            name="disabilities"
            options={disabilities}
            onChange={handleChange}
            defaultValue={data && data.disabilities}
          />
        </>
      )}
      {!data && (
        <>
          <GreenTab title="Atractivos de la experiencia" />
          <Label title="Fauna" />
          <Article name={`fauna${fauna + 1}`} onChange={handleChange} />
          <Label title="Flora" />
          <Article name={`flora${flora + 1}`} onChange={handleChange} />
        </>
      )}
      <Button unable={!validForm} onClick={handleClick} />
    </>
  )
}

export default Step3
