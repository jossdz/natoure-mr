import React, { forwardRef, useEffect, useState } from 'react'
import DatePicker, { registerLocale } from 'react-datepicker'
import es from 'date-fns/locale/es'
import format from 'date-fns/format'
import { BsPlusCircleFill } from 'react-icons/bs'
import { HiMinusCircle } from 'react-icons/hi'
import { Button, Checkbox, Input, Label, Select } from '../../styles/forms'
import { frequency, repetition, ordinals, doubleOrdinals, weekDays } from '../data'
import styled from 'styled-components'

const Wrapper = styled.button`
  position: relative;
  width: 100%;
  max-width: 85vw;
  margin: 0 auto;
  font-family: 'Montserrat', sans-serif;
  height: 44px;
  padding: 12px 18px;
  border-radius: 8px;
  border: solid 1px var(--grey);
  background-color: var(--white);
  text-align: left;
  color: var(--dark-grey);
  overflow-x: scroll;
  ::-webkit-scrollbar {
    display: none;
  }
`

registerLocale('es', es)

type Props = {
  onStepChange: (data, step) => void
  data?: {
    frequency: string
    repetition: string
    experienceHours: string[]
    days: string[] | string
    ordinalNumber: string
  }
}

type PropsCalendar = {
  onClick?: () => void
  value?: string
}

type RefType = number

type Field = {
  value: string
  name: string
  valid: boolean
  optional: boolean
}

type Form = {
  repetition?: Field
}

const Step5 = ({ onStepChange, data }: Props): JSX.Element => {
  const [form, setForm] = useState<Form>({})
  const [validForm, setValidForm] = useState(false)
  const [custom, setCustom] = useState(false)
  const [times, setTimes] = useState(1)
  const [dates, setDates] = useState([])
  const [repeat, setRepeat] = useState('')

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const CalendarInput = forwardRef<RefType, PropsCalendar>(({ onClick }, ref: any) => (
    <Wrapper onClick={onClick} ref={ref}>
      {dates.length === 0
        ? 'Selecciona uno o más días'
        : dates.map((item, index) => (
            <span key={index} style={{ paddingRight: 8, color: '#1c1c1c' }}>
              {item}
            </span>
          ))}
    </Wrapper>
  ))
  CalendarInput.displayName = 'CalendarInput'

  const handleChange = (e) => {
    if (e.name === 'repetition') setRepeat(e.value)
    if (e.value === 'Personalizado') {
      setCustom(true)
    } else if (e.value === 'Diariamente') {
      setCustom(false)
      delete form['calendar']
      delete form['ordinalNumber']
      delete form['repetition']
      delete form['days']
    }
    if (e.value === 'Semanalmente') delete form['ordinalNumber']

    setForm((prevState) => {
      return {
        ...prevState,
        [e.name]: e,
      }
    })
  }

  const handleClick = () => {
    const data = Object.keys(form).reduce((acc, key) => {
      if (form[key].valid) {
        return { ...acc, [key]: form[key].value }
      } else {
        return { ...acc }
      }
    }, {})

    const experienceHours = []

    Object.keys(data).map((item) => {
      if (item.includes('experienceHour')) {
        experienceHours.push(data[item])
        delete data[item]
      }
    })

    const days = []

    Object.keys(data).map((item) => {
      if (item === 'ordinalNumber') {
        const stringDays = `${data['ordinalNumber']} ${data['days']}`
        days.push(stringDays)
        delete data['ordinalNumber']
        delete data['days']
      }
    })

    onStepChange({ experienceHours, days, ...data }, 6)
  }

  const onDeleteSchedule = (name) => {
    setTimes(times - 1)
    delete form[name]
  }

  useEffect(() => {
    const requiredFields = Object.values(form).filter((item) => !item.optional)
    const isValid = requiredFields.every((e) => e.valid)
    setValidForm(isValid)
  }, [form])

  useEffect(() => {
    repeat === 'Anualmente' &&
      setForm((prevState) => {
        return {
          ...prevState,
          days: { name: 'days', value: dates, valid: dates.length !== 0, optional: false },
        }
      })
  }, [repeat])

  useEffect(() => {
    if (dates.length !== 0) {
      delete form['ordinalNumber']
      setForm((prevState) => {
        return {
          ...prevState,
          days: { name: 'days', value: dates, valid: dates.length !== 0, optional: false },
        }
      })
    }
  }, [dates])

  return (
    <React.Fragment>
      <Label title="Frecuencia de la experiencia" />
      <Select
        name="frequency"
        options={frequency}
        onChange={handleChange}
        defaultValue={data && data.frequency}
      />
      {custom && (
        <React.Fragment>
          <Label title="Repetir" />
          <Select
            name="repetition"
            options={repetition}
            onChange={handleChange}
            defaultValue={data && data.repetition}
          />
          {repeat === 'Semanalmente' ? (
            <React.Fragment>
              <Label title="Elige los días de la semana" />
              <Checkbox
                name="days"
                options={weekDays}
                onChange={handleChange}
                defaultValue={data && (data.days as string[])}
              />
            </React.Fragment>
          ) : repeat === 'Quincenalmente' || repeat === 'Mensualmente' ? (
            <React.Fragment>
              <Label title="Elige los días" />
              <div
                style={{
                  width: '100%',
                  maxWidth: '85vw',
                  display: 'flex',
                  justifyContent: 'space-between',
                  margin: '0 auto 64px auto',
                }}
              >
                <Select
                  style={{ width: 'calc(100% - 16px)' }}
                  name="ordinalNumber"
                  options={repeat === 'Quincenalmente' ? doubleOrdinals : ordinals}
                  onChange={handleChange}
                  optional={!custom}
                  defaultValue={data && (data.days as string)}
                />
                <Select
                  style={{ width: 'calc(100% - 16px)' }}
                  name="days"
                  options={weekDays}
                  onChange={handleChange}
                  optional={!custom}
                  defaultValue={data && (data.days as string)}
                />
              </div>
            </React.Fragment>
          ) : repeat === 'Anualmente' ? (
            <React.Fragment>
              <Label title="Elige los días del mes en que se llevará a cabo la experiencia" />
              <div style={{ width: '100%', maxWidth: '85vw', margin: '0 auto 64px auto' }}>
                <DatePicker
                  onChange={(date) => {
                    const formated = format(date, 'dd/MM/yyyy')
                    dates.includes(formated)
                      ? setDates((prevState) => {
                          return prevState.filter((item) => item !== formated)
                        })
                      : setDates((prevState) => {
                          return [...prevState, formated]
                        })
                  }}
                  locale="es"
                  minDate={new Date()}
                  placeholderText="I have been cleared!"
                  customInput={<CalendarInput />}
                />
              </div>
            </React.Fragment>
          ) : (
            ''
          )}
        </React.Fragment>
      )}
      <Label title="¿A qué hora comienza la experiencia?" />
      <div
        style={{
          display: 'flex',
          width: '100%',
          maxWidth: '85vw',
          margin: '16px auto 48px auto',
          alignItems: 'flex-end',
        }}
      >
        <div>
          {[...Array(times)].map((item, index) => (
            <div key={index} style={{ display: 'flex' }}>
              <Input
                adjust
                style={{ width: 128, marginBottom: 16 }}
                name={`experienceHour${index + 1}`}
                placeholder="13:00 hrs"
                onChange={handleChange}
                defaultValue={data && 'experienceHours' in data && data.experienceHours[index]}
              />
              {index !== 0 && (
                <button
                  style={{
                    padding: 0,
                    backgroundColor: 'transparent',
                    border: 'none',
                    fontSize: 20,
                    color: '#e02020',
                    margin: '0 0 8px 8px',
                  }}
                  onClick={() => onDeleteSchedule(`experienceHour${index + 1}`)}
                >
                  <HiMinusCircle />
                </button>
              )}
            </div>
          ))}
        </div>
        <button
          style={{
            padding: 0,
            backgroundColor: 'transparent',
            border: 'none',
            fontSize: 32,
            color: '#06bc68',
            margin: '0 0 12px 16px',
          }}
          onClick={() => setTimes(times + 1)}
        >
          <BsPlusCircleFill />
        </button>
      </div>
      <Button unable={!validForm} onClick={handleClick} />
    </React.Fragment>
  )
}

export default Step5
