import React, { useEffect, useState } from 'react'
import { BsPlus } from 'react-icons/bs'

import {
  Button,
  ButtonOutlined,
  Checkbox,
  Chips,
  Disclaimer,
  Input,
  InputCard,
  Label,
  SectionHeading,
  SectionText,
  Select,
  SmallText,
  Switch,
  Upload,
} from '../../styles/forms'

import { organizationType, macros, activityTypes, politicas, comunities, langs } from '../data'

type Field = {
  value: string
  name: string
  valid: boolean
  optional: boolean
}

type Form = {
  organizationPhotos?: Field
}

type Props = {
  onStepChange: (data, step) => void
  data?: {
    organizationName: string
    organizationCountry: string
    organizationType: string[]
    isTouristic: string
    experienceTypes: string[]
    activityTypes: string[]
    hospedaje: string
    restaurant: string
    organizationPeople: string
    organizationWomen: string
    organizationStory: string
    comunity?: string[]
    language?: string[]
    organizationPolitics: string[]
    organizationRegister?: string
    guideAcreditation?: string
    organizationPhotos: string[]
    rules: string[]
    organizationDescription: string
  }
}

const Step1 = ({ onStepChange, data }: Props): JSX.Element => {
  const [form, setForm] = useState<Form>({})
  const [validForm, setValidForm] = useState(false)
  const [rules, setRules] = useState(1)

  const handleChange = (e) => {
    setForm((prevState) => {
      return {
        ...prevState,
        [e.name]: e,
      }
    })
  }

  const handleClick = () => {
    const data = Object.keys(form).reduce((acc, key) => {
      if (form[key].valid) {
        return { ...acc, [key]: form[key].value }
      } else {
        return { ...acc }
      }
    }, {})

    const rules = []

    Object.keys(data).map((item) => {
      if (item.includes('rule')) {
        rules.push(data[item])
        delete data[item]
      }
    })

    onStepChange({ rules, ...data }, 2)
  }

  const handleDeleteRule = (name) => {
    delete form[name]
  }

  useEffect(() => {
    const requiredFields = Object.values(form).filter((item) => !item.optional)
    const isValid = requiredFields.every((e) => e.valid)
    setValidForm(isValid)
  }, [form])

  useEffect(() => {
    data && 'rules' in data && setRules(data.rules.length)
  }, [data])

  return (
    <React.Fragment>
      <SectionText
        style={{ marginBottom: 64 }}
        title="Rellena los siguientes campos con información acerca de tu organización, así podras registrate y comenzar a ofrecer productos a los viajeros."
      />
      <Label title="Agrega el logo de tu organización" />
      <Upload
        name="organizationPhotos"
        onChange={handleChange}
        defaultValue={data && data.organizationPhotos}
        onlyPhoto
      />
      <Label title="¿Cuál es el nombre de tu organización?" />
      <Input
        name="organizationName"
        placeholder="Ingresa el nombre"
        onChange={handleChange}
        defaultValue={data && data.organizationName}
      />
      <Label title="País" />
      <Select
        name="organizationCountry"
        options={['México', 'Estados Unidos', 'Colombia']}
        onChange={handleChange}
        defaultValue={data && data.organizationCountry}
      />
      <Label title="¿Qué tipo de organización son?" />
      <Chips
        name="organizationType"
        options={organizationType}
        onChange={handleChange}
        value={data && data.organizationType}
        defaultValue={data && data.organizationType}
      />
      <Label title="¿Brindan algún tipo de experiencia turística?" />
      <Switch
        name="isTouristic"
        first="No"
        last="Sí"
        onChange={handleChange}
        defaultValue={data && data.isTouristic}
      />
      <Label title="Tipo de experiencias turísticas que brindan" />
      <Checkbox
        name="experienceTypes"
        options={Object.keys(macros)}
        onChange={handleChange}
        defaultValue={data && data.experienceTypes}
      />
      <Label title="Actividades que realizan además del turismo" optional />
      <Checkbox
        name="activityTypes"
        options={activityTypes}
        onChange={handleChange}
        defaultValue={data && data.activityTypes}
        optional
      />
      <Label title="¿Cuentan con algún tipo de hospedaje?" />
      <Switch
        name="hospedaje"
        first="No"
        last="Si"
        onChange={handleChange}
        defaultValue={data && data.hospedaje}
      />
      <Label title="¿Cuentan con un restaurante o experiencia gastronómica?" />
      <Switch
        name="restaurant"
        first="No"
        last="Si"
        onChange={handleChange}
        defaultValue={data && data.restaurant}
      />
      <Label title="¿Cuántas personas conforman su organización?" />
      <div
        style={{
          display: 'flex',
          width: '100%',
          maxWidth: '85vw',
          margin: '0 auto 64px auto',
          alignItems: 'center',
        }}
      >
        <Input
          number
          style={{ width: 40 }}
          adjust
          name="organizationPeople"
          placeholder="4"
          onChange={handleChange}
          defaultValue={data && data.organizationPeople}
        />
        <SmallText black title="Personas" style={{ margin: '0 12px' }} />
      </div>
      <Label title="De estas ¿cuántas son mujeres?" />
      <div
        style={{
          display: 'flex',
          width: '100%',
          maxWidth: '85vw',
          margin: '0 auto 64px auto',
          alignItems: 'center',
        }}
      >
        <Input
          number
          style={{ width: 40 }}
          adjust
          name="organizationWomen"
          placeholder="4"
          onChange={handleChange}
          defaultValue={data && data.organizationWomen}
        />
        <SmallText black title="Mujeres" style={{ margin: '0 12px' }} />
      </div>
      <Label title="¿Quiénes son? ¿Cuál es su historia?" />
      <Disclaimer
        style={{ marginTop: 12, marginBottom: 24 }}
        text="Cuéntanos qué es lo que los motiva, cuál es el cambio que quieren lograr, desde cuándo existen como organización, su experiencia y trayectoria... en fin, todo eso que quieres que el viajero conozca de ustedes."
      />
      <Input
        name="organizationStory"
        placeholder="Máximo 1000 caracteres"
        textarea
        onChange={handleChange}
        defaultValue={data && data.organizationStory}
      />
      <Label title="¿Qué hacemos? (Explícanos a grandes rasgos de qué tratan los servicios turísticos que brindan)" />
      <Input
        name="organizationDescription"
        placeholder="Máximo 1000 caracteres"
        textarea
        onChange={handleChange}
        defaultValue={data && data.organizationDescription}
      />
      <Label title="¿Pertenecen a algún pueblo o comunidad indígena?" optional />
      <Chips
        name="comunity"
        options={comunities}
        onChange={handleChange}
        optional
        defaultValue={data && data.comunity}
      />
      <Label title="¿Hablan alguna lengua o dialecto indígena?" optional />
      <Chips
        name="language"
        options={langs}
        onChange={handleChange}
        optional
        defaultValue={data && data.language}
      />
      <Label title="Nuestras políticas de turismo incluyente" />
      <Checkbox
        name="organizationPolitics"
        options={politicas}
        onChange={handleChange}
        defaultValue={data && data.organizationPolitics}
      />
      <Label optional title="Número de inscripción al Registro Nacional de Turismo" />
      <Input
        name="organizationRegister"
        placeholder="Ingresa el número"
        onChange={handleChange}
        optional
        defaultValue={data && data.organizationRegister}
      />
      <Label optional title="Número de Acreditación de guía de turistas por la CONANP" />
      <Input
        name="guideAcreditation"
        placeholder="Ingresa el número"
        onChange={handleChange}
        optional
        defaultValue={data && data.guideAcreditation}
      />
      <SectionHeading title="Normas registradas para el viajero" />
      <SectionText
        style={{ marginBottom: 48 }}
        title='Son las reglas que deben seguir los "NATOURISTAS" dentro de las Áreas de Importancia de Conservación para preservarlas y cuidarlas. Agrega al menos una regla.'
      />
      <Label title="Normas registradas" />
      {[...Array(rules)].map((item, index) => (
        <InputCard
          key={index}
          index={index + 1}
          name={`rule${index + 1}`}
          placeholder="Ingresa una norma"
          onChange={handleChange}
          onDelete={handleDeleteRule}
          defaultValue={data && 'rules' in data && data.rules[index]}
        />
      ))}
      <ButtonOutlined
        onClick={() => setRules(rules + 1)}
        style={{ marginBottom: 64 }}
        text={
          <React.Fragment>
            Agregar norma <BsPlus />
          </React.Fragment>
        }
      />
      <Button style={{ marginBottom: 96 }} unable={!validForm} onClick={handleClick} />
    </React.Fragment>
  )
}

export default Step1
