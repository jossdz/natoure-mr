import React, { useEffect, useRef, useState } from 'react'
import mapboxgl from 'mapbox-gl'
import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder'

import { Button, Disclaimer, Input, Label, Map, Select, SmallText } from '../../styles/forms'

import { states, consarea } from '../data'

mapboxgl.accessToken =
  'pk.eyJ1IjoibWx6eiIsImEiOiJjandrNmVzNzUwNWZjNGFqdGcwNmJ2ZWhpIn0.ybY6wnAtJwj-Tq0c46sW6A'

type Props = {
  onStepChange: (data, step) => void
  data?: {
    orgState: string
    orgMunicipio: string
    consarea: string
    arrive: string
    destino: string
    address: {
      coords: number[]
      address: string
    }
    airport: string
    bus: string
    city: string
  }
}

type Field = {
  value: string
  name: string
  valid: boolean
  optional: boolean
}

type Form = {
  orgState?: Field
}

const Step2 = ({ onStepChange, data }: Props): JSX.Element => {
  const [form, setForm] = useState<Form>({})
  const [validForm, setValidForm] = useState(false)
  const [address, setAddress] = useState({})
  const [city, setCity] = useState('')
  const [airport, setAirport] = useState('')
  const [bus, setBus] = useState('')

  const node1 = useRef(null)
  const node2 = useRef(null)
  const node3 = useRef(null)
  const node4 = useRef(null)

  const handleChange = (e) => {
    setForm((prevState) => {
      return {
        ...prevState,
        [e.name]: e,
      }
    })
  }

  const handleClick = () => {
    const data = Object.keys(form).reduce((acc, key) => {
      if (form[key].valid) {
        return { ...acc, [key]: form[key].value }
      } else {
        return { ...acc }
      }
    }, {})
    onStepChange(data, 3)
  }

  useEffect(() => {
    const requiredFields = Object.values(form).filter((item) => !item.optional)
    const isValid = requiredFields.every((e) => e.valid)
    setValidForm(isValid)
  }, [form])

  useEffect(() => {
    const fixedLocation = [-99.1412, 19.4352]

    const map1 = new mapboxgl.Map({
      container: node1.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: fixedLocation,
      zoom: 5,
    })

    const map2 = new mapboxgl.Map({
      container: node2.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: fixedLocation,
      zoom: 5,
    })

    const map3 = new mapboxgl.Map({
      container: node3.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: fixedLocation,
      zoom: 5,
    })

    const map4 = new mapboxgl.Map({
      container: node4.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: fixedLocation,
      zoom: 5,
    })

    const geocoder1 = new MapboxGeocoder({
      accessToken: mapboxgl.accessToken,
      mapboxgl: mapboxgl,
      language: 'es',
      placeholder: 'Buscar',
    })

    const geocoder2 = new MapboxGeocoder({
      accessToken: mapboxgl.accessToken,
      mapboxgl: mapboxgl,
      language: 'es',
      placeholder: 'Buscar',
    })

    const geocoder3 = new MapboxGeocoder({
      accessToken: mapboxgl.accessToken,
      mapboxgl: mapboxgl,
      language: 'es',
      placeholder: 'Buscar',
    })

    const geocoder4 = new MapboxGeocoder({
      accessToken: mapboxgl.accessToken,
      mapboxgl: mapboxgl,
      language: 'es',
      placeholder: 'Buscar',
    })

    map1.addControl(geocoder1, 'top-left')

    map2.addControl(geocoder2, 'top-left')

    map3.addControl(geocoder3, 'top-left')

    map4.addControl(geocoder4, 'top-left')

    geocoder1.on('result', ({ result }) => {
      setAddress({ coords: result.center, address: result.place_name })
    })

    geocoder2.on('result', ({ result }) => {
      setCity(result.place_name)
    })

    geocoder3.on('result', ({ result }) => {
      setAirport(result.place_name)
    })

    geocoder4.on('result', ({ result }) => {
      setBus(result.place_name)
    })
  }, [])

  useEffect(
    () =>
      handleChange({
        name: 'address',
        value: address,
        valid: Object.keys(address).length > 0,
        optional: false,
      }),
    [address]
  )

  useEffect(
    () =>
      handleChange({
        name: 'city',
        value: city,
        valid: Object.keys(city).length > 0,
        optional: false,
      }),
    [city]
  )

  useEffect(
    () =>
      handleChange({
        name: 'airport',
        value: airport,
        valid: Object.keys(airport).length > 0,
        optional: false,
      }),
    [airport]
  )

  useEffect(
    () =>
      handleChange({
        name: 'bus',
        value: bus,
        valid: Object.keys(bus).length > 0,
        optional: false,
      }),
    [bus]
  )

  useEffect(() => {
    data &&
      setForm((prevState) => {
        return {
          ...prevState,
          address: {
            name: 'address',
            value: data.address,
            valid: true,
            optional: false,
          },
          airport: {
            name: 'airport',
            value: data.airport,
            valid: true,
            optional: false,
          },
          bus: {
            name: 'bus',
            value: data.bus,
            valid: true,
            optional: false,
          },
          city: {
            name: 'city',
            value: data.city,
            valid: true,
            optional: false,
          },
        }
      })
  }, [data])

  return (
    <React.Fragment>
      <Label title="Estado / Departamento" />
      <Select
        name="orgState"
        options={states}
        onChange={handleChange}
        defaultValue={data && data.orgState}
      />
      <Label title="Municipio" />
      <Input
        name="orgMunicipio"
        placeholder="Ingresa el nombre"
        onChange={handleChange}
        defaultValue={data && data.orgMunicipio}
      />
      <Label title="¿Estás cerca de un área de conservación?" optional />
      <Disclaimer
        style={{ marginTop: 12, marginBottom: 24 }}
        text={
          <React.Fragment>
            Un Área de Importancia de Conservación (AIC) es un espacio natural o restaurado, el cual
            se encuentra bajo una jurisdicción Internacional, nacional, estatal, municipal y/o
            local, administrado de manera pública, privada, en cogestión o de forma comunitaria.
            <br />
            <br />
            También se consideran todos los territorios indígenas y autónomos, así como territorios
            que brindan conectividad para que los servicios y bienes ecosistémicos se mantengan en
            el tiempo.
          </React.Fragment>
        }
      />
      <SmallText black title="Afíliate a un Área de Conservación" />
      <Select
        name="consarea"
        options={consarea}
        onChange={handleChange}
        defaultValue={data && data.consarea}
        optional
      />
      <Label title="Elige el destino sostenible más cercano a ti" optional />
      <Select
        name="destino"
        options={['Ciudad de México']}
        onChange={handleChange}
        defaultValue={data && data.destino}
        optional
      />
      <Label title="¿Dónde esta ubicada tu organización?" />
      <div style={{ width: '100%', maxWidth: '85vw', margin: '0 auto 64px auto' }}>
        <Map ref={node1} />
      </div>
      <Label title="Describe cómo llegar (asumiendo que un viajero va por su cuenta)" />
      <Input
        name="arrive"
        placeholder="Máximo 1000 caracteres"
        textarea
        onChange={handleChange}
        defaultValue={data && data.arrive}
      />
      <Label title="¿Cuál es la población o ciudad más cercana?" />
      <div style={{ width: '100%', maxWidth: '85vw', margin: '0 auto 64px auto' }}>
        <Map ref={node2} />
      </div>
      <Label title="¿Cuál es el aeropuerto más cercano?" />
      <div style={{ width: '100%', maxWidth: '85vw', margin: '0 auto 64px auto' }}>
        <Map ref={node3} />
      </div>
      <Label title="¿Cuál es la terminal de bus más cercana?" />
      <div style={{ width: '100%', maxWidth: '85vw', margin: '0 auto 64px auto' }}>
        <Map ref={node4} />
      </div>
      <Button unable={!validForm} onClick={handleClick} />
    </React.Fragment>
  )
}

export default Step2
