import React, { useEffect, useState } from 'react'
import PhoneInput from 'react-phone-number-input'

import { Button, Disclaimer, Input, Label, SmallText, Switch } from '../../styles/forms'
import { PhoneStepper } from '../../styles/general'

type Props = {
  onStepChange: (data, step) => void
  data?: {
    orgMail: string
    orgNumber: string
    haveWA: string
    orgFB?: string
    orgIG?: string
    orgWeb?: string
  }
}

type Field = {
  value: string
  name: string
  valid: boolean
  optional: boolean
}

type Form = {
  orgMail?: Field
}

const Step3 = ({ onStepChange, data }: Props): JSX.Element => {
  const [form, setForm] = useState<Form>({})
  const [validForm, setValidForm] = useState(false)

  const handleChange = (e) => {
    setForm((prevState) => {
      return {
        ...prevState,
        [e.name]: e,
      }
    })
  }

  const handleClick = () => {
    const data = Object.keys(form).reduce((acc, key) => {
      if (form[key].valid) {
        return { ...acc, [key]: form[key].value }
      } else {
        return { ...acc }
      }
    }, {})
    onStepChange(data, 4)
  }

  useEffect(() => {
    const requiredFields = Object.values(form).filter((item) => !item.optional)
    const isValid = requiredFields.every((e) => e.valid)
    setValidForm(isValid)
  }, [form])

  useEffect(
    () =>
      handleChange({
        name: 'orgNumber',
        value: '',
        optional: false,
        valid: false,
      }),
    []
  )

  return (
    <React.Fragment>
      <Disclaimer
        text="El e-mail será el principal medio de contacto con Natoure, en el recibirás avisos cada vez que un natourista reserve una de tus experiencias."
        style={{ marginTop: 48, marginBottom: 48 }}
      />
      <Label title="E-mail de tu organización" />
      <Input
        name="orgMail"
        placeholder="mail@organizacion.com"
        onChange={handleChange}
        errorMessage="Ingresa un email válido"
        email
        defaultValue={data && data.orgMail}
      />
      <Label title="Número de Teléfono" />
      <PhoneStepper>
        <PhoneInput
          defaultCountry="MX"
          placeholder="55 3708 8976"
          name="orgNumber"
          onChange={(e: undefined | string) =>
            e !== undefined &&
            handleChange({ name: 'orgNumber', value: e, valid: e.length > 8, optional: false })
          }
          defaultValue={data && data.orgNumber}
        />
      </PhoneStepper>
      <SmallText title="¿Este número cuenta con Whatsapp?" />
      <Switch
        name="haveWA"
        first="No"
        last="Sí"
        onChange={handleChange}
        defaultValue={data && data.haveWA}
      />
      <Label optional title="Redes sociales" />
      <Input
        style={{ marginBottom: 24 }}
        name="orgFB"
        placeholder="facebook.com/"
        onChange={handleChange}
        optional
        defaultValue={data && data.orgFB}
      />
      <Input
        style={{ marginBottom: 24 }}
        name="orgIG"
        placeholder="instagram.com/"
        onChange={handleChange}
        optional
        defaultValue={data && data.orgIG}
      />
      <Input
        name="orgWeb"
        placeholder="www.tusitioweb.com/"
        onChange={handleChange}
        optional
        defaultValue={data && data.orgWeb}
      />
      <Button unable={!validForm} onClick={handleClick} />
    </React.Fragment>
  )
}

export default Step3
