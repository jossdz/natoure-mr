import React, { useEffect, useState } from 'react'

import { Button, Checkbox, SectionText } from '../../styles/forms'

type Props = {
  onStepChange: (data, step) => void
  onSubmit: () => void
}

type Field = {
  value: string
  name: string
  valid: boolean
  optional: boolean
}

type Form = {
  confirmation?: Field
}

const Step6 = ({ onStepChange, onSubmit }: Props): JSX.Element => {
  const [form, setForm] = useState<Form>({})
  const [validForm, setValidForm] = useState(false)

  const handleChange = (e) => {
    setForm((prevState) => {
      return {
        ...prevState,
        [e.name]: e,
      }
    })
    const data = e.valid ? { [e.name]: e.value } : {}
    onStepChange(data, 6)
  }

  useEffect(() => {
    const requiredFields = Object.values(form).filter((item) => !item.optional)
    const isValid = requiredFields.every((e) => e.valid)
    setValidForm(isValid)
  }, [form])

  return (
    <React.Fragment>
      <SectionText title="Si estás listo para publicar acepta los términos y condiciones y presiona el botón de abajo para que podamos aprobar tu registro." />
      <Checkbox
        name="confirmation"
        options={['Acepto los términos y condiciones como anfitrión en la plataforma de Natoure.']}
        onChange={handleChange}
        disclaimer={false}
      />
      <Button unable={!validForm} onClick={onSubmit} text="Publicar" />
    </React.Fragment>
  )
}

export default Step6
