import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { RiLeafFill, RiLeafLine } from 'react-icons/ri'

import { Button, Disclaimer, GreenTab, Label, Leafs } from '../../styles/forms'

import { leafsQuestions } from '../data'

const Col1 = styled.div`
  width: 45%;
  @media (min-width: 768px) {
    width: 180px;
  }
`
const Col2 = styled.p`
  width: 55%;
  font-weight: 600;
  margin: 0;
`

type Props = {
  onStepChange: (data, step) => void
  data?: {
    ambiental1: number
    ambiental2: number
    ambiental3: number
    ambiental4: number
    ambiental5: number
    ambiental6: number
    social1: number
    social2: number
    social3: number
    economico1: number
    economico2: number
  }
}

type Field = {
  value: string
  name: string
  valid: boolean
  optional: boolean
}

type Form = {
  ambiental1?: Field
}

const Step4 = ({ onStepChange, data }: Props): JSX.Element => {
  const [form, setForm] = useState<Form>({})
  const [validForm, setValidForm] = useState(false)

  const leafStyle = { width: 20, height: 20, color: '#06bc68', marginRight: 6 }

  const disclaimer = [
    {
      title: '1 hoja significa:',
      fill: 1,
      line: 4,
      paragraph: 'Aún no cumplimos pero trabajemos en ello',
    },
    {
      title: '5 hojas significan:',
      fill: 5,
      line: 0,
      paragraph: 'Cumplimos totalmente',
    },
  ]

  const handleChange = (e) => {
    setForm((prevState) => {
      return {
        ...prevState,
        [e.name]: e,
      }
    })
  }

  const handleClick = () => {
    const data = Object.keys(form).reduce((acc, key) => {
      if (form[key].valid) {
        return { ...acc, [key]: form[key].value }
      } else {
        return { ...acc }
      }
    }, {})
    onStepChange(data, 5)
  }

  useEffect(() => {
    const requiredFields = Object.values(form).filter((item) => !item.optional)
    const isValid = requiredFields.every((e) => e.valid)
    setValidForm(isValid)
  }, [form])

  return (
    <React.Fragment>
      <Disclaimer
        style={{ marginTop: 48, marginBottom: 48 }}
        text={
          <React.Fragment>
            Según tu situación actual elige la opción que más se acerque a tu organización.
            {disclaimer.map((item, index) => (
              <div key={index}>
                <p style={{ fontWeight: 600, margin: '32px 0 16px 0' }}>{item.title}</p>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <Col1>
                    {[...Array(item.fill)].map((item, index) => (
                      <RiLeafFill key={index} style={leafStyle} />
                    ))}
                    {[...Array(item.line)].map((item, index) => (
                      <RiLeafLine key={index} style={leafStyle} />
                    ))}
                  </Col1>
                  <Col2>{item.paragraph}</Col2>
                </div>
              </div>
            ))}
          </React.Fragment>
        }
      />
      {leafsQuestions.map(({ title, questions }, index) => (
        <div key={index}>
          <GreenTab title={title} />
          {questions.map((item, index) => {
            const name = `${title.toLowerCase().replace('ó', 'o')}${index + 1}`
            return (
              <div key={index}>
                <Label title={item} />
                <Leafs name={name} onChange={handleChange} defaultValue={data && data[name]} />
              </div>
            )
          })}
        </div>
      ))}
      <Button unable={!validForm} onClick={handleClick} />
    </React.Fragment>
  )
}

export default Step4
