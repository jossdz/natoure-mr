import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { RiLeafFill } from 'react-icons/ri'

import { Button, GreenTab, Label, SectionText, Upload } from '../../styles/forms'

import { consejos2, consejos3 } from '../data'

const Img = styled.img`
  width: 100%;
  max-width: 85vw;
  display: block;
  margin: 0 auto 64px auto;
  @media (min-width: 768px) {
    max-width: 425px;
  }
`

type Props = {
  onStepChange: (data, step) => void
  data?: {
    mainPhoto: string[]
    photos: string[]
  }
}

type Field = {
  value: string
  name: string
  valid: boolean
  optional: boolean
}

type Form = {
  mainPhoto?: Field
}

const Step5 = ({ onStepChange, data }: Props): JSX.Element => {
  const [form, setForm] = useState<Form>({})
  const [validForm, setValidForm] = useState(false)

  const handleChange = (e) => {
    setForm((prevState) => {
      return {
        ...prevState,
        [e.name]: e,
      }
    })
  }

  const handleClick = () => {
    const data = Object.keys(form).reduce((acc, key) => {
      if (form[key].valid) {
        return { ...acc, [key]: form[key].value }
      } else {
        return { ...acc }
      }
    }, {})
    onStepChange(data, 6)
  }

  useEffect(() => {
    const requiredFields = Object.values(form).filter((item) => !item.optional)
    const isValid = requiredFields.every((e) => e.valid)
    setValidForm(isValid)
  }, [form])

  return (
    <React.Fragment>
      <GreenTab title="Video o foto de presentación" />
      <Label title="Sube tu video o foto" />
      <Upload name="mainPhoto" onChange={handleChange} defaultValue={data && data.mainPhoto} />
      <Img src="/icons/preview2.png" alt="preview" />
      <SectionText title='Es la forma de presentarte con los natouristas y de mostrar el trabajo que hacen como organización. Los videos se muestran en tu perfil de "ANFITRIÓN"' />
      <div style={{ width: '100%', maxWidth: '85vw', margin: '64px auto' }}>
        <div style={{ color: '#06bc68', marginBottom: 32 }}>Consejos:</div>
        {consejos2.map((item, index) => (
          <div key={index} style={{ marginBottom: 32 }}>
            <RiLeafFill style={{ position: 'absolute', color: '#06bc68' }} />
            <div style={{ marginLeft: 24 }}>{item}</div>
          </div>
        ))}
      </div>
      <GreenTab title="Tu galería" />
      <Label title="Sube más fotos para mostrar en tu galería" />
      <Upload name="photos" onChange={handleChange} defaultValue={data && data.photos} />
      <Img src="/icons/preview3.png" alt="preview" />
      <SectionText title="Muestrale al mundo quienes son a través de imágenes o de un vídeo de máximo 2 minutos." />
      <div style={{ width: '100%', maxWidth: '85vw', margin: '64px auto' }}>
        <div style={{ color: '#06bc68', marginBottom: 32 }}>Consejos:</div>
        {consejos3.map((item, index) => (
          <div key={index} style={{ marginBottom: 32 }}>
            <RiLeafFill style={{ position: 'absolute', color: '#06bc68' }} />
            <div style={{ marginLeft: 24 }}>{item}</div>
          </div>
        ))}
      </div>
      <Button unable={!validForm} onClick={handleClick} />
    </React.Fragment>
  )
}

export default Step5
