import React, { createContext, useState } from 'react'

type AppContextWrapperProps = {
  children: React.ReactNode
}

type Context = {
  checkoutInfo?: any
  setCheckoutInfo?: any
}

export const AppContext = createContext<Context>({})

const AppContextWrapper = ({ children }: AppContextWrapperProps): JSX.Element => {
  const [checkoutInfo, setCheckoutInfo] = useState({})

  return (
    <AppContext.Provider value={{ checkoutInfo, setCheckoutInfo }}>{children}</AppContext.Provider>
  )
}

export default AppContextWrapper
