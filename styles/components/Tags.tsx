import React, { useState } from 'react'
import styled from 'styled-components'
import { HorizontalSlide } from '../general'

export interface TagElement {
  name: string
}

export interface ITagProps {
  tags: TagElement[]
  onClick: any
}

const TagStyles = styled.section`
  width: 100%;
  margin-bottom: 8px;
`

const Tag = styled.span`
  padding: 8px;
  background-color: #fafcff;
  border-radius: 8px;
  cursor: pointer;
`

export default function Tags(props: ITagProps): JSX.Element {
  return (
    <TagStyles>
      <HorizontalSlide>
        {props.tags.map((tag, i) => {
          return (
            <Tag onClick={() => props.onClick(tag)} key={i}>
              {tag.name}
            </Tag>
          )
        })}
      </HorizontalSlide>
    </TagStyles>
  )
}
