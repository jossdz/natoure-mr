import React from 'react'
import SearchStyles from '../general/SearchStyles'

interface ISearchBar {
  placeholder: string
  searchText?: string
  onChange: (e: string) => void
  onClick: () => void
  value: string
}

export default function SearchBar({
  placeholder,
  searchText = 'Buscar',
  onClick,
  onChange,
  value,
}: ISearchBar): JSX.Element {
  const handleChange = ({ value }) => {
    onChange(value)
  }

  return (
    <SearchStyles>
      <input
        type="text"
        value={value}
        placeholder={placeholder}
        onChange={({ target }) => handleChange(target)}
      />
      <button onClick={onClick}>{searchText}</button>
    </SearchStyles>
  )
}

SearchBar.defaultProps = {
  onChange: (e: string) => console.log(e),
}
