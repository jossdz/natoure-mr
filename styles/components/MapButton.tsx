import styled from 'styled-components'

const MapButton = styled.button(
  ({ toggleMap }: { toggleMap: boolean }) => `
  cursor: pointer;
  background-color: var(--green-natoure) !important;
  margin: 0 32px;
  border-radius: 999px;
  padding: 10px !important;
  display: flex;
  align-items: center;
  position: relative;
  svg {
    color: white;
    width: 16px;
    height: 16px;
  }
  &::before {
    width: 58px;
    content: ${toggleMap ? "'Ver lista'" : "'Ver mapa'"};
    position: absolute;
    font-size: 12px;
    top: 40px;
    left: -10px;
    text-align: center;
    cursor: normal;
  }
  @media (max-width: 768px) {
    display: none;
  }
`
)

export default MapButton
