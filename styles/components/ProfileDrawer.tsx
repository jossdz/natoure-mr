import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import styled from 'styled-components'
import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Drawer from '@material-ui/core/Drawer'
import Link from 'next/link'

import { FaWhatsapp } from 'react-icons/fa'

const GreenButton = styled.a`
  height: 40px;
  width: 188px;
  background-color: var(--green-natoure);
  border: none;
  border-radius: 26.5px;
  color: var(--white);
  font-family: 'Montserrat', sans-serif;
  font-size: 14px;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.29;
  letter-spacing: -0.73px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: auto;
  margin-right: auto;
  text-decoration: none;
  svg {
    width: 32px;
    height: 32px;
    margin-left: 12px;
  }
`

const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
  paper: {
    marginTop: '75px',
    background: 'white',
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    width: 220,
    minHeight: 380,
    height: '50vh',
    padding: '32px 12px',
    zIndex: 9999,
  },
})

const Img = styled.img`
  width: 36px;
  height: 36px;
  border-radius: 50%;
  border: 2px solid var(--dark-blue);
`

const Profile = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 100%;
  ul {
    margin-top: 24px;
    overflow-y: scroll;
    > div {
      padding-top: 0;
      padding-bottom: 0;
    }
    div > div {
      padding: 18px 8px;
      border-top: solid 0.5px rgba(44, 55, 90, 0.36);
      margin: 0;
      span {
        font-family: 'Montserrat', sans-serif;
        font-size: 14px;
        font-weight: 500;
        line-height: 1.2;
        letter-spacing: -0.78px;
        text-align: right;
        color: var(--dark-blue);
      }
    }
  }
`

const ProfileHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding: 0 8px;
  img {
    width: 60px;
    height: 60px;
    border-radius: 50%;
    border: 2px solid var(--dark-blue);
  }
  div {
    display: flex;
    flex-direction: column;
    text-align: right;
    margin-right: 16px;
    font-size: 18px;
    letter-spacing: -0.94px;
    color: var(--dark-blue);
    a {
      font-size: 12px;
      letter-spacing: -0.68px;
      color: var(--green-natoure);
      margin-top: 8px;
    }
  }
`
interface DrawerElement {
  photo: string
  name: string
  anfitrion?: boolean
}

const ProfileDrawer = (props: DrawerElement): JSX.Element => {
  const { push } = useRouter()
  const classes = useStyles()
  const [state, setState] = React.useState({
    right: false,
  })

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [userInfo, setUserInfo] = useState<any>()

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem('NatoureUser'))
    setUserInfo(user)
  }, [])

  const listData = props.anfitrion
    ? [
        {
          text: 'Mis datos',
          link: '/anfprofile',
        },
      ]
    : [
        {
          text: 'Convertirme en anfitrión',
          link: '/form-organizacion',
        },
      ]

  const commons = [
    {
      text: 'Términos y condiciones',
      link: '/terminos-y-condiciones',
    },
    {
      text: 'Aviso de privacidad',
      link: '/aviso-de-privacidad',
    },
    {
      text: 'Cerrar sesión',
      link: '/',
    },
  ]

  const data = [...listData, ...commons]

  const toggleDrawer = (anchor, open) => (event) => {
    if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return
    }

    setState({ ...state, [anchor]: open })
  }

  const list = (anchor): JSX.Element => (
    <Profile role="presentation" onKeyDown={toggleDrawer(anchor, false)}>
      <div>
        <ProfileHeader>
          <div>
            <b>
              {userInfo?.name} {userInfo?.last_name}
            </b>
            <Link href={props.anfitrion ? '/anfprofile' : '/profile'}>Ver perfil</Link>
          </div>
          <Img src={props.photo || '/assets/icon-blue.png'} alt="avatar" />
        </ProfileHeader>
        <List>
          {data.map((item, index) => (
            <ListItem
              button
              key={index}
              onClick={() => {
                item.text === 'Cerrar sesión' && localStorage.clear()
                push(item.link)
              }}
            >
              <ListItemText primary={item.text} />
            </ListItem>
          ))}
        </List>
      </div>
      <GreenButton href="https://wa.me/5215631674651" target="_blank" rel="noopener noreferrer">
        Contáctanos <FaWhatsapp />
      </GreenButton>
    </Profile>
  )

  return (
    <div>
      {['right'].map((anchor) => (
        <React.Fragment key={anchor}>
          <Button
            style={{ justifyContent: 'flex-end', padding: 0 }}
            onClick={toggleDrawer(anchor, true)}
          >
            <Img src={props.photo || '/assets/icon-blue.png'} alt="profile-pic" />
          </Button>
          <Drawer
            classes={{ paper: classes.paper }}
            anchor="right"
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
          >
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  )
}

export default ProfileDrawer
