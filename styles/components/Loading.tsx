import React from 'react'
import styled, { keyframes } from 'styled-components'
import { AiOutlineLoading3Quarters } from 'react-icons/ai'

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`

const Wrapper = styled.section`
  width: 100vw;
  height: 100vh;
  position: absolute;
  top: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: rgba(0, 0, 0, 0.2);
  z-index: 4;
  svg {
    animation: ${rotate} 1.5s linear infinite;
    color: var(--green-natoure);
    width: 100px;
    height: 100px;
  }
`

const Loading = (): JSX.Element => {
  return (
    <Wrapper>
      <AiOutlineLoading3Quarters />
    </Wrapper>
  )
}

export default Loading
