import React from 'react'
import SectionHeadingS from '../general/SectionHeadingS'

interface Props {
  title: string
  linkTitle?: string
  link?: string
  removeMarginTop?: boolean
  removeMarginBottom?: boolean
}

const SectionHeading = ({
  title,
  linkTitle,
  link = '#',
  removeMarginTop,
  removeMarginBottom,
}: Props): JSX.Element => {
  return (
    <SectionHeadingS removeMarginTop={removeMarginTop} removeMarginBottom={removeMarginBottom}>
      <p>{title}</p>
      {link && linkTitle && <a href={link}>{linkTitle}</a>}
    </SectionHeadingS>
  )
}

export default SectionHeading
