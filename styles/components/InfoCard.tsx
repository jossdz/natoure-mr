import styled from 'styled-components'

type InfoCardProps = {
  backgroundURL?: string
}

const InfoCard = styled.section<InfoCardProps>`
  width: 100%;
  height: 210px;
  font-size: 24px;
  text-align: center;
  margin-bottom: 64px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  background-image: url(${(props) =>
    props.backgroundURL ||
    'https://imagenescityexpress.scdn6.secure.raxcdn.com/sites/default/files/styles/hotel_landing_page/public/2017-09/paisajes-mexico.jpg?itok=g44qH3IG'});
  background-position: left;
  background-size: cover;
  color: white;
  border-radius: 8px;
  overlay: 0.5;
  align-items: center;
  position: relative;
  &::before {
    content: '';
    width: 100%;
    height: 210px;
    position: absolute;
    background-color: #232323;
    opacity: 0.5;
    top: 0;
    right: 0;
    border-radius: 8px;
  }
  & > * {
    z-index: 2;
  }
  @media (max-width: 768px) {
    display: none;
  }
`

export default InfoCard
