import React from 'react'
import FilterStyles from '../general/FilterStyles'
import { AiOutlineSearch } from 'react-icons/ai'
import { FaSlidersH } from 'react-icons/fa'

interface IFilterBar {
  placeholder: string
  onChange: (e: string) => void
  value?: string
}

export default function FilterBar({ placeholder, onChange, value }: IFilterBar): JSX.Element {
  const handleChange = ({ value }) => {
    onChange?.(value)
  }

  return (
    <FilterStyles>
      <button>
        <AiOutlineSearch />
      </button>
      <input
        type="text"
        value={value}
        placeholder={placeholder}
        onChange={({ target }) => handleChange(target)}
      />
      <button>
        <FaSlidersH style={{ width: 14 }} />
      </button>
    </FilterStyles>
  )
}

FilterBar.defaultProps = {
  onChange: (e: string) => console.log(e),
}
