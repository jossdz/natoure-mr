import styled from 'styled-components'

const Container = styled.section`
  margin: 0 auto;
  width: 85vw;
  @media (min-width: 768px) {
    padding: 0 2rem;
  }
  @media (min-width: 768px) and (max-width: 1279px) {
    max-width: 768px;
  }
  @media (min-width: 1280px) {
    max-width: 992px;
  }
`

export default Container
