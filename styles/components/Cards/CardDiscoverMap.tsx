import React from 'react'
import { useRouter } from 'next/router'
import styled from 'styled-components'
import { RiRoadMapLine } from 'react-icons/ri'

import { MegaLabel } from '../../../styles/landing'
import { Button } from '../../general'

const CardDiscoverStyles = styled.article`
  margin-top: 68px;
  background-image: url('/assets/mapa-bg.jpg');
  background-size: cover;
  background-position: center;
  text-align: center;
  padding: 32px;
  width 100%;
  height: 412px;
  box-sizing: border-box;
  color: white;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: flex-end;
  position: relative;
  @media (min-width: 768px) {
    background: linear-gradient(to bottom, rgba(255, 255, 255, 0.1), rgba(255, 255, 255, 0.1)),
      url('/assets/mapa.png');
    border-radius: 46px;
    background-position: bottom;
    align-items: flex-end;
    background-size: contain;
    height: 396px;
    padding: 64px 32px;
  }
  @media (min-width: 1024px) {
    height: 538px;
    margin-bottom: 108px;
  }
  @media (min-width: 1366px) {
    height: 720px;
    padding: 132px 120px;
  }
  .text {
    display: flex;
    align-items: center;
    flex-direction: column;
    @media (min-width: 768px) {
      display: none;
    }
    svg {
      width: 51px;
      height: 51px;
      margin-bottom: 12px;
      color: white;
    }
    b {
      font-size: 15px;
      text-decoration: underline;
      margin: 0;
    }
    p {
      margin: 16px auto 32px auto;
      font-weight: 200;
      font-size: 14px;
    }
  }
`

const Square = styled.div`
  position: absolute;
  box-sizing: border-box;
  background-color: white;
  color: var(--dark-blue);
  top: 0;
  left: 0;
  width: 616px;
  padding: 40px 32px 40px 80px;
  text-align: left;
  border-top-right-radius: 46px;
  border-bottom-right-radius: 46px;
  @media (max-width: 767px) {
    display: none;
  }
  @media (max-width: 1023px) {
    padding: 40px 32px;
  }
  p {
    padding-left: 16px;
    font-size: 14px;
  }
`

const CardDiscoverMap = (): JSX.Element => {
  const { push } = useRouter()

  return (
    <CardDiscoverStyles>
      <Square>
        <MegaLabel>Conoce nuestro mapa de áreas protegidas y turismo</MegaLabel>
        <p>
          Vive experiencias de viaje únicas que impulsan economías locales, generan conservación de
          la naturaleza y frenan el Cambio Climático.
        </p>
      </Square>
      <div className="text">
        <RiRoadMapLine />
        <b>Conoce el primer Mapa de Conservación y Viajes del mundo</b>
        <p>Vive experiencias de viaje únicas que impulsan economías locales.</p>
      </div>
      <Button btnType="primary" onClick={() => push('/mapa')}>
        Ver Mapa
      </Button>
    </CardDiscoverStyles>
  )
}

export default CardDiscoverMap
