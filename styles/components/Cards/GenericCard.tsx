import styled from 'styled-components'

type CardThumbnail = { image: string }

export const CardWrapper = styled.div`
  box-sizing: border-box;
  margin-bottom: 24px;
  width: 200px;
  height: 250px;
  border-radius: 8px;
  @media screen and (max-width: 430px) {
    width: 47%;
    max-width: 47%;
  }
`
export const CardImage = styled.div<CardThumbnail>`
  background-image: url('${(props) => props.image}');
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  box-sizing: border-box;
  padding: 8px 16px;
  height: 200px;
  width: 200px;
  border-radius: 8px;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  color: var(--dark-blue);
`

export const CardText = styled.div`
  font-family: Montserrat;
  font-size: 22px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.25;
  letter-spacing: -0.03px;
  color: var(--dark-blue);
  overflow: auto;
  text-overflow: ellipsis;
  max-height: 80px;
`

export const CardSubText = styled.div`
  font-family: Montserrat;
  font-size: 12px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 2.21;
  letter-spacing: -0.03px;
  color: var(--dark-blue);
`

export const CardSubText2 = styled.div`
  font-family: Montserrat;
  font-size: 10px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 2.21;
  letter-spacing: -0.03px;
  color: var(--dark-blue);
`
