/* eslint-disable jsx-a11y/accessible-emoji */
import React, { useState } from 'react'
import styled from 'styled-components'
import { FaHeart, FaRegHeart } from 'react-icons/fa'

interface Props {
  sustainLevel: number
  likes: number
  location: string
  name: string
  image: string
  description: string
}

const CardStyles = styled.div`
  width: 315px;
  height: 300px;
  border-radius: 8px;
  overflow: hidden;
  div:nth-child(1) {
    width: 100%;
    height: 201px;
    border-radius: 8px;
    overflow: hidden;
    position: relative;
    img {
      width: 100%;
      height: 90%;
      object-fit: cover;
      margin-bottom: 10px;
    }
    b {
      position: absolute;
      top: 10px;
      margin: 0;
      color: white;
      font-size: 13px;
      button {
        padding: 0;
        border: none;
        background-color: transparent;
      }
      p {
        margin: 0;
        text-align: center;
      }
    }
    #sust {
      left: 10px;
    }
    b:last-child {
      right: 10px;
    }
  }
  div:nth-child(2) {
    font-size: 13px;
    height: 73px;
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
    padding: 0 10px;
    p {
      margin: 0;
    }
    span {
      color: var(--light-green);
      font-weight: 900;
      margin: 0 8px;
    }
    #name {
      font-size: 14px;
    }
  }
`

const CardExplore = ({ likes, location, name, image, description }: Props): JSX.Element => {
  const [active, setActive] = useState(false)
  return (
    <CardStyles>
      <div>
        <img src={image} alt={name} />
        <b>
          {active ? (
            <button onClick={() => setActive(!active)}>
              <FaHeart style={{ width: 20, height: 20, color: '#e02020' }} />
            </button>
          ) : (
            <button onClick={() => setActive(!active)}>
              <FaRegHeart style={{ width: 20, height: 20, color: '#ffffff' }} />
            </button>
          )}
          <p>{likes}</p>
        </b>
      </div>
      <div>
        <p>
          📍&nbsp; <b>{location}</b> &nbsp;
        </p>
        <p>
          <b id={name}>{name}</b>
        </p>
        <p>{description}</p>
      </div>
    </CardStyles>
  )
}

export default CardExplore
