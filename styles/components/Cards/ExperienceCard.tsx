import styled from 'styled-components'

type CardThumbnail = { image: string }
type CardChip = { background: string }
type CardIcons = { onlyLikes?: boolean }

export const CardWrapper = styled.div`
  display: flex;
  box-sizing: border-box;
  flex-direction: column;
  margin: 0 0 48px 19px;
  max-width: 23%;
  width: 23%;
  position: relative;
  color: var(--dark-blue);
  @media screen and (max-width: 425px) {
    width: 47%;
    max-width: 47%;
    margin: 0 0 48px 9px;
  }
  @media (min-width: 426px) and (max-width: 1023px) {
    width: 30%;
    max-width: 30%;
    margin: 0 0 48px 18px;
  }
  @media (min-width: 1024px) and (max-width: 1279px) {
    margin: 0 0 48px 15px;
  }
`

export const CardThumbnail = styled.div<CardThumbnail>(
  ({ image }) => `
    background-image: url("${image}");
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    box-sizing: border-box;
    height: 200px;
    min-height: 200px;
    margin-bottom: 8px;
    border-radius: 8px;
    &::before {
      content: '';
      width: 100%;
      height: 200px;
      position: absolute;
      background-color: #232323;
      opacity: 0.1;
      top: 0;
      right: 0;
      border-radius: 8px;
    }
`
)

export const CardText = styled.div`
  font-family: Montserrat;
  font-size: 14px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.25;
  letter-spacing: -0.03px;
  color: var(--dark-blue);
  max-height: 80px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`

export const CardSubText = styled.div`
  font-family: Montserrat;
  font-size: 12px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: -0.03px;
  color: var(--dark-blue);
`

export const ChipsWrapper = styled.div`
  position: absolute;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  bottom: 80px;
  right: 16px;
`

export const CardChip = styled.div<CardChip>(
  ({ background }) => `
  padding: 4px 12px;
  border-radius: 12.5px;
  background-color: ${background};
  font-family: Montserrat;
  font-size: 10px;
  font-weight: 600;
  letter-spacing: -0.03px;
  color: #fff;
  width: max-content;
  margin-top: 6px;
`
)

export const CardIcons = styled.div<CardIcons>(
  ({ onlyLikes }) => `
  position: absolute;
  padding: 16px;
  width: calc(100% - 32px);
  display: flex;
  justify-content: ${onlyLikes ? 'flex-end' : 'space-between'};
  svg.leaf {
    color: var(--green-natoure);
    margin-right: 4px;
  }
`
)

export const ButtonSave = styled.button`
  border: none;
  padding: 0;
  height: 20px;
  background: transparent;
  cursor: pointer;
  svg {
    color: #fff;
    width: 20px;
    height: 20px;
  }
`

export const ViewButton = styled.div`
  position: absolute;
  height: 200px;
  background-color: transparent;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  border: none;
  svg {
    width: 48px;
    height: 48px;
    color: rgba(255, 255, 255, 0.67);
    cursor: pointer;
  }
`

export const EditButton = styled.button`
  position: absolute;
  height: 50px;
  background-color: rgba(6, 188, 104, 0.84);
  width: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  border: none;
  top: 150px;
  right: 0;
  border-bottom-right-radius: 8px;
  cursor: pointer;
  svg {
    width: 20px;
    height: 20px;
    color: #fff;
  }
`

export const PauseButton = styled.button`
  padding: 8px;
  background: var(--green-natoure);
  margin-top: 8px;
  border-radius: 2px;
  border: none;
  align-items: center;
  justify-content: center;
  display: flex;
  cursor: pointer;
  svg {
    width: 16px;
    height: 16px;
    color: #fff;
    margin-right: 4px;
  }
`
