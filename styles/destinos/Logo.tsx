import styled from 'styled-components'

type ImageCoverProps = { url?: string }

export const Logo = styled.div<ImageCoverProps>`
  width: 60px;
  height: 60px;
  border-radius: 50%;
  background: var(--green-natoure);
  background: url(${({ url }) => url});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
`
