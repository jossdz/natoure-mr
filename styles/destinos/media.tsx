import styled from 'styled-components'

export const VideoContainer = styled.video`
  background: var(--dark-blue);
  width: 100%;
  height: 430px;
  border-radius: 10px;
  margin-bottom: 1rem;
`
type ImageCoverProps = { url?: string }

export const ImageCover = styled.div<ImageCoverProps>`
  background: var(--dark-blue);
  background: url(${({ url }) => url});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  width: 100%;
  height: 430px;
  border-radius: 10px;
  margin-bottom: 1rem;
`

export const GalleryImg = styled.img`
  width: 120px;
  height: 120px;
  border-radius: 5px;
`
