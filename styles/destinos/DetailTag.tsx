import styled from 'styled-components'

export const DetailTag = styled.div`
  background: ${({ color }) => (color ? color : 'var(--green-natoure)')};
  color: white;
  padding: 8px 32px;
  border-top-right-radius: 22px;
  border-bottom-right-radius: 22px;
`

export const Tag = styled.span`
  background: ${({ color }) => (color ? color : 'var(--green-natoure)')};
  color: white;
  padding: 8px 16px;
  margin: 8px;
  border-radius: 22px;
  width: fit-content;
`
