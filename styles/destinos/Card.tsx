import styled from 'styled-components'

type CardThumbnail = { image: string }

export const CardWrapper = styled.div<CardThumbnail>`
  background-image: url('${(props) => props.image}');
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  box-sizing: border-box;
  margin-bottom: 24px;
  width: 24%;
  height: 200px;
  max-height: 200px;
  border-radius: 8px;
  @media screen and (max-width: 430px) {
    width: 47%;
    max-width: 47%;
  }
`
export const CardCover = styled.div`
  box-sizing: border-box;
  padding: 8px 16px;
  height: 100%;
  width: 100%;
  border-radius: 8px;
  background: rgba(0, 0, 0, 0.2);
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  color: white;
`

export const CardText = styled.div`
  font-family: Montserrat;
  font-size: 22px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.25;
  letter-spacing: -0.03px;
  color: white;
  overflow: auto;
  text-overflow: ellipsis;
  max-height: 80px;
`

export const CardSubText = styled.div`
  font-family: Montserrat;
  font-size: 12px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 2.21;
  letter-spacing: -0.03px;
  color: white;
`

export const CardSubText2 = styled.div`
  font-family: Montserrat;
  font-size: 10px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 2.21;
  letter-spacing: -0.03px;
  color: white;
`

type CardBoxProps = { color?: string }

export const CardBox = styled.div<CardBoxProps>`
  border-radius: 5px;
  background: ${({ color }) => color || 'var(--blue)'};
  padding: 10px;
  box-sizing: border-box;
  margin: 10px;
  width: 100px;
  height: 100px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  color: white;
  & svg {
    font-size: 2.5rem;
  }
`
