import styled from 'styled-components'

type MainHostProps = { bg?: string }

export const HostSection = styled.div`
  margin-top: 36px;
`

export const Menu = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  button {
    font-family: Montserrat;
    font-size: 15px;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.2;
    letter-spacing: -0.78px;
    color: #2c375a;
    padding-bottom: 2px;
    background: none;
    border: none;
    &.active {
      font-weight: bold;
      border-bottom: 3px solid var(--green-natoure);
    }
    :not(:only-of-type):not(:last-of-type) {
      margin-right: 20px;
    }
  }
`

export const HorizontalScroller = styled.div`
  display: flex;
  overflow-x: scroll;

  ::-webkit-scrollbar {
    display: none;
  }
`

export const MainHostCard = styled.div<MainHostProps>`
  box-sizing: border-box;
  display: inline-flex;
  min-width: 308px;
  height: 257px;
  min-height: 257px;
  border-radius: 8px;
  overflow: hidden;
  background-position: center;
  background-size: cover;
  cursor: pointer;

  ${({ bg }) => (bg ? `background-image: url("${bg}");` : '')}
  :not(:only-of-type):not(:last-of-type) {
    margin-right: 24px;
  }
`

export const MainHostInfo = styled.div`
  width: 100%;
  box-sizing: border-box;
  height: 100%;
  padding: 20px;
  display: flex;
  justify-content: flex-end;
  flex-direction: column;
  background-image: linear-gradient(
    to bottom,
    rgba(0, 0, 0, 0),
    rgba(0, 0, 0, 0.65) 50%,
    rgba(0, 0, 0, 0.58),
    rgba(0, 0, 0, 0.39)
  );
`

export const MainHostInfoTitle = styled.div`
  font-family: Montserrat;
  font-size: 18px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1;
  letter-spacing: -0.94px;
  color: #ffffff;
  position: relative;
  margin-bottom: 8px;
  :before {
    position: absolute;
    content: '';
    width: 6px;
    height: 6px;
    border-radius: 50%;
    top: calc(50% - 3px);
    left: -10px;
    background-color: var(--green-natoure);
  }
`
export const MainHostInfoDescription = styled.div`
  overflow: auto;
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
  font-family: Montserrat;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.29;
  letter-spacing: -0.03px;
  color: #ffffff;
  height: 54px;
  max-height: 54px;
`

export const HostStoryImg = styled.div<MainHostProps>`
  width: 50%;
  height: 390px;
  ${({ bg }) =>
    bg &&
    `
  background-image: url(${bg});
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  `}
  border-radius: 10px;
`

export const HostStoryInfo = styled.div`
  width: 50%;
  height: 390px;
  padding-left: 32px;
  display: flex;
  flex-direction: column;
  .h2 {
    position: relative;
    font-family: Montserrat;
    font-size: 28px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: -1.46px;
    color: #2c375a;
    &::before {
      content: '';
      top: 3px;
      left: -16px;
      width: 6px;
      height: 60px;
      position: absolute;
      border-radius: 3px;
      background-color: #06bc68;
    }
  }
  .divider {
    margin-top: 60px;
    width: 100%;
    height: 3px;
    border-radius: 3px;
    background-color: #2c375a;
  }
`

export const HostStoryMiniImage = styled.div<MainHostProps>`
  margin-top: 24px;
  border-radius: 10px;
  width: 19%;
  max-width: 19%;
  height: 180px;
  ${({ bg }) =>
    bg &&
    `
  background-image: url(${bg});
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  `}
`
