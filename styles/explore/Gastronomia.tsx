/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react'

import { Label } from '../../styles/forms'
import { FlexWrapper } from '../checkout'
import { EmptyMessage } from '../../styles/general'
import RestaurantCard from '../../components/Explore/components/RestaurantCard'
import PlateCard from '../../components/Explore/components/PlateCard'

type Props = {
  restaurants: any[]
  plates: any[]
}

const Gastronomia = ({ restaurants, plates }: Props): JSX.Element => {
  return restaurants?.length > 0 ? (
    <React.Fragment>
      <Label title="Restaurantes más sostenibles" />
      <FlexWrapper wrap="wrap" justify="start">
        {restaurants.map((item, index) => (
          <RestaurantCard key={index} {...item} />
        ))}
      </FlexWrapper>
      <Label title="Platillos típicos" />
      <FlexWrapper wrap="wrap" justify="start">
        {plates.map((item, index) => (
          <PlateCard key={index} {...item} />
        ))}
      </FlexWrapper>
    </React.Fragment>
  ) : (
    <EmptyMessage>No hay información para mostrar</EmptyMessage>
  )
}

export default Gastronomia
