import styled from 'styled-components'

type props = {
  mainpage?: boolean
}

export default styled.div(
  ({ mainpage }: props) => `
  display: flex;
  width: 85vw;
  margin: 0 auto;
  align-items: center;
  justify-content: space-between;
  padding-bottom: 4px;
  border-bottom: 1px solid rgba(44, 55, 90, 0.36);
  box-sizing: border-box;
  div {
    display: flex;
    align-items: center;
    & > img {
      width: 21px;
      height: 25px;
      margin-right: 9px;
    }
    p {
      font-weight: bold;
      font-size: ${mainpage ? '15px' : '18px'};
      color: ${mainpage ? 'white' : 'var(--dark-blue)'};
      margin: 0;
    }
  }
  & > img {
    width: 42px;
    height: 42px;
    border-radius: 50%;
    border: 2px solid var(--dark-blue);
  }
  &>* {
    z-index: 2;
  }
  @media (min-width: 769px) {
    display: none;
  }
`
)
