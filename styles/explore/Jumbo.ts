import styled from 'styled-components'

export default styled.div`
  min-height: 10px;
  max-height: auto;
  height: auto;
  max-width: 100vw;
  margin: 16px auto 36px auto;
  box-sizing: border-box;
  background-position: center;
  background-size: cover;
  background-color: white;
  color: var(--dark-blue);
  display: flex;
  flex-direction: column;
  button {
    padding: 0;
    font-family: 'Montserrat', sans-serif;
    font-size: 15px;
    border: none;
    height: fit-content;
    background-color: transparent;
  }
  .here {
    border-bottom: 3px solid var(--green-natoure);
    padding-bottom: 2px;
    color: var(--dark-blue);
    font-weight: bold;
  }
  &::before {
    content: '';
    width: 100%;
    height: auto;
    position: absolute;
    color: var(--dark-blue);
    background-color: white;
    opacity: 0.32;
    top: 0;
    right: 0;
  }
  & > * {
    z-index: 2;
  }
  hr {
    width: 357px;
    height: 2px;
    margin: 12px 0 0;
    border: solid 0.5px rgba(44, 55, 90, 0.36);
  }
  main {
    text-align: center;
    h3 {
      font-size: 24px;
      margin: 0;
      font-weight: bold;
      text-decoration: underline;
    }
    p {
      margin: 0;
      font-size: 14px;
      font-weight: 100;
    }
  }
  @media (min-width: 426px) {
    padding: 0;
    width: auto;
    margin-bottom: 3rem;
  }
  @media (min-width: 769px) {
    display: flex;
    flex-direction: row-reverse;
    align-items: center;
  }
`
