/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react'

import { Label } from '../../styles/forms'
import { EmptyMessage } from '../../styles/general'
import { FlexWrapper } from '../checkout'
import ExperienceCard from '../../components/Explore/components/ExperienceCard'

type Props = {
  experiences: any[]
}

const Experiencias = ({ experiences }: Props): JSX.Element => {
  return experiences?.length > 0 ? (
    <React.Fragment>
      <Label title="Acércate a lo desconocido" />
      <FlexWrapper wrap="wrap" justify="start">
        {experiences.map((item, index) => (
          <ExperienceCard key={index} {...item} />
        ))}
      </FlexWrapper>
    </React.Fragment>
  ) : (
    <EmptyMessage>No hay información para mostrar</EmptyMessage>
  )
}

export default Experiencias
