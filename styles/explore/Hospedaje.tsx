/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react'

import { Label } from '../../styles/forms'
import { EmptyMessage } from '../../styles/general'
import { FlexWrapper } from '../checkout'
import HotelCard from '../../components/Explore/components/HotelCard'

type Props = {
  lodgings: any[]
}

const Hospedaje = ({ lodgings }: Props): JSX.Element => {
  return lodgings?.length > 0 ? (
    <React.Fragment>
      <Label title="Hospedajes sostenibles" />
      <FlexWrapper wrap="wrap" justify="start">
        {lodgings.map((item, index) => (
          <HotelCard key={index} {...item} />
        ))}
      </FlexWrapper>
    </React.Fragment>
  ) : (
    <EmptyMessage>No hay información para mostrar</EmptyMessage>
  )
}

export default Hospedaje
