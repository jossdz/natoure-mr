/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react'

import { Label } from '../../styles/forms'
import { FlexWrapper } from '../checkout'
import { EmptyMessage } from '../../styles/general'
import ProductCard from '../../components/Explore/components/ProductCard'

type Props = {
  products: any[]
}

const Productos = ({ products }: Props): JSX.Element => {
  return products?.length > 0 ? (
    <React.Fragment>
      <Label title="Conoce nuestros productos" />
      <FlexWrapper wrap="wrap" justify="start">
        {products.map((item, index) => (
          <ProductCard key={index} {...item} />
        ))}
      </FlexWrapper>
    </React.Fragment>
  ) : (
    <EmptyMessage>No hay información para mostrar</EmptyMessage>
  )
}

export default Productos
