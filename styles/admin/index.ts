import styled from 'styled-components'

type MehaLinkLabelProps = {
  isActive?: boolean
}

export const AdminLoginWrapper = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: var(--dark-blue);
`

export const AdminLogin = styled.div`
  border-radius: 4px;
  width: 30%;
  padding: 24px;
  background-color: white;
  display: flex;
  align-items: center;
  flex-direction: column;
  img {
    width: 40px;
    margin-bottom: 16px;
  }
`

export const AdminSectionTitle = styled.h2`
  font-family: Montserrat;
  text-transform: uppercase;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 0.75;
  letter-spacing: -1.25px;
  color: #06bc68;
  display: inline-block;
  font-size: 28px;
  margin-right: 35px;
`

export const GreenButton = styled.button`
  color: white;
  display: flex;
  align-items: center;
  height: 34px;
  padding: 8px 38px;
  border-radius: 16px;
  background-color: #06bc68;
  border: 1px solid #06bc68;
  outline: none;
  font-family: Montserrat;
  font-size: 14px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.29;
  letter-spacing: -0.73px;
  color: #fff;
  cursor: pointer;
  transition: all 0.3s;
  :hover {
    background-color: #fff;
    color: #06bc68;
  }
  svg {
    margin-right: 8px;
  }
`

export const Divider = styled.div`
  width: 2px;
  height: 24px;
  margin: 0 24px;
  background-color: #bcc3cc;
`

export const MegaLabelLink = styled.div<MehaLinkLabelProps>`
  font-family: Montserrat;
  font-size: 24px;
  font-weight: ${({ isActive }) => (isActive ? 'bold' : '500')};
  font-stretch: normal;
  padding-left: 12px;
  font-style: normal;
  line-height: 0.75;
  letter-spacing: -1.25px;
  color: #2c375a;
  position: relative;
  display: flex;
  align-items: center;
  cursor: pointer;
  ${({ isActive }) =>
    isActive &&
    `::before {
    content: '';
    width: 4px;
    height: 20px;
    left: 0;
    position: absolute;
    border-radius: 3.5px;
    background-color: #06bc68;
  }`}
`
