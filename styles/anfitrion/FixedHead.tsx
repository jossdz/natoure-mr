import styled from 'styled-components'
import React from 'react'
import {
  AiOutlineArrowLeft,
  AiOutlinePlayCircle,
  AiOutlineHeart,
  AiOutlineShareAlt,
  AiOutlineTwitter,
  AiOutlineFacebook,
  AiOutlineInstagram,
} from 'react-icons/ai'
import { FcLike } from 'react-icons/fc'

export interface ExperieceElement {
  title: string
  sustainable: number //TODO: check if the name it's ok
  likes: number
  rating: number
  duration: string
  image: string
}

const Card = styled.section`
  * {
    margin: 0;
    padding: 0;
  }

  width: 100%;
  height: 72px;
  border-radius: 8px;
  bottom: 83px;
  background-size: 100vw 50vh;
  background-repeat: no-repeat;
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  justify-content: space-between;
  b {
    font-size: 12px;
    color: white;
  }
  p {
    color: white;
  }
  .blue {
    background-color: #2c375a;
    border-radius: 8px;
    width: 65%;
    height: 100%;
    box-sizing: border-box;
    display: flex;
    flex-direction: column;
    padding: 5px;
    text-align: center;
  }
  .redes {
    background-color: white;
    width: 35%;
    height: 100%;
    display: flex;
    justify-content: space-around;
    align-items: center;
    font-size: 25px;
  }
  .rating {
    display: flex;
    justify-content: space-around;
  }
  .arrowIco {
    font-size: 30px;
  }
  .hearth {
    font-size: 20px;
  }
  .play {
    font-size: 60px;
  }
  .link {
    font-size: 12px;
    color: var(--green-natoure);
    text-decoration: underline solid 1px var(--green-natoure);
  }
`

export const FixedHead = (props: ExperieceElement): JSX.Element => {
  return (
    <Card>
      <div className="redes">
        <AiOutlineInstagram />
        <AiOutlineTwitter />
        <AiOutlineFacebook />
      </div>
      <div className="blue">
        <p className="rating">
          <span></span>
          <span></span>
          <span role="img" aria-label="heart">
            Sustainable: 🥬 {props.sustainable}
          </span>
          <span></span>
          <span></span>
        </p>
        <p>
          <span>Rating:</span> ⭑ {props.rating}
        </p>
      </div>
    </Card>
  )
}

export default FixedHead
