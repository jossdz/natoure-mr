import React from 'react'
import { useRouter } from 'next/router'
import {
  RiStarSFill,
  RiMapPinLine,
  RiHeartFill,
  RiPencilFill,
  RiPauseLine,
  RiLeafFill,
} from 'react-icons/ri'

import { FlexWrapper, IconWrapper } from '../checkout'
import { Dot } from '../checkout/Card'
import {
  CardWrapper,
  CardThumbnail,
  CardText,
  CardSubText,
  CardIcons,
  EditButton,
  PauseButton,
} from '../components/Cards/ExperienceCard'

type Props = {
  platilloCoverPhoto: string
  platilloName: string
  _id: string
  orgId: {
    orgState: string
    sustentabilityPromedy: number
  }
}

const PlateCard = ({ platilloCoverPhoto, platilloName, _id, orgId }: Props): JSX.Element => {
  const { push } = useRouter()

  return (
    <CardWrapper>
      <CardThumbnail image={platilloCoverPhoto[0]} />

      <CardIcons>
        <FlexWrapper>
          <RiLeafFill className="leaf" />
          <CardSubText style={{ fontWeight: 600, color: '#fff' }}>
            {orgId.sustentabilityPromedy.toFixed(1)}
          </CardSubText>
        </FlexWrapper>
        <RiHeartFill style={{ color: 'white', width: '20px', height: '20px' }} />
      </CardIcons>

      <React.Fragment>
        <EditButton
          onClick={() => {
            localStorage.setItem('plateID', _id)
            push('/edit-platillo')
          }}
        >
          <RiPencilFill />
        </EditButton>
      </React.Fragment>

      <FlexWrapper style={{ padding: '0 8px' }}>
        <FlexWrapper>
          <IconWrapper marginRight="4px" size="12px">
            <RiMapPinLine />
          </IconWrapper>
          <CardSubText>{orgId.orgState}</CardSubText>
        </FlexWrapper>
        <FlexWrapper>
          <IconWrapper marginRight="4px">
            <RiStarSFill size="12px" />
          </IconWrapper>
          <CardSubText>4.5</CardSubText>
        </FlexWrapper>
      </FlexWrapper>

      <CardText style={{ margin: '8px 0', padding: '0 8px' }}>{platilloName}</CardText>

      <FlexWrapper justify="flex-start">
        <Dot />
        <CardSubText style={{ fontWeight: 'bold', color: 'var(--green-natoure)' }}>
          Activo
        </CardSubText>
      </FlexWrapper>

      <PauseButton>
        <RiPauseLine />
        <CardSubText style={{ fontWeight: 600, color: '#fff' }}>Pausar</CardSubText>
      </PauseButton>
    </CardWrapper>
  )
}

export default PlateCard
