import styled from 'styled-components'
import { useRouter } from 'next/router'
import React from 'react'
import { AiFillPlusCircle } from 'react-icons/ai'

export interface ExperieceElement {
  product: string
  ruta: string
}

const Card = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  max-width: 85vw;
  padding: 24px 0;
  border-radius: 8px;
  border-style: dashed;
  border-color: #06bc68;
  margin: 0 auto 48px auto;
  svg {
    color: #06bc68;
    width: 56px;
    height: 56px;
  }
  @media (max-width: 767px) {
    flex-direction: column;
  }
`

const Button = styled.button`
  width: 270px;
  height: 40px;
  padding: 10px 24px;
  border-radius: 4px;
  border: none;
  color: white;
  background-color: #06bc68;
  margin-left: 32px;
  font-family: Roboto;
  font-weight: 500;
  letter-spacing: 0.5px;
  :hover {
    cursor: pointer;
  }
  @media (max-width: 767px) {
    margin-top: 20px;
    margin-left: 0;
  }
`

export const AddProduct = ({ ruta, product }: ExperieceElement): JSX.Element => {
  const { push } = useRouter()

  return (
    <Card>
      <AiFillPlusCircle />
      <Button onClick={() => push(ruta)}>Añade {product}</Button>
    </Card>
  )
}

export default AddProduct
