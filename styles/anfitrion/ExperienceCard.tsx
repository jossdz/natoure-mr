import React from 'react'
import Link from 'next/link'
import Dinero from 'dinero.js'
import { useRouter } from 'next/router'
import {
  RiStarSFill,
  RiMapPinLine,
  RiHeartFill,
  RiEyeFill,
  RiPencilFill,
  RiPauseLine,
  RiLeafFill,
} from 'react-icons/ri'

import { FlexWrapper, IconWrapper } from '../checkout'
import { Dot } from '../checkout/Card'
import {
  CardWrapper,
  CardThumbnail,
  CardText,
  CardSubText,
  CardIcons,
  ViewButton,
  EditButton,
  PauseButton,
} from '../components/Cards/ExperienceCard'

type Props = {
  mainPhoto: string
  experienceName: string
  personCost: string
  _id: string
  orgId: {
    sustentabilityPromedy: number
    orgState: string
  }
}

const ExperienceCard = ({
  mainPhoto,
  experienceName,
  personCost,
  _id,
  orgId,
}: Props): JSX.Element => {
  const { push } = useRouter()
  const cost = Dinero({ amount: Number(personCost) * 100, currency: 'MXN' })

  return (
    <CardWrapper>
      <CardThumbnail image={mainPhoto[0]} />

      <CardIcons>
        <FlexWrapper>
          <RiLeafFill className="leaf" />
          <CardSubText style={{ fontWeight: 600, color: '#fff' }}>
            {orgId.sustentabilityPromedy.toFixed(1)}
          </CardSubText>
        </FlexWrapper>
        <RiHeartFill style={{ color: 'white', width: '20px', height: '20px' }} />
      </CardIcons>

      <React.Fragment>
        <Link href={`/experience/${_id}`}>
          <ViewButton>
            <RiEyeFill />
          </ViewButton>
        </Link>
        <EditButton
          onClick={() => {
            localStorage.setItem('experienceID', _id)
            push('/edit-experiencia')
          }}
        >
          <RiPencilFill />
        </EditButton>
      </React.Fragment>

      <FlexWrapper style={{ padding: '0 8px' }}>
        <FlexWrapper>
          <IconWrapper marginRight="4px" size="12px">
            <RiMapPinLine />
          </IconWrapper>
          <CardSubText>{orgId.orgState}</CardSubText>
        </FlexWrapper>
        <FlexWrapper>
          <IconWrapper marginRight="4px">
            <RiStarSFill size="12px" />
          </IconWrapper>
          <CardSubText>4.5</CardSubText>
        </FlexWrapper>
      </FlexWrapper>

      <CardText style={{ margin: '8px 0', padding: '0 8px' }}>{experienceName}</CardText>

      <FlexWrapper justify="flex-start" style={{ padding: '0 8px' }}>
        <CardSubText style={{ fontWeight: 'bold' }}>{cost.toFormat('$0,0')}</CardSubText>
        <Dot />
        <CardSubText style={{ fontWeight: 'bold', color: 'var(--green-natoure)' }}>
          Activa
        </CardSubText>
      </FlexWrapper>

      <PauseButton>
        <RiPauseLine />
        <CardSubText style={{ fontWeight: 600, color: '#fff' }}>Pausar</CardSubText>
      </PauseButton>
    </CardWrapper>
  )
}

export default ExperienceCard
