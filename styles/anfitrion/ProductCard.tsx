import React from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import {
  RiStarSFill,
  RiMapPinLine,
  RiHeartFill,
  RiEyeFill,
  RiPencilFill,
  RiPauseLine,
  RiLeafFill,
} from 'react-icons/ri'

import { FlexWrapper, IconWrapper } from '../checkout'
import { Dot } from '../checkout/Card'
import {
  CardWrapper,
  CardThumbnail,
  CardText,
  CardSubText,
  CardIcons,
  ViewButton,
  EditButton,
  PauseButton,
} from '../components/Cards/ExperienceCard'

type Props = {
  articuloCoverPhoto: string
  articuloName: string
  _id: string
  orgId: {
    orgState: string
    sustentabilityPromedy: number
  }
}

const ProductCard = ({ articuloCoverPhoto, articuloName, _id, orgId }: Props): JSX.Element => {
  const { push } = useRouter()

  return (
    <CardWrapper>
      <CardThumbnail image={articuloCoverPhoto[0]} />

      <CardIcons>
        <FlexWrapper>
          <RiLeafFill className="leaf" />
          <CardSubText style={{ fontWeight: 600, color: '#fff' }}>
            {orgId.sustentabilityPromedy.toFixed(1)}
          </CardSubText>
        </FlexWrapper>
        <RiHeartFill style={{ color: 'white', width: '20px', height: '20px' }} />
      </CardIcons>

      <React.Fragment>
        <Link href={`/producto/${_id}`}>
          <ViewButton>
            <RiEyeFill />
          </ViewButton>
        </Link>
        <EditButton
          onClick={() => {
            localStorage.setItem('articleID', _id)
            push('/edit-producto')
          }}
        >
          <RiPencilFill />
        </EditButton>
      </React.Fragment>

      <FlexWrapper style={{ padding: '0 8px' }}>
        <FlexWrapper>
          <IconWrapper marginRight="4px" size="12px">
            <RiMapPinLine />
          </IconWrapper>
          <CardSubText>{orgId.orgState}</CardSubText>
        </FlexWrapper>
        <FlexWrapper>
          <IconWrapper marginRight="4px">
            <RiStarSFill size="12px" />
          </IconWrapper>
          <CardSubText>4.5</CardSubText>
        </FlexWrapper>
      </FlexWrapper>

      <CardText style={{ margin: '8px 0', padding: '0 8px' }}>{articuloName}</CardText>

      <FlexWrapper justify="flex-start">
        <Dot />
        <CardSubText style={{ fontWeight: 'bold', color: 'var(--green-natoure)' }}>
          Activo
        </CardSubText>
      </FlexWrapper>

      <PauseButton>
        <RiPauseLine />
        <CardSubText style={{ fontWeight: 600, color: '#fff' }}>Pausar</CardSubText>
      </PauseButton>
    </CardWrapper>
  )
}

export default ProductCard
