import styled from 'styled-components'

export const LayoutContainerStyled = styled.div`
  min-height: calc(100vh - 304px);
  @media (max-width: 1023px) {
    margin-bottom: 72px;
    padding-top: 30px;
    min-height: auto;
  }
`
