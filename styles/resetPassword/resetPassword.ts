import styled from 'styled-components'
import { SuccessAlert } from '../general'

export const ResetPasswordSection = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: var(--dark-blue);
`

export const FormWrapper = styled.div`
  width: 400px;
  text-align: center;
  border: 1px solid #000;
  height: auto;
  padding: 16px 32px 24px;
  box-shadow: 0px 3px 5px -1px rgb(0 0 0 / 20%), 0px 5px 8px 0px rgb(0 0 0 / 14%),
    0px 1px 14px 0px rgb(0 0 0 / 12%);
  border-radius: 16px;
  background-color: #fff;
  ${SuccessAlert} {
    margin-top: 16px;
  }
`

export const Subtitle = styled.div`
  margin-bottom: 16px;
`

export const HidePasswordBtn = styled.button`
  border: none;
  background: none;
  color: var(--dark-blue);
  margin: 16px 0;
  align-self: flex-start;
  cursor: pointer;
  :hover,
  :active {
    text-decoration: underline;
  }
`
