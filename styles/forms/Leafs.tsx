import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { BiMinus } from 'react-icons/bi'
import { BsPlus } from 'react-icons/bs'
import { RiLeafFill, RiLeafLine } from 'react-icons/ri'

const Wrapper = styled.div`
  font-size: 16px;
  color: var(--black);
  width: 80vw;
  margin-left: auto;
  margin-right: auto;
  display: flex;
  justify-content: space-between;
  @media (min-width: 768px) {
    width: 320px;
  }
`
const Box = styled.div`
  margin: 0 16px;
  background-color: var(--light-grey);
  height: 56px;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  width: 100%;
  border-radius: 9999px;
`

const Button = styled.button`
  padding: 0;
  background-color: transparent;
  border: none;
  .qty {
    stroke-width: 1px;
    height: 20px;
    width: 20px;
    border: none;
  }
`

type Props = {
  // eslint-disable-next-line @typescript-eslint/ban-types
  style: object
  name: string
  optional: boolean
  onChange: (object) => void
  defaultValue?: number
}

const Leafs = ({ style, name, optional, onChange, defaultValue }: Props): JSX.Element => {
  const [fill, setFill] = useState(1)

  const leafStyle = { width: 24, height: 24, color: '#06bc68' }

  useEffect(() => {
    onChange!({ name: name, value: fill, valid: true, optional })
  }, [fill])

  useEffect(() => {
    defaultValue && setFill(defaultValue)
  }, [defaultValue])

  return (
    <Wrapper style={style}>
      <Button onClick={() => fill > 1 && setFill(fill - 1)}>
        <BiMinus className="qty" />
      </Button>
      <Box>
        {[...Array(fill)].map((item, index) => (
          <RiLeafFill key={index} style={leafStyle} />
        ))}
        {[...Array(5 - fill)].map((item, index) => (
          <RiLeafLine key={index} style={leafStyle} />
        ))}
      </Box>
      <Button onClick={() => fill < 5 && setFill(fill + 1)}>
        <BsPlus className="qty" />
      </Button>
    </Wrapper>
  )
}

export default Leafs

Leafs.defaultProps = {
  /* TODO: REMOVE this default styles */
  style: { marginBottom: 64 },
  optional: false,
  onChange: (values) => console.log(values),
}
