import React from 'react'
import styled from 'styled-components'

import { Button } from '../forms'

const Wrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  background-color: rgba(27, 27, 27, 0.79);
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 2;
`

const Card = styled.div`
  width: 100%;
  max-width: calc(85vw - 36px);
  height: 50vh;
  margin-left: 0;
  margin-right: 0;
  padding: 12px 18px;
  border-radius: 8px;
  background-color: var(--white);
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  img {
    width: 60px;
  }
  @media (min-width: 768px) {
    max-width: 400px;
    max-height: 360px;
  }
`

const Label = styled.div(
  ({ error }: { error: boolean }) => `
  color: ${error ? 'red' : 'var(--green-natoure)'};
  font-size: 18px;
  font-family: 'Montserrat', sans-serif;
  letter-spacing: 0.72px;
  width: ${error ? '100%' : '75%'};
  text-align: center;
  font-weight: 600;
  margin: 20px 0 28px 0;
`
)

const Text = styled.div`
  color: var(--black);
  font-size: 14px;
  font-family: 'Montserrat', sans-serif;
  letter-spacing: 0.56px;
  line-height: 1.43;
  width: 80%;
  text-align: center;
`

type Props = {
  name: string
  description: string
  onClick: () => void
  error?: boolean
}

const Modal = ({ name, description, onClick, error }: Props): JSX.Element => {
  return (
    <Wrapper>
      <Card>
        <img src="/icons/avatar.png" alt="avatar" />
        <Label error={error}>{name}</Label>
        <Text>{description}</Text>
        <Button
          style={{ width: 254, display: 'block', marginTop: 48 }}
          text="Entendido"
          onClick={onClick}
        />
      </Card>
    </Wrapper>
  )
}

export default Modal

Modal.defaultProps = {
  onClick: (values) => console.log(values),
}
