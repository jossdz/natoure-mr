import styled from 'styled-components'

export const MapWrapper = styled.div`
  width: 100%;
  max-width: 85vw;
  display: flex;
  justify-content: center;
  margin: 90px auto 64px auto;
  border-radius: 10px;
  position: relative;
  .mapboxgl-ctrl-bottom-right {
    display: none;
  }
  .mapboxgl-ctrl button:not(:disabled):hover {
    background-color: transparent;
  }
  .geocoder {
    position: absolute;
    z-index: 1;
    width: 85vw;
    max-width: 100%;
    top: -75px;
    @media (min-width: 768px) and (max-width: 1279px) {
      width: 768px;
    }
    @media (min-width: 1280px) {
      width: 1024px;
    }
    .mapboxgl-ctrl-geocoder {
      display: flex;
      align-items: center;
      width: calc(100% - 40px);
      height: 44px;
      padding: 12px 22px 12px 18px;
      border-radius: 8px;
      border: solid 1px #ced0da;
      background-color: #ffffff;
      box-shadow: none;
      .mapboxgl-ctrl-geocoder--input {
        padding: 0;
        font-family: 'Montserrat', sans-serif;
        font-size: 16px;
      }
      .mapboxgl-ctrl-geocoder--icon-search {
        display: none;
      }
      .mapboxgl-ctrl-geocoder--icon-close {
        margin-top: 0px;
      }
      .mapboxgl-ctrl-geocoder--pin-right > * {
        top: 12px;
      }
    }
  }
  .mapboxgl-ctrl-geocoder {
    min-width: 100%;
  }
`

const Map = styled.div`
  height: 340px;
  width: 100%;
  overflow: hidden;
  border-radius: 8px;
  .mapboxgl-ctrl-geocoder--input,
  .mapboxgl-ctrl-geocoder {
    width: 80vw !important;
  }
  .mapboxgl-ctrl-bottom-left,
  .mapboxgl-ctrl-bottom-right {
    display: none;
  }
`

export default Map
