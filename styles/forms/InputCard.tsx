import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { BsPencilSquare, BsFillTrashFill } from 'react-icons/bs'
import { BsCheckBox } from 'react-icons/bs'

import { Input } from '../forms'

const Wrapper = styled.div`
  font-family: 'Montserrat', sans-serif;
  border-radius: 4px;
  box-shadow: -1px 2px 9px 0 rgba(0, 0, 0, 0.07);
  width: 100%;
  max-width: calc(85vw - 48px);
  margin-left: auto;
  margin-right: auto;
  padding: 24px;
  display: flex;
  justify-content: space-between;
  -webkit-appearance: none;
  -moz-appearance: none;
  -ms-appearance: none;
  @media (min-width: 1024px) {
    width: calc(100% - 48px);
  }
  span {
    margin: 0;
    font-weight: 600;
  }
  p {
    margin: 0;
    text-align: left;
    width: calc(48vw + 36px);
  }
`

const Div = styled.div`
  display: flex;
  width: 90%;
  span {
    display: inline-block;
    margin-right: 12px;
  }
  p,
  div,
  input {
    width: 100%;
  }
  div,
  input {
    max-width: calc(100% - 32px);
  }
`

const Icons = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 8px;
  svg {
    width: 20px;
    height: 20px;
    margin-left: 4px;
  }
  button {
    &:nth-child(1) {
      color: var(--green-natoure);
      margin-bottom: 12px;
    }
    &:nth-child(2) {
      color: var(--red);
    }
    &:nth-child(3) {
      color: var(--green-natoure);
      margin-bottom: 12px;
    }
  }
`

const Button = styled.button`
  padding: 0;
  background-color: transparent;
  border: none;
  display: flex;
  align-items: center;
  font-family: Roboto;
  font-size: 14px;
`

type Props = {
  // eslint-disable-next-line @typescript-eslint/ban-types
  style: object
  placeholder: string
  name: string
  onChange: (object) => void
  index: number
  onDelete: (object) => void
  defaultValue?: string
}

const InputCard = ({
  style,
  placeholder,
  name,
  onChange,
  index,
  onDelete,
  defaultValue,
}: Props): JSX.Element => {
  const [value, setValue] = useState('')
  const [saved, setSaved] = useState(false)
  const [error, setError] = useState(false)
  const [deleted, setDeleted] = useState(false)

  const handleChange = (e) => {
    setValue(e.value)
    setError(!e.valid)
  }

  const handleSave = () => {
    setSaved(true)
  }

  const handleDelete = () => {
    setDeleted(true)
    onDelete!(name)
  }

  useEffect(
    () => onChange!({ name: name, value: value, valid: value.length > 0, optional: false }),
    [value]
  )

  useEffect(() => {
    defaultValue && setValue(defaultValue)
  }, [defaultValue])

  return (
    !deleted && (
      <Wrapper style={style}>
        <Div>
          <span>{index}.</span>
          {saved ? (
            <p>{value}</p>
          ) : (
            <Input
              name={name}
              placeholder={placeholder}
              onChange={handleChange}
              defaultValue={value}
              adjust
              style={{ width: '100%', margin: 0 }}
            />
          )}
        </Div>
        <Icons>
          {saved ? (
            <React.Fragment>
              <Button onClick={() => setSaved(false)}>
                Editar <BsPencilSquare />
              </Button>
              {index > 1 && (
                <Button onClick={() => handleDelete()}>
                  Borrar <BsFillTrashFill />
                </Button>
              )}
            </React.Fragment>
          ) : (
            <Button disabled={error} onClick={() => handleSave()}>
              Guardar <BsCheckBox />
            </Button>
          )}
        </Icons>
      </Wrapper>
    )
  )
}

export default InputCard

InputCard.defaultProps = {
  /* TODO: REMOVE this default styles */
  style: { marginBottom: 48 },
  onChange: (values) => console.log(values),
  placeholder: 'Ingresa una norma',
  onDelete: (values) => console.log(values),
}
