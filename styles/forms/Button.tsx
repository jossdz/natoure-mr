import React, { useEffect, useState } from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  position: relative;
  width: 100%;
  max-width: 360px;
  margin: 0 auto;
`

const Cta = styled.button(
  ({ unable }: any) => `
  height: 40px;
  min-width: 122px;
  width: 100%;
  max-width: 360px;
  background-color: var(--green-natoure);
  border: none;
  border-radius: 8px;
  color: var(--white);
  font-family: 'Montserrat', sans-serif;
  font-weight: 600;
  font-size: 16px;
  letter-spacing: 0.5px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: auto;
  margin-right: auto;
  text-decoration: none;
  opacity: ${unable ? '0.6' : '1'};
  `
)

const Label = styled.div`
  position: absolute;
  top: 48px;
  color: var(--red);
  font-size: 12px;
  font-family: 'Roboto', sans-serif;
  letter-spacing: 0.04em;
  line-height: 1.66;
`

type Props = {
  text: string
  // eslint-disable-next-line @typescript-eslint/ban-types
  style: object
  onClick?: () => void
  errorMessage: string
  unable: boolean
  disabled?: boolean
}

const Button = ({
  text,
  style,
  onClick,
  errorMessage,
  unable,
  disabled = false,
}: Props): JSX.Element => {
  const [error, setError] = useState(false)

  const handleClick = () => {
    if (unable) {
      setError(true)
    } else {
      onClick()
    }
  }

  useEffect(() => {
    setError(false)
  }, [unable])

  return (
    <Wrapper>
      <Cta style={style} disabled={disabled} onClick={handleClick} {...{ unable }}>
        {text}
      </Cta>
      {error && <Label>{errorMessage}</Label>}
    </Wrapper>
  )
}

export default Button

Button.defaultProps = {
  /* TODO: REMOVE this default styles */
  style: { marginBottom: 32 },
  unable: false,
  text: 'Continuar',
  errorMessage: 'Completa el formulario para continuar',
}
