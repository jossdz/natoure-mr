import React, { useEffect, useState } from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  position: relative;
  width: 100%;
  max-width: 85vw;
  margin-left: auto;
  margin-right: auto;
`

const Field = styled.select(
  ({ selected, error, optional }: any) => `
  width: 100%;
  max-width: 85vw;
  padding: 12px 18px;
  border-radius: 8px;
  border: solid 1px ${error && !optional ? 'var(--red)' : 'var(--grey)'};
  background-color: var(--white);
  display: block;
  color: ${selected === 'default' && 'var(--dark-grey)'};
  font-family: 'Montserrat', sans-serif;
  -webkit-appearance: none;
  -moz-appearance: none;
  -ms-appearance: none;
`
)

const Label = styled.div`
  position: absolute;
  top: 48px;
  left: 8px;
  color: var(--red);
  font-size: 12px;
  font-family: 'Roboto', sans-serif;
  letter-spacing: 0.04em;
  line-height: 1.66;
`

type Props = {
  name: string
  // eslint-disable-next-line @typescript-eslint/ban-types
  style?: object
  options?: string[]
  onChange?: (object) => void
  errorMessage?: string
  optional: boolean
  defaultValue?: string
  placeholder?: string
  disabled?: boolean
}

const Select = ({
  name,
  style,
  options,
  onChange,
  errorMessage,
  optional,
  placeholder,
  defaultValue,
  disabled = false,
}: Props): JSX.Element => {
  const [error, setError] = useState(false)
  const [selected, setSelected] = useState('default')

  const handleInputs = ({ value }) => {
    setError(false)
    setSelected(value)
  }

  const handleBlur = () => {
    setError(selected === 'default')
  }

  useEffect(() => {
    onChange!({ name: name, value: selected, valid: selected !== 'default', optional })
  }, [selected])

  useEffect(() => {
    defaultValue && setSelected(defaultValue)
  }, [defaultValue])

  return (
    <Wrapper>
      <Field
        disabled={disabled}
        style={style}
        name={name}
        value={selected}
        onChange={({ target }) => handleInputs(target)}
        onBlur={() => handleBlur()}
        {...{ selected, error, optional }}
      >
        <option disabled value="default">
          {placeholder ? placeholder : 'Selecciona una opción'}
        </option>
        {options.map((item, index) => (
          <option key={index} value={item}>
            {item}
          </option>
        ))}
      </Field>
      {error && !optional && <Label>{errorMessage}</Label>}
    </Wrapper>
  )
}

export default Select

Select.defaultProps = {
  /* TODO: REMOVE this default styles */
  style: { marginBottom: 64 },
  onChange: (values) => console.log(values),
  errorMessage: 'Selecciona una opción válida',
  optional: false,
}
