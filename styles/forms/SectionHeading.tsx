import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.p`
  font-size: 18px;
  font-weight: bold;
  color: var(--black);
  letter-spacing: 0.2px;
  width: 100%;
  max-width: 85vw;
`

type Props = {
  title: string
  // eslint-disable-next-line @typescript-eslint/ban-types
  style: object
}

const SectionHeading = ({ title, style }: Props): JSX.Element => {
  return <Wrapper style={style}>{title}</Wrapper>
}

export default SectionHeading

SectionHeading.defaultProps = {
  /* TODO: REMOVE this default styles */
  style: { margin: '48px auto' },
}
