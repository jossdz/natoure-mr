import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { BsPlusCircle } from 'react-icons/bs'

import { ButtonText, ButtonOutlined, Input, SmallText, Switch, Upload } from '../../styles/forms'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border: solid 2px var(--green-natoure);
  border-style: dashed;
  border-radius: 8px;
  width: 100%;
  max-width: calc(85vw - 4px);
  height: 230px;
  margin-left: auto;
  margin-right: auto;
`

const Button = styled.label`
  color: var(--green-natoure);
  font-family: 'Roboto', sans-serif;
  font-size: 16px;
  letter-spacing: 0.5px;
  text-align: center;
`

const Icon = styled.button`
  background-color: transparent;
  border: none;
  font-weight: normal;
  font-size: 90px;
  color: var(--green-natoure);
`

type Props = {
  // eslint-disable-next-line @typescript-eslint/ban-types
  style: object
  name: string
  onChange: (object) => void
  errorMessage: string
  optional: boolean
}

type Field = {
  value: string
  name: string
  valid: boolean
  optional: boolean
}

type Form = {
  extraAttr?: Field
}

const Article = ({ style, name, onChange }: Props): JSX.Element => {
  const [charging, setCharging] = useState(false)
  const [form, setForm] = useState<Form>({})
  const [validForm, setValidForm] = useState(false)

  const handleChange = (e) => {
    setForm((prevState) => {
      return {
        ...prevState,
        [e.name]: e,
      }
    })
  }

  const handleClick = () => {
    const data = Object.keys(form).reduce((acc, key) => {
      if (form[key].valid) {
        return { ...acc, [key]: form[key].value }
      } else {
        return { ...acc }
      }
    }, {})
    onChange!({ name: name, value: data, valid: validForm, optional: false })
    setCharging(false)
  }

  useEffect(() => {
    const requiredFields = Object.values(form).filter((item) => !item.optional)
    const isValid = requiredFields.every((e) => e.valid)
    setValidForm(isValid)
  }, [form])

  return (
    <>
      {charging ? (
        <>
          <SmallText black title="Nombre común" />
          <Input name="commonName" placeholder="Ingresa el nombre" onChange={handleChange} />
          <SmallText black title="Nombre científico" />
          <Input name="cientificName" placeholder="Ingresa el nombre" onChange={handleChange} />
          <SmallText black title="¿Esta especie es prioritaria?" />
          <Switch name="priority" first="No" last="Sí" onChange={handleChange} />
          <SmallText black title="Sube fotos de la especie" />
          <Upload name="photos" onChange={handleChange} />
          <ButtonOutlined unable={!validForm} onClick={handleClick} />
          <ButtonText
            style={{ margin: '32px auto 64px auto' }}
            text="Cancelar"
            onClick={() => setCharging(false)}
          />
        </>
      ) : (
        <Wrapper style={style}>
          <Icon onClick={() => setCharging(true)}>
            <BsPlusCircle />
          </Icon>
          <Button htmlFor="#upload">Cargar archivo</Button>
        </Wrapper>
      )}
    </>
  )
}

export default Article

Article.defaultProps = {
  /* TODO: REMOVE this default styles */
  style: { marginTop: 12, marginBottom: 64 },
  onChange: (values) => console.log(values),
  errorMessage: 'No se eligió ningún archivo.',
  optional: false,
}
