import React from 'react'
import styled from 'styled-components'

type Props = {
  title: string
  // eslint-disable-next-line @typescript-eslint/ban-types
  style: object
}

const Wrapper = styled.div`
  color: white;
  width: 100%;
  max-width: calc(90vw - 29px);
  background-color: var(--green-natoure);
  border-top-right-radius: 8px;
  border-top-left-radius: 8px;
  border: none;
  font-weight: 600;
  padding: 24px 16px;
  display: block;
  @media (min-width: 426px) {
    width: calc(100% - 34px);
  }
  @media (max-width: 425px) {
    margin-left: -2.5vw;
  }
`

const GreenTab = ({ title, style }: Props): JSX.Element => {
  return <Wrapper style={style}>{title}</Wrapper>
}

export default GreenTab

GreenTab.defaultProps = {
  /* TODO: REMOVE this default styles */
  style: { marginBottom: 64 },
}
