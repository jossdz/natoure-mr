/* eslint-disable @typescript-eslint/no-empty-function */
import React, { useEffect, useState } from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  position: relative;
  width: 100%;
  max-width: 360px;
  margin: 0 auto;
  svg {
    stroke-width: 1px;
    margin-left: 8px;
    margin-top: 2px;
  }
`

const Cta = styled.button(
  ({ unable }: any) => `
  height: 40px;
  width: 280px;
  border: 2px solid var(--green-natoure);
  border-radius: 8px;
  color: var(--green-natoure);
  font-family: 'Montserrat', sans-serif;
  font-weight: 600;
  font-size: 16px;
  letter-spacing: 0.5px;
  text-align: center;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: auto;
  margin-right: auto;
  background-color: transparent;
  opacity: ${unable ? '0.6' : '1'}
`
)

const Label = styled.div`
  position: absolute;
  top: 48px;
  color: var(--red);
  font-size: 12px;
  font-family: 'Roboto', sans-serif;
  letter-spacing: 0.04em;
  line-height: 1.66;
`

type Props = {
  text: string | JSX.Element
  // eslint-disable-next-line @typescript-eslint/ban-types
  style?: object
  onClick?: () => void
  errorMessage: string
  unable: boolean
}

const ButtonOutlined = ({ text, style, onClick, errorMessage, unable }: Props): JSX.Element => {
  const [error, setError] = useState(false)

  const handleClick = () => {
    if (unable) {
      setError(true)
    } else {
      onClick()
    }
  }

  useEffect(() => {
    setError(false)
  }, [unable])
  return (
    <Wrapper>
      <Cta style={style} onClick={handleClick} {...{ unable }}>
        {text}
      </Cta>
      {error && <Label>{errorMessage}</Label>}
    </Wrapper>
  )
}

export default ButtonOutlined

ButtonOutlined.defaultProps = {
  unable: false,
  text: 'Guardar',
  errorMessage: 'Completa el formulario para continuar',
  onClick: () => {},
}
