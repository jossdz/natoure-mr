import React, { useEffect, useState } from 'react'
import styled from 'styled-components'

import { SmallText } from '../forms'

const Wrapper = styled.div`
  font-size: 16px;
  color: var(--black);
  letter-spacing: 0.64px;
  line-height: 24px;
  width: 100%;
  max-width: 85vw;
  margin-left: auto;
  margin-right: auto;
`
const Box = styled.div`
  margin-bottom: 12px;
  position: relative;
`

const Field = styled.input`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  appearance: none;
  width: 16px;
  height: 16px;
  margin: 3px 8px 0 0;
  border: 1px solid var(--green-natoure);
  border-radius: 6.5px;
  position: absolute;
  &:checked {
    &:before {
      content: '✓';
      color: var(--green-natoure);
    }
  }
`

const Label = styled.div`
  margin-left: 24px;
`

type Props = {
  // eslint-disable-next-line @typescript-eslint/ban-types
  style: object
  options: string[]
  name: string
  optional: boolean
  onChange: (object) => void
  disclaimer: boolean
  defaultValue?: string[]
}

const Checkbox = ({
  style,
  options,
  name,
  optional,
  onChange,
  disclaimer,
  defaultValue,
}: Props): JSX.Element => {
  const [selected, setSelected] = useState([])

  const handleInputs = ({ checked, name }) => {
    if (checked) {
      setSelected((prevState) => {
        return [...prevState, name]
      })
    } else {
      setSelected(selected.filter((item) => item !== name))
    }
  }

  useEffect(() => {
    onChange!({
      name: name,
      value: selected,
      valid: selected !== undefined && selected.length > 0,
      optional,
    })
  }, [selected])

  useEffect(() => {
    defaultValue !== undefined && setSelected(defaultValue)
  }, [defaultValue])

  return (
    <Wrapper style={style}>
      {disclaimer && <SmallText title="Puedes elegir más de uno" />}
      {options.map((item, index) => (
        <Box key={index}>
          <Field
            type="checkbox"
            checked={selected.includes(item)}
            name={item}
            onChange={({ target }) => handleInputs(target)}
          />
          <Label>{item}</Label>
        </Box>
      ))}
    </Wrapper>
  )
}

export default Checkbox

Checkbox.defaultProps = {
  /* TODO: REMOVE this default styles */
  style: { marginBottom: 64 },
  optional: false,
  onChange: (values) => console.log(values),
  disclaimer: true,
}
