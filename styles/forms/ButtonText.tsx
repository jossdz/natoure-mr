import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  position: relative;
`

const Cta = styled.button`
  width: max-content;
  border: none;
  font-family: 'Montserrat', sans-serif;
  font-size: 16px;
  letter-spacing: 0.5px;
  margin-left: auto;
  margin-right: auto;
  display: block;
  background-color: transparent;
`

type Props = {
  text: string
  // eslint-disable-next-line @typescript-eslint/ban-types
  style: object
  onClick?: () => void
}

const ButtonText = ({ text, style, onClick }: Props): JSX.Element => {
  return (
    <Wrapper>
      <Cta style={style} onClick={onClick}>
        {text}
      </Cta>
    </Wrapper>
  )
}

export default ButtonText

ButtonText.defaultProps = {
  /* TODO: REMOVE this default styles */
  style: { marginBottom: 96 },
  text: 'Regresar',
}
