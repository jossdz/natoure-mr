/* eslint-disable jsx-a11y/media-has-caption */
import React, { useEffect, useState } from 'react'
import styled, { keyframes } from 'styled-components'
import { BsCardImage } from 'react-icons/bs'
import { uploadMedia } from '../../pages/api/api'
import { AiOutlineCloseSquare, AiOutlineLoading3Quarters } from 'react-icons/ai'
import { SmallText } from '../forms'

const Wrapper = styled.div(
  ({ status }: { status: string }) => `
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border: solid 2px ${
    status === 'error'
      ? 'var(--red)'
      : status === 'finished'
      ? 'var(--green-natoure)'
      : 'var(--blue)'
  };
  border-style: dashed;
  border-radius: 8px;
  width: 100%;
  max-width: calc(85vw - 4px);
  height: 230px;
  margin-left: auto;
  margin-right: auto;
  position: relative;
  .imagesDiv {
    display: flex;
    align-items: flex-start;
  }
  .closeBtn {
    color: red;
    font-weight: bold;
    font-size: 20px;
    background-color: white;
    border: none;
    padding: 0;
    margin: 0 16px 0 0;
  }
  .upImage {
    height: 180px;
  }
  .upImagesContainer {
    display: flex;
    width: 90%;
    overflow-x: scroll;
    margin-left: 10px;
    ::-webkit-scrollbar {
      display: none;
    }
  }
`
)

const Button = styled.label`
  padding: 10.5px 18px;
  min-width: 160px;
  width: max-content;
  background-color: var(--blue);
  border: none;
  border-radius: 8px;
  color: var(--white);
  font-family: 'Roboto', sans-serif;
  font-size: 16px;
  letter-spacing: 0.5px;
  text-align: center;
  color: #ffffff;
  :not(:only-child),
  :not(:last-child) {
    margin-bottom: 8px;
  }
`

const Input = styled.input`
  height: 0px;
  width: 0px;
`

const Icon = styled.div`
  font-weight: normal;
  font-size: 90px;
  color: var(--green-natoure);
`

const Label = styled.div`
  position: absolute;
  top: 236px;
  left: 8px;
  color: var(--red);
  font-size: 12px;
  font-family: 'Roboto', sans-serif;
  letter-spacing: 0.04em;
  line-height: 1.66;
`

const SuccessLabel = styled.div`
  position: absolute;
  top: 236px;
  left: 8px;
  color: var(--green-natoure);
  font-size: 12px;
  font-family: 'Roboto', sans-serif;
  letter-spacing: 0.04em;
  line-height: 1.66;
`

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`

const Loader = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;
  svg {
    animation: ${rotate} 1.5s linear infinite;
    color: var(--green-natoure);
    width: 64px;
    height: 64px;
  }
`

type Props = {
  // eslint-disable-next-line @typescript-eslint/ban-types
  style: object
  name: string
  onChange: (object) => void
  errorMessage: string
  errorUpload: string
  successMessage: string
  defaultValue?: string[]
  onlyPhoto?: boolean
}

const Upload = ({
  style,
  name,
  onChange,
  errorMessage,
  errorUpload,
  onlyPhoto = false,
  successMessage,
  defaultValue,
}: Props): JSX.Element => {
  const [photos, setPhotos] = useState([])
  const [status, setStatus] = useState<'' | 'fetching' | 'finished' | 'error'>('')
  const [error, setError] = useState(errorMessage)

  const deleteImage = (index) => {
    const newFiles = [...photos]

    newFiles.splice(index, 1)
    setPhotos(newFiles)

    photos.length === 1 && setStatus('error')
  }

  const handleInputs = ({ files }) => {
    setStatus('fetching')

    const arr = [...files]
    const formData = new FormData()

    arr.forEach((file) => {
      formData.append('images', file)
      uploadMedia(formData)
        .then(({ files }) => {
          setPhotos(files)
          setStatus('finished')
        })
        .catch(() => {
          setStatus('error')
          setError(errorUpload)
        })
    })
  }

  useEffect(() => {
    onChange!({ name: name, value: photos, valid: photos.length > 0 })
  }, [photos])

  useEffect(() => {
    defaultValue && setPhotos(defaultValue)
  }, [defaultValue])

  return (
    <React.Fragment>
      <SmallText
        title={`Formatos aceptados: imágenes jpg, png, jpeg de máximo 20MB ${
          !onlyPhoto ? 'y videos mp4 o mov de hasta 100MB' : ''
        }`}
      />
      <Wrapper style={style} {...{ status }}>
        {(photos.length === 0 && status === '') || status === 'error' ? (
          <React.Fragment>
            <Icon>
              <BsCardImage />
            </Icon>
            <Button htmlFor={`upload-${name}`}>
              {onlyPhoto ? 'Cargar imagen' : 'Cargar imágenes y/o videos'}
            </Button>
          </React.Fragment>
        ) : status === 'fetching' ? (
          <Loader>
            <AiOutlineLoading3Quarters />
          </Loader>
        ) : (
          photos.length > 0 && (
            <div className="upImagesContainer">
              {photos.map((image, index) => (
                <div className="imagesDiv" key={index}>
                  {image.includes('mov' || 'mp4') ? (
                    <video className="upImage" controls>
                      <source src={image} type="video/mp4" />
                      Your browser does not support the video tag.
                    </video>
                  ) : (
                    <img className="upImage" src={image} alt="pic" />
                  )}
                  <button className="closeBtn" onClick={() => deleteImage(index)}>
                    <AiOutlineCloseSquare />
                  </button>
                </div>
              ))}
            </div>
          )
        )}
        <Input
          id={`upload-${name}`}
          type="file"
          name={name}
          multiple
          onChange={({ target }) => handleInputs(target)}
          onBlur={({ target }) => target.files.length < 1 && setStatus('error')}
        />
        {status === 'error' ? (
          <Label>{error}</Label>
        ) : status === 'finished' ? (
          <SuccessLabel>{successMessage}</SuccessLabel>
        ) : null}
      </Wrapper>
    </React.Fragment>
  )
}

export default Upload

Upload.defaultProps = {
  style: { marginTop: 12, marginBottom: 64 },
  onChange: (values) => console.log(values),
  errorMessage: 'No se eligió ningún archivo.',
  errorUpload:
    'Error al subir el archivo, verifica que no exceda el peso máximo y que cumpla el formato requerido.',
  successMessage: 'Archivos subidos con exito',
}
