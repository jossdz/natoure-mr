import React, { useEffect, useState } from 'react'
import styled from 'styled-components'

const Wrapper = styled.div(
  ({ textarea, error, adjust, optional, disabled }: any) => `
  position: relative;
  width: 100%;
  max-width: ${adjust ? 'max-content' : '85vw'};
  margin-left: ${adjust ? '0' : 'auto'};
  margin-right: ${adjust ? '0' : 'auto'};
  input, textarea {
    font-family: 'Montserrat', sans-serif;
    width: 100%;
    max-width: calc(85vw - 38px);
    height: ${textarea ? '64px' : '18px'};
    padding: 12px 18px;
    border-radius: 8px;
    border: solid 1px ${error && !optional ? 'var(--red)' : 'var(--grey)'};
    color:${disabled ? 'var(--dark-grey)' : 'var(--black)'};
    background-color: var(--white);
    display: block;
    -webkit-appearance: none;
    -moz-appearance: none;
    -ms-appearance: none;
    ::placeholder,
    ::-webkit-input-placeholder {
      color: var(--dark-grey);
      font-family: 'Montserrat', sans-serif;
    }
    @media (min-width: 1024px) {
      width: calc(100% - 38px)
    }
  }
`
)

const Label = styled.div(
  ({ textarea }: any) => `
  position: absolute;
  top: ${textarea ? '126px' : '48px'};
  left: 8px;
  color: var(--red);
  font-size: 12px;
  font-family: 'Roboto', sans-serif;
  letter-spacing: 0.04em;
  line-height: 1.66;
  width: max-content;
`
)

type Props = {
  // eslint-disable-next-line @typescript-eslint/ban-types
  style: object
  placeholder: string
  name: string
  onChange: (object) => void
  errorMessage: string
  textarea: boolean
  number: boolean
  optional: boolean
  adjust: boolean
  defaultValue?: string
  email: boolean
  min?: number
  disabled?: boolean
}

const Input = ({
  style,
  placeholder,
  name,
  onChange,
  errorMessage,
  textarea,
  number,
  optional,
  adjust,
  defaultValue,
  email,
  min,
  disabled = false,
}: Props): JSX.Element => {
  const Tag = textarea ? 'textarea' : 'input'
  const [error, setError] = useState(false)
  const [value, setValue] = useState('')

  const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/

  const handleInputs = ({ value }) => {
    setError(false)
    setValue(value)
  }

  const handleBlur = ({ value }) => {
    min
      ? setError(value < min)
      : setError(email ? Boolean(!value.match(emailRegex)) : value.length < 1)
  }

  useEffect(
    () =>
      onChange!({
        name: name,
        value: value,
        valid: min
          ? Number(value) >= min
          : email
          ? Boolean(value.match(emailRegex))
          : value.length > 0,
        optional,
      }),
    [value]
  )

  useEffect(() => {
    defaultValue && setValue(defaultValue)
  }, [defaultValue])

  return (
    <Wrapper {...{ textarea, error, adjust, optional, disabled }}>
      <Tag
        style={style}
        type={number ? 'number' : email ? 'email' : 'text'}
        name={name}
        defaultValue={defaultValue}
        min={min && min}
        placeholder={placeholder}
        onChange={({ target }) => handleInputs(target)}
        onBlur={({ target }) => handleBlur(target)}
        disabled={disabled}
      />
      {error && !optional && (
        <Label {...{ textarea }}>{min ? 'Ingresa un rango válido' : errorMessage}</Label>
      )}
    </Wrapper>
  )
}

export default Input

Input.defaultProps = {
  /* TODO: REMOVE this default styles */
  style: { marginBottom: 64 },
  errorMessage: 'Este campo no puede estar vacío.',
  onChange: (values) => console.log(values),
  textarea: false,
  number: false,
  optional: false,
  adjust: false,
  email: false,
}
