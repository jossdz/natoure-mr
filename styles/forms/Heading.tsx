import React from 'react'
import styled from 'styled-components'
import { useRouter } from 'next/router'
import { BsX, BsChevronLeft } from 'react-icons/bs'

import ButtonOutlined from './ButtonOutlined'

import { Container } from '../components'

const Wrapper = styled.section(
  ({ simple }: any) => `
  text-align: center;
  width: 100vw;
  height: ${!simple && '164px'};
  background-color: var(--dark-blue);
  color: var(--white);
  padding: ${simple ? '48px' : '32px'} 0;
`
)

const Head = styled.div(
  ({ simple }: any) => `
  font-family: 'Roboto', sans-serif;
  font-size: 18px;
  font-weight: 500;
  letter-spacing: 0.2px;
  display: flex;
  width: 100%;
  max-width: 85vw;
  justify-content: space-between;
  margin: 0 auto ${simple ? '0' : '40px'} auto;
  svg {
    width: 24px;
    height: 24px;
    stroke-width: 1px;
  }
  @media (min-width: 1024px) {
    font-size: 24px;
  }
`
)

const SmallText = styled.p`
  font-family: 'Montserrat', sans-serif;
  font-size: 12px;
  line-height: 1.5;
  text-align: center;
  color: var(--white);
  margin: 24px 0 12px 0;
`

const Step = styled.div`
  background-color: var(--green-natoure);
  height: 7px;
  width: 36px;
  margin-right: 5px;
  display: inline-block;
`

const Pending = styled.div`
  background-color: var(--green-natoure);
  height: 7px;
  width: 36px;
  margin-right: 5px;
  display: inline-block;
  opacity: 0.39;
`

const Back = styled.button(
  ({ step }: any) => `
  padding: 0;
  background-color: transparent;
  border: none;
  color: ${step === 1 ? 'var(--dark-blue)' : 'var(--white)'};
  z-index: ${step === 1 ? -1 : 1};
`
)

const Close = styled.button`
  padding: 0;
  background-color: transparent;
  border: none;
  color: var(--white);
`

type Props = {
  title: string
  stepName?: string
  step: number
  total?: number
  onBackStep?: () => void
  simple: boolean
}

export const Heading = ({
  title,
  stepName,
  step,
  total,
  onBackStep,
  simple,
}: Props): JSX.Element => {
  const { back } = useRouter()

  const steps = []
  for (let i = 1; i <= step; i++) {
    steps.push(<Step key={i} />)
  }

  const pending = []
  for (let i = 1; i <= total - step; i++) {
    pending.push(<Pending key={i} />)
  }

  return (
    <Wrapper {...{ simple }}>
      <Container>
        <Head {...{ simple }}>
          <Back onClick={onBackStep} {...{ step }}>
            <BsChevronLeft />
          </Back>
          {title}
          <Close onClick={() => back()}>
            <BsX />
          </Close>
        </Head>
        {!simple && (
          <>
            <ButtonOutlined text={stepName} />
            <SmallText>
              Paso {step} de {total}
            </SmallText>
            {steps}
            {pending}
          </>
        )}
      </Container>
    </Wrapper>
  )
}

export default Heading

Heading.defaultProps = {
  simple: false,
}
