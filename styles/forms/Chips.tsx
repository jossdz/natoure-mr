import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { BsX } from 'react-icons/bs'

import { SmallText } from '../forms'

const Wrapper = styled.div`
  position: relative;
  width: 100%;
  max-width: 85vw;
  margin-left: auto;
  margin-right: auto;
`

const Field = styled.select(
  ({ error }: any) => `  
  width: 100%;
  max-width: 85vw;
  padding: 12px 18px;
  border-radius: 8px;
  border: solid 1px ${error ? 'var(--red)' : 'var(--grey)'};
  background-color: var(--white);
  display: block;
  color: var(--dark-grey);
  font-family: 'Montserrat', sans-serif;
  -webkit-appearance: none;
  -moz-appearance: none;
  -ms-appearance: none;
`
)

const Input = styled.input(
  ({ error }: any) => `  
  width: 100%;
  padding: 12px 18px;
  border-radius: 8px;
  border: solid 1px ${error ? 'var(--red)' : 'var(--grey)'};
  background-color: var(--white);
  display: block;
  font-family: 'Montserrat', sans-serif;
  position: absolute;
  -webkit-appearance: none;
  -moz-appearance: none;
  -ms-appearance: none;
  top: 28px;
  ::placeholder,
  ::-webkit-input-placeholder {
    color: var(--dark-grey);
    font-family: 'Montserrat', sans-serif;
  }
  @media (max-width: 425px) {
    max-width: 76vw;
  }
  @media (min-width: 426px) {
    width: calc(100% - 38px)
  }
`
)

const Box = styled.div`
  width: 100%;
  max-width: calc(85vw + 8px);
  margin: 12px auto 0 auto;
  display: flex;
  flex-wrap: wrap;
`

const Badge = styled.div`
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  background-color: var(--green-natoure);
  width: max-content;
  padding: 8px 12px 8px 16px;
  color: var(--white);
  border-radius: 8px;
  margin: 4px;
  display: flex;
  align-items: center;
  button {
    margin-left: 8px;
    color: var(--white);
    padding: 0;
    border: none;
    background-color: transparent;
    svg {
      stroke-width: 1px;
      display: flex;
      align-items: center;
      width: 16px;
      height: 16px;
    }
  }
`

const Label = styled.div`
  position: absolute;
  top: 78px;
  left: 8px;
  color: var(--red);
  font-size: 12px;
  font-family: 'Roboto', sans-serif;
  letter-spacing: 0.04em;
  line-height: 1.66;
`

const Add = styled.button`
  font-family: 'Montserrat', sans-serif;
  color: var(--green-natoure);
  text-decoration: underline;
  font-weight: 500;
  border: none;
  padding: 8px 0;
  position: absolute;
  background-color: transparent;
  top: 35px;
  right: 24px;
  z-index: 3;
`

type Props = {
  name: string
  // eslint-disable-next-line @typescript-eslint/ban-types
  style: object
  options: string[]
  onChange: (object) => void
  errorMessage: string
  optional: boolean
  defaultValue?: string[]
  value?: string[]
}

const Chips = ({
  name,
  style,
  options,
  onChange,
  errorMessage,
  optional,
  defaultValue,
}: Props): JSX.Element => {
  const [error, setError] = useState(false)
  const [selected, setSelected] = useState([])
  const [input, setInput] = useState(false)
  const [string, setString] = useState('')
  const [newSelect, setNewSelect] = useState([])

  const handleInputs = ({ value }) => {
    setError(false)
    !selected.includes(value) &&
      setSelected((prevState) => {
        return [...prevState, value]
      })
  }

  const handleClick = (value) => {
    const filtered = selected.filter((item) => item !== value)
    setSelected(filtered)
    setError(filtered.length === 0 && !optional)
  }

  useEffect(() => {
    onChange!({
      name: name,
      value: selected,
      valid: selected !== undefined && selected.length !== 0,
      optional,
    })
  }, [selected])

  useEffect(() => {
    defaultValue && setSelected(defaultValue)
  }, [defaultValue])

  return (
    <Wrapper style={style}>
      <SmallText title="Puedes elegir más de uno" />
      <Field
        name={name}
        value="default"
        onChange={({ target }) => handleInputs(target)}
        {...{ error }}
      >
        <option value="default" disabled>
          Selecciona de la lista
        </option>
        {options.map((item, index) => (
          <option key={index} value={item}>
            {item}
          </option>
        ))}
      </Field>
      <Add
        onClick={() => {
          setInput(!input)
          input &&
            string.length !== 0 &&
            setSelected((prevState) => {
              return [...prevState, string]
            })
        }}
      >
        {input ? '✓ Listo' : '+ Agregar'}
      </Add>
      {input && (
        <Input
          placeholder="Ingresa una nueva opción"
          onChange={({ target }) => setString(target.value)}
        />
      )}
      {error && <Label>{errorMessage}</Label>}
      {selected !== undefined && selected.length > 0 && (
        <Box>
          {selected.map((item, index) => (
            <Badge key={index}>
              {item}
              <button onClick={() => handleClick(item)}>
                <BsX />
              </button>
            </Badge>
          ))}
        </Box>
      )}
    </Wrapper>
  )
}

export default Chips

Chips.defaultProps = {
  /* TODO: REMOVE this default styles */
  style: { marginBottom: 64 },
  onChange: (values) => console.log(values),
  errorMessage: 'Selecciona o agrega al menos una opción',
  optional: false,
}
