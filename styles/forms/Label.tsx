import React from 'react'
import styled from 'styled-components'

type LabelWrapperStyledProps = {
  marginBottom?: string
}

const Wrapper = styled.div<LabelWrapperStyledProps>(
  ({ marginBottom }) => `
  ${marginBottom ? `margin: 0 auto ${marginBottom};` : 'margin: 0 auto 12px;'}
  width: 100%;
  max-width: 88vw;
  padding-left: 10px;
  color: var(--black);
  &::before {
    content: '';
    margin-left: -10px;
    width: 4px;
    height: 17px;
    background-color: var(--green-natoure);
    position: absolute;
    border-radius: 3px;
  }
`
)

const Optional = styled.span`
  padding-left: 10px;
  color: var(--green-natoure);
  font-weight: bold;
  font-size: 14px;
`

type Props = {
  title: any
  optional?: boolean
  marginBottom?: string
}

const Label = ({ title, optional, marginBottom }: Props): JSX.Element => {
  return (
    <Wrapper marginBottom={marginBottom}>
      {title}
      {optional && <Optional>(Opcional)</Optional>}
    </Wrapper>
  )
}

export default Label
