import styled from 'styled-components'

export default styled.div`
  height: 100vh;
  max-height: 100vh;
  overflow-y: hidden;
  overflow-x: hidden;
  ::-webkit-scrollbar {
    display: none;
  }
  box-sizing: border-box;
  background-position: center;
  background-color: #2c375a;
`
