import styled from 'styled-components'

type props = {
  bgImg: string
}

export default styled.div(
  ({ bgImg }: props) => `
  box-sizing: border-box;
  color: white;
  display: flex;
  background: linear-gradient(to bottom, rgba(44, 55, 90, 0) 25%, rgba(44, 55, 90, 1) 75%),
    url(${bgImg});
  background-size: cover;
  background-position: top center;
  padding: 80px 0 40px 0;
  height: inherit;
  &::before {
    content: '';
    width: 100%;
    height: 100vh;
    position: absolute;
    background-color: #2c375a;
    background-size: cover;
    opacity: 0.6;
    top: 0;
    right: 0;
  }
  & > * {
    z-index: 2;
  }
  section {
    text-align: center;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    height: auto;
    .title {
      font-size: 24px;
      font-weight: bold;
      text-decoration: underline;
    }
    .description {
      margin-top: 1;
      font-size: 16px;
      font-weight: 100;
      span {
        color: #06bc68;
        font-weight: bold;
      }
    }
    .dots {
      width: 100px;
      display: block;
      margin: 32px auto 0 auto;
    }
  }
`
)
