import React from 'react'
import styled from 'styled-components'
import { SectionHeading } from '../../styles/components'
import HorizontalS from '../../styles/general/HorizontalSlide'

export interface ExperieceElement {
  commonName: string
  cientificName: string
  photos: string
}

export interface IExperienceCardsProps {
  flora?: ExperieceElement[]
  fauna?: ExperieceElement[]
}

const CardsCollection = styled.section`
  color: #2c375a;
  font-size: 14px;
  .title {
    font-weight: bold;
    font-size: 16px;
  }
`

const Card = styled.article`
  width: 168px;
  margin-right: 24px;
  flex-shrink: 0;
  img {
    height: 160px;
  }
  span {
    display: block;
    font-size: 13px;
  }
`

export const FloraFaunaCard = (props: ExperieceElement): JSX.Element => {
  return (
    <Card>
      <img src={props.photos && props.photos[0]} alt={props.commonName} />
      <b>{props.commonName}</b>
      <span>{props.cientificName}</span>
    </Card>
  )
}

export default function FloraFaunaCards(props: IExperienceCardsProps): JSX.Element {
  return (
    <CardsCollection>
      <p className="title">Especies que podrás observar</p>
      {props.flora && props.flora.length > 0 && (
        <>
          <SectionHeading removeMarginBottom removeMarginTop title="Flora" />
          <HorizontalS>
            {props.flora &&
              props.flora.length > 0 &&
              props.flora.map((specie, i) => {
                return <FloraFaunaCard key={i} {...specie} />
              })}
          </HorizontalS>
        </>
      )}
      {props.fauna && props.fauna.length > 0 && (
        <>
          <SectionHeading removeMarginBottom removeMarginTop title="Fauna" />
          <HorizontalS>
            {props.fauna &&
              props.fauna.length > 0 &&
              props.fauna.map((specie, i) => {
                return <FloraFaunaCard key={i} {...specie} />
              })}
          </HorizontalS>
        </>
      )}
    </CardsCollection>
  )
}
