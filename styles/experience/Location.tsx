import styled from 'styled-components'
import React, { useRef, useEffect } from 'react'
import mapboxgl from 'mapbox-gl'

import Map from '../../styles/forms/Map'

mapboxgl.accessToken =
  'pk.eyJ1IjoibWx6eiIsImEiOiJjandrNmVzNzUwNWZjNGFqdGcwNmJ2ZWhpIn0.ybY6wnAtJwj-Tq0c46sW6A'

export interface ExperieceElement {
  address: {
    address: string
    coords: number[]
  }
  addressDetails: string
}

const Card = styled.section`
  color: #2c375a;
  font-size: 14px;
  .title {
    font-weight: bold;
    font-size: 16px;
  }
`

export const Location = (props: ExperieceElement): JSX.Element => {
  const node = useRef(null)

  useEffect(() => {
    const fixedLocation = props.address ? props.address.coords : [-99.1412, 19.4352]

    const map = new mapboxgl.Map({
      container: node.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: fixedLocation,
      zoom: 13,
    })

    new mapboxgl.Marker().setLngLat(fixedLocation).addTo(map)
  }, [props.address])

  return (
    <Card>
      <p className="title">Ubicación</p>
      <Map ref={node} />
      <p>{props.address && props.address.address}</p>
      <p>{props.addressDetails}</p>
    </Card>
  )
}

export default Location
