import styled from 'styled-components'
import React from 'react'
import { AiOutlineClockCircle } from 'react-icons/ai'
import { useRouter } from 'next/router'

export interface ExperieceElement {
  duration: string
  price: number
  href: string
  onClick: (e: React.MouseEvent) => void
}

const Wrapper = styled.section`
  width: 100%;
  margin: 0 auto;
  @media (min-width: 1024px) and (max-width: 1279px) {
    width: 768px;
  }
  @media (min-width: 1280px) {
    width: 992px;
  }
`

const Card = styled.div`
  width: 100%;
  height: 108px;
  background-color: var(--dark-blue);
  border-bottom-right-radius: 8px;
  border-bottom-left-radius: 8px;
  display: flex;
  color: white;
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  @media (min-width: 1024px) {
    border-radius: 8px;
    width: 60%;
  }
  .firstdiv {
    font-weight: 600;
    display: flex;
    align-items: center;
    svg {
      margin: 0 8px;
    }
  }
  button {
    width: 128px;
    height: 48px;
    border-radius: 8px;
    background-color: #fa4775;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    border: none;
    color: white;
  }
`

export const SustainCard = (props: ExperieceElement): JSX.Element => {
  const { push } = useRouter()
  const handleClick = (e) => {
    if (props.onClick) props.onClick(e)
    push(props.href)
  }
  return (
    <Wrapper>
      <Card>
        <div className="firstdiv">
          Duración:
          <AiOutlineClockCircle />
          {props.duration}
        </div>
        <button onClick={handleClick}>
          <b>${props.price}</b>
          Reserva ahora
        </button>
      </Card>
    </Wrapper>
  )
}

export default SustainCard
