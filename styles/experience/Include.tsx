import styled from 'styled-components'
import React from 'react'

export interface ExperieceElement {
  included: string[]
  notIncluded: string[]
}

const Card = styled.section`
  color: #2c375a;
  .title {
    font-weight: bold;
  }
  ul {
    margin-bottom: 20px;
    font-size: 14px;
  }
  @media (min-width: 1024px) {
    width: 60%;
  }
`

export const Include = (props: ExperieceElement): JSX.Element => {
  return (
    <Card>
      <div>
        <p className="title">¿Qué incluye?</p>
        <ul>
          {props.included && props.included.map((service, index) => <li key={index}>{service}</li>)}
        </ul>
      </div>
      <div className="second">
        <p className="title">¿Qué no incluye?</p>
        <ul>
          {props.notIncluded &&
            props.notIncluded.map((service, index) => <li key={index}>{service}</li>)}
        </ul>
      </div>
    </Card>
  )
}

export default Include
