import styled from 'styled-components'
import React from 'react'

export interface ExperieceElement {
  difficulty: string
  age: string
  duration: string
  people: string
  language?: string[]
  maxPersons: string
  sugestedClothing?: string[]
}

const Card = styled.section`
  color: var(--dark-blue);
  .title {
    font-weight: bold;
  }
  .list {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    color: #2c375a;
    font-size: 13px;
    p {
      margin: 0 0 8px 0;
    }
    .props {
      color: var(--green-natoure);
      display: flex;
      flex-wrap: wrap;
      text-align: right;
      justify-content: flex-end;
      max-width: 70%;
      span {
        margin-left: 4px;
      }
    }
  }
  @media (min-width: 1024px) {
    width: 60%;
  }
`

export const interestCard = (props: ExperieceElement): JSX.Element => {
  return (
    <Card>
      <p className="title">Datos de interés</p>

      <div className="list">
        <p>Dificultad</p>
        <p className="props">{props.difficulty}</p>
      </div>
      <div className="list">
        <p>Edad Recomendada</p>
        <p className="props">{props.age}</p>
      </div>
      <div className="list">
        <p>Límite de participantes</p>
        <p className="props">{props.maxPersons}</p>
      </div>
      {props.sugestedClothing && (
        <div className="list">
          <p>Ropa y equipo recomendado</p>
          <p className="props">
            {props.sugestedClothing.map((item, index) => (
              <span key={index}>• {item}</span>
            ))}
          </p>
        </div>
      )}
      {props.language && (
        <div className="list">
          <p>Idiomas</p>
          <p className="props">
            {props.language.map((item, index) => (
              <span key={index}>• {item}</span>
            ))}
          </p>
        </div>
      )}
    </Card>
  )
}

export default interestCard
