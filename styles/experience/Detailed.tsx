import styled from 'styled-components'
import React from 'react'

export interface ExperieceElement {
  days: {
    name: string
    description: string
    photos: string[]
  }[]
}

const Card = styled.section`
  @media (min-width: 1024px) {
    width: 60%;
  }
  color: #2c375a;
  .title {
    font-weight: bold;
  }
  div {
    font-size: 14px;
    .head {
      display: flex;
      align-items: center;
      font-weight: bold;
      .circle {
        width: 56px;
        height: 56px;
        border: solid 2px #06bc68;
        display: flex;
        align-items: center;
        justify-content: center;
        border-radius: 50%;
        margin-right: 12px;
      }
    }
  }
`

export const Detailed = (props: ExperieceElement): JSX.Element => {
  return (
    <Card>
      <p className="title">Descripción detallada</p>
      {props.days.map((item, index) => (
        <div key={index}>
          <div className="head">
            <div className="circle">Día {index + 1}</div>
            <span>{item.name}</span>
          </div>
          <p>{item.description}</p>
        </div>
      ))}
    </Card>
  )
}

export default Detailed
