import styled from 'styled-components'
import React from 'react'

import Container from '../../styles/components/Container'

export interface ExperieceElement {
  photos: string[]
  isSecondary?: boolean
}

const Wrapper = styled.section`
  color: #2c375a;
  padding: 0;
  .title {
    font-weight: bold;
    @media (min-width: 1024px) {
      display: none;
    }
  }
`

const Card = styled.div(
  ({ isSecondary }: { isSecondary: boolean }) => `
  width: 100%;
  min-height: 50px;
  display: flex;
  overflow-x: scroll;
  ::-webkit-scrollbar {
    display: none;
  }
  overflow-y: hidden;
  margin-bottom: 20px;
  img {
    height: 240px;
    margin-right: 10px;
    position: relative;
    @media (min-width: 1280px) {
      height: ${isSecondary ? '170px' : '420px'};
      border-radius: 16px;
    }
  }

  @media (min-width: 1024px) {
    border-radius: 16px;
  }
`
)

export const Gallery = (props: ExperieceElement): JSX.Element => {
  return (
    <Wrapper>
      <Container>
        <p className="title">Galería de fotos</p>
      </Container>
      <Card isSecondary={props.isSecondary}>
        {props.photos !== undefined &&
          props.photos.map((photo, index) => <img key={index} src={photo} alt="" />)}
      </Card>
    </Wrapper>
  )
}

export default Gallery
