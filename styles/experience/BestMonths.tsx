import styled from 'styled-components'
import React from 'react'

export interface ExperieceElement {
  bestMonths?: string[]
  title: string
}

const Card = styled.section`
  color: var(--dark-blue);
  .title {
    font-weight: bold;
  }
  div {
    display: flex;
    flex-wrap: wrap;
    span {
      font-family: 'Roboto', sans-serif;
      font-size: 14px;
      border: solid 1px #06bc68;
      width: max-content;
      padding: 8px 12px;
      border-radius: 8px;
      margin: 4px;
      display: flex;
      align-items: center;
    }
  }
  @media (min-width: 1024px) {
    width: 60%;
  }
`

export const BestMonths = (props: ExperieceElement): JSX.Element => {
  return (
    <Card>
      <p className="title">{props.title}</p>
      <div>
        {props.bestMonths &&
          props.bestMonths.map((month, index) => <span key={index}>{month}</span>)}
      </div>
    </Card>
  )
}

export default BestMonths
