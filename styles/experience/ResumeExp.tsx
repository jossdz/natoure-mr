import styled from 'styled-components'
import React from 'react'

export interface ExperieceElement {
  title: string
  description: string
}

const Card = styled.section`
  color: var(--dark-blue);
  margin-top: 20px;
  margin-bottom: 20px;
  font-size: 14px;
  .title {
    font-weight: bold;
    font-size: 16px;
  }
  @media (min-width: 1024px) {
    width: 60%;
  }
`

export const ResumeExp = (props: ExperieceElement): JSX.Element => {
  return (
    <Card>
      <p className="title">{props.title}</p>
      <p>{props.description}</p>
    </Card>
  )
}

export default ResumeExp
