import styled from 'styled-components'
import React from 'react'
export interface ExperieceElement {
  title: string
  activities: string[]
}

const Card = styled.section`
  color: var(--dark-blue);
  .title {
    font-weight: bold;
  }
  div {
    display: flex;
    flex-wrap: wrap;
    span {
      font-family: 'Roboto', sans-serif;
      font-size: 14px;
      background-color: var(--green-natoure);
      width: max-content;
      padding: 8px 12px;
      color: var(--white);
      border-radius: 8px;
      margin: 4px;
      display: flex;
      align-items: center;
    }
  }
  @media (min-width: 1024px) {
    width: 60%;
  }
`

export const Activities = (props: ExperieceElement): JSX.Element => {
  return (
    <Card>
      <p className="title">{props.title}</p>
      <div>
        {props.activities.map((activity, index) => (
          <span key={index}>{activity}</span>
        ))}
      </div>
    </Card>
  )
}

export default Activities
