import styled from 'styled-components'
import React from 'react'

export interface ExperieceElement {
  title: string
  attributes?: string[]
}

const Card = styled.section`
  color: #2c375a;
  .title {
    font-weight: bold;
  }
  ul {
    margin-bottom: 20px;
    font-size: 14px;
    font-weight: 400;
  }
  @media (min-width: 1024px) {
    width: 60%;
  }
`

export const Atributes = (props: ExperieceElement): JSX.Element => {
  return (
    <Card>
      <p className="title">{props.title}</p>
      <ul>
        {props.attributes &&
          props.attributes.map((attribute, index) => <li key={index}>{attribute}</li>)}
      </ul>
    </Card>
  )
}

export default Atributes
