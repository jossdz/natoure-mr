/* eslint-disable jsx-a11y/interactive-supports-focus */
import styled from 'styled-components'
import React from 'react'

import HorizontalS from '../../styles/general/HorizontalSlide'

export interface ExperieceElement {
  tag?: string[]
  width?: string
  onClick?: (tab: string) => void
  secondary?: boolean
  margin?: string
  activeTag?: string
}

interface StyledElement {
  width: string
  secondary: boolean
  margin: string
}

const Card = styled.section(
  ({ width = '60%', margin = '0', secondary = false }: StyledElement) => `
    margin: ${margin};
    width: ${width};
    span {
      width: max-content;
      padding: ${secondary ? '6px 12px' : '12px 20px'};
      border-radius: 16px;
      color: var(--dark-blue);
      font-family: 'Montserrat', sans-serif;
      font-size: ${secondary ? '12px' : '14px'};
      background-color: #f5f8fc;
      cursor: pointer;
      height: 18px;
      text-transform: ${secondary ? 'lowercase' : ''};
      font-weight: ${secondary ? '500' : '600'};
      border: ${secondary ? '1px solid var(--green-natoure)' : 'none'}
    }
    .active{
      background-color: #dadfe6;
    }
  `
)

const TagCard = (props: ExperieceElement): JSX.Element => {
  return (
    <Card margin={props.margin} secondary={props.secondary} width={props.width}>
      <HorizontalS>
        {props.tag.map((tag, index) => (
          // eslint-disable-next-line jsx-a11y/click-events-have-key-events
          <span
            className={props.activeTag === tag ? 'active' : ''}
            role="button"
            key={index}
            onClick={() => props.onClick && props.onClick(tag)}
          >
            {tag}
          </span>
        ))}
      </HorizontalS>
    </Card>
  )
}
export default TagCard
