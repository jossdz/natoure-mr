import styled from 'styled-components'
import React from 'react'
import { AiOutlineEnvironment } from 'react-icons/ai'
import { BsChevronLeft } from 'react-icons/bs'

export interface ExperieceElement {
  title: string
  image: string
  place?: string
  back: string
}

const Card = styled.section(
  ({ image }: { image: string }) => `
  width: 100vw;
  height: 60vh;
  background: linear-gradient(to bottom, rgba(35, 35, 35, 0.32), rgba(35, 35, 35, 0.32)), url(${image});
  background-size: cover;
  background-repeat: no-repeat;
  background-position:center;
  opacity: 0.9;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 32px;
  box-sizing: border-box;
  color: white;
  a {
    svg {
      width: 24px;
      height: 24px;
      stroke-width: 1px;
      color: white;
    }
  }
  div {
    .title {
      font-family: Montserrat;
      font-size: 18px;
      font-weight: bold;
      letter-spacing: -0.04px;
    }
    .place {
      display: flex;
      align-items: center;
      font-family: Montserrat;
      font-size: 13px;
      font-weight: bold;
      line-height: 1.5;
      letter-spacing: -0.03px;
      svg {
        width: 32px;
        height: 32px;
        margin-right: 8px;
      }
    }
  }
`
)

export const Heading = (props: ExperieceElement): JSX.Element => {
  return (
    <Card image={props.image}>
      <a href={props.back}>
        <BsChevronLeft />
      </a>

      <div>
        <p className="title">{props.title}</p>
        {props.place && (
          <p className="place">
            <AiOutlineEnvironment />
            <span>{props.place}</span>
          </p>
        )}
      </div>
    </Card>
  )
}

export default Heading
