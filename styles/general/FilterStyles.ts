import styled from 'styled-components'

export default styled.div`
  margin-bottom: 15px;
  width: 100%;
  height: 44px;
  border-radius: 27px;
  display: flex;
  overflow: hidden;
  background-color: transparent;
  color: white;
  box-shadow: rgba(0, 0, 0, 0.1) 0px 5px 15px;
  align-items: center;
  input {
    flex-grow: 1;
    border: var(--dark-blue);
    color: var(--dark-blue);
    text-align: left;
    background-color: rgba(255, 255, 255, 0.13);
    font-family: 'Montserrat', sans-serif;
  }
  input::placeholder {
    color: var(--dark-blue);
    font-size: 12px;
    text-align: left;
    font-family: 'Montserrat', sans-serif;
  }
  button {
    width: 48px;
    border: none;
    font-size: 20px;
    color: var(--dark-blue);
    background-color: transparent;
  }
  @media (min-width: 1024px) {
    margin-bottom: 0;
  }
`
