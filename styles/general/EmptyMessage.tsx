import styled from 'styled-components'

export default styled.p`
  font-size: 1.4em;
  font-weight: bold;
  color: var(--dark-blue);
  margin-bottom: 64px;
`
