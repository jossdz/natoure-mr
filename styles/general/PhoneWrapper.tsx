import styled from 'styled-components'

export default styled.div`
  margin: 8px 0 8px 8px;
  width: 100%;
  input {
    border-top: none;
    border-left: none;
    border-right: none;
    border-bottom: 1px solid rgba(0, 0, 0, 0.42);
    height: 32px;
    margin-left: 8px;
    font-size: 16px;
    width: 100%;
    ::placeholder,
    ::-webkit-input-placeholder {
      color: #a2a2a2;
    }
  }
`

export const PhoneStepper = styled.div`
  margin-bottom: 24px;
  padding-left: 4px;
  width: 100%;
  box-sizing: border-box;
  input {
    margin-left: 8px;
    font-family: 'Montserrat', sans-serif;
    width: 100%;
    max-width: calc(85vw - 38px);
    height: 18px;
    padding: 12px 18px;
    border-radius: 8px;
    border: solid 1px var(--grey);
    background-color: var(--white);
    display: block;
    -webkit-appearance: none;
    -moz-appearance: none;
    -ms-appearance: none;
    ::placeholder,
    ::-webkit-input-placeholder {
      color: var(--dark-grey);
      font-family: 'Montserrat', sans-serif;
    }
    @media (min-width: 1024px) {
      width: calc(100% - 38px);
    }
  }
`
