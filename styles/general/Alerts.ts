import styled from 'styled-components'

export const SuccessAlert = styled.div`
  padding: 8px 16px;
  color: #00ae42;
  background-color: rgba(0, 174, 66, 0.4);
  border-radius: 4px;
`

export const ErrorAlert = styled.div`
  padding: 8px 16px;
  color: #f75f5f;
  background-color: rgba(247, 95, 95, 0.4);
  border-radius: 4px;
`
