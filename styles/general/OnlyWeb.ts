import styled from 'styled-components'

export default styled.div`
  @media (max-width: 1023px) {
    display: none;
  }
`
