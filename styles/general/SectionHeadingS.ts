import styled from 'styled-components'

type SectionHeadingStyledProps = {
  removeMarginTop?: boolean
  removeMarginBottom?: boolean
}

export default styled.div<SectionHeadingStyledProps>(
  ({ removeMarginTop, removeMarginBottom }) => `
  ${removeMarginTop ? '' : 'margin-top: 36px;'}
  ${removeMarginBottom ? '' : 'margin-bottom: 32px;'}
  width: 100%;
  height: 32px;
  display: flex;
  font-size: 15px;
  align-items: center;
  justify-content: space-between;
  margin-left: auto;
  margin-right: auto;
  max-width: 88vw;
  padding-left: 10px;
  color: var(--dark-blue);
  @media (max-width: 425px) {
    width: calc(100% - 10px);
  }
  &::before {
    content: '';
    margin-left: -10px;
    width: 4px;
    height: 17px;
    background-color: var(--green-natoure);
    position: absolute;
    border-radius: 3px;
  }
  p {
    margin: 0;
    font-weight: bold;
  }
  a {
    color: var(--light-green);
  }
`
)
