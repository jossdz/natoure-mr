import styled from 'styled-components'

export default styled.div<{ marginLeft?: string; shadow?: boolean; paddingLeft?: boolean }>`
  width: 100%;
  min-height: 24px;
  display: flex;
  overflow-x: auto;
  overflow-y: hidden;
  position: relative;
  font-size: 14px;
  box-sizing: border-box;
  padding-left: ${(props) => (props.paddingLeft ? '7.5vw' : '0')};
  padding-bottom: ${(props) => (props.shadow ? '24px' : null)};
  box-shadow: ${(props) => (props.shadow ? '-2px 4px 12px 0 rgba(0, 0, 0, 0.08)' : null)};
  ::-webkit-scrollbar {
    display: none;
  }
  & > * {
    flex-shrink: 0;
    margin: 0 ${(props) => (props.marginLeft ? props.marginLeft : '16px')} 0 0;
  }
  @media (min-width: 769px) {
    font-size: 14px;
  }
`
