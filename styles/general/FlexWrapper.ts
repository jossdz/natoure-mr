import styled from 'styled-components'

type FlexWrapperProps = {
  justify?: string
  align?: string
  fluid?: boolean
  direction?: string
  wrap?: string
}

type IconWrapperProps = {
  marginLeft?: string
  marginRight?: string
  size?: string
  svgColor?: string
}

export const FlexWrapper = styled.div<FlexWrapperProps>(
  ({ justify = 'space-between', align = 'center', direction, fluid = false, wrap = 'no-wrap' }) => `
  display: flex;
  align-items: ${align};
  flex-wrap: ${wrap};
  justify-content: ${justify};
  ${direction ? `flex-direction: ${direction}` : ''};
  ${fluid ? 'width:100%' : ''};
  `
)

export const IconWrapper = styled.div<IconWrapperProps>(
  ({ marginLeft, marginRight, size, svgColor }) => `
    display: flex;
    align-items: center;
    background:none;
    border:none;
    ${svgColor ? `svg{color: ${svgColor}};` : ''}
    ${
      size
        ? `
        svg{
        height:${size};
        width:${size};
    }`
        : ''
    }
  ${marginLeft ? `margin-left: ${marginLeft};` : ''}
  ${marginRight ? `margin-right: ${marginRight};` : ''}
  `
)
