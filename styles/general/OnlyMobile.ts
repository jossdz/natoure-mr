import styled from 'styled-components'

export default styled.div`
  @media (min-width: 1024px) {
    display: none;
  }
`
