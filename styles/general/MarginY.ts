import styled from 'styled-components'

export default styled.div`
  width: 100%;
  justify-content: center;
  margin-bottom: 120px;
  @media (min-width: 1024px) {
    margin-top: 120px;
    margin-bottom: 0;
  }
`
