import styled from 'styled-components'
import React, { useEffect, useState } from 'react'
import { FaPen, FaPlusCircle } from 'react-icons/fa'
import Link from 'next/link'

export interface ExperieceElement {
  description: string
  person: string
  image: string
  sustain: number
}

const Card = styled.section`
  * {
    margin: 0;
    padding: 0;
    font-family: 'Montserrat', sans-serif;
  }
  width: 100%;
  height: auto;
  display: flex;
  flex-direction: row;
  margin-top: 10px;
  img {
    width: 116px;
    height: 116px;
    border: solid 2px black;
    border-radius: 50%;
  }
  b {
    margin-right: 10px;
  }
  .titleSize {
    font-size: 15px;
    color: #2c375a;
  }
  .personLetter {
    color: var(--green-natoure);
  }
  .desBox {
    width: 85%;
    color: #2c375a;
  }
  .firstDiv {
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    margin-bottom: 15px;
  }
  .link {
    font-size: 12px;
    text-decoration: underline;
    color: #2c375a;
  }
  .fields {
    width: 70%;
  }
  .sustain {
    font-size: 12px;
  }
  .fieldOne {
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }
  .fieldName {
    font-size: 10px;
    color: gray;
  }
  .fieldDes {
    font-size: 14px;
    color: #2c375a;
  }
`

export const ProfileCard = (props: ExperieceElement): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [userInfo, setUserInfo] = useState<any>()

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem('NatoureUser'))
    setUserInfo(user)
  }, [])
  return (
    <Card>
      <div className="firstDiv" style={{ marginTop: '32px' }}>
        <p>
          <img src={userInfo?.image || '/assets/icon-blue.png'} alt=""></img>
        </p>

        <br />
        <b>
          <p>
            {userInfo?.name} {userInfo?.last_name}
          </p>
        </b>
        <br />
        <br />
        <br />

        <div className="fields">
          <p className="fieldName">Pais</p>
          <div className="fieldOne">
            <div>
              <p className="fieldDes">Mexico</p>
            </div>
          </div>
        </div>
        <br />
        <hr className="divisor"></hr>
        <br />

        <div className="fields">
          <p className="fieldName">Correo</p>
          <div className="fieldOne">
            <div>
              <p className="fieldDes">{userInfo?.email}</p>
            </div>
          </div>
        </div>
        <br />
        <hr className="divisor"></hr>
        <br />
      </div>
    </Card>
  )
}

export default ProfileCard
