import styled from 'styled-components'

export const StatsGraphSection = styled.div`
  margin-top: 60px;
`

export const GraphContainer = styled.div`
  width: 100%;
  max-width: 100%;
  box-sizing: border-box;
  padding: 14px;
  border-radius: 4px;
  box-shadow: -1px 2px 9px 0 rgba(0, 0, 0, 0.07);
  background-color: #ffffff;
  :not(:last-of-type) {
    margin-bottom: 60px;
  }
`
