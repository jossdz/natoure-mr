import styled from 'styled-components'

type RatingTabsItemProps = { isActive?: boolean }

export const RatingContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  margin-bottom: 16px;
  svg {
    fill: #ffeb3b;
  }
`

export const Score = styled.div(
  ({ color }) => `
font-family: Roboto;
font-size: 20px;
font-weight: 500;
font-stretch: normal;
font-style: normal;
line-height: normal;
letter-spacing: 0.8px;
color: ${color ? color : '#2c375a'};
`
)

export const SostenibilityContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 20px;
  border-radius: 4px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.16);
  background-color: #ffffff;
  svg {
    fill: #06bc68;
  }
  div:first-of-type {
    display: flex;
    justify-content: center;
    flex-direction: column;
    div:first-of-type {
      font-family: Roboto;
      font-size: 14px;
      font-weight: normal;
      font-stretch: normal;
      font-style: normal;
      line-height: normal;
      letter-spacing: 0.56px;
      color: #1c1c1c;
    }
    div:nth-of-type(2) {
      font-family: Roboto;
      font-size: 12px;
      font-weight: normal;
      font-stretch: normal;
      font-style: normal;
      line-height: normal;
      letter-spacing: 0.48px;
      color: #06bc68;
    }
  }
`

export const SostenibilityRatingContainer = styled.div`
  display: flex;
  align-items: center;
  ${Score} {
    margin-left: 8px;
  }
  svg,
  span {
    height: 20px;
  }
`

export const RatingTabsSection = styled.div`
  display: flex;
  flex-direction: column;
`

export const RatingTabsSectionFooter = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
`

export const RatingTabsSectionScore = styled.div`
  padding: 15px;
  border-radius: 4px;
  background-color: #06bc68;
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-size: 16px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: 0.64px;
  color: #ffffff;
  ${Score} {
    margin-left: 24px;
    display: flex;
    align-items: center;
    svg {
      margin-right: 8px;
    }
  }
`

export const RatingTabs = styled.ul`
  display: flex;
  justify-content: center;
  align-items: flex-end;
  list-style: none;
  width: 100%;
  max-width: 100%;
  margin: 40px auto;
  padding: 0;
  height: 40px;
`

export const RatingTabsItem = styled.li<RatingTabsItemProps>(
  ({ isActive = false }) => `
padding: ${isActive ? '10px 20px' : '6px 20px'};
border-top-left-radius: 8px;
border-top-right-radius: 8px;
background-color: ${isActive ? '#06bc68' : '#80ebba'};
color: white;
transition:all .2s;
`
)

export const RatingTabsContent = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 40px;
`

export const RatingCriteriaItem = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  :not(:last-of-type) {
    margin-bottom: 40px;
  }
  svg {
    fill: #06bc68;
  }
`

export const ContentItemText = styled.div`
  font-family: Roboto;
  font-size: 16px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: 0.64px;
  color: #1c1c1c;
`
