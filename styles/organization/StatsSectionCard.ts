import styled from 'styled-components'

type CardTextProps = { fontSize: string; lineHeight: string; isBold?: boolean; color?: string }

export const CardStyled = styled.div`
  display: flex;
  flex-direction: column;
  border-radius: 8px;
  box-shadow: 0 3px 12px 0 rgba(0, 0, 0, 0.07);
  border: solid 2px #42ce7d;
  box-sizing: border-box;
  margin: 30px auto;
  overflow: hidden;
  width: 100%;
`

export const CardHeaderStyled = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
  padding: 20px 8px;
  width: 100%;
  margin-bottom: 36px;
  box-shadow: 0 3px 12px 0 rgba(0, 0, 0, 0.07);
  background-image: linear-gradient(201deg, #82e293 10%, #42ce7d 26%);
  box-sizing: border-box;
  color: white;
`

export const CardText = styled.div<CardTextProps>(
  ({ fontSize, lineHeight, color, isBold }) => `
font-family: Montserrat;
font-size: ${fontSize};
font-weight: ${isBold ? 'bold' : 'normal'};
font-stretch: normal;
font-style: normal;
line-height: ${lineHeight};
letter-spacing: -0.03px;
overflow: hidden;
white-space: nowrap;
text-overflow: ellipsis;
color:${color ? color : '#ffffff'};
`
)

export const CardBodyStyled = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0 8px 30px;
`

export const CardReservationItem = styled.div`
  display: flex;
  width: 100%;
  box-sizing: border-box;
  justify-content: space-between;
  align-items: center;
  img {
    width: 46px;
    height: 46px;
    border-radius: 50%;
  }
  :not(:last-of-type) {
    margin-bottom: 25px;
  }
`

export const CardReservationInfo = styled.div`
  margin: 0 8px;
  box-sizing: border-box;
  max-width: 45%;
`
