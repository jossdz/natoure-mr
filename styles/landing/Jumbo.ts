import styled from 'styled-components'

export default styled.div`
  height: calc(100vh - 357px);
  min-height: 457px;
  color: white;
  display: flex;
  flex-direction: column;
  justify-content: center;
  @media (min-width: 1024px) {
    height: calc(100vh - 120px);
  }
  &::before {
    content: '';
    width: 100%;
    height: calc(100vh - 280px);
    min-height: 532px;
    position: absolute;
    background: linear-gradient(to bottom, rgba(35, 35, 35, 0.32), rgba(35, 35, 35, 0.32)),
      url('/assets/main-hero.jpeg');
    background-size: cover;
    background-position: center;
    top: 0;
    right: 0;
    @media (min-width: 1024px) {
      height: 100vh;
    }
  }
  & > * {
    z-index: 2;
  }
  .main-text {
    text-align: center;
    margin-bottom: 54px;
    @media (min-width: 1024px) {
      display: none;
    }
    strong {
      font-size: 24px;
      font-weight: bold;
      text-decoration: underline;
    }
    p {
      margin: 0;
      font-size: 14px;
      font-weight: 100;
    }
  }
  .web-text {
    color: white;
    width: 480px;
    @media (max-width: 1023px) {
      display: none;
    }
    strong {
      padding-left: 16px;
      border-left: 6px solid var(--green-natoure);
      font-size: 38px;
      font-weight: bold;
      margin-bottom: 24px;
      display: block;
    }
    p {
      padding-left: 16px;
      margin: 24px 0 86px 0;
      font-size: 14px;
      font-weight: 100;
    }
  }
`
