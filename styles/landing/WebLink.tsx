import React from 'react'
import Link from 'next/link'
import styled from 'styled-components'
import { FaArrowRight } from 'react-icons/fa'

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-family: Montserrat;
  margin-bottom: 24px;
  p {
    font-size: 14px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.29;
    letter-spacing: -0.73px;
    color: var(--dark-blue);
    strong {
      font-weight: 600;
    }
  }
  span {
    font-size: 16px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.94;
    letter-spacing: -0.03px;
    text-align: right;
    color: var(--green-natoure);
    display: flex;
    align-items: center;
    text-decoration: underline;
    svg {
      width: 28px;
      height: 26px;
      margin-left: 8px;
    }
  }
`

type Props = {
  strong: string
  text: string
  href: string
}

const WebLink = ({ strong, text, href }: Props): JSX.Element => {
  return (
    <Wrapper>
      <p>
        <strong>{strong}</strong> {text}
      </p>
      <Link href={href}>
        <span>
          Ver todas <FaArrowRight />
        </span>
      </Link>
    </Wrapper>
  )
}

export default WebLink

WebLink.defaultProps = {
  strong: 'Vive experiencias únicas',
  text:
    'de la mano de los mejores anfitriones, mientra ayudas a la conservación de áreas naturales',
  href: '/explora',
}
