import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  img {
    margin-right: 37px;
    margin-bottom: 48px;
    width: 128px;
    height: 100%;
  }
`

const Logos = ({ images }: { images: string[] }): JSX.Element => {
  return (
    <Wrapper>
      {images.map((imagen, index) => (
        <img src={imagen} alt="logo" key={index} />
      ))}
    </Wrapper>
  )
}

export default Logos
