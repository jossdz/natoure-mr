import React from 'react'
import styled from 'styled-components'
import { FaArrowRight, FaSpotify } from 'react-icons/fa'

const data = [
  {
    img: '/assets/spoti1.png',
    link:
      'https://open.spotify.com/playlist/76jViDLo3Xa62RivcXrRfm?si=99cxgU3_SjCbuUjZ715PSg&dl_branch=1&nd=1',
  },
  {
    img: '/assets/spoti2.png',
    link:
      'https://open.spotify.com/playlist/3YFSjJMDdV2NeCpM4Bsazr?si=Fiyu9Vj2RJS0JXJEICQdhw&dl_branch=1&nd=1',
  },
  {
    img: '/assets/spoti3.png',
    link:
      'https://open.spotify.com/playlist/3l3m27yXXNVGHL3iRPpy1f?si=YPGkm0s7TpOFX3IZQAyZxA&dl_branch=1&nd=1',
  },
  {
    img: '/assets/spoti4.png',
    link:
      'https://open.spotify.com/playlist/5YqNTvVdvPekro56uTRFF5?si=N-qsp9rXTLmaI6cpP6AU-w&dl_branch=1&nd=1',
  },
  {
    img: '/assets/spoti5.png',
    link:
      'https://open.spotify.com/playlist/37i9dQZF1DWUKPeBypcpcP?si=7K1nVlFQQA2kG2dprDAFnA&dl_branch=1&nd=1',
  },
]

const Wrapper = styled.div`
  margin: 0 auto 108px auto;
  > div {
    display: flex;
    justify-content: space-between;
    width: 100%;
    > div {
      width: 17.5%;
      img {
        width: 100%;
      }
      a {
        font-size: 14px;
        font-weight: bold;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.94;
        letter-spacing: -0.03px;
        text-align: right;
        color: var(--green-natoure);
        display: flex;
        align-items: center;
        justify-content: center;
        text-decoration: none;
        svg {
          width: 20px;
          height: 16px;
          margin-left: 4px;
        }
      }
    }
  }
  > svg {
    width: 50px;
    height: 50px;
    margin: 32px auto 12px auto;
    display: block;
    color: var(--green-natoure);
  }
  > p {
    margin: 0 auto;
    text-align: center;
    color: var(--dark-blue);
    font-size: 14px;
    font-weight: 800;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.29;
    letter-spacing: -0.73px;
  }
`

const EcoRate = (): JSX.Element => {
  return (
    <Wrapper>
      <div>
        {data.map(({ img, link }, index) => (
          <div key={index}>
            <img src={img} alt="Spotify" />
            <a href={link}>
              Escuchar <FaArrowRight />
            </a>
          </div>
        ))}
      </div>
      <FaSpotify />
      <p>¡Escúchalas en Spotify!</p>
    </Wrapper>
  )
}

export default EcoRate
