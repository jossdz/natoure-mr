import styled from 'styled-components'

export default styled.div`
  margin-top: 8px;
  width: 100%;
  display: flex;
  align-items: flex-end;
  flex-direction: column;
  img {
    width: 36px;
    height: 36px;
    border-radius: 50%;
    object-fit: cover;
  }
  p {
    margin-top: 5px;
    font-size: 7.8px;
    margin-right: 5px;
  }
`
