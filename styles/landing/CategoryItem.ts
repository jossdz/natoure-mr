import styled from 'styled-components'

type props = {
  color: string
  image: string
}
export default styled.article(
  ({ color, image }: props) => `
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 68px;
  margin-bottom: 16px;
  @media (min-width: 1024px) {
    background: linear-gradient(to bottom, rgba(35, 35, 35, 0.32), rgba(35, 35, 35, 0.32)),
    url(${image});
    background-size: cover;
    color: white;
    width: calc(14% - 16px);
    height: 128px;
    border-radius: 20px;
    text-transform: uppercase;
    font-weight: 600;
    line-height: 1.94;
    letter-spacing: -0.03px;
  }
    svg {
      width: 32px;
      height: 32px;
      border-radius: 50%;
      margin-bottom: 7px;
      padding: 10px;
      color: white;
      @media (max-width: 1023px) {
        background-color: ${color};
      }
    }
    p {
      font-size: 12px;
      margin: 0;
      text-align: center;
      line-height: 1;
      height: 12px;
      overflow-y: hidden;
    }
`
)
