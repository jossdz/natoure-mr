import styled from 'styled-components'

type props = { onlyWeb?: boolean; withLink?: boolean }

export default styled.div(
  ({ onlyWeb, withLink }: props) => `
  font-family: Montserrat;
  font-size: 28px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: -1.46px;
  color: var(--dark-blue);
  padding-left: 16px;
  border-left: 6px solid var(--green-natoure);
  text-transform: uppercase;
  margin-bottom: ${withLink ? '0px' : '24px'};
  @media (max-width: 1023px) {
    display:${onlyWeb ? 'none' : 'block'} ;
  }
`
)
