import styled from 'styled-components'

export default styled.section`
  width: 100%;
  display: flex;
  justify-content: space-evenly;
  flex-wrap: wrap;
  @media (max-width: 425px) {
    justify-content: center;
  }
  article {
    width: 104px;
    height: 168px;
    position: relative;
    margin-right: 12px;
    margin-bottom: 24px;
    img {
      width: 100%;
      height: 100%;
      object-fit: cover;
      border-radius: 8px;
    }
    p {
      top: 0;
      margin: 0;
      position: absolute;
      width: 100%;
      height: 100%;
      color: white;
      font-size: 14px;
      text-align: center;
      display: flex;
      align-items: center;
      flex-direction: column;
      justify-content: center;
    }
    &::before {
      content: '';
      width: 100%;
      height: 100%;
      position: absolute;
      background-color: var(--black);
      opacity: 0.65;
      top: 0;
      right: 0;
      border-radius: 8px;
    }
    & > * {
      z-index: 2;
    }
  }
`
