import React from 'react'
import styled from 'styled-components'
import { RiLeafFill } from 'react-icons/ri'

const Wrapper = styled.div`
  width: 96%;
  margin: 48px auto 108px auto;
  padding: 96px;
  box-sizing: border-box;
  background: rgba(234, 251, 246, 0.51);
  color: var(--dark-blue);
  text-align: center;
  border-radius: 13px;
  div {
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 46%;
    margin: 0 auto;
  }
  svg {
    width: 64px;
    height: 64px;
    margin: 16px 0 24px 8px;
    color: var(--green-natoure);
  }
  strong {
    font-size: 18px;
    font-weight: bold;
    line-height: 2;
    letter-spacing: -0.94px;
  }
  p {
    font-size: 14px;
    line-height: 1.29;
    letter-spacing: -0.73px;
  }
  span {
    font-size: 16px;
    font-weight: bold;
    letter-spacing: -0.83px;
  }
`

const EcoRate = (): JSX.Element => {
  return (
    <Wrapper>
      <div>
        <strong>¿Qué significan estas hojas verdes?</strong>
        <RiLeafFill />
        <span>EcoRate® es el primer indicador de sostenibilidad turística en el mundo.</span>
        <p>
          Miden qué tan sostenible es la actividad de cada comunidad anfitriona y al mismo tiempo te
          aseguran la mejor experiencia turística posible en un entorno conservado donde encontrarás
          lugares llenos de armonía.
        </p>
      </div>
    </Wrapper>
  )
}

export default EcoRate
