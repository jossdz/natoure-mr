import styled from 'styled-components'

export default styled.div`
  color: var(--dark-blue);
  padding: 48px 0;
  @media (min-width: 1024px) {
    padding: 108px 0;
  }
  > p {
    font-size: 15px;
    font-weight: bold;
    text-align: center;
    margin: 24px 0 0 0;
    @media (min-width: 1024px) {
      display: none;
    }
  }
  section {
    align-items: flex-start;
    display: flex;
    justify-content: space-evenly;
    flex-wrap: wrap;
    @media (min-width: 1024px) {
      justify-content: space-between;
    }
  }
`
