import React from 'react'
import styled from 'styled-components'
import { BsChevronRight } from 'react-icons/bs'

import MegaLabel from './MegaLabel'

const Wrapper = styled.div(
  ({ reverse }: { reverse: boolean }) => `
  display:flex;
  flex-direction: ${reverse ? 'row-reverse' : 'row'};
  width: 100%;
  margin-bottom: 108px;
  margin-top: ${reverse ? '64px' : '0px'};
  img {
    width: 50%;
    border-radius: 46px;
    height: 100%;
  }
  > div {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: center;
    padding-left: ${reverse ? '0' : '64px'};
    padding-right: ${reverse ? '64px' : '0'};
    color: var(--dark-blue);
    > p {
      padding-left: 16px;
      font-size: 14px;
      line-height: 1.29;
      letter-spacing: -0.73px;
      margin: 0 0 48px 0;
    }
    > div.buttons {
      display:flex;
      margin-left:16px;
      flex-direction: column;
      width:100%;
      padding-top: 48px;
      border-top: 1px solid rgba(44, 55, 90, 0.36);
      > span {
        font-size: 14px;
        font-weight: 500;
        line-height: 1.29;
        letter-spacing: -0.73px;
      }
      > a {
        font-size: 14px;
        font-weight: 800;
        line-height: 1.29;
        letter-spacing: -0.73px;
        text-decoration: none;
        color: var(--dark-blue);
        display:flex;
        align-items: center;
        margin-top: 8px;
        font-family: 'Montserrat', sans-serif;
        svg {
          width: 24px;
          height: 24px;
          stroke-width: 1px;
          margin-left:8px;
        }
      }
    }
  }
`
)

type props = {
  image: string
  title: string
  description: string
  linkText: string
  reverse?: boolean
  href: string
}

const DoubleCard = ({
  image,
  title,
  description,
  linkText,
  reverse = false,
  href,
}: props): JSX.Element => {
  return (
    <Wrapper reverse={reverse}>
      <img src={image} alt={title} />
      <div>
        <MegaLabel>{title}</MegaLabel>
        <p>{description}</p>
        <div className="buttons">
          <span>¿Quieres conocer más?</span>
          <a href={href}>
            {linkText} <BsChevronRight />
          </a>
        </div>
      </div>
    </Wrapper>
  )
}

export default DoubleCard
