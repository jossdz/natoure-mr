import styled from 'styled-components'

type CardStyledProps = {
  imageUrl?: string
  width?: string
  widthMobile?: string
}

type FlexWrapperProps = {
  justify?: string
  align?: string
  direction?: 'column' | 'row'
  fluid?: boolean
}

type FlagWrapperStyles = {
  isActive?: boolean
}

export const MapPageWrapper = styled.div(
  ({ isToggled }: { isToggled?: boolean }) => `
  position: relative;
  width: 100vw;
  height: ${isToggled ? 'calc(100vh - 211px)' : '100vh'};
  .mapboxgl-popup-content {
    .mapboxgl-popup-close-button {
      z-index: 999;
    }
    width: 300px;
    .popup-container {
      display: flex;
      align-items: center;
      justify-content: space-between;
      width: 100%;
      .popup-img {
        width: 40%;
        height: 100%;
        margin-right: 8px;
      }
      .popup-info {
        display: flex;
        flex-direction: column;
        .popup-title {
          font-weight: bold;
          font-size: 16px;
          margin-bottom: 8px;
        }
        .popup-link {
          color: var(--light-green);
          outline: none;
        }
      }
    }
  }
`
)

export const MapContainer = styled.div`
  width: 100%;
  height: 100%;
`

export const MapFiltersStyled = styled.div`
  position: absolute;
  z-index: 99;
  height: 100px;
  width: 100%;
  top: 0;
`

export const PlacesWrapper = styled.div`
  margin-top: 30px;
  padding: 0 23px;
  box-sizing: border-box;
  width: 100%;
  > div {
    display: flex;
    margin-bottom: 12px;
    align-items: center;
    width: 100%;
    > section {
      width: calc(100% - 48px);
    }
    svg {
      width: 32px;
      height: 32px;
      margin-right: 16px;
      cursor: pointer;
    }
  }
  > section {
    width: calc(100% - 48px);
    margin-left: 48px;
  }
`

export const ChipsWrapper = styled.div`
  display: -webkit-box;
  overflow-x: scroll;

  ::-webkit-scrollbar {
    display: none;
  }
`

export const FilterChip = styled.button`
  width: auto;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  height: 32px;
  margin-top: 33px;
  margin-left: 23px;
  padding: 14px;
  border-radius: 16px;
  border: none;
  box-shadow: -2px 4px 12px 0 rgba(0, 0, 0, 0.1);
  background-color: #ffffff;
  font-family: Montserrat;
  font-size: 14px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 2.21;
  letter-spacing: -0.03px;
  color: #2c375a;
  span {
    display: flex;
    height: 16px;
    margin-left: 8px;
    svg {
      width: 16px;
      height: 16px;
      transform: rotate(90deg);
    }
  }
`

export const CardWrapper = styled.div`
  width: 100%;
  position: absolute;
  bottom: 0;
  z-index: 99;
  margin-bottom: 41px;
  .swiper-slide {
    height: 272px;
    width: 192px;
  }
  .swiper-slide.swiper-slide-active,
  .swiper-slide.swiper-slide-active > div:first-of-type {
    height: 309px;
  }
  .swiper-wrapper {
    align-items: flex-end;
  }
`

export const CardOverlay = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: flex-end;
  padding: 0 12px 32px;
  background-image: linear-gradient(
    to bottom,
    rgba(0, 0, 0, 0),
    rgba(0, 0, 0, 0.65) 50%,
    rgba(0, 0, 0, 0.58),
    rgba(0, 0, 0, 0.39)
  );
`

export const Card = styled.div<CardStyledProps>`
  width: ${({ width = '192px' }) => width};
  height: 272px;
  overflow: hidden;
  border-radius: 10px;
  display: inline-flex;
  box-sizing: border-box;
  align-items: center;
  color: white;
  ${({ imageUrl }) =>
    imageUrl &&
    `
  background-image: url(${imageUrl});
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  `}
  p {
    font-family: Montserrat;
    font-size: 24px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.21;
    letter-spacing: -1.25px;
    color: #ffffff;
    margin-bottom: 8px;
    cursor: pointer;
    :hover {
      color: var(--green-natoure);
    }
  }
  @media (max-width: 767px) {
    width: ${({ widthMobile = '192px' }) => widthMobile};
    margin-bottom: ${({ widthMobile = '192px' }) => (widthMobile ? '16px' : '0')};
  }
`

export const FilterWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const FlexWrapper = styled.div<FlexWrapperProps>(
  ({ justify = 'space-between', align, direction = 'row', fluid }) => `
  display: flex;
  justify-content: ${justify};
  flex-direction: ${direction};
  ${fluid ? 'width:100%' : ''};
  ${align ? `align-items: ${align}` : ''};
  width: 100%;
  max-width: 100%;
  flex-wrap: wrap;
  :not(:last-of-type) {
    margin-bottom: 16px};
  }
`
)

export const InputWrapper = styled.div`
  display: flex;
  align-items: center;
`

export const FlagWrapper = styled.button<FlagWrapperStyles>(
  ({ isActive }) => `
display: flex;
  border-radius: 50%;
  width: ${isActive ? '55px' : '42px'};
  height: ${isActive ? '55px' : '42px'};
  overflow: hidden;
  align-items: center;
  opacity: ${isActive ? 1 : 0.6};
  justify-content: center;
  border: none;
  :hover,
  :focus {
    opacity: 1;
  }
  ${
    isActive
      ? `
    > img {
      width: 73px !important;
      height: 73px !important;
    }
  `
      : ''
  }
`
)
