import styled from 'styled-components'

type SaleLinkProps = { isActive: boolean }
type InRowWrapperProps = { justify?: string; marginBottom?: string }

export const InRowWrapper = styled.div<InRowWrapperProps>(
  ({ justify = 'space-between', marginBottom }) => `
display: flex;
justify-content: ${justify};
align-items: center;
${marginBottom ? `margin-bottom: ${marginBottom}` : ''}
`
)

export const FlagWrapper = styled.div`
  display: flex;
  border-radius: 50%;
  overflow: hidden;
  align-items: center;
  justify-content: center;
  border: none;
  width: 25px;
  height: 25px;
`

export const SalesNavWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 30px auto;
`

export const SalesLinkStyled = styled.a<SaleLinkProps>(
  ({ isActive }) => `
text-decoration: none;
font-family: Montserrat;
font-size: 15px;
font-weight: ${isActive ? 'bold' : '500'};
font-stretch: normal;
font-style: normal;
line-height: 1.2;
letter-spacing: -0.78px;
color: #2c375a;
`
)
