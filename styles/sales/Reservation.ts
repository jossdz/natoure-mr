import styled from 'styled-components'

type ElementWithImage = { image?: string; traveler?: boolean }
type ReservationExtraInfoProps = { isExpanded?: boolean; traveler?: boolean }
type ReservationTitleProps = { removeMargins?: boolean }
type IsTravelerProps = { traveler?: boolean }

export const ReservationWrapper = styled.div`
  width: 100;
  :not(:last-child) {
    padding-bottom: 32px;
    border-bottom: 1px solid lightgrey;
    margin-bottom: 32px;
  }
`

export const ReservationHeaderStyled = styled.div<IsTravelerProps>(
  ({ traveler }) => `
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-bottom: 25px;
    @media (min-width: 1024px) {
      flex-direction: ${traveler ? 'row' : 'row-reverse'};
      width: max-content;
      display: inline-flex;
      align-items: flex-start;
    }
  `
)

export const ReservationDateTimeWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  min-height: 100px;
`

export const ReservationDate = styled.div<IsTravelerProps>(
  ({ traveler }) => `
    display: flex;
    flex-direction: column;
    border-radius: 1px;
    box-shadow: -2px 4px 12px 0 rgba(0, 0, 0, 0.08);
    background-color: ${traveler ? 'white' : 'var(--dark-blue)'};
    padding: 8px;
    width: 82px;
    height: 64px;
    margin-right: 8px;
    margin-top: 12px;
    justify-content: center;
    span:first-of-type {
      font-family: Montserrat;
      font-size: 28px;
      font-weight: bold;
      font-stretch: normal;
      font-style: normal;
      letter-spacing: 1.81px;
      text-align: center;
      color: ${traveler ? 'var(--dark-blue)' : 'white'};
    }
    span:nth-of-type(2) {
      font-family: Montserrat;
      font-size: 12px;
      font-weight: bold;
      font-stretch: normal;
      font-style: normal;
      letter-spacing: 0.97px;
      text-align: center;
      color: ${traveler ? 'var(--dark-blue)' : 'white'};
    }
  `
)

export const ReservationTime = styled.div`
  font-family: Montserrat;
  font-size: 15px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.2;
  letter-spacing: 0.97px;
  color: #06bc68;
  text-align: center;
  padding-right: 8px;
  margin-top: 8px;
`

export const ReserveThumbnail = styled.div<ElementWithImage>(
  ({ image, traveler }) => `
  width: 70%;
  min-height: 100px;
  border-radius: 8px;
  overflow: hidden;
  background-image: url("${image}");
  background-size: cover;
  background-position: center;
  background-repear: no-repeat;
  display:flex;
  align-items: flex-end;
  padding: 11px;
  box-sizing: border-box;
  font-family: Montserrat;
  font-size: 14px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 2.21;
  letter-spacing: -0.03px;
  color: #ffffff;
  @media (min-width: 1024px) {
    width: 260px;
    height: 180px;
    margin-right: ${traveler ? '0' : '32px'};
    margin-left: ${traveler ? '56px' : '0'};
  }
`
)

export const ReservationBody = styled.div<IsTravelerProps>(
  ({ traveler }) => `
    width: 100%;
    @media (min-width: 1024px) {
      display: inline-block;
      margin-left: ${traveler ? '0' : '48px'};
      width: 64%;
    }
  `
)

export const ReservationTitle = styled.div<ReservationTitleProps>(
  ({ removeMargins }) => `
    font-family: Montserrat;
    font-size: 15px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.2;
    letter-spacing: -0.78px;
    color: #2c375a;
    margin-bottom: ${removeMargins ? 0 : '16px'};
  `
)

export const ReservationDetailsWrapper = styled.div<IsTravelerProps>(
  ({ traveler }) => `
    display: flex;
    align-items: center;
    justify-content: space-between;
    box-sizing: border-box;
    width: 100%;
    margin-bottom: 16px;
    @media (min-width: 1024px) {
      margin-bottom: ${!traveler ? '44px' : ''};
      flex-direction: ${traveler ? 'column' : ''};
      align-items: ${traveler ? 'flex-start' : ''};
    }
  `
)

export const ReservationDetails = styled.div<IsTravelerProps>(
  ({ traveler }) => `
  display: flex;
  align-items: center;
  justify-content: space-between;
  box-sizing: border-box;
  max-width: ${traveler ? '100%' : '33%'};
  span {
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
  }
  svg {
    margin-right: 8px;
    min-width: 18px;
    min-height: 18px;
  }
`
)
export const ReservationContactInfo = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 16px;
`

export const ReservationClient = styled.div`
  display: flex;
  align-items: center;
`

export const ReservationAvatar = styled.div<ElementWithImage>(
  ({ image }) => `
    width: 49px;
    height: 49px;
    border-radius: 50%;
    overflow: hidden;
    margin-right: 10px;
    box-sizing: border-box;
    ${
      !image
        ? `padding: 16px;
    background-color: #eeeeee;
    display: flex;
    align-items: center;
    justify-content: center;`
        : ''
    }
    ${
      image
        ? `background-image: url('${image}');
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;`
        : ''
    }
  `
)

export const ReservationClientName = styled.div`
  font-family: Montserrat;
  font-size: 13px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.38;
  letter-spacing: 0.84px;
  color: #2c375a;
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
`

export const ReservationAction = styled.div`
  padding: 8px 15px;
  border-radius: 8px;
  box-shadow: -2px 4px 12px 0 rgba(0, 0, 0, 0.08);
  background-color: #06bc68;
  font-family: Montserrat;
  font-size: 12px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.5;
  letter-spacing: -0.63px;
  color: #ffffff;
  display: flex;
  align-items: center;
  justify-content: center;
  svg {
    width: 24px;
    height: 24px;
    margin-left: 8px;
  }
  @media (min-width: 1024px) {
    width: 160px;
  }
`

export const ReservationExtraInfo = styled.div<ReservationExtraInfoProps>(
  ({ isExpanded, traveler }) => `
    height: ${isExpanded ? 'auto' : 0};
    overflow: hidden;
    transition: all 0.3s;
    margin-top: ${traveler ? '16px' : '0'};
  `
)

export const ReservationFooter = styled.button<IsTravelerProps>(
  ({ traveler }) => `
    text-decoration: underline;
    outline: none;
    background: none;
    border: none;
    font-family: Montserrat;
    font-size: 14px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.29;
    letter-spacing: -0.73px;
    color: #06bc68;
    text-align: ${traveler ? 'left' : 'center'};
    width: 100%;
  `
)

export const ReservationGuestWrapper = styled.div`
  width: 100%;
  margin-bottom: 40px;
`

export const ReservationGuest = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  box-sizing: border-box;
  margin-bottom: 8px;
`

export const ReservatioNumber = styled.div`
  font-family: Montserrat;
  font-size: 23px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.35;
  letter-spacing: -1.2px;
  color: #2c375a;
`

export const ReservationTransport = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin-bottom: 16px;
  svg {
    width: 32px;
    height: 32px;
    margin-right: 12px;
  }
  @media (min-width: 1024px) {
    width: 260px;
    margin-right: 32px;
  }
`

export const ReservationMeetingPoint = styled.div`
  margin-bottom: 24px;
  div {
    display: flex;
    svg {
      width: 32px;
      height: 32px;
      margin-right: 12px;
      overflow: visible;
    }
    @media (min-width: 1024px) {
      align-items: center;
    }
  }
`
export const ReservationFlexWrapper = styled.div<IsTravelerProps>(
  ({ traveler }) => `
    @media (min-width: 1024px) {
      width: 100%;
      display: flex;
      align-items: ${traveler ? 'center' : ''};
      justify-content: ${traveler ? 'space-between' : ''};
    }
  `
)

export const ReservationInfo = styled.div`
  @media (min-width: 1024px) {
    margin-left: 24px;
  }
`
