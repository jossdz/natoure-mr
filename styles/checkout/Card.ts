import styled from 'styled-components'

type CardThumbnail = { image: string }

export const CheckoutCardWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 24px;
`

export const CheckoutCardThumbnail = styled.div<CardThumbnail>(
  ({ image }) => `
    background-image: url("${image}");
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    height: auto;
    min-height: 420px;
    display: flex;
    justify-content: space-between;
    margin-bottom: 8px;
    padding: 8px 16px;
    border-radius: 8px;
`
)

export const CheckoutText = styled.div`
  font-family: Montserrat;
  font-size: 16px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.25;
  letter-spacing: -0.03px;
  color: var(--dark-blue);
`

export const CheckoutSubText = styled.div`
  font-family: Montserrat;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 2.21;
  letter-spacing: -0.03px;
  color: var(--dark-blue);
`

export const Dot = styled.div`
  width: 5px;
  height: 5px;
  background-color: var(--green-natoure);
  border-radius: 50%;
  margin: 0 8px;
`
