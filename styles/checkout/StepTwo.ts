import styled from 'styled-components'

type ResumeItemProps = {
  isTotal?: boolean
}

type PaymentMethodProps = {
  checked?: boolean
}

export const ResumeItem = styled.div<ResumeItemProps>(
  ({ isTotal = false }) => `
  font-family: Montserrat;
  font-size: ${isTotal ? '16px' : '13px'};
  font-weight: ${isTotal ? 'bold' : 'normal'};
  font-stretch: normal;
  font-style: normal;
  letter-spacing: -0.68px;
  color: #2c375a;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 16px;
  ${isTotal ? 'margin-top: 24px;' : ''}
`
)

export const PaymentMethodsWrapper = styled.div`
  display: flex;
  overflow: auto;
  position: relative;

  ::-webkit-scrollbar {
    display: none;
  }
`

export const PaymentMethod = styled.div<PaymentMethodProps>(
  ({ checked = false }) => `
min-width: 260px;
overflow: hidden;
box-sizing: border-box;
border-radius: 2px;
border: 1px solid #d5d2d2;
padding: 16px;
display: flex;
justify-content: center;
align-items: center;
flex-direction: column;
cursor: pointer;
${checked ? 'background-color: #eaeaea;' : ''}
svg {
  width: 40px;
  height: auto;
  margin-bottom: 24px;
  :not(:last-of-type) {
    margin-right: 16px;
  }
}
&:first-of-type:not(:only-of-type) {
  margin-right: 16px;
}
`
)

export const PaymentMethodProcess = styled.div`
  margin-top: 40px;
`

export const Error = styled.div`
  background-color: rgba(247, 95, 95, 0.3);
  color: #f75f5f;
  padding: 8px 16px;
  border-radius: 5px;
  :not(:only-of-type):not(:last-of-type) {
    margin-bottom: 16px;
  }
`
