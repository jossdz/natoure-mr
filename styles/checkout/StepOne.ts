import styled from 'styled-components'

type InfoTextProps = {
  marginTop?: string
  marginBottom?: string
}

export const Input = styled.input`
  padding: 8px 16px;
  border-radius: 8px;
  box-shadow: 0 3px 12px 0 rgba(0, 0, 0, 0.07);
  background-color: #ffffff;
  border: none;
  max-width: 5%;
  text-align: center;
  font-family: Montserrat;
  font-size: 15px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.33;
  letter-spacing: -0.78px;
  color: #2c375a;
  ::-webkit-outer-spin-button,
  ::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  ::placeholder {
    opacity: 0.4;
  }
`

export const CalendarFakeInputWrapper = styled.button`
  position: relative;
  width: 100%;
  margin: 0 auto;
  font-family: Montserrat;
  height: 44px;
  padding: 12px 18px;
  border-radius: 8px;
  border: solid 1px var(--grey);
  background-color: var(--white);
  text-align: left;
  color: var(--dark-blue);
  overflow-x: scroll;
  ::-webkit-scrollbar {
    display: none;
  }
`

export const DeparturePointWrapper = styled.label`
  display: flex;
  align-items: center;
  margin: 30px 0;
  div:nth-of-type(2) {
    padding: 0 20px;
  }
`

export const InfoText = styled.div<InfoTextProps>(
  ({ marginTop, marginBottom }) => `
  font-family: Montserrat;
  font-size: 15px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.47;
  letter-spacing: -0.78px;
  color: #2c375a;
  ${marginTop ? `margin-top: ${marginTop};` : ''}
  ${marginBottom ? `margin-bottom: ${marginBottom};` : ''}
`
)
