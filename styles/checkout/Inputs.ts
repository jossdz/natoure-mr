import styled from 'styled-components'

export const InputWrapper = styled.div`
  margin-bottom: 24px;
  label {
    font-family: Montserrat;
    font-size: 15px;
    font-weight: 500;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.2;
    letter-spacing: -0.78px;
    color: #2c375a;
  }
  :not(:last-of-type):not(:only-of-type) {
    margin-right: 8px;
  }
`

export const InputStyles = styled.div`
  box-sizing: border-box;
  border-radius: 4px;
  border: solid 1px #ced0da;
  background-color: #ffffff;
  padding: 8px;
  display: flex;
  margin-top: 16px;
  input {
    font-size: 16px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #2c375a;
    border: none;
    width: 100%;
    ::placeholder {
      color: #9a9fa8;
      opacity: 0.4;
    }
  }
  svg {
    width: 20px;
    height: auto;
  }
`
