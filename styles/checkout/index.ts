import styled from 'styled-components'

type FlexWrapperProps = {
  justify?: string
  align?: string
  fluid?: boolean
  direction?: string
  wrap?: string
}

type IconWrapperProps = {
  marginLeft?: string
  marginRight?: string
  size?: string
  svgColor?: string
}

export const CheckoutHeaderStyled = styled.div`
  position: relative;
  box-sizing: border-box;
  box-shadow: -2px 4px 12px 0 rgba(0, 0, 0, 0.08);
  background-color: #2c375a;
  padding: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-family: Montserrat;
  font-size: 18px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.33;
  letter-spacing: -0.04px;
  text-align: center;
  color: #ffffff;
`

export const FlexWrapper = styled.div<FlexWrapperProps>(
  ({ justify = 'space-between', align = 'center', direction, fluid = false, wrap = 'no-wrap' }) => `
  display: flex;
  align-items: ${align};
  flex-wrap: ${wrap};
  justify-content: ${justify};
  ${direction ? `flex-direction: ${direction}` : ''};
  ${fluid ? 'width:100%' : ''};
  `
)

export const CheckoutHeaderClose = styled.button`
  position: absolute;
  background: none;
  border: none;
  color: #ffffff;
  right: 0;
  cursor: pointer;
  svg {
    height: 50px;
    width: 50px;
  }
`

export const CheckoutBody = styled.div`
  padding-top: 30px;
`

export const CheckoutFooterStyled = styled.div`
  padding: 20px;
  box-sizing: border-box;
  background-color: #2c375a;
  display: flex;
  align-items: center;
  justify-content: center;
`

export const IconWrapper = styled.div<IconWrapperProps>(
  ({ marginLeft, marginRight, size, svgColor }) => `
    display: flex;
    align-items: center;
    background:none;
    border:none;
    ${svgColor ? `svg{color: ${svgColor}};` : ''}
    ${
      size
        ? `
        svg{
        height:${size};
        width:${size};
    }`
        : ''
    }
  ${marginLeft ? `margin-left: ${marginLeft};` : ''}
  ${marginRight ? `margin-right: ${marginRight};` : ''}
  `
)

export const Divider = styled.div`
  border-top: 2px solid rgba(44, 55, 90, 0.36);
`

export const CheckoutSection = styled.div`
  padding: 40px 0;
`

export const CheckoutSubtitle = styled.div`
  color: var(--dark-blue);
  size: 16px;
  line-height: 22px;
  letter-spacing: -0.78px;
  font-weight: bold;
`

export const TextHighlighted = styled.div`
  font-size: 14px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.29;
  letter-spacing: -0.73px;
  color: var(--green-natoure);
`

export const CheckoutButton = styled.button`
  padding: 16px;
  min-width: 122px;
  border-radius: 8px;
  background-color: #fa4775;
  display: flex;
  align-items: center;
  justify-content: space-around;
  box-sizing: border-box;
  font-family: Montserrat;
  font-size: 18px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.72;
  letter-spacing: -0.04px;
  color: #ffffff;
  border: none;
  span:first-of-type:not(:only-of-type) {
    margin-right: 16px;
  }
  :first-of-type:not(:only-of-type) {
    margin-right: 16px;
  }
`
