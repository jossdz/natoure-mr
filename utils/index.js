export const capitalize = ([first, ...rest]) => {
  return first.toUpperCase() + rest.join('').toLowerCase()
}

export const objectToQueryString = ({ params }) => {
  return `?${Object.keys(params)
    .filter((key) => params[key] !== '')
    .map((key) => key + '=' + params[key])
    .join('&')}`
}
