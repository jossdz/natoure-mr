import React, { ReactNode, useEffect } from 'react'

import {
  Overlay,
  DrawerContainer,
  DrawerHeaderStyled,
  DrawerHeaderActionsStyled,
  DrawerContentStyled,
  DrawerSectionStyled,
  DrawerFooterStyled,
} from './styles'

export type DrawerProps = {
  id?: string
  isOpen?: boolean
  children: ReactNode
  onClose: () => void
}

export type BaseProps = {
  id?: string
  children: ReactNode
}

const Drawer = ({ id, children, onClose, isOpen = false }: DrawerProps): JSX.Element => {
  useEffect(() => {
    const callback = (e: KeyboardEvent) => {
      if (e.key === 'Escape') {
        onClose()
      }
    }
    window.addEventListener('keydown', callback)
    return () => window.removeEventListener('keydown', callback)
  }, [])

  return (
    <React.Fragment>
      <Overlay data-testid="overlay" isOpen={isOpen} onClick={onClose} />
      <DrawerContainer isOpen={isOpen} role="dialog" aria-modal="true" data-testid="drawer" id={id}>
        {children}
      </DrawerContainer>
    </React.Fragment>
  )
}

export const DrawerHeader = ({ id, children }: BaseProps): JSX.Element => {
  return <DrawerHeaderStyled id={id}>{children}</DrawerHeaderStyled>
}

export const DrawerHeaderActions = ({ id, children }: BaseProps): JSX.Element => (
  <DrawerHeaderActionsStyled id={id}>{children}</DrawerHeaderActionsStyled>
)

export const DrawerSection = ({ id, children }: BaseProps): JSX.Element => (
  <DrawerSectionStyled id={id}>{children}</DrawerSectionStyled>
)

export const DrawerContent = ({ id, children }: BaseProps): JSX.Element => (
  <DrawerContentStyled id={id}>{children}</DrawerContentStyled>
)
export const DrawerFooter = ({ id, children }: BaseProps): JSX.Element => (
  <DrawerFooterStyled title="drawerFooter" id={id}>
    {children}
  </DrawerFooterStyled>
)

Drawer.Header = DrawerHeader
Drawer.HeaderActions = DrawerHeaderActions
Drawer.ContentSection = DrawerSection
Drawer.Content = DrawerContent
Drawer.Footer = DrawerFooter

export { Drawer }
