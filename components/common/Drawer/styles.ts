import styled from 'styled-components'

type styledProps = { isOpen: boolean }

export const Overlay = styled.div(
  ({ isOpen }: styledProps) => `
    top: 0;
    left: 0;
    height: 100vh;
    width: 100vw;
    background: rgba(0, 0, 0, 0.4);
    position: fixed;
    z-index: 999;
    transition: all 0.2s;
    visibility: ${isOpen ? 'visible' : 'hidden'};
    opacity: ${isOpen ? 1 : 0};
  `
)

export const DrawerContainer = styled.div<styledProps>(
  ({ isOpen }) => `
      top: 0;
      left: 0;
      border-top-right-radius: 20px;
      border-bottom-right-radius: 20px;
      box-sizing: border-box;
      max-width: 90%;
      width: 90%;
      background-color: white;
      position: fixed;
      z-index: 999;
      height: 100%;
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      transition: transform 0.3s, opacity 300ms, visibility 300ms;
      visibility: ${isOpen ? 'visible' : 'hidden'};
      opacity: ${isOpen ? 1 : 0};
      transform: ${isOpen ? 'translateX(0%)' : 'translateX(-100%)'};
      [title='drawerFooter'] {
        justify-content: center;
      }
    `
)

export const DrawerContentStyled = styled.div`
  flex-grow: 1;
  box-sizing: border-box;
  padding: 16px;
  overflow: auto;
`

export const DrawerHeaderStyled = styled.div`
  box-sizing: border-box;
  padding: 16px;
  height: 72px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  h6 {
    font-family: Montserrat;
    font-size: 18px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: 1;
    letter-spacing: -0.94px;
    color: #2c375a;
  }
`

export const DrawerHeaderActionsStyled = styled.div`
  display: flex;
  align-items: center;
  button {
    width: 20px;
    outline: none;
    background: none;
    border: none;
    margin: 0 16px;
    svg {
      width: 20px;
      height: 20px;
    }
  }
`

export const DrawerSectionStyled = styled.div`
  :not(:last-child) {
    margin-bottom: 38px;
  }
  p {
    font-family: Roboto;
    font-size: 16px;
    font-weight: 500;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: -0.41px;
    color: #06bc68;
    text-transform: uppercase;
    margin-bottom: 32px;
  }
`

export const DrawerFooterStyled = styled.div`
  padding: 16px;
  display: flex;
  align-items: center;
`
