import styled from 'styled-components'

export const ChipStyled = styled.div`
  width: max-content;
  font-size: 14px;
  padding: 10px 18px;
  border-radius: 22px;
  background-color: rgba(255, 255, 255, 0.8);
  cursor: pointer;
  :hover,
  :focus,
  &.active {
    background-color: var(--green-natoure);
    color: white;
  }
  :not(only-of-type),
  :not(first-of-type) {
    margin-left: 8px;
  }
`
