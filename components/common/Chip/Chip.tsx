import React from 'react'
import { ChipStyled } from './styles'

type ChipProps = { label: string; onClick?: () => void; isActive: boolean }

export const Chip = ({ label, onClick, isActive }: ChipProps): JSX.Element => {
  return (
    <ChipStyled className={isActive ? 'active' : ''} onClick={onClick}>
      {label}
    </ChipStyled>
  )
}
