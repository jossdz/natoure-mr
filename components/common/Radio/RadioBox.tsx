import React, { ReactNode } from 'react'
import { RadioBoxLabelStyled, RadioBoxInputStyled, RadioBoxStyled } from './styles'

type RadioProps = {
  fluid?: boolean
  id?: string
  name?: string
  label?: string
  value?: any // eslint-disable-line @typescript-eslint/no-explicit-any
  required?: boolean
  disabled?: boolean
  checked?: boolean
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void
  children: ReactNode
}

export const RadioBox = ({
  id,
  name,
  value,
  checked,
  onChange,
  disabled,
  children,
}: RadioProps): JSX.Element => {
  return (
    <RadioBoxLabelStyled>
      <RadioBoxInputStyled
        name={name}
        checked={checked}
        id={id}
        type="radio"
        value={value}
        onChange={onChange}
        disabled={disabled}
      />
      <RadioBoxStyled>{children}</RadioBoxStyled>
    </RadioBoxLabelStyled>
  )
}
