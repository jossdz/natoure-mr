import styled from 'styled-components'

type RadioStyled = {
  fluid?: boolean
}

export const RadioWrapper = styled.div<RadioStyled>(
  ({ fluid }) => `
    display: inline-flex;
    align-items: center;
    :not(:last-of-type){
        margin-bottom:16px;
    }
    ${fluid ? `width: 100%;` : ''}
`
)

export const RadioStyled = styled.input`
  position: absolute;
  opacity: 0;
  + label {
    &:before {
      content: '';
      background: #f4f4f4;
      border-radius: 100%;
      border: 1px solid #b4b4b4;
      display: inline-block;
      width: 1em;
      height: 1em;
      position: relative;
      margin-right: 1em;
      vertical-align: top;
      cursor: pointer;
      text-align: center;
      transition: all 250ms ease;
    }
  }
  &:checked {
    + label {
      &:before {
        border: 1px solid #06bc68;
        background-color: #06bc68;
        box-shadow: inset 0 0 0 3px #f4f4f4;
      }
    }
  }
  &:focus {
    + label {
      &:before {
        outline: none;
        border-color: #06bc68;
      }
    }
  }
  &:disabled {
    + label {
      &:before {
        box-shadow: inset 0 0 0 3px #f4f4f4;
        border-color: #b4b4b4;
        background: #b4b4b4;
      }
    }
  }
  + label {
    &:empty {
      &:before {
        margin-right: 0;
      }
    }
  }
`

export const RadioBoxStyled = styled.div`
  width: 100%;
  min-height: 100px;
  text-align: center;
  background-color: white;
  border: 1px solid var(--green-natoure);
  border-radius: 8px;
  overflow: hidden;
  padding: 16px;
  box-sizing: border-box;
  transition: all 250ms ease;
  will-change: transition;
  display: inline-block;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  position: relative;
  font-weight: 900;
  &:active {
    transform: translateY(10px);
  }
`
export const RadioBoxInputStyled = styled.input`
  width: 0;
  height: 0;
  margin: 0;
  padding: 0;
  &:checked {
    + ${RadioBoxStyled} {
      background-color: var(--green-natoure);
      color: white;
      div {
        color: white;
        text-align: center;
      }
    }
  }
`
export const RadioBoxLabelStyled = styled.label`
  max-width: 45%;
  border-radius: 8px;
  overflow: hidden;
`
