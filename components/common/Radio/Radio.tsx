import React from 'react'
import { RadioWrapper, RadioStyled } from './styles'

type RadioProps = {
  fluid?: boolean
  id?: string
  name?: string
  label?: string
  value?: any // eslint-disable-line @typescript-eslint/no-explicit-any
  required?: boolean
  disabled?: boolean
  checked?: boolean
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void
}

export const Radio = ({
  fluid = false,
  label,
  id,
  name,
  value,
  checked,
  onChange,
  disabled,
}: RadioProps): JSX.Element => {
  return (
    <RadioWrapper fluid={fluid}>
      <RadioStyled
        name={name}
        checked={checked}
        id={id}
        type="radio"
        value={value}
        onChange={onChange}
        disabled={disabled}
      />
      <label htmlFor={id}>{label}</label>
    </RadioWrapper>
  )
}
