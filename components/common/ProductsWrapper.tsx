/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react'
import { EmptyMessage } from '../../styles/general'
import { FlexWrapper } from '../../styles/checkout'

type Props = {
  products: any[]
  Card: any
}

const ProductsWrapper = ({ products, Card }: Props): JSX.Element => {
  return (
    <React.Fragment>
      {products?.length > 0 ? (
        <React.Fragment>
          <FlexWrapper wrap="wrap" justify="start">
            {products.map((item, index) => (
              <Card key={index} {...item} />
            ))}
          </FlexWrapper>
        </React.Fragment>
      ) : (
        <EmptyMessage>No hay información para mostrar</EmptyMessage>
      )}
    </React.Fragment>
  )
}

export default ProductsWrapper
