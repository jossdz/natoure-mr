import styled from 'styled-components'
import { TableBaseStyledProps } from './types/Table'
import { TableHeaderProps } from './types/TableHeader'
import { TableHeaderCellProps } from './types/TableHeaderCell'
import { TableBodyProps } from './types/TableBody'
import { TableRowProps } from './types/TableRow'
import { TableRowCellProps } from './types/TableRowCell'

const basicTableElementStyle = ({ columnCount, customGridTemplate }: TableBaseStyledProps) => {
  const gridTemplate = customGridTemplate ? customGridTemplate : `repeat(${columnCount}, 1fr)`

  return `
    display: grid;
    grid-template-columns: ${gridTemplate};
    grid-gap: 16px;
    border-bottom: 1px solid grey;
    margin-top: 0;
    min-width: 768px;
    overflow-x: auto;
    box-sizing: border-box;
  `
}

export const TableStyled = styled.div`
  overflow-x: auto;
`

export const TableHeaderStyled = styled.div<TableHeaderProps & TableBaseStyledProps>`
  ${(props) => basicTableElementStyle(props)}
  height: 56px;
  grid-template-rows: 24px;
  font-size: 14px;
  color: #2c375a;
  padding: 16px 24px;
  line-height: 1.29;
  letter-spacing: -0.56px;
  font-weight: 500;
  text-align: left;
  box-sizing: border-box;
`

export const TableHeaderItemStyled = styled.div<TableHeaderCellProps>(
  ({ onClick }) => `
    display: flex;
    align-items: center;
    min-width: 0;
    ${onClick ? `cursor: pointer;` : `cursor: default;`}
  `
)
export const TableBodyStyled = styled.div<TableBodyProps>(
  ({ bodyMaxHeight }) => `
    ${bodyMaxHeight && `max-height: ${bodyMaxHeight}px;`}
    overflow-y: auto;
    min-width: 768px;
  `
)
export const TableRowStyled = styled.div<TableBaseStyledProps & TableRowProps>(
  basicTableElementStyle,
  ({ rowLg, onClick }) => `
    font-size: 14px;
    font-weight: 500;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.29;
    letter-spacing: -0.73px;
    color: #2c375a;
    grid-template-rows: ${rowLg ? '32px' : '16px'};
    padding: 8px 16px;
    :hover {
      background-color: rgba(44,55,90, 0.1);
      ${onClick ? `cursor: pointer;` : `cursor: default;`}
    }
  `
)
export const TableRowItemStyled = styled.div<TableRowCellProps>`
  display: flex;
  align-items: center;
`
