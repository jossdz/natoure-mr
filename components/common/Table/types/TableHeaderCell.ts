import { BasicProps } from './types'

export type TableHeaderCellProps = {
  onClick?: () => void
} & BasicProps
