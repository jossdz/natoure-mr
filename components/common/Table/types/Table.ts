import { ReactNode } from 'react'

export type TableProps = {
  /** Table Children */
  children: ReactNode
  /** Table column sizes and distributio */
  customGridTemplate?: string
  /** Table column number (makes every column have the same size) */
  columnCount?: number
  /** Change the height of the row */
  rowLg?: boolean
  /** Change the max-height property of the <Table.Body> and adds an scroll if the content is bigger*/
  bodyMaxHeight?: number | string
  id?: string
}

export type TableBaseStyledProps = {
  columnCount: number
  customGridTemplate?: string
  rowLg: boolean
}
