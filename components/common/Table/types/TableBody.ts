import { BasicProps } from './types'

export type TableBodyProps = {
  bodyMaxHeight?: string | number
} & BasicProps
