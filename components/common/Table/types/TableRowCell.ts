import { BasicProps } from './types'

export type TableRowCellProps = BasicProps
