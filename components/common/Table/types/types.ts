import { ReactNode, Dispatch, SetStateAction } from 'react'

export type ContextTypes = {
  customGridTemplate?: string
  setCustomGridTemplate: Dispatch<SetStateAction<string | undefined>>
  rowLg: boolean
  setRowLg: Dispatch<SetStateAction<boolean>>
  columnCount: number
  setColumnCount: Dispatch<SetStateAction<number>>
  bodyMaxHeight?: number | string
  setBodyMaxHeight: Dispatch<SetStateAction<number | string | undefined>>
}

export type BasicProps = {
  children: ReactNode
  id?: string
}
