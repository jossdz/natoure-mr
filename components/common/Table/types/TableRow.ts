import { BasicProps } from './types'

export type TableRowProps = {
  onClick?: () => void
} & BasicProps
