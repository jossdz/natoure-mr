import React, { useState, createContext } from 'react'
import { BasicProps, ContextTypes } from './types/types'

export const TableContext = createContext<ContextTypes>({} as ContextTypes)

export const TableContextProvider = ({ children }: BasicProps): JSX.Element => {
  const [customGridTemplate, setCustomGridTemplate] = useState<string | undefined>()
  const [rowLg, setRowLg] = useState<boolean>(false)
  const [columnCount, setColumnCount] = useState<number>(1)
  const [bodyMaxHeight, setBodyMaxHeight] = useState<number | string>()

  return (
    <TableContext.Provider
      value={{
        customGridTemplate,
        setCustomGridTemplate,
        rowLg,
        setRowLg,
        columnCount,
        setColumnCount,
        bodyMaxHeight,
        setBodyMaxHeight,
      }}
    >
      {children}
    </TableContext.Provider>
  )
}
