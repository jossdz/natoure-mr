import React, { useContext, useEffect } from 'react'
import {
  TableStyled,
  TableHeaderStyled,
  TableHeaderItemStyled,
  TableBodyStyled,
  TableRowStyled,
  TableRowItemStyled,
} from './styles'
import { TableProps } from './types/Table'
import { TableHeaderProps } from './types/TableHeader'
import { TableHeaderCellProps } from './types/TableHeaderCell'
import { TableBodyProps } from './types/TableBody'
import { TableRowProps } from './types/TableRow'
import { TableRowCellProps } from './types/TableRowCell'
import { TableContext, TableContextProvider } from './TableContext'

const TableContainer = ({
  children,
  customGridTemplate,
  columnCount = 1,
  rowLg = false,
  id,
  bodyMaxHeight,
}: TableProps) => {
  const { setCustomGridTemplate, setRowLg, setColumnCount, setBodyMaxHeight } = useContext(
    TableContext
  )

  useEffect(() => {
    setCustomGridTemplate(customGridTemplate)
    setRowLg(rowLg)
    setColumnCount(columnCount)
    setBodyMaxHeight(bodyMaxHeight)
  }, [customGridTemplate, columnCount, rowLg, bodyMaxHeight])

  return <TableStyled id={id}>{children}</TableStyled>
}

const TableHeader = ({ children, id }: TableHeaderProps) => {
  const { rowLg, customGridTemplate, columnCount } = useContext(TableContext)
  return (
    <TableHeaderStyled
      customGridTemplate={customGridTemplate}
      columnCount={columnCount}
      rowLg={rowLg}
      id={id}
    >
      {children}
    </TableHeaderStyled>
  )
}

const TableHeaderCell = ({ children, onClick, id }: TableHeaderCellProps) => {
  return (
    <TableHeaderItemStyled id={id} onClick={onClick} role="columnheader">
      {children}
    </TableHeaderItemStyled>
  )
}
const TableBody = ({ children, id }: TableBodyProps) => {
  const { bodyMaxHeight } = useContext(TableContext)
  return (
    <TableBodyStyled bodyMaxHeight={bodyMaxHeight} id={id}>
      {children}
    </TableBodyStyled>
  )
}
const TableRow = ({ children, onClick, id }: TableRowProps) => {
  const { rowLg, customGridTemplate, columnCount } = useContext(TableContext)
  return (
    <TableRowStyled
      columnCount={columnCount}
      customGridTemplate={customGridTemplate}
      rowLg={rowLg}
      onClick={onClick}
      id={id}
      role="row"
    >
      {children}
    </TableRowStyled>
  )
}

const TableCell = ({ children, id }: TableRowCellProps) => {
  return <TableRowItemStyled id={id}>{children}</TableRowItemStyled>
}

const Table = ({
  children,
  rowLg,
  customGridTemplate,
  columnCount,
  id,
  bodyMaxHeight,
}: TableProps): JSX.Element => {
  return (
    <TableContextProvider>
      <TableContainer
        id={id}
        columnCount={columnCount}
        customGridTemplate={customGridTemplate}
        rowLg={rowLg}
        bodyMaxHeight={bodyMaxHeight}
      >
        {children}
      </TableContainer>
    </TableContextProvider>
  )
}

Table.Header = TableHeader
Table.HeaderCell = TableHeaderCell
Table.Body = TableBody
Table.Row = TableRow
Table.Cell = TableCell

export { Table }
