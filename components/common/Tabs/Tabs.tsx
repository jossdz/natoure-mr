import React from 'react'
import { TabsWrapper, TabsStyled } from './styles'

export const Tabs = (): JSX.Element => {
  return (
    <TabsWrapper>
      <TabsStyled>Áreas Naturales</TabsStyled>
      <TabsStyled>Destinos Sostenibles</TabsStyled>
      <TabsStyled>Rutas Turísticas</TabsStyled>
    </TabsWrapper>
  )
}
