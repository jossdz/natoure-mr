import styled from 'styled-components'

export const TabsWrapper = styled.ul`
  display: flex;
  list-style: none;
  overflow: auto;
  padding: 0 30px;
  ::-webkit-scrollbar {
    display: none;
  }
`

export const TabsStyled = styled.li`
  list-style: none;
  white-space: nowrap;
  padding-bottom: 15px;
  border-bottom: 3px solid transparent;
  transition: all 0.2s;
  :not(:first-of-type) {
    margin-left: 30px;
  }
  :hover {
    border-bottom: 3px solid var(--green-natoure);
  }
  &.active,
  :active {
    border-bottom: 3px solid var(--green-natoure);
  }
`
