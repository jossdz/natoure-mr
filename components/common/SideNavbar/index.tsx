import React from 'react'
import {
  SideBar,
  LogoDetails,
  LogoName,
  NavList,
  NavListItem,
  Tooltip,
  LinkName,
  Profile,
  ManuBtnWrapper,
  NavIcon,
  Logout,
  ProfileDetails,
  NavAnchor,
} from './styles'
import Link from 'next/link'
import { FaUsers, FaBars, FaTree, FaSignOutAlt, FaMapMarkedAlt } from 'react-icons/fa'
import { useRouter } from 'next/router'

type SideNavbarProps = { isOpen: boolean; handleExpand: () => void }

const SideNavbar = ({ isOpen, handleExpand }: SideNavbarProps): JSX.Element => {
  const router = useRouter()
  return (
    <SideBar open={isOpen}>
      <LogoDetails>
        <NavIcon open={isOpen}>
          <img src="/assets/icon-white.png" alt="natoure-logo" />
        </NavIcon>
        <LogoName open={isOpen}>Natoure</LogoName>
        <ManuBtnWrapper onClick={handleExpand}>
          <FaBars />
        </ManuBtnWrapper>
      </LogoDetails>
      <NavList>
        <NavListItem>
          <Link href="/admin/usuarios" passHref>
            <NavAnchor className={router.pathname.includes('usuarios') ? 'active' : ''}>
              <FaUsers />
              <LinkName open={isOpen}>Usuarios</LinkName>
            </NavAnchor>
          </Link>
          <Tooltip open={isOpen}>Usuarios</Tooltip>
        </NavListItem>
        <NavListItem>
          <Link href="/admin/areas" passHref>
            <NavAnchor className={router.pathname.includes('areas') ? 'active' : ''}>
              <FaTree />
              <LinkName open={isOpen}>Áreas Naturales</LinkName>
            </NavAnchor>
          </Link>
          <Tooltip open={isOpen}>Áreas Naturales</Tooltip>
        </NavListItem>
        <NavListItem>
          <Link href="/admin/destinos" passHref>
            <NavAnchor className={router.pathname.includes('destinos') ? 'active' : ''}>
              <FaMapMarkedAlt />
              <LinkName open={isOpen}>Destinos sostenibles</LinkName>
            </NavAnchor>
          </Link>
          <Tooltip open={isOpen}>Destinos sostenibles</Tooltip>
        </NavListItem>

        <Profile open={isOpen}>
          <ProfileDetails open={isOpen}>
            <img src="/assets/icon-white.png" alt="profileImg" />
            <div className="name_job">
              <div className="name">Prem Shahi</div>
              <div className="job">Web designer</div>
            </div>
          </ProfileDetails>
          <Logout open={isOpen}>
            <FaSignOutAlt />
          </Logout>
        </Profile>
      </NavList>
    </SideBar>
  )
}

export default SideNavbar
