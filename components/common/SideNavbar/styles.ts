import styled from 'styled-components'

type SideNavbarProps = {
  open: boolean
}

export const SideBar = styled.div(
  ({ open }: SideNavbarProps) => `
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  position: fixed;
  left: 0;
  top: 0;
  height: 100%;
  width: ${open ? '250px' : '78px'};
  background: #11101D;
  padding: 6px 14px;
  z-index: 99;
  transition: all 0.5s ease;
  svg{
    color: #fff;
    height: 25px;
    min-width: 50px;
    font-size: 28px;
    text-align: center;
    line-height: 60px;
  }
  `
)

export const LogoDetails = styled.div`
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  height: 60px;
  display: flex;
  align-items: center;
  position: relative;
`

export const NavIcon = styled.div`
  opacity: ${({ open }: SideNavbarProps) => (open ? 1 : 0)};
  transition: all 0.5s ease;
  display: flex;
  align-items: center;
  img {
    height: 25px;
    margin-right: 16px;
    margin-left: 8px;
  }
`

export const LogoName = styled.div`
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  color: #fff;
  font-size: 20px;
  font-weight: 600;
  opacity: ${({ open }: SideNavbarProps) => (open ? 1 : 0)};
  transition: all 0.5s ease;
`

export const ManuBtnWrapper = styled.button`
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  position: absolute;
  top: 50%;
  right: 0;
  transform: translateY(-50%);
  font-size: 22px;
  transition: all 0.4s ease;
  font-size: 23px;
  text-align: center;
  cursor: pointer;
  transition: all 0.5s ease;
  border: none;
  outline: none;
  background: none;
  color: white;
`

export const NavList = styled.ul`
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  margin-top: 20px;
  height: 100%;
`

export const Tooltip = styled.span`
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  position: absolute;
  top: -20px;
  left: calc(100% + 15px);
  z-index: 3;
  background: #fff;
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.3);
  padding: 6px 12px;
  border-radius: 4px;
  font-size: 15px;
  font-weight: 400;
  opacity: 0;
  white-space: nowrap;
  pointer-events: none;
  transition: 0s;
  ${({ open }: SideNavbarProps) => (open ? 'display: none;' : '')}
`

export const LinkName = styled.span(
  ({ open }: SideNavbarProps) => `
  margin: 0;
  padding: 0;
  box-sizing: border-box;
color: #fff;
font-size: 15px;
font-weight: 400;
white-space: nowrap;
opacity: ${open ? 1 : 0};
pointer-events: ${open ? 'auto' : 'none'};
transition: 0.4s;
`
)

export const NavListItem = styled.li`
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  position: relative;
  margin: 12px 0;
  list-style: none;
  display: flex;
  align-items: center;
  svg {
    height: 25px;
    line-height: 25px;
    font-size: 18px;
    border-radius: 12px;
  }
  :hover {
    ${Tooltip} {
      opacity: 1;
      pointer-events: auto;
      transition: all 0.4s ease;
      top: 50%;
      transform: translateY(-50%);
    }
  }
`

export const NavAnchor = styled.a`
  display: flex;
  height: 100%;
  width: 100%;
  border-radius: 12px;
  align-items: center;
  text-decoration: none;
  transition: all 0.4s ease;
  background: #11101d;
  padding: 12px 0;
  :hover,
  &.active {
    background: #fff;
    ${LinkName}, svg {
      transition: all 0.5s ease;
      color: #11101d;
    }
  }
`

export const Profile = styled.li(
  ({ open }: SideNavbarProps) => `
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  position: fixed;
  height: 60px;
  width: ${open ? '250px' : '78px'};
  left: 0;
  bottom: 0px;
  display:flex;
  padding: 10px 14px;
  background: #1d1b31;
  transition: all 0.5s ease;
  overflow: hidden;
  img{
    height: 35px;
    width: 35px;
    border-radius: 6px;
    margin-right: 10px;
  }
`
)

export const ProfileDetails = styled.div(
  ({ open }: SideNavbarProps) => `
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  display: flex;
  align-items: center;
  flex-wrap: nowrap;
  opacity: ${open ? 1 : 0};
  pointer-events: ${open ? 'auto' : 'none'};
  .name,
  .job {
    font-size: 15px;
    font-weight: 400;
    color: #fff;
    white-space: nowrap;
  }
  .job {
    font-size: 12px;
  }
`
)

export const Logout = styled.button(
  ({ open }: SideNavbarProps) => `
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  position: absolute;
  top: 47.5%;
  right: 0;
  transform: translateY(-50%);
  background: ${open ? 'none' : '#1d1b31'};
  width: ${open ? '50px' : '100%'};
  height: 40px;
  line-height: 60px;
  border-radius: 0px;
  transition: all 0.5s ease;
  border: none;
  outline: none;
  background: none;
`
)
