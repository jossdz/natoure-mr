import React from 'react'
import Link from 'next/link'
import Dinero from 'dinero.js'
import { RiLeafFill, RiHeartLine, RiStarSFill, RiMapPinLine } from 'react-icons/ri'

import { FlexWrapper, IconWrapper } from '../../../styles/checkout'
import { Dot } from '../../../styles/checkout/Card'
import {
  CardWrapper,
  CardThumbnail,
  CardText,
  CardSubText,
  CardChip,
  ChipsWrapper,
  CardIcons,
  ButtonSave,
} from '../../../styles/components/Cards/ExperienceCard'

type Props = {
  mainPhoto: string
  experienceName: string
  personCost: string
  _id: string
  experienceTime: string
  daysHours: string
  experienceMacros: string[]
  orgId: {
    sustentabilityPromedy: number
    orgState: string
  }
}

const ExperienceCard = ({
  mainPhoto,
  experienceName,
  personCost,
  _id,
  experienceTime,
  daysHours,
  experienceMacros,
  orgId,
}: Props): JSX.Element => {
  const cost = Dinero({ amount: Number(personCost) * 100, currency: 'MXN' })

  const getColor = (pathname) => {
    switch (pathname) {
      case 'Biocultural':
        return '#fa4775'
      case 'Naturaleza':
        return 'var(--green-natoure)'
      case 'Rural':
        return '#f7b500'
      case 'Aventura':
        return '#8500f7'
      case 'Regenerativo':
        return '#65afff'
      case 'Agroturismo':
        return '#e1cd68'
      case 'Rutas gastronómicas':
        return '#fa7e47'
      case 'Ciencia y conservación':
        return '#c39571'
    }
  }

  return (
    <CardWrapper>
      <CardThumbnail image={mainPhoto[0]} />

      <CardIcons>
        <FlexWrapper>
          <RiLeafFill className="leaf" />
          <CardSubText style={{ fontWeight: 600, color: '#fff' }}>
            {orgId.sustentabilityPromedy.toFixed(1)}
          </CardSubText>
        </FlexWrapper>
        <ButtonSave>
          <RiHeartLine />
        </ButtonSave>
      </CardIcons>

      <ChipsWrapper>
        {experienceMacros.map((item, index) => (
          <CardChip key={index} background={getColor(item)}>
            {item}
          </CardChip>
        ))}
      </ChipsWrapper>

      <FlexWrapper style={{ padding: '0 8px' }}>
        <FlexWrapper>
          <IconWrapper marginRight="4px" size="12px">
            <RiMapPinLine />
          </IconWrapper>
          <CardSubText>{orgId.orgState}</CardSubText>
        </FlexWrapper>
        <FlexWrapper>
          <IconWrapper marginRight="4px">
            <RiStarSFill size="12px" />
          </IconWrapper>
          <CardSubText>4.5</CardSubText>
        </FlexWrapper>
      </FlexWrapper>

      <Link href={`/experience/${_id}`}>
        <CardText style={{ cursor: 'pointer', margin: '8px 0', padding: '0 8px' }}>
          {experienceName}
        </CardText>
      </Link>

      <FlexWrapper justify="flex-start" style={{ padding: '0 8px' }}>
        <CardSubText style={{ fontWeight: 'bold' }}>{cost.toFormat('$0,0')}</CardSubText>
        <Dot />
        <CardSubText>
          {experienceTime} {daysHours}
        </CardSubText>
      </FlexWrapper>
    </CardWrapper>
  )
}

export default ExperienceCard
