import React from 'react'
import Link from 'next/link'

import {
  CardWrapper,
  CardThumbnail,
  CardText,
} from '../../../styles/components/Cards/ExperienceCard'

type Props = {
  platilloCoverPhoto: string[]
  platilloName: string
  _id: string
}

const PlateCard = ({ platilloCoverPhoto, platilloName, _id }: Props): JSX.Element => {
  return (
    <CardWrapper>
      <CardThumbnail image={platilloCoverPhoto && platilloCoverPhoto[0]} />
      <CardText style={{ cursor: 'pointer', margin: '8px 0', padding: '0 8px' }}>
        {platilloName}
      </CardText>
    </CardWrapper>
  )
}

export default PlateCard
