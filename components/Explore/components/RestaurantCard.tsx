import React from 'react'
import Link from 'next/link'
import { RiLeafFill, RiHeartLine, RiStarSFill, RiMapPinLine } from 'react-icons/ri'

import { FlexWrapper, IconWrapper } from '../../../styles/checkout'
import {
  CardWrapper,
  CardThumbnail,
  CardText,
  CardSubText,
  CardIcons,
  ButtonSave,
} from '../../../styles/components/Cards/ExperienceCard'

type Props = {
  restaurantCoverPhotos: string[]
  restaurantName: string
  restaurantTags: string[]
  _id: string
  orgId: {
    sustentabilityPromedy: number
    orgState: string
  }
}

const RestaurantCard = ({
  restaurantCoverPhotos,
  restaurantName,
  restaurantTags,
  orgId,
  _id,
}: Props): JSX.Element => {
  return (
    <CardWrapper>
      <CardThumbnail image={restaurantCoverPhotos[0]} />

      <CardIcons>
        <FlexWrapper>
          <RiLeafFill className="leaf" />
          <CardSubText style={{ fontWeight: 600, color: '#fff' }}>
            {orgId.sustentabilityPromedy.toFixed(1)}
          </CardSubText>
        </FlexWrapper>
        <ButtonSave>
          <RiHeartLine />
        </ButtonSave>
      </CardIcons>

      <FlexWrapper style={{ padding: '0 8px' }}>
        <FlexWrapper>
          <IconWrapper marginRight="4px" size="12px">
            <RiMapPinLine />
          </IconWrapper>
          <CardSubText>{orgId.orgState}</CardSubText>
        </FlexWrapper>
        <FlexWrapper>
          <IconWrapper marginRight="4px">
            <RiStarSFill size="12px" />
          </IconWrapper>
          <CardSubText>4.5</CardSubText>
        </FlexWrapper>
      </FlexWrapper>

      <Link href={`/restaurant/${_id}`}>
        <CardText style={{ cursor: 'pointer', margin: '8px 0', padding: '0 8px' }}>
          {restaurantName}
        </CardText>
      </Link>

      <FlexWrapper justify="flex-start" style={{ padding: '0 8px' }}>
        <CardSubText>{restaurantTags[0]}</CardSubText>
      </FlexWrapper>
    </CardWrapper>
  )
}

export default RestaurantCard
