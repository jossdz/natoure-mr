import React from 'react'
import Link from 'next/link'
import Dinero from 'dinero.js'
import { RiLeafFill, RiHeartLine, RiStarSFill, RiMapPinLine } from 'react-icons/ri'

import { FlexWrapper, IconWrapper } from '../../../styles/checkout'
import { Dot } from '../../../styles/checkout/Card'
import {
  CardWrapper,
  CardThumbnail,
  CardText,
  CardSubText,
  CardIcons,
  ButtonSave,
} from '../../../styles/components/Cards/ExperienceCard'

type Props = {
  hotelPhotosCover: string
  hotelName: string
  price: string
  _id: string
  hotelType: string
  experienceMacros: string[]
  orgId: {
    sustentabilityPromedy: number
    orgState: string
  }
}

const HotelCard = ({
  hotelPhotosCover,
  hotelName,
  price,
  _id,
  hotelType,
  orgId,
}: Props): JSX.Element => {
  const cost = Dinero({ amount: Number(price) * 100, currency: 'MXN' })

  return (
    <CardWrapper>
      <CardThumbnail image={hotelPhotosCover[0]} />

      <CardIcons>
        <FlexWrapper>
          <RiLeafFill className="leaf" />
          <CardSubText style={{ fontWeight: 600, color: '#fff' }}>
            {orgId.sustentabilityPromedy.toFixed(1)}
          </CardSubText>
        </FlexWrapper>
        <ButtonSave>
          <RiHeartLine />
        </ButtonSave>
      </CardIcons>

      <FlexWrapper style={{ padding: '0 8px' }}>
        <FlexWrapper>
          <IconWrapper marginRight="4px" size="12px">
            <RiMapPinLine />
          </IconWrapper>
          <CardSubText>{orgId.orgState}</CardSubText>
        </FlexWrapper>
        <FlexWrapper>
          <IconWrapper marginRight="4px">
            <RiStarSFill size="12px" />
          </IconWrapper>
          <CardSubText>4.5</CardSubText>
        </FlexWrapper>
      </FlexWrapper>

      <Link href={`/hotel/${_id}`}>
        <CardText style={{ cursor: 'pointer', margin: '8px 0', padding: '0 8px' }}>
          {hotelName}
        </CardText>
      </Link>

      <FlexWrapper justify="flex-start" style={{ padding: '0 8px' }}>
        <CardSubText style={{ fontWeight: 'bold' }}>{cost.toFormat('$0,0')}</CardSubText>
        <Dot />
        <CardSubText>{hotelType}</CardSubText>
      </FlexWrapper>
    </CardWrapper>
  )
}

export default HotelCard
