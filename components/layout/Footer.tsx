import React from 'react'
import Link from 'next/link'
import styled from 'styled-components'
import { FaFacebookF, FaInstagram, FaTwitter, FaVimeoV } from 'react-icons/fa'

const Wrapper = styled.div`
  width: 100%;
  height: 240px;
  color: white;
  position: relative;
  @media (min-width: 1366px) and (max-width: 1619px) {
    height: 304px;
  }
  @media (min-width: 1620px) {
    height: 375px;
  }
`

const Background = styled.div`
  width: 61%;
  height: 100%;
  background-image: url(/assets/footer.svg);
  background-repeat: no-repeat;
  background-size: cover;
  background-position: top;
  padding: 9% 2rem 0 1rem;
  box-sizing: border-box;
  font-family: Roboto;
  line-height: 1.5;
  font-weight: 400;
  font-size: 12px;
  display: flex;
  justify-content: space-between;
  z-index: 2;
  @media (min-width: 1366px) {
    width: 57.5%;
  }
  a {
    display: block;
    text-decoration: none;
    font-weight: 400;
    color: white;
    cursor: pointer;
  }
`

const Info = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 100%;
  width: 260px;
`

const CopyRight = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  height: 100%;
  align-items: center;
  img {
    width: 80px;
  }
`

const Social = styled.div`
  position: absolute;
  top: 0;
  width: 100%;
  display: flex;
  justify-content: center;
  margin: 5rem 0;
  z-index: 1;
  a {
    background-color: var(--green-natoure);
    height: 48px;
    width: 48px;
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 50%;
    :not(:last-child) {
      margin-right: 24px;
    }
    svg {
      width: 32px;
      height: 32px;
      color: var(--white);
    }
  }
`

const Footer = (): JSX.Element => {
  return (
    <Wrapper>
      <Background>
        <Info>
          <div>
            <Link href="/terminos-y-condiciones">TÉRMINOS Y CONDICIONES</Link>
            <Link href="/aviso-de-privacidad">AVISO DE PRIVACIDAD</Link>
          </div>
          <p>
            NATOURE ® <br />
            Av. de los Insurgentes Sur 601, WeWork Piso 14 Nápoles, 03810 Ciudad de México, CDMX
          </p>
        </Info>
        <CopyRight>
          <img src="/assets/logo-white.png" alt="Natoure" />
          <p>©2021 Copyright: natoure.org</p>
        </CopyRight>
      </Background>
      <Social>
        <a href="https://www.facebook.com/Natoure.org/" target="_blank" rel="noopener noreferrer">
          <FaFacebookF />
        </a>
        <a
          href="https://www.instagram.com/natoure.rocks/"
          target="_blank"
          rel="noopener noreferrer"
        >
          <FaInstagram />
        </a>
        <a href="https://twitter.com/natoureorg" target="_blank" rel="noopener noreferrer">
          <FaTwitter />
        </a>
        <a href="https://vimeo.com/natoure" target="_blank" rel="noopener noreferrer">
          <FaVimeoV />
        </a>
      </Social>
    </Wrapper>
  )
}

export default Footer
