import React from 'react'
import styled from 'styled-components'

const Section = styled.div(
  ({ isOpen }: { isOpen: boolean }) => `
  position: relative;
  min-height: 100vh;
  top: 0;
  left: ${isOpen ? '250px' : '78px'};
  width: calc(100% - ${isOpen ? '250px' : '78px'});
  max-width: calc(100% - ${isOpen ? '250px' : '78px'});
  box-sizing: border-box;
  transition: all 0.5s ease;
  z-index: 2;
  padding: 36px 42px;
`
)

type AdminContentSectionProps = {
  isOpen: boolean
  children: React.ReactNode
}
const AdminContentSection = ({ children, isOpen }: AdminContentSectionProps): JSX.Element => {
  return <Section isOpen={isOpen}>{children}</Section>
}

export default AdminContentSection
