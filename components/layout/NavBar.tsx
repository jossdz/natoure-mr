import React, { useState, useEffect } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import styled from 'styled-components'

import {
  AiOutlineHome,
  AiOutlineCompass,
  AiOutlineFlag,
  AiFillHome,
  AiFillCompass,
  AiFillFlag,
  AiFillEnvironment,
  AiOutlineEnvironment,
  AiFillTags,
  AiOutlineTags,
} from 'react-icons/ai'
import { BsPerson, BsPersonFill, BsPeople, BsPeopleFill } from 'react-icons/bs'
import { FaRegMoneyBillAlt, FaMoneyBillAlt } from 'react-icons/fa'
import { IoStorefrontOutline, IoStorefrontSharp } from 'react-icons/io5'
import { IoMdNotificationsOutline } from 'react-icons/io'

import ProfileDrawer from '../../styles/components/ProfileDrawer'
import Container from '../../styles/components/Container'

type props = {
  mainpage: boolean
}

const MobNav = styled.div`
  position: fixed;
  bottom: 0;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  width: 100vw;
  height: 72px;
  box-shadow: 0 2px 11px 0 rgba(0, 0, 0, 0.18);
  background-color: white;
  z-index: 9999;
  .link {
    display: inline-flex;
    justify-content: center;
    flex-direction: column;
    line-height: 24px;
    align-items: center;
    font-size: 10px;
    text-align: center;
    color: var(--dark-blue);
  }
  .bold {
    font-weight: bold;
  }
  svg {
    width: 24px;
    height: 24px;
    color: var(--dark-blue);
  }
  @media (min-width: 1024px) {
    display: none;
  }
`

const DeskNav = styled.div(
  ({ mainpage }: props) => `
  position: fixed;
  top: 0;
  width: 100vw;
  height: 87px;
  box-shadow: ${mainpage ? 'none' : '0 2px 11px 0 rgba(0, 0, 0, 0.18)'};
  background-color: ${mainpage ? 'transparent' : 'white'};
  z-index: 999;
  section {
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 100%;
    .logo {
      width: 140px;
      height: 34px;
    }
    .link {
      font-size: 14px;
      font-weight: 500;
      line-height: 1.29;
      letter-spacing: -0.73px;
      text-align: center;
      color: ${mainpage ? 'white' : 'var(--dark-blue)'};
      margin-right: 32px;
      cursor: pointer;
    }
    .active {
      font-weight: bold;
      border-bottom: 3px solid var(--green-natoure);
    }
    .icons {
      display: flex;
      align-items: center;
      svg {
        width: 28px;
        height: 28px;
        color: ${mainpage ? 'white' : 'var(--dark-blue)'};
        stroke-width: 6px;
        margin-right: 16px;
      }
    }
  }
  @media (max-width: 1023px) {
    display: none;
  }
`
)

const userDataLoggedIn = [
  { path: '/inicio', fill: AiFillHome, empty: AiOutlineHome, text: 'Inicio' },
  { path: '/explora', fill: AiFillCompass, empty: AiOutlineCompass, text: 'Explora' },
  { path: '/misviajes', fill: AiFillFlag, empty: AiOutlineFlag, text: 'Mis viajes' },
  {
    path: '/natural-areas',
    fill: AiFillEnvironment,
    empty: AiOutlineEnvironment,
    text: 'Destinos',
  },
  { path: '/anfitriones', fill: BsPersonFill, empty: BsPerson, text: 'Anfitriones' },
]

const userData = [
  { path: '/inicio', fill: AiFillHome, empty: AiOutlineHome, text: 'Inicio' },
  { path: '/explora', fill: AiFillCompass, empty: AiOutlineCompass, text: 'Explora' },
  {
    path: '/natural-areas',
    fill: AiFillEnvironment,
    empty: AiOutlineEnvironment,
    text: 'Destinos',
  },
  { path: '/anfitriones', fill: BsPersonFill, empty: BsPerson, text: 'Anfitriones' },
  { path: '/nosotros', fill: AiFillFlag, empty: AiOutlineFlag, text: 'Acerca de nosotros' },
]

const anfData = [
  { path: '/anfprofile', fill: AiFillHome, empty: AiOutlineHome, text: 'Inicio' },
  {
    path: '/mispagos',
    fill: FaMoneyBillAlt,
    empty: FaRegMoneyBillAlt,
    text: 'Mis pagos',
  },
  { path: '/misventas', fill: AiFillTags, empty: AiOutlineTags, text: 'Mis ventas' },
  {
    path: '/misproductos',
    fill: IoStorefrontSharp,
    empty: IoStorefrontOutline,
    text: 'Mis productos',
  },
  { path: '/miorganizacion', fill: BsPeopleFill, empty: BsPeople, text: 'Organización' },
]

type Props = {
  anfitrion?: boolean
  name?: string
  photo?: string
}

const NavBar = (props: Props): JSX.Element => {
  const { pathname } = useRouter()
  const mainpage = pathname === '/inicio'

  const [fixed, setFixed] = useState(false)
  const [menuItems, setMenuItems] = useState(userData)

  useEffect(() => {
    const user = localStorage.getItem('NatoureUser')
    const selectedMenuItems = props.anfitrion ? anfData : user ? userDataLoggedIn : userData
    setMenuItems(selectedMenuItems)
    if (typeof window !== 'undefined') {
      window.addEventListener('scroll', () => setFixed(window.scrollY > 30))
    }
  }, [])

  return (
    <>
      <MobNav>
        {menuItems.map((item, index) => (
          <Link key={index} href={item.path}>
            <div className={`link ${pathname === item.path && 'bold'}`}>
              {pathname === item.path ? <item.fill /> : <item.empty />}
              <span>{item.text}</span>
            </div>
          </Link>
        ))}
      </MobNav>
      <DeskNav mainpage={mainpage && !fixed}>
        <Container>
          <img
            className="logo"
            src={mainpage && !fixed ? '/assets/logo-white.png' : '/assets/logo-blue.png'}
            alt="Natoure"
          />
          <div>
            {menuItems.map((item, index) => (
              <Link key={index} href={item.path}>
                <span className={`link ${pathname === item.path && 'active'}`}>{item.text}</span>
              </Link>
            ))}
          </div>
          <div className="icons">
            <IoMdNotificationsOutline />
            <ProfileDrawer anfitrion={props.anfitrion} name={props.name} photo={props.photo} />
          </div>
        </Container>
      </DeskNav>
    </>
  )
}

export default NavBar
