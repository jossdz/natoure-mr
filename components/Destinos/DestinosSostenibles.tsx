import React from 'react'
import InfoCard from '../../styles/components/InfoCard'
import { Label } from '../../styles/forms'
import { FlexWrapper, IconWrapper } from '../../styles/checkout'
import Card from './Card'
import { RiLeafFill } from 'react-icons/ri'

const backgroundURL =
  'https://images.daznservices.com/di/library/Goal_Mexico/39/5f/estadio-hidalgo-ok_1c1bpim5sgww911f34ri62bzbk.jpg?t=259501240&quality=60&w=1200&h=800'

const DestinosSostenibles = ({ data }: any): JSX.Element => {
  return (
    <React.Fragment>
      {/* recomendations */}
      <Label
        title={
          <FlexWrapper justify="flex-start">
            Destinos sostenibles cerca de tí
            <IconWrapper marginLeft="8px" svgColor="var(--green-natoure)">
              <RiLeafFill />
            </IconWrapper>
          </FlexWrapper>
        }
      />
      <FlexWrapper wrap="wrap">
        {data.map((item, index) => (
          <Card key={index} {...item} />
        ))}
      </FlexWrapper>

      <InfoCard backgroundURL={backgroundURL}>
        <div>
          Todas las experiencias que vivirás con Natoure son
          <br />
          <b>experiencias únicas únicas que ayudan a conservar el medio ambiente</b>
        </div>
      </InfoCard>

      {/* fauna */}

      <Label
        title={
          <FlexWrapper justify="flex-start">
            Destinos que no te puedes perder
            <IconWrapper marginLeft="8px" svgColor="var(--green-natoure)">
              <RiLeafFill />
            </IconWrapper>
          </FlexWrapper>
        }
      />
      <FlexWrapper wrap="wrap">
        {data.map((item, index) => (
          <Card key={index} {...item} />
        ))}
      </FlexWrapper>
    </React.Fragment>
  )
}

export default DestinosSostenibles
