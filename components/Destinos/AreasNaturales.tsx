import React from 'react'
import InfoCard from '../../styles/components/InfoCard'
import { Label } from '../../styles/forms'
import { FlexWrapper, IconWrapper } from '../../styles/general/FlexWrapper'
import Card from './Card'
import { RiLeafFill } from 'react-icons/ri'

const backgroundURL = '/assets/naturaleza.jpg'

const AreasNaturales = ({ data }: any): JSX.Element => {
  return (
    <React.Fragment>
      {/* recomendations */}
      <Label
        title={
          <FlexWrapper justify="flex-start">
            Recomendado para tí
            <IconWrapper marginLeft="8px" svgColor="var(--green-natoure)">
              <RiLeafFill />
            </IconWrapper>
          </FlexWrapper>
        }
      />
      <FlexWrapper wrap="wrap">
        {data?.map((item, index) => (
          <Card key={index} {...item} isNaturalArea />
        ))}
      </FlexWrapper>

      <InfoCard backgroundURL={backgroundURL}>
        <div>
          Todas las AreasNaturales que viviras con Natoure son <br />
          <b>AreasNaturales únicas que ayudan a conservar el medio ambiente</b>
        </div>
      </InfoCard>

      {/* fauna */}

      <Label
        title={
          <FlexWrapper justify="flex-start">
            Conoce la fauna de la región
            <IconWrapper marginLeft="8px" svgColor="var(--green-natoure)">
              <RiLeafFill />
            </IconWrapper>
          </FlexWrapper>
        }
      />
      <FlexWrapper wrap="wrap">
        {data.map((item, index) => (
          <Card key={index} {...item} />
        ))}
      </FlexWrapper>
    </React.Fragment>
  )
}

export default AreasNaturales
