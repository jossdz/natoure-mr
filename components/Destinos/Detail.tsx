import React from 'react'
import { RiShareLine, RiHeart3Fill } from 'react-icons/ri'
import Link from 'next/link'

import { DetailTag, ImageCover } from '../../styles/destinos'
import { FlexWrapper } from '../../styles/general/FlexWrapper'
import { Video, BasicInfo, WeatherAndEcosistems, CardsCarousel, Map } from './detail/index'
import InfoCard from '../../styles/components/InfoCard'
import GenericCard from './cards/GenericCard'
import ExperienceCard from '../Explore/components/ExperienceCard'
import Card from './Card'

const backgroundURL = '/assets/naturaleza.jpg'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const Detail = ({ data }): JSX.Element => {
  const { coverMedia } = data
  //change for real data
  const floraArray = [
    {
      title: 'Flor',
      subtitle: 'Species',
      image:
        'https://detallesamatista.com/blog/wp-content/uploads/2018/01/hermosas-flores-envio-caja-tulipanes-delivey.jpg',
      link: '/ruta',
    },
    {
      title: 'Flor',
      subtitle: 'Species',
      image:
        'https://detallesamatista.com/blog/wp-content/uploads/2018/01/hermosas-flores-envio-caja-tulipanes-delivey.jpg',
      link: '/ruta',
    },
    {
      title: 'Flor',
      subtitle: 'Species',
      image:
        'https://detallesamatista.com/blog/wp-content/uploads/2018/01/hermosas-flores-envio-caja-tulipanes-delivey.jpg',
      link: '/ruta',
    },
    {
      title: 'Flor',
      subtitle: 'Species',
      image:
        'https://detallesamatista.com/blog/wp-content/uploads/2018/01/hermosas-flores-envio-caja-tulipanes-delivey.jpg',
      link: '/ruta',
    },
  ]
  const faunaArray = [
    {
      title: 'Animal',
      subtitle: 'Species',
      image: 'https://dam.ngenespanol.com/wp-content/uploads/2021/02/GettyImages-1033902830.jpg',
      link: '/ruta',
    },
    {
      title: 'Animal',
      subtitle: 'Species',
      image: 'https://dam.ngenespanol.com/wp-content/uploads/2021/02/GettyImages-1033902830.jpg',
      link: '/ruta',
    },
    {
      title: 'Animal',
      subtitle: 'Species',
      image: 'https://dam.ngenespanol.com/wp-content/uploads/2021/02/GettyImages-1033902830.jpg',
      link: '/ruta',
    },
    {
      title: 'Animal',
      subtitle: 'Species',
      image: 'https://dam.ngenespanol.com/wp-content/uploads/2021/02/GettyImages-1033902830.jpg',
      link: '/ruta',
    },
  ]
  const prioritariosArray = [
    {
      title: 'Animal',
      subtitle: 'Species',
      image: 'https://newsweekespanol.com/wp-content/uploads/2019/04/000_1FY6AJ.jpg',
      link: '/ruta',
    },
    {
      title: 'Animal',
      subtitle: 'Species',
      image:
        'https://i1.wp.com/www.florestore.com/flores-a-domicilio/wp-content/uploads/2016/07/regalar-girasoles-flor-mes-florestore.jpg?fit=846%2C529&ssl=1',
      link: '/ruta',
    },
    {
      title: 'Animal',
      subtitle: 'Species',
      image: 'https://newsweekespanol.com/wp-content/uploads/2019/04/000_1FY6AJ.jpg',
      link: '/ruta',
    },
    {
      title: 'Animal',
      subtitle: 'Species',
      image:
        'https://i1.wp.com/www.florestore.com/flores-a-domicilio/wp-content/uploads/2016/07/regalar-girasoles-flor-mes-florestore.jpg?fit=846%2C529&ssl=1',
      link: '/ruta',
    },
  ]
  const atractivosNaturalesArray = [
    {
      title: 'Animal',
      subtitle: 'Species',
      image:
        'https://www.bmc-switzerland.com/media/catalog/category/BMC_Parent_Category_Header_Image_Mountain_All_Mountain_1.jpg',
      link: '/ruta',
    },
    {
      title: 'Animal',
      subtitle: 'Species',
      image:
        'https://www.bmc-switzerland.com/media/catalog/category/BMC_Parent_Category_Header_Image_Mountain_All_Mountain_1.jpg',
      link: '/ruta',
    },
    {
      title: 'Animal',
      subtitle: 'Species',
      image:
        'https://www.bmc-switzerland.com/media/catalog/category/BMC_Parent_Category_Header_Image_Mountain_All_Mountain_1.jpg',
      link: '/ruta',
    },
    {
      title: 'Animal',
      subtitle: 'Species',
      image:
        'https://www.bmc-switzerland.com/media/catalog/category/BMC_Parent_Category_Header_Image_Mountain_All_Mountain_1.jpg',
      link: '/ruta',
    },
  ]
  const atractivosCulturalesArray = [
    {
      title: 'Animal',
      subtitle: 'Species',
      image: 'https://www.freevector.com/uploads/vector/preview/2099/FreeVector-Mexican-Skull.jpg',
      link: '/ruta',
    },
    {
      title: 'Animal',
      subtitle: 'Species',
      image: 'https://www.freevector.com/uploads/vector/preview/2099/FreeVector-Mexican-Skull.jpg',
      link: '/ruta',
    },
    {
      title: 'Animal',
      subtitle: 'Species',
      image: 'https://www.freevector.com/uploads/vector/preview/2099/FreeVector-Mexican-Skull.jpg',
      link: '/ruta',
    },
    {
      title: 'Animal',
      subtitle: 'Species',
      image: 'https://www.freevector.com/uploads/vector/preview/2099/FreeVector-Mexican-Skull.jpg',
      link: '/ruta',
    },
  ]
  const experienciasArray = [
    {
      mainPhoto: [
        'https://www.bmc-switzerland.com/media/catalog/category/BMC_Parent_Category_Header_Image_Mountain_All_Mountain_1.jpg',
      ],
      experienceName: 'Experience',
      personCost: '10',
      _id: '',
      experienceTime: '1',
      daysHours: '1',
      orgId: {
        sustentabilityPromedy: 4.5,
        orgState: 'CDMX',
      },
      experienceMacros: ['Biocultural'],
    },
    {
      mainPhoto: [
        'https://www.bmc-switzerland.com/media/catalog/category/BMC_Parent_Category_Header_Image_Mountain_All_Mountain_1.jpg',
      ],
      experienceName: 'Experience',
      personCost: '10',
      _id: '',
      experienceTime: '1',
      daysHours: '1',
      orgId: {
        sustentabilityPromedy: 4.5,
        orgState: 'CDMX',
      },
      experienceMacros: ['Biocultural'],
    },
    {
      mainPhoto: [
        'https://www.bmc-switzerland.com/media/catalog/category/BMC_Parent_Category_Header_Image_Mountain_All_Mountain_1.jpg',
      ],
      experienceName: 'Experience',
      personCost: '10',
      _id: '',
      experienceTime: '1',
      daysHours: '1',
      orgId: {
        sustentabilityPromedy: 4.5,
        orgState: 'CDMX',
      },
      experienceMacros: ['Biocultural'],
    },
    {
      mainPhoto: [
        'https://www.bmc-switzerland.com/media/catalog/category/BMC_Parent_Category_Header_Image_Mountain_All_Mountain_1.jpg',
      ],
      experienceName: 'Experience',
      personCost: '10',
      _id: '',
      experienceTime: '1',
      daysHours: '1',
      orgId: {
        sustentabilityPromedy: 4.5,
        orgState: 'CDMX',
      },
      experienceMacros: ['Biocultural'],
    },
  ]
  const productsArray = [
    {
      title: 'Producto',
      subtitle: '...',
      image:
        'https://i0.wp.com/mas-mexico.com.mx/wp-content/uploads/2018/03/trompo.png?resize=1200%2C675&ssl=1',
      link: '/ruta',
    },
    {
      title: 'Producto',
      subtitle: '...',
      image:
        'https://i0.wp.com/mas-mexico.com.mx/wp-content/uploads/2018/03/trompo.png?resize=1200%2C675&ssl=1',
      link: '/ruta',
    },
    {
      title: 'Producto',
      subtitle: '...',
      image:
        'https://i0.wp.com/mas-mexico.com.mx/wp-content/uploads/2018/03/trompo.png?resize=1200%2C675&ssl=1',
      link: '/ruta',
    },
    {
      title: 'Producto',
      subtitle: '...',
      image:
        'https://i0.wp.com/mas-mexico.com.mx/wp-content/uploads/2018/03/trompo.png?resize=1200%2C675&ssl=1',
      link: '/ruta',
    },
  ]
  const anfitrionesArray = [
    {
      name: 'Anfitrion',
      subtitle: '...',
      gallery: [
        'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/famous-hispanic-people-danny-trejo-1598643850.jpg',
      ],
      link: '/ruta',
    },
    {
      name: 'Anfitrion',
      subtitle: '...',
      gallery: [
        'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/famous-hispanic-people-danny-trejo-1598643850.jpg',
      ],
      link: '/ruta',
    },
    {
      name: 'Anfitrion',
      subtitle: '...',
      gallery: [
        'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/famous-hispanic-people-danny-trejo-1598643850.jpg',
      ],
      link: '/ruta',
    },
    {
      name: 'Anfitrion',
      subtitle: '...',
      gallery: [
        'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/famous-hispanic-people-danny-trejo-1598643850.jpg',
      ],
      link: '/ruta',
    },
  ]

  const isVideo = (mediaString) => {
    return mediaString?.includes('mp4')
  }

  return (
    <React.Fragment>
      <FlexWrapper align="center">
        <DetailTag>Área Natural</DetailTag>
        <FlexWrapper align="flext-start">
          <div style={{ width: '50%' }}>
            <RiShareLine fontSize="1.5rem" />
          </div>
          <div style={{ width: '50%', textAlign: 'center' }}>
            <RiHeart3Fill fontSize="1.5rem" />
            <p style={{ marginTop: '0' }}>100K</p>
          </div>
        </FlexWrapper>
      </FlexWrapper>
      {isVideo(coverMedia) ? <Video video={''} /> : <ImageCover url={coverMedia} />}
      <BasicInfo {...data} />
      <WeatherAndEcosistems />
      {/* flora */}
      <CardsCarousel label="FLORA" array={floraArray} Card={GenericCard} />
      {/* fauna */}
      <CardsCarousel label="FAUNA" array={faunaArray} Card={GenericCard} />
      <InfoCard backgroundURL={backgroundURL}>
        <div>
          Todas las AreasNaturales que viviras con Natoure son <br />
          <b>AreasNaturales únicas que ayudan a conservar el medio ambiente</b>
        </div>
      </InfoCard>
      {/* prioritarias */}
      <CardsCarousel label="ESPECIES PRIORITARIAS" array={prioritariosArray} Card={GenericCard} />
      {/* atractivos naturales */}
      <CardsCarousel
        label="ATRACTIVOS NATURALES"
        array={atractivosNaturalesArray}
        Card={GenericCard}
      />
      {/* atractivos culturales */}
      <CardsCarousel
        label="ATRACTIVOS CULTURALES"
        array={atractivosCulturalesArray}
        Card={GenericCard}
      />
      {/* LOCATION  change component*/}
      <CardsCarousel label="Ubicación" array={[]} Card={<div />} />
      <Map {...data} />
      {/* experiencias */}
      <CardsCarousel
        label="Experiencias Imperdibles"
        array={experienciasArray}
        Card={ExperienceCard}
      />
      {/* productos */}
      <CardsCarousel label="Productos" array={productsArray} Card={GenericCard} />
      {/* Anfitriones locales */}
      <CardsCarousel label="Anfitriones locales" array={anfitrionesArray} Card={Card} />
      {/* Experiencias virtuales */}
      <CardsCarousel
        label="Experiencias virtuales"
        array={experienciasArray}
        Card={ExperienceCard}
      />
      <div className="proyectos"></div>
      <div className="plan"></div>
      <div className="normas"></div>
      <div className="reseñas"></div>
    </React.Fragment>
  )
}

export default Detail
