import React from 'react'
import { RiMapPin2Line } from 'react-icons/ri'
import Link from 'next/link'

import {
  CardWrapper,
  CardText,
  CardSubText,
  CardSubText2,
  CardImage,
} from '../../../styles/components/Cards/GenericCard'

export type CardProps = {
  image: string
  title: string
  _id: string
  subtitle: string
  link: string
}

const logo = '/assets/logo-white.png'
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const GenericCard = ({ image, title, subtitle, link }: CardProps): JSX.Element => {
  return (
    <CardWrapper>
      <CardImage image={image || logo} />
      <Link href={link}>
        <React.Fragment>
          <CardSubText>{title}</CardSubText>
          <CardSubText2>{subtitle}</CardSubText2>
        </React.Fragment>
      </Link>
    </CardWrapper>
  )
}

export default GenericCard
