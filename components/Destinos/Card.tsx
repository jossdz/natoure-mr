import React from 'react'
import { RiMapPin2Line } from 'react-icons/ri'
import Link from 'next/link'

import { CardWrapper, CardText, CardSubText, CardSubText2, CardCover } from '../../styles/destinos'
import { FlexWrapper, IconWrapper } from '../../styles/checkout'
import { capitalize } from '../../utils'

const logo = '/assets/logo-white.png'
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const Card = ({
  gallery,
  coverMedia,
  name,
  _id,
  location,
  distance = 'a 10hrs de tu ubicación',
  isNaturalArea,
}: any): JSX.Element => {
  return (
    <CardWrapper image={gallery[0] || coverMedia || logo}>
      <CardCover>
        <Link href={isNaturalArea ? `/natural-areas/${_id}` : `/destinations/${_id}`}>
          <CardText>{capitalize(name)}</CardText>
        </Link>
        <FlexWrapper justify="flex-start">
          <IconWrapper marginRight="8px">
            <RiMapPin2Line />
          </IconWrapper>
          <CardSubText>{location?.city}</CardSubText>
        </FlexWrapper>
        <CardSubText2>{distance}</CardSubText2>
      </CardCover>
    </CardWrapper>
  )
}

export default Card
