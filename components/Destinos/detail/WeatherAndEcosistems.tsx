import React from 'react'
import { WiCloudy, WiDayCloudyWindy, WiDaySunny, WiDayCloudy } from 'react-icons/wi'
import { RiAncientGateFill } from 'react-icons/ri'

import Link from 'next/link'

import { CardBox } from '../../../styles/destinos'
import { Label } from '../../../styles/forms'
import { FlexWrapper } from '../../../styles/general/FlexWrapper'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const WeatherAndEcosistems = (): JSX.Element => {
  return (
    <React.Fragment>
      <FlexWrapper justify="space-around" align="flex-start" style={{ margin: '20px 0' }}>
        <div style={{ width: '40%' }}>
          <Label title={'Clima Actual'} />
          <div style={{ width: '100%', textAlign: 'center' }}>
            <WiCloudy size="6rem" />
            <p>Parcialmente Nublado</p>
            <p>27ºC / 81ºF</p>
            <p>Hoy Viernes 22 de mayo</p>
            <FlexWrapper>
              <div>
                <WiCloudy size="2rem" />
                <p>Sab</p>
              </div>
              <div>
                <WiDayCloudyWindy size="2rem" />
                <p>Dom</p>
              </div>
              <div>
                <WiDaySunny size="2rem" />
                <p>Lun</p>
              </div>
              <div>
                <WiDayCloudy size="2rem" />
                <p>Mar</p>
              </div>
            </FlexWrapper>
          </div>
        </div>
        <div
          style={{
            width: '40%',
            display: 'flex',
            flexWrap: 'wrap',
            justifyContent: 'space-around',
          }}
        >
          <Label title={'Condiciones Físicas'} />
          <CardBox>
            <RiAncientGateFill />
            <p>Provisión</p>
          </CardBox>
          <CardBox>
            <RiAncientGateFill />
            <p>Regulación</p>
          </CardBox>
          <CardBox>
            <RiAncientGateFill />
            <p>Cultural</p>
          </CardBox>
          <CardBox color="var(--green-natoure)">
            <RiAncientGateFill />
            <p>Cultural</p>
          </CardBox>
          <CardBox color="var(--green-natoure)">
            <RiAncientGateFill />
            <p>Cultural</p>
          </CardBox>
          <CardBox color="var(--green-natoure)">
            <RiAncientGateFill />
            <p>Cultural</p>
          </CardBox>
        </div>
      </FlexWrapper>
    </React.Fragment>
  )
}

export default WeatherAndEcosistems
