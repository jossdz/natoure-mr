import React from 'react'
import Link from 'next/link'

import { Label } from '../../../styles/forms'
import { FlexWrapper } from '../../../styles/general/FlexWrapper'
import HorizontalS from '../../../styles/general/HorizontalSlide'
import HorizontalSlide from '../../../styles/general/HorizontalSlide'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const CardsCarousel = ({ array, label, Card }): JSX.Element => {
  return (
    <div style={{ marginBottom: '30px' }}>
      <Label title={label} />
      <HorizontalSlide>
        {array?.map((data, key) => (
          <Card key={key} {...data} />
        ))}
      </HorizontalSlide>
    </div>
  )
}

export default CardsCarousel
