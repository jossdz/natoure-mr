import React from 'react'
import { RiMapPinLine, RiTeamFill, RiUser3Fill, RiTempColdLine } from 'react-icons/ri'
import Link from 'next/link'

import { Logo, Tag, GalleryImg } from '../../../styles/destinos'
import { Label } from '../../../styles/forms'
import { FlexWrapper } from '../../../styles/general/FlexWrapper'
import HorizontalS from '../../../styles/general/HorizontalSlide'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const BasicInfo = ({
  name,
  conservationType,
  logo,
  predominantEcosystems,
  adminName,
}: any): JSX.Element => {
  return (
    <React.Fragment>
      <FlexWrapper justify="space-between" align="flex-start">
        <div>
          <Label title={<Logo url={logo} />} />
        </div>
        {/* basic data */}
        <div style={{ width: '50%', margin: '0 1rem' }}>
          <h2 style={{ margin: 0 }}>{name}</h2>
          <div>
            <span style={{ marginRight: '.5rem' }}>
              {' '}
              <RiMapPinLine />
              CDMX
            </span>
            {/* <span>a 10 hrs de tu ubicación</span> */}
          </div>
          <div>
            <b>Área de conservación administrada por:</b> {adminName}
          </div>
          <div>
            <b>Tipos de área:</b>
            <ul>
              {conservationType?.map((el, key) => (
                <li key={key}> {el}</li>
              ))}
            </ul>
          </div>
          <div>
            <b>Ecosistemas:</b>

            <ul>
              {predominantEcosystems?.map((el, key) => (
                <li key={key}> {el}</li>
              ))}
            </ul>
          </div>
          <div>
            <b>Pueblos originarios / Comunidades indígenas:</b>

            {/* map real info */}
            <ul>
              {[1, 2, 3].map((el, key) => (
                <Tag key={key} color="#68e1a9">
                  Pueblo {el}
                </Tag>
              ))}
            </ul>
          </div>
          <div>
            <b>Lenguas:</b>
            <ul>
              {/* map real info */}
              {[1, 2, 3].map((el, key) => (
                <Tag key={key} color="#e2d059">
                  Pueblo {el}
                </Tag>
              ))}
            </ul>
          </div>
          <p>
            {' '}
            <RiMapPinLine color="var(--green-natoure)" style={{ marginRight: '.5rem' }} />
            <b>Horario de atención:</b> 9:00 - 20:00
          </p>
          <p>
            {' '}
            <RiUser3Fill color="var(--green-natoure)" style={{ marginRight: '.5rem' }} />
            <b>Capacidad de carga diaria:</b> 100 personas
          </p>
          <p>
            {' '}
            <RiTeamFill color="var(--green-natoure)" style={{ marginRight: '.5rem' }} />
            <b>Grupo máximo permitido:</b> 10 personas
          </p>
        </div>
        {/* basics 2 */}
        <div style={{ width: '40%' }}>
          <HorizontalS style={{ marginBottom: '8px' }}>
            <GalleryImg src="https://cdn.pixabay.com/photo/2020/12/05/20/04/parque-5807041_960_720.jpg" />
            <GalleryImg src="https://concepto.de/wp-content/uploads/2015/03/naturaleza-medio-ambiente-e1505407093531-1.jpeg" />
            <GalleryImg src="https://mymodernmet.com/wp/wp-content/uploads/2021/04/Nature-Sounds-For-Well-Being-03.jpg" />
          </HorizontalS>
          <FlexWrapper
            justify="space-between"
            align="center"
            style={{
              padding: '16px',
              background: 'var(--dark-blue)',
              color: 'white',
              borderRadius: '5px',
              marginBottom: '8px',
            }}
          >
            <p style={{ fontSize: '.8rem' }}>Reserva tu acceso al Área:</p>
            <button
              style={{
                height: '60px',
                width: '100px',
                padding: '8px 16px',
                background: 'var(--pink)',
                border: 'none',
                color: 'white',
                borderRadius: '5px',
              }}
            >
              <p style={{ margin: '0' }}>
                <b>$100</b>
              </p>
              <span style={{ fontSize: '.5rem' }}>Reserva Ahora</span>
            </button>
          </FlexWrapper>
          <div>
            <Label title={'Condiciones Físicas'} />
            <FlexWrapper justify="flex-start">
              <RiTempColdLine fontSize="3.5rem" />
              <div>
                <h4 style={{ margin: '0' }}>Temperatura</h4>
                <p>5˚|----------| 10˚</p>
              </div>
            </FlexWrapper>
            <FlexWrapper justify="flex-start">
              <RiTempColdLine fontSize="3.5rem" />
              <div>
                <h4 style={{ margin: '0' }}>Altitud (msnm)</h4>
                <p>2000|----------| 3000</p>
              </div>
            </FlexWrapper>
            <FlexWrapper justify="flex-start">
              <RiTempColdLine fontSize="3.5rem" />
              <div>
                <h4 style={{ margin: '0' }}>Extensión total:</h4>
                <p>5˚|----------| 10˚</p>
              </div>
            </FlexWrapper>
          </div>
        </div>
      </FlexWrapper>
    </React.Fragment>
  )
}

export default BasicInfo
