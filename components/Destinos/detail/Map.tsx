import React, { useRef, useEffect, useState } from 'react'
import { MapPageWrapper, MapContainer } from '../../../styles/MapPage/styles'
import mapboxgl from 'mapbox-gl'
import { FlexWrapper } from '../../../styles/general/FlexWrapper'
// import { MapFilterDrawer } from '../../MapFilterDrawer'

mapboxgl.accessToken =
  'pk.eyJ1IjoibWx6eiIsImEiOiJjandrNmVzNzUwNWZjNGFqdGcwNmJ2ZWhpIn0.ybY6wnAtJwj-Tq0c46sW6A'

function Map({ location, closesBusStation, closestAirport, closestCity }): JSX.Element {
  const mapContainer = useRef(null)

  useEffect(() => {
    const map = new mapboxgl.Map({
      container: mapContainer.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: location?.coordinates || [0, 0],
      zoom: 5,
    })
    const marker = new mapboxgl.Marker().setLngLat(location?.coordinates || [0, 0]).addTo(map)
  }, [location])

  return (
    <div>
      <MapPageWrapper style={{ width: '100%', height: '350px', marginBottom: '20px' }}>
        <MapContainer ref={mapContainer}></MapContainer>
      </MapPageWrapper>
      <FlexWrapper justify="space-around">
        <div style={{ width: '45%' }}>
          <b>
            <p>Dirección</p>
          </b>
          <p style={{ fontSize: '.8rem' }}>{location?.address}</p>
        </div>
        <div style={{ width: '45%' }}>
          <b>
            <p>Ciudad más cercana</p>
          </b>
          <p style={{ fontSize: '.8rem' }}>{closestCity?.address}</p>
        </div>
      </FlexWrapper>

      <FlexWrapper justify="space-around">
        <div style={{ width: '45%' }}>
          <b>
            <p>Aeropuerto más cercano</p>
          </b>
          <p style={{ fontSize: '.8rem' }}>{closestAirport?.address}</p>
        </div>
        <div style={{ width: '45%' }}>
          <b>
            <p>Autobús más cercano</p>
          </b>
          <p style={{ fontSize: '.8rem' }}>{closesBusStation?.address}</p>
        </div>
      </FlexWrapper>
    </div>
  )
}

export default Map
