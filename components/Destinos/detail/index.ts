import Video from './Video'
import BasicInfo from './BasicInfo'
import WeatherAndEcosistems from './WeatherAndEcosistems'
import CardsCarousel from './CardsCarousel'
import Map from './Map'

export { Video, BasicInfo, WeatherAndEcosistems, CardsCarousel, Map }
