import React from 'react'
import { RiShareLine, RiHeart3Fill, RiHeart3Line } from 'react-icons/ri'
import Link from 'next/link'

import { VideoContainer } from '../../../styles/destinos'
import { FlexWrapper } from '../../../styles/general/FlexWrapper'

const videosrc = '/assets/video.mp4'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const Video = ({ video }): JSX.Element => {
  return (
    <div style={{ margin: '10px 0' }}>
      <VideoContainer autoPlay playsInline muted controls>
        <source src={video || videosrc} />
      </VideoContainer>
    </div>
  )
}

export default Video
