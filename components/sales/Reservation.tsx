import React, { useState } from 'react'
import { AiOutlineClockCircle } from 'react-icons/ai'
import { RiShieldCrossFill, RiCheckFill } from 'react-icons/ri'
import { BsPeople } from 'react-icons/bs'
import { HiTranslate, HiLocationMarker } from 'react-icons/hi'
import {
  ReservationWrapper,
  ReservationHeaderStyled,
  ReservationDateTimeWrapper,
  ReservationDate,
  ReservationTime,
  ReserveThumbnail,
  ReservationBody,
  ReservationTitle,
  ReservationDetailsWrapper,
  ReservationDetails,
  ReservationContactInfo,
  ReservationClient,
  ReservationAvatar,
  ReservationClientName,
  ReservationAction,
  ReservationExtraInfo,
  ReservationFooter,
  ReservationTransport,
  ReservationMeetingPoint,
  ReservationFlexWrapper,
} from '../../styles/sales/Reservation'

type Reserve = {
  experience: any
  guest: any
  checkin: Date
  checkout?: Date
  guest_number: number
  paid: boolean
  status: string
  schedule: string
  ensurance: boolean
  specialRequirements?: string[]
  invoice: any
}

export const Reservation = ({
  experience,
  guest,
  checkin,
  guest_number,
  schedule,
  ensurance,
  status,
  specialRequirements,
}: Reserve): JSX.Element => {
  const [toggle, setToggle] = useState(false)
  const handleClick = () => setToggle((prev) => !prev)
  const months = [
    'enero',
    'febrero',
    'marzo',
    'abril',
    'mayo',
    'junio',
    'julio',
    'agosto',
    'septiembre',
    'octubre',
    'noviembre',
    'diciembre',
  ]

  return (
    <ReservationWrapper>
      <ReservationFlexWrapper>
        <ReservationHeaderStyled>
          <ReservationDateTimeWrapper>
            <ReservationDate>
              <span>{new Date(checkin).getDate()}</span>
              <span>{months[new Date(checkin).getMonth()]}</span>
            </ReservationDate>
            <ReservationTime>{schedule}</ReservationTime>
          </ReservationDateTimeWrapper>
          <ReserveThumbnail image={experience.mainPhoto[0]}>
            {experience.experienceName}
          </ReserveThumbnail>
        </ReservationHeaderStyled>

        <ReservationBody>
          <ReservationTitle>Detalles de la reservación:</ReservationTitle>
          <ReservationDetailsWrapper>
            <ReservationDetails>
              <BsPeople />
              <span>{`${guest_number} ${guest_number > 1 ? 'personas' : 'persona'}`} </span>
            </ReservationDetails>
            <ReservationDetails>
              <HiTranslate />
              <span>Español</span>
            </ReservationDetails>
            <ReservationDetails title="Duración">
              <AiOutlineClockCircle />
              <span>{`${experience.experienceTime} ${
                experience.experienceDays.length > 0 ? 'días' : 'horas'
              }`}</span>
            </ReservationDetails>
          </ReservationDetailsWrapper>

          <ReservationTitle>Reservado por:</ReservationTitle>
          <ReservationContactInfo>
            <ReservationClient>
              <ReservationAvatar>
                {`${guest.name.charAt(0)}${guest.last_name.charAt(0)}`}
              </ReservationAvatar>
              <ReservationClientName>{`${guest.name} ${guest.last_name}`}</ReservationClientName>
            </ReservationClient>
            {status === 'Pendiente' && (
              <ReservationAction>
                Aprobar
                <RiCheckFill />
              </ReservationAction>
            )}
          </ReservationContactInfo>
        </ReservationBody>
      </ReservationFlexWrapper>

      <ReservationExtraInfo isExpanded={toggle}>
        <ReservationFlexWrapper>
          {ensurance && (
            <div>
              <ReservationTitle>También incluye:</ReservationTitle>
              <ReservationTransport>
                <RiShieldCrossFill />
                <ReservationTitle removeMargins>Incluye seguro de viaje</ReservationTitle>
              </ReservationTransport>
            </div>
          )}

          <div>
            <ReservationMeetingPoint>
              <ReservationTitle>Punto de encuentro:</ReservationTitle>
              <div>
                <HiLocationMarker />
                <span>{experience.address.address}</span>
              </div>
            </ReservationMeetingPoint>
            {specialRequirements && (
              <div>
                <ReservationTitle>Requerimientos especiales:</ReservationTitle>
                {specialRequirements.map((item, index) => (
                  <p key={index}>{item}</p>
                ))}
              </div>
            )}
          </div>
        </ReservationFlexWrapper>
      </ReservationExtraInfo>
      <ReservationFooter onClick={handleClick}>
        Ver {toggle ? 'menos' : 'más'} información
      </ReservationFooter>
    </ReservationWrapper>
  )
}
