import React, { useState } from 'react'
import {
  MapFiltersStyled,
  ChipsWrapper,
  FilterChip,
  PlacesWrapper,
} from '../../styles/MapPage/styles'
import { Chip } from '../common'
import { BsChevronLeft } from 'react-icons/bs'
import { GoSettings } from 'react-icons/go'
import Link from 'next/link'

import TagCard from '../../styles/experience/tags'

import { macros } from '../../steppers/data'

type MapFiltersProps = {
  setMacro: (macro: string) => void
  macro?: string
  tag?: string
  setTag: (tag: string) => void
}

export const MapPageFilter = ({ macro, tag, setMacro, setTag }: MapFiltersProps): JSX.Element => {
  return (
    <MapFiltersStyled>
      {/* <Tabs /> */}
      <PlacesWrapper>
        <div>
          <Link href="/explora">
            <span>
              <BsChevronLeft />
            </span>
          </Link>
          <TagCard
            width="100%"
            tag={Object.keys(macros)}
            activeTag={macro}
            onClick={(item) => setMacro(item)}
          />
        </div>
        {macro.length !== 0 && (
          <TagCard
            secondary
            width="100%"
            tag={macros[macro]}
            activeTag={tag}
            onClick={(item) => setTag(item)}
          />
        )}
      </PlacesWrapper>

      {/* <FilterChip onClick={onOpenFilters}>
        Filtros{' '}
        <span>
          <GoSettings />
        </span>
      </FilterChip> */}
    </MapFiltersStyled>
  )
}
