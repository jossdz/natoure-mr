import React, { useRef, useEffect, useState } from 'react'
import { MapPageWrapper, MapContainer } from '../../styles/MapPage/styles'
import mapboxgl from 'mapbox-gl'
import { MapPageFilter } from './MapFilters'
import { MapCards } from './MapCards'
import { MapFilterDrawer } from './MapFilterDrawer'
import { getAllExperiences } from '../../pages/api/api'

mapboxgl.accessToken =
  'pk.eyJ1IjoibWx6eiIsImEiOiJjandrNmVzNzUwNWZjNGFqdGcwNmJ2ZWhpIn0.ybY6wnAtJwj-Tq0c46sW6A'

export default function MapPage({ isToggled }: { isToggled?: boolean }): JSX.Element {
  const mapContainer = useRef(null)
  const [swiper, setSwiper] = useState(null)
  const [mapInstance, setMapInstance] = useState(null)
  const [mapElements, setMapElements] = useState(null)
  const [experiences, setExperiences] = useState([])

  const [macro, setMacro] = useState('')
  const [tag, setTag] = useState('')
  const [isOpen, setIsOpen] = useState(false)
  const toggleDrawer = () => setIsOpen((prev) => !prev)

  const handleMarkerClick = (index) => {
    swiper.slideTo(index)
  }

  const handleTagSelection = (tag) => setTag(tag)
  const handleMacroSelection = (macro) => {
    setTag('')
    setMacro(macro)
  }

  const onActiveIndexChange = (swiper) => {
    mapElements[swiper.previousIndex].popup.remove()
    mapElements[swiper.activeIndex].popup.addTo(mapInstance)
  }

  const storeSwiper = (swiper) => setSwiper(swiper)

  useEffect(() => {
    const map = new mapboxgl.Map({
      container: mapContainer.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [-99.14212556250001, 19.43611103125],
      zoom: 5,
    })
    setMapInstance(map)
  }, [])

  useEffect(() => {
    if (mapElements) mapElements.forEach((mapObj) => mapObj.marker.remove())
    if (mapInstance && experiences) {
      const mapElements = experiences.map((experience, index) => {
        const popup = new mapboxgl.Popup()
          .setHTML(
            `<div class="popup-container">
          <img class="popup-img" src="${experience.mainPhoto[0]}" />
          <div class="popup-info">
            <div class="popup-title">${experience.experienceName}</div>
            <div>${experience.address.address}</div>
            <a class="popup-link" href="/experience/${experience._id}">Ver experiencia</a>
          </div>
        </div>`
          )
          .on('open', () => {
            handleMarkerClick(index)
          })
        const marker = new mapboxgl.Marker({ color: 'green' })
          .setLngLat(experience.address.coords)
          .setPopup(popup)
        marker.addTo(mapInstance)

        return { marker, popup }
      })
      setMapElements(mapElements)
    }
  }, [experiences])

  useEffect(() => {
    let query = ''
    if (macro !== '') query = `?experienceMacros=${macro}`
    if (tag !== '') query = `${query}&experienceActivities=${tag}`
    getAllExperiences(query)
      .then(({ experiences }) => {
        setExperiences(experiences)
      })
      .catch((err) => console.error(err))
  }, [macro, tag])

  return (
    <MapPageWrapper isToggled={isToggled}>
      <MapFilterDrawer isOpen={isOpen} handleOnClose={toggleDrawer} />
      <MapPageFilter
        tag={tag}
        macro={macro}
        setTag={handleTagSelection}
        setMacro={handleMacroSelection}
      />
      <MapContainer ref={mapContainer}></MapContainer>
      <MapCards
        experineces={experiences}
        storeSwiper={storeSwiper}
        onActiveIndexChange={onActiveIndexChange}
      />
    </MapPageWrapper>
  )
}
