/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react'
import { Swiper, SwiperSlide } from 'swiper/react'
import { CardWrapper, Card, CardOverlay } from '../../styles/MapPage/styles'
import Link from 'next/link'

type MapCardsProps = {
  experineces: any[]
  storeSwiper: (swiper: any) => void
  onActiveIndexChange: (swiper: any) => void
}

export const MapCards = ({
  experineces,
  storeSwiper,
  onActiveIndexChange,
}: MapCardsProps): JSX.Element => {
  return (
    <CardWrapper>
      <Swiper
        slidesPerView="auto"
        onSwiper={storeSwiper}
        centeredSlides
        spaceBetween={16}
        allowTouchMove
        onActiveIndexChange={onActiveIndexChange}
      >
        {experineces.map((item) => (
          <SwiperSlide key={Math.random()}>
            <Card imageUrl={item.mainPhoto[0]}>
              <CardOverlay>
                <Link href={`/experience/${item._id}`}>
                  <p>{item.experienceName}</p>
                </Link>
              </CardOverlay>
            </Card>
          </SwiperSlide>
        ))}
      </Swiper>
    </CardWrapper>
  )
}
