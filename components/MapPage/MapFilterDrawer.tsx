import React from 'react'
import { Drawer } from '../common/Drawer'
import { Button, Select, Checkbox, Input, SmallText } from '../../styles/forms'
import { GrClose } from 'react-icons/gr'
import ReactCountryFlag from 'react-country-flag'
import { FlexWrapper, InputWrapper, FlagWrapper } from '../../styles/MapPage/styles'
import { Radio } from '../common/Radio'

type MapDrawerProps = { isOpen: boolean; handleOnClose: () => void }

export const MapFilterDrawer = ({ isOpen, handleOnClose }: MapDrawerProps): JSX.Element => {
  return (
    <Drawer isOpen={isOpen} onClose={handleOnClose}>
      <Drawer.Header>
        <h6>Filtros</h6>
        <Drawer.HeaderActions>
          <button onClick={handleOnClose}>
            <GrClose />
          </button>
        </Drawer.HeaderActions>
      </Drawer.Header>
      <Drawer.Content>
        <Drawer.ContentSection>
          <FlexWrapper>
            <FlagWrapper>
              <ReactCountryFlag
                countryCode="MX"
                title="MX"
                svg
                style={{ width: '50px', height: '50px' }}
              />
            </FlagWrapper>
            <FlagWrapper>
              <ReactCountryFlag
                countryCode="MX"
                title="MX"
                svg
                style={{ width: '50px', height: '50px' }}
              />
            </FlagWrapper>
            <FlagWrapper isActive>
              <ReactCountryFlag
                countryCode="MX"
                title="MX"
                svg
                style={{ width: '50px', height: '50px' }}
              />
            </FlagWrapper>
            <FlagWrapper>
              <ReactCountryFlag
                countryCode="MX"
                title="MX"
                svg
                style={{ width: '50px', height: '50px' }}
              />
            </FlagWrapper>
            <FlagWrapper>
              <ReactCountryFlag
                countryCode="MX"
                title="MX"
                svg
                style={{ width: '50px', height: '50px' }}
              />
            </FlagWrapper>
          </FlexWrapper>
        </Drawer.ContentSection>
        <Drawer.ContentSection>
          <p>Estado</p>
          <Select name="state" options={[]} style={{ marginBottom: 40 }} />
        </Drawer.ContentSection>
        <Drawer.ContentSection>
          <p>Organizar por</p>
          <FlexWrapper direction="column">
            <Radio label="Sostenibilidad" name="organization" />
            <Radio label="Precio" name="organization" checked />
            <Radio label="disabled" name="organization" disabled />
          </FlexWrapper>
        </Drawer.ContentSection>
        <Drawer.ContentSection>
          <p>Precio</p>
          <FlexWrapper align="center">
            <InputWrapper>
              <SmallText
                style={{ width: 'max-content', margin: '0 12px 0 0' }}
                black
                title="Min."
              />
              <Input number style={{ width: 70 }} adjust name="ageRangeFirst" placeholder="$100" />
            </InputWrapper>
            <InputWrapper>
              <SmallText
                style={{ width: 'max-content', margin: '0 12px 0 0' }}
                black
                title="Max."
              />
              <Input
                number
                style={{ width: 70 }}
                adjust
                name="ageRangeSecond"
                placeholder="$10000"
              />
            </InputWrapper>
          </FlexWrapper>
        </Drawer.ContentSection>
        <Drawer.ContentSection>
          <p>Tipo de Experiencias</p>
          <FlexWrapper direction="column">
            <Radio label="Naturaleza" name="type" />
            <Radio label="Rural" name="type" checked />
            <Radio label="Aventura" name="type" disabled />
          </FlexWrapper>
        </Drawer.ContentSection>
        <Drawer.ContentSection>
          <p>Atributos Extra</p>
          <Checkbox name="extra-attr" options={['voluntariado']} />
        </Drawer.ContentSection>
      </Drawer.Content>
      <Drawer.Footer>
        <Button text="Aplicar" />
      </Drawer.Footer>
    </Drawer>
  )
}
