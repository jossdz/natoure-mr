import React, { useState } from 'react'
import { AiOutlineClockCircle } from 'react-icons/ai'
import { RiShieldCrossFill } from 'react-icons/ri'
import { BsPeople } from 'react-icons/bs'
import { HiTranslate, HiLocationMarker } from 'react-icons/hi'
import {
  ReservationWrapper,
  ReservationHeaderStyled,
  ReservationDateTimeWrapper,
  ReservationDate,
  ReservationTime,
  ReserveThumbnail,
  ReservationBody,
  ReservationTitle,
  ReservationDetailsWrapper,
  ReservationDetails,
  ReservationAction,
  ReservationExtraInfo,
  ReservationFooter,
  ReservationTransport,
  ReservationMeetingPoint,
  ReservationFlexWrapper,
  ReservationInfo,
} from '../../styles/sales/Reservation'

type Reserve = {
  experience: any
  guest: any
  checkin: Date
  checkout?: Date
  guest_number: number
  paid: boolean
  status: string
  schedule: string
  ensurance: boolean
  specialRequirements?: string[]
}

export const Reservation = ({
  experience,
  checkin,
  guest_number,
  schedule,
  ensurance,
  specialRequirements,
}: Reserve): JSX.Element => {
  const [toggle, setToggle] = useState(false)
  const handleClick = () => setToggle((prev) => !prev)
  const months = [
    'enero',
    'febrero',
    'marzo',
    'abril',
    'mayo',
    'junio',
    'julio',
    'agosto',
    'septiembre',
    'octubre',
    'noviembre',
    'diciembre',
  ]

  return (
    <ReservationWrapper>
      <ReservationFlexWrapper>
        <ReservationHeaderStyled traveler>
          <ReservationDateTimeWrapper>
            <ReservationDate traveler>
              <span>{new Date(checkin).getDate()}</span>
              <span>{months[new Date(checkin).getMonth()]}</span>
            </ReservationDate>
            <ReservationTime>{schedule}</ReservationTime>
          </ReservationDateTimeWrapper>
          <ReserveThumbnail traveler image={experience.mainPhoto[0]}>
            {experience.experienceName}
          </ReserveThumbnail>
        </ReservationHeaderStyled>

        <ReservationInfo>
          <ReservationFlexWrapper traveler>
            <ReservationBody traveler>
              <ReservationTitle>Detalles de la reservación:</ReservationTitle>
              <ReservationDetailsWrapper traveler>
                <ReservationDetails traveler>
                  <BsPeople />
                  <span>{`${guest_number} ${guest_number > 1 ? 'personas' : 'persona'}`} </span>
                </ReservationDetails>
                <ReservationDetails traveler>
                  <HiTranslate />
                  <span>Español</span>
                </ReservationDetails>
                <ReservationDetails traveler title="Duración">
                  <AiOutlineClockCircle />
                  <span>{`${experience.experienceTime} ${
                    experience.experienceDays.length > 0 ? 'días' : 'horas'
                  }`}</span>
                </ReservationDetails>
              </ReservationDetailsWrapper>
            </ReservationBody>

            <ReservationAction>Volver a reservar</ReservationAction>
          </ReservationFlexWrapper>
          <ReservationExtraInfo traveler isExpanded={toggle}>
            {ensurance && (
              <div>
                <ReservationTitle>También incluye:</ReservationTitle>
                <ReservationTransport>
                  <RiShieldCrossFill />
                  <ReservationTitle removeMargins>Incluye seguro de viaje</ReservationTitle>
                </ReservationTransport>
              </div>
            )}

            <div>
              <ReservationMeetingPoint>
                <ReservationTitle>Punto de encuentro:</ReservationTitle>
                <div>
                  <HiLocationMarker />
                  <span>{experience.address.address}</span>
                </div>
              </ReservationMeetingPoint>
              {specialRequirements && (
                <div>
                  <ReservationTitle>Requerimientos especiales:</ReservationTitle>
                  {specialRequirements.map((item, index) => (
                    <p key={index}>{item}</p>
                  ))}
                </div>
              )}
            </div>
          </ReservationExtraInfo>
          <ReservationFooter traveler onClick={handleClick}>
            Ver {toggle ? 'menos' : 'más'} información
          </ReservationFooter>
        </ReservationInfo>
      </ReservationFlexWrapper>
    </ReservationWrapper>
  )
}
