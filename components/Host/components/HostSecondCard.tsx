import React from 'react'
import { Card, CardOverlay } from '../../../styles/MapPage/styles'
import { FlexWrapper } from '../../../styles/checkout'
import Link from 'next/link'

export const HostSecondCard = ({ image, name, address, id }) => {
  return (
    <Card imageUrl={image} width="23%" widthMobile="48%">
      <CardOverlay>
        <FlexWrapper direction="column" align="flex-start">
          <Link href={`/anfitrion/${id}`}>
            <p>{name}</p>
          </Link>

          <span>{address}</span>
        </FlexWrapper>
      </CardOverlay>
    </Card>
  )
}
