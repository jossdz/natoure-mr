import React, { useState, useEffect } from 'react'
import Rating from 'react-rating'
import { FaStar, FaRegStar } from 'react-icons/fa'
import { RiLeafFill, RiLeafLine } from 'react-icons/ri'

const mockData = {
  ambiental: [
    { criteria: 'Ambiental', score: 4.5 },
    { criteria: 'Ambiental', score: 4.2 },
  ],
  social: [
    { criteria: 'Social', score: 4.5 },
    { criteria: 'Social', score: 4.7 },
  ],
  economico: [{ criteria: 'Económico', score: 4.5 }],
}

import { SectionHeading } from '../../styles/components'
import {
  RatingContainer,
  SostenibilityContainer,
  Score,
  SostenibilityRatingContainer,
  RatingTabs,
  RatingTabsItem,
  RatingTabsSection,
  RatingTabsContent,
  RatingCriteriaItem,
  ContentItemText,
  RatingTabsSectionFooter,
  RatingTabsSectionScore,
} from '../../styles/organization/StatsSectionRating'

const renderCriteria = (data) => {
  if (!data) return null
  return data.map((item, index) => (
    <RatingCriteriaItem key={`${item.criteria}-${index}`}>
      <ContentItemText>
        Criterio {item.criteria} {index + 1}
      </ContentItemText>
      <Rating
        emptySymbol={<RiLeafLine />}
        fullSymbol={<RiLeafFill />}
        fractions={2}
        initialRating={item.score}
        readonly
      />
      <Score color="#06bc68">{item.score}</Score>
    </RatingCriteriaItem>
  ))
}

export const StatsSectionRating = (): JSX.Element => {
  const [activeTab, setActiveTab] = useState('ambiental')
  const [tabCriterias, setTabCriterias] = useState(mockData.ambiental)
  const [sectionAvg, setSectionAvg] = useState(0)

  const handleTabs = (tabIndex) => setActiveTab(tabIndex)

  useEffect(() => {
    const selectedData = mockData[activeTab]
    const sum = mockData[activeTab].reduce((acc, item) => (acc += item.score), 0)
    const TabSectionAvgScore = sum / selectedData.length
    setSectionAvg(TabSectionAvgScore)
    setTabCriterias(selectedData)
  }, [activeTab])

  return (
    <React.Fragment>
      <SectionHeading title="Tu calificación" />
      <RatingContainer>
        <Rating
          emptySymbol={<FaRegStar />}
          fullSymbol={<FaStar />}
          fractions={2}
          initialRating={2.5}
        />
        <div>Servicio: 4.5</div>
      </RatingContainer>
      <SectionHeading title="Tu Ranking: #13" removeMarginTop />

      <SostenibilityContainer>
        <div>
          <div>Mi nivle de Sostenibilidad</div>
          <div>(basado en 73 calificaciones)</div>
        </div>
        <SostenibilityRatingContainer>
          <Rating
            emptySymbol={<RiLeafLine />}
            fullSymbol={<RiLeafFill />}
            fractions={2}
            initialRating={4.5}
            readonly
          />
          <Score>4.5</Score>
        </SostenibilityRatingContainer>
      </SostenibilityContainer>

      <RatingTabsSection>
        <RatingTabs>
          <RatingTabsItem
            onClick={() => handleTabs('ambiental')}
            isActive={activeTab === 'ambiental'}
          >
            Ambiental
          </RatingTabsItem>
          <RatingTabsItem onClick={() => handleTabs('social')} isActive={activeTab === 'social'}>
            Social
          </RatingTabsItem>
          <RatingTabsItem
            onClick={() => handleTabs('economico')}
            isActive={activeTab === 'economico'}
          >
            Económico
          </RatingTabsItem>
        </RatingTabs>
        <RatingTabsContent>{renderCriteria(tabCriterias)}</RatingTabsContent>
        <RatingTabsSectionFooter>
          <RatingTabsSectionScore>
            Nota Total:{' '}
            <Score color="white">
              <RiLeafFill />
              {sectionAvg}
            </Score>
          </RatingTabsSectionScore>
        </RatingTabsSectionFooter>
      </RatingTabsSection>
    </React.Fragment>
  )
}
