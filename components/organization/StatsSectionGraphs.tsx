import React from 'react'
import { GraphContainer, StatsGraphSection } from '../../styles/organization/StatsSectioGraphs'
import { Bar, Doughnut } from 'react-chartjs-2'

const ageMockData = {
  labels: ['18-20', '21-30', '31-40', '41-50', '51-60'],
  datasets: [
    {
      label: 'Edad promedio de tus viajeros',
      data: [12, 19, 3, 5, 2],
      backgroundColor: '#06bc68',
      borderColor: '#06bc68',
      borderWidth: 1,
    },
  ],
}

const countriesMockData = {
  labels: ['México', 'E.U.A', 'Chile', 'Canada'],
  datasets: [
    {
      label: 'Principales paises de proveniencia',
      data: [19, 3, 5, 2],
      backgroundColor: '#06bc68',
      borderColor: '#06bc68',
      borderWidth: 1,
    },
  ],
}

const genderMockData = {
  labels: ['Mujeres', 'Hombres'],
  datasets: [
    {
      label: 'Género de tus viajeros',
      data: [53, 47],
      backgroundColor: '#06bc68',
      borderColor: '#06bc68',
      borderWidth: 1,
    },
  ],
}

const activityTypeMockData = {
  labels: ['Aventura', 'Naturaleza'],
  datasets: [
    {
      label: 'Tipo de Viajeros',
      data: [80, 20],
      backgroundColor: ['#06bc68', '#f8c239'],
      borderColor: ['#06bc68', '#f8c239'],
      borderWidth: 1,
    },
  ],
}

const options = {
  maintainAspectRatio: false,
  scales: {
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
        },
      },
    ],
  },
}

export const StatsSectionGraphs = (): JSX.Element => {
  return (
    <StatsGraphSection>
      <GraphContainer>
        <Bar type="bar" data={ageMockData} options={options} />
      </GraphContainer>
      <GraphContainer>
        <Bar type="bar" data={countriesMockData} options={options} />
      </GraphContainer>
      <GraphContainer>
        <Bar type="bar" data={genderMockData} options={options} />
      </GraphContainer>
      <GraphContainer>
        <Doughnut type="doughnut" data={activityTypeMockData} />
      </GraphContainer>
    </StatsGraphSection>
  )
}
