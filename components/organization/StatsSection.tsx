import React from 'react'
import { SectionHeading } from '../../styles/components'
import { Select } from '../../styles/forms'
import { StatsSectionCard } from './StatsSectionCard'
import { StatsSectionRating } from './StatsSectionRating'
import { StatsSectionGraphs } from './StatsSectionGraphs'

export const StatsSection = (): JSX.Element => {
  return (
    <>
      <React.Fragment>
        <SectionHeading title="Periodo" />
        <Select name="state" options={[]} style={{ marginBottom: 40 }} />
      </React.Fragment>
      <StatsSectionCard />
      <StatsSectionRating />
      <StatsSectionGraphs />
    </>
  )
}
