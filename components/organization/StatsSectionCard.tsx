import React from 'react'
import {
  CardStyled,
  CardHeaderStyled,
  CardText,
  CardBodyStyled,
  CardReservationItem,
  CardReservationInfo,
} from '../../styles/organization/StatsSectionCard'

export const StatsSectionCard = (): JSX.Element => {
  return (
    <CardStyled>
      <CardHeaderStyled>
        <div>
          <CardText fontSize="12px" lineHeight="14px">
            Mayo-Junio
          </CardText>
          <CardText fontSize="14px" lineHeight="25px">
            Ganancia total:
          </CardText>
          <CardText fontSize="24px" lineHeight="31px">
            $18,000 m.n
          </CardText>
        </div>
        <div>
          <CardText fontSize="14px" lineHeight="25px">
            Reservaciones
          </CardText>
          <CardText fontSize="24px" lineHeight="31px">
            7
          </CardText>
        </div>
      </CardHeaderStyled>
      <CardBodyStyled>
        <CardReservationItem>
          <img
            src="https://images.unsplash.com/photo-1551913898-88fc3dbbe36e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80"
            alt="reservation-img"
          />
          <CardReservationInfo>
            <CardText fontSize="14px" lineHeight="25px" color="#2c375a" isBold>
              Experiencia
            </CardText>
            <CardText fontSize="14px" lineHeight="25px" color="#2c375a">
              Cosecha en Xochimilco
            </CardText>
            <CardText fontSize="14px" lineHeight="25px" color="#06bc68">
              5 de junio
            </CardText>
          </CardReservationInfo>
          <CardText fontSize="14px" lineHeight="25px" isBold color="#2c375a">
            + $530.00 MXN
          </CardText>
        </CardReservationItem>

        <CardReservationItem>
          <img
            src="https://images.unsplash.com/photo-1551913898-88fc3dbbe36e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80"
            alt="reservation-img"
          />
          <CardReservationInfo>
            <CardText fontSize="14px" lineHeight="25px" color="#2c375a" isBold>
              Experiencia
            </CardText>
            <CardText fontSize="14px" lineHeight="25px" color="#2c375a">
              Cosecha en Xochimilco
            </CardText>
            <CardText fontSize="14px" lineHeight="25px" color="#06bc68">
              5 de junio
            </CardText>
          </CardReservationInfo>
          <CardText fontSize="14px" lineHeight="25px" isBold color="#2c375a">
            + $530.00 MXN
          </CardText>
        </CardReservationItem>
      </CardBodyStyled>
    </CardStyled>
  )
}
