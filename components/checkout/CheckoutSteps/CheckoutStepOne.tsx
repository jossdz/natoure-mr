import React, { useState, useContext, useEffect } from 'react'
import { CheckoutCard } from '../CheckoutCard'
import {
  Divider,
  CheckoutSection,
  FlexWrapper,
  IconWrapper,
  CheckoutButton,
  CheckoutSubtitle,
  TextHighlighted,
} from '../../../styles/checkout'
import {
  Input as QuantityInput,
  CalendarFakeInputWrapper,
  DeparturePointWrapper,
  InfoText,
} from '../../../styles/checkout/StepOne'
import { Label, Select, Switch, Input } from '../../../styles/forms'
import { BsFillPeopleFill } from 'react-icons/bs'
import { FaMinus, FaPlus } from 'react-icons/fa'
import DatePicker, { registerLocale } from 'react-datepicker'
import es from 'date-fns/locale/es'
import format from 'date-fns/format'
import { Radio } from '../../common/Radio'
import { RiMapPinLine } from 'react-icons/ri'
import { RadioBox } from '../../common'
import { Container } from '../../../styles/components'
import { AppContext } from '../../../context/AppContext'
import { useRouter } from 'next/router'
import { CheckoutFooter } from '../CheckoutFooter'

registerLocale('es', es)

type PropsCal = {
  onClick?: () => void
  value?: string
}

type RefType = number

type StepProps = {
  onNextStep: (step: number) => void
}

export const CheckoutStepOne = ({ onNextStep }: StepProps): JSX.Element => {
  const { back } = useRouter()
  const { checkoutInfo, setCheckoutInfo } = useContext(AppContext)
  const [startDate, setStartDate] = useState(checkoutInfo.startDate || null)
  const [endDate, setEndDate] = useState(checkoutInfo.endDate || null)
  const [assistants, setAssistants] = useState(checkoutInfo.assistants || 1)
  const [ensurance] = useState(checkoutInfo.ensurance)
  const [reservationTime] = useState(checkoutInfo.reservationTime)
  const onChange = (dates) => {
    const [start, end] = dates
    setStartDate(start)
    setEndDate(end)
    setCheckoutInfo((prev) => ({ ...prev, startDate: start, endDate: end }))
  }

  const handleAssistantInput = (e) => {
    const { value } = e.target
    setAssistants(value)
    setCheckoutInfo((prev) => ({ ...prev, assistants: value }))
  }

  const handleAssistants = (isIncrease = false) => {
    const newAssistants = isIncrease ? Number(assistants) + 1 : Number(assistants) - 1
    setAssistants(newAssistants)
    setCheckoutInfo((prev) => ({ ...prev, assistants: newAssistants }))
  }

  const handleHour = ({ value }) => {
    setCheckoutInfo((prev) => ({ ...prev, reservationTime: value }))
  }

  const handleEnsurance = ({ value }) => {
    setCheckoutInfo((prev) => ({ ...prev, ensurance: value }))
  }

  useEffect(() => {
    if (!checkoutInfo.experience) back()
  }, [])

  const fromDate = new Date()
  fromDate.setDate(fromDate.getDate() + 1)

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const CalendarInput = React.forwardRef<RefType, PropsCal>(({ onClick }, ref: any) => (
    <CalendarFakeInputWrapper onClick={onClick} ref={ref}>
      {!startDate && !endDate
        ? 'Selecciona uno o más días'
        : startDate && endDate
        ? `De: ${format(startDate, 'dd/MM/yyyy')} a ${format(endDate, 'dd/MM/yyyy')}`
        : startDate && !endDate
        ? `${format(startDate, 'dd/MM/yyyy')}`
        : `${format(endDate, 'dd/MM/yyyy')}`}
    </CalendarFakeInputWrapper>
  ))
  CalendarInput.displayName = 'CalendarInput'
  return (
    <React.Fragment>
      <Container>
        <CheckoutCard experience={checkoutInfo.experience} />
        <Divider />

        <CheckoutSection>
          <Label title="¿Cuántas personas viajan? (máximo 5)" />
          <FlexWrapper justify="center">
            <IconWrapper size="30px" marginRight="8px">
              <BsFillPeopleFill />
            </IconWrapper>
            <IconWrapper
              size="15px"
              marginRight="8px"
              marginLeft="8px"
              as="button"
              onClick={() => handleAssistants()}
            >
              <FaMinus />
            </IconWrapper>
            <QuantityInput
              placeholder="1"
              name="assistants"
              value={assistants || checkoutInfo.assistants}
              type="number"
              min={1}
              onChange={handleAssistantInput}
            />
            <IconWrapper
              size="15px"
              marginLeft="8px"
              as="button"
              onClick={() => handleAssistants(true)}
            >
              <FaPlus />
            </IconWrapper>
          </FlexWrapper>
        </CheckoutSection>

        <CheckoutSection>
          <Label title="Selecciona fecha y horario" />
          <p>Elige una fecha para disfrutar tu experiencia</p>
          <DatePicker
            selected={startDate}
            onChange={onChange}
            startDate={startDate}
            endDate={endDate}
            selectsRange
            minDate={fromDate}
            customInput={<CalendarInput />}
          />
          <p>Hora:</p>
          <Select
            name="state"
            onChange={handleHour}
            placeholder="Selecciona horario"
            defaultValue={reservationTime}
            options={checkoutInfo?.experience?.experienceHours || []}
            style={{}}
          />
        </CheckoutSection>

        <CheckoutSection>
          <Label title="¿Deseas añadir seguro de viajero?  (+$200)" />
          <Switch
            name="travaller-ensurance"
            first="No"
            last="Sí"
            initial={ensurance === 'Sí' ? true : false}
            onChange={handleEnsurance}
            defaultValue={ensurance}
            style={{}}
          />
        </CheckoutSection>

        {/* <CheckoutSection>
        <Label title="¿Deseas añadir transporte redondo?" />
        <Switch name="roundtrip" first="No" last="Sí" defaultValue="last" style={{}} />
      </CheckoutSection>

      <CheckoutSection>
        <Label title="Escoge un punto de partida" />
        <DeparturePointWrapper>
          <Radio name="exitPoint" />
          <FlexWrapper direction="column">
            <FlexWrapper fluid>
              <IconWrapper>
                <RiMapPinLine />
              </IconWrapper>
              <CheckoutSubtitle>Centro de la ciudad</CheckoutSubtitle>
              <TextHighlighted>+$200</TextHighlighted>
            </FlexWrapper>
            <InfoText marginTop="8px">
              Centro de la ciudad Calle del Mercado 133, San Jerónimo, Xochimilco, 16420 Ciudad de
              México, CDMX
            </InfoText>
          </FlexWrapper>
        </DeparturePointWrapper>
        <Divider />
        <DeparturePointWrapper>
          <Radio name="exitPoint" />
          <FlexWrapper direction="column">
            <FlexWrapper fluid>
              <IconWrapper>
                <RiMapPinLine />
              </IconWrapper>
              <CheckoutSubtitle>Aeropuerto Internacional Benito Juarez</CheckoutSubtitle>
              <TextHighlighted>+$140</TextHighlighted>
            </FlexWrapper>
            <InfoText marginTop="8px">
              Aeropuerto Internacional Benito Juarez Av. Capitán Carlos León S/N, Peñón de los
              Baños, Venustiano Carranza
            </InfoText>
          </FlexWrapper>
        </DeparturePointWrapper>
      </CheckoutSection>

      <CheckoutSection>
        <Label title="Costo de acceso al Área Natural" />
        <FlexWrapper>
          <RadioBox name="cost">
            <FlexWrapper direction="column">
              <InfoText marginBottom="8px">Pagar el costo de acceso único al área natural</InfoText>
              <TextHighlighted>Costo: $140</TextHighlighted>
            </FlexWrapper>
          </RadioBox>
          <RadioBox name="cost">
            <FlexWrapper direction="column">
              <InfoText marginBottom="8px">Pasaporte Anual para el acceso al Área Natural</InfoText>
              <TextHighlighted>Costo: $140</TextHighlighted>
            </FlexWrapper>
          </RadioBox>
        </FlexWrapper>
      </CheckoutSection>

      <CheckoutSection>
        <Label title="Añade un código de descuento" />
        <Input name="discount" placeholder="Ingresa tu código" />
      </CheckoutSection>*/}
      </Container>
      <CheckoutFooter>
        <CheckoutButton onClick={() => onNextStep(1)}>
          <span>Siguiente</span>
        </CheckoutButton>
      </CheckoutFooter>
    </React.Fragment>
  )
}
