import React, { useState, useContext } from 'react'
import { Label, Chips, Button } from '../../../styles/forms'
import { CheckoutSection, FlexWrapper } from '../../../styles/checkout'
import { useRouter } from 'next/router'
import { Container } from '../../../styles/components'
import { AppContext } from '../../../context/AppContext'
import { createReservation } from '../../../pages/api/reservations'
import OpenPayHelper from '../../../helpers/OpenPay'

type StepProps = {
  onNextStep: (step: number) => void
}

const CheckoutStepTree = ({ onNextStep }: StepProps): JSX.Element => {
  const { checkoutInfo, setCheckoutInfo } = useContext(AppContext)
  const { push } = useRouter()
  const [form, setForm] = useState({})
  const [status, setStatus] = useState<'' | 'fetching' | 'finished'>('')

  const handleChange = (e) => {
    const { name, value } = e
    setForm((prevState) => {
      return {
        ...prevState,
        [name]: value,
      }
    })
    setCheckoutInfo((prev) => ({ ...prev, ...form }))
  }

  const createReservationHandler = () => {
    const {
      experience: { _id },
      startDate: checkin,
      endDate: checkout,
      assistants: guest_number,
      ensurance,
      specialRequirements,
      reservationTime: schedule,
      total,
      token,
    } = checkoutInfo

    const ResrvationInfo = {
      experience: _id,
      guest_number,
      checkin,
      checkout,
      schedule,
      total,
      specialRequirements,
      device_session_id: OpenPayHelper.generateDeviceId(),
      token,
      ensurance: `${ensurance === 'Sí' ? 'true' : 'false'}`,
    }

    setStatus('fetching')

    createReservation(ResrvationInfo)
      .then((data) => {
        setStatus('finished')
        setCheckoutInfo({})
        push('/misviajes')
      })
      .catch((err) => {
        console.error(err)
        setStatus('finished')
        onNextStep(1)
      })
  }
  return (
    <Container>
      <CheckoutSection>
        <Label title="Puedes elegir de entre los siguientes o escribirlos en el campo de texto de abajo:" />
        <Chips
          name="specialRequirements"
          onChange={handleChange}
          options={[
            'Menú vegano',
            'Menú vegetariano',
            'Atención a persona en silla de ruedas',
            'Atención a personas de la 3ra edad',
            'Atención a mascotas',
          ]}
        />
      </CheckoutSection>
      <FlexWrapper direction="column">
        <Button
          unable={status === 'fetching' || status === 'finished'}
          onClick={createReservationHandler}
          text={status === 'fetching' ? 'Procesando...' : 'Listo'}
        />
      </FlexWrapper>
    </Container>
  )
}

export default CheckoutStepTree
