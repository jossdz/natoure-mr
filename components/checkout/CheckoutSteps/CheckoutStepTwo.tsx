import React, { useState, useEffect, useContext } from 'react'
import { Divider, CheckoutSection, FlexWrapper, CheckoutButton } from '../../../styles/checkout'
import {
  ResumeItem,
  PaymentMethod,
  PaymentMethodsWrapper,
  PaymentMethodProcess,
  Error,
} from '../../../styles/checkout/StepTwo'
import { CardInput } from '../Components/CardInput'
import { Input } from '../Components/Input'
import { Radio } from '../../common/Radio'
import { Label } from '../../../styles/forms'
import { FaCcVisa, FaCcMastercard, FaCcAmex, FaPaypal } from 'react-icons/fa'
import OpenPayHelper from '../../../helpers/OpenPay'
import { PayPalButtons } from '@paypal/react-paypal-js'
import { Container } from '../../../styles/components'
import { AppContext } from '../../../context/AppContext'
import { CheckoutFooter } from '../CheckoutFooter'

type errors = {
  cardNumber?: string
  expirationDate?: string
  cardCVV?: string
}

type StepProps = {
  onNextStep: (step: number) => void
}

const CheckoutStepTwo = ({ onNextStep }: StepProps): JSX.Element => {
  const { checkoutInfo, setCheckoutInfo } = useContext(AppContext)
  const [paymentMethod, setPaymentMethod] = useState('card')
  const [errors, setErrors] = useState<errors>({})
  const [cardInfo, setCardInfo] = useState({
    cardNumber: '',
    cardName: '',
    cardMonth: '',
    cardYear: '',
    cardCVV: '',
  })
  const [succeeded, setSucceeded] = useState(false)
  const [paypalErrorMessage, setPaypalErrorMessage] = useState<string>()
  const [orderID, setOrderID] = useState(false)
  const [billingDetails, setBillingDetails] = useState('')
  const [total, setTotal] = useState(0)

  // creates a paypal order
  const createOrder = (data, actions) => {
    return actions.order
      .create({
        purchase_units: [
          {
            amount: {
              // charge users $4.99 per order
              value: 499,
            },
          },
        ],

        // remove the applicaiton_context object if you need your users to add a shipping address
        application_context: {
          shipping_preference: 'NO_SHIPPING',
        },
      })
      .then((orderID) => {
        setOrderID(orderID)
        return orderID
      })
  }

  // handles when a payment is confirmed by paypal
  const onApprove = (data, actions) => {
    return actions.order.capture().then(function (details) {
      setBillingDetails(details)
      setSucceeded(true)
    })
  }

  // handles when a payment is declined
  const onError = () => {
    setPaypalErrorMessage('Hubo un error al procesar tu pago con paypal')
  }
  const handleChange = (e) => {
    if (e.target) return setPaymentMethod(e.target.value)
    setPaymentMethod(e)
  }
  const handleCardInfoChange = (e) => {
    const { name, value } = e.target
    setCardInfo((prev) => ({ ...prev, [name]: value }))
  }
  const handleCardNumberChange = (e) => {
    const input = e.target

    let trimmed = input.value.replace(/\s+/g, '')
    if (trimmed.length > 16) {
      trimmed = trimmed.substr(0, 16)
    }

    const numbers = []
    for (let i = 0; i < trimmed.length; i += 4) {
      numbers.push(trimmed.substr(i, 4))
    }

    const cardNumber = numbers.join(' ')
    setCardInfo((prev) => ({ ...prev, [input.name]: cardNumber }))
  }

  const onBlurValidation = (e) => {
    const inputName = e.target.name
    switch (inputName) {
      case 'cardNumber':
        if (!OpenPayHelper.validateCardNumber(cardInfo.cardNumber)) {
          setErrors((prev) => ({ ...prev, [inputName]: 'Número de tarjeta inválido' }))
        } else {
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          const { [inputName as keyof typeof errors]: remove, ...rest } = errors
          setErrors(rest)
        }
        break
      case 'cardCVV':
        if (!OpenPayHelper.validateCVC(cardInfo.cardCVV)) {
          setErrors((prev) => ({ ...prev, [inputName]: 'CVC inválido' }))
        } else {
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          const { [inputName as keyof typeof errors]: remove, ...rest } = errors
          setErrors(rest)
        }
        break
      case 'cardMonth':
      case 'cardYear':
        if (cardInfo.cardMonth && cardInfo.cardYear) {
          if (!OpenPayHelper.validateExpirationDate(cardInfo.cardMonth, cardInfo.cardYear)) {
            setErrors((prev) => ({
              ...prev,
              expirationDate: 'La tarjeta expiró o la fecha es inválida',
            }))
          } else {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const { expirationDate, ...rest } = errors
            setErrors(rest)
          }
        }
        break
      default:
        break
    }
  }

  const getTotal = () => {
    let assistantsCost =
      Number(checkoutInfo?.experience?.personCost) * Number(checkoutInfo?.assistants)
    if (checkoutInfo?.ensurance === 'Sí') {
      assistantsCost = assistantsCost + 200
    }
    setTotal(assistantsCost)
  }

  const handleCreateToken = () => {
    const noErrors = Object.keys(errors).length === 0
    const hasEmptyValues = Object.values(cardInfo).some((value) => value === '')
    const hasAllValues = Object.values(cardInfo).length >= 5
    if (noErrors && !hasEmptyValues && hasAllValues) {
      OpenPayHelper.tokenize(
        { ...cardInfo, cardNumber: cardInfo.cardNumber.replace(/\s/g, '') },
        ({ data }) => {
          setCheckoutInfo((prev) => ({ ...prev, token: data.id, total }))
          onNextStep(2)
        },
        (err) => console.log(err)
      )
    }
  }

  useEffect(() => {
    OpenPayHelper.initOpenPay()
    getTotal()
  }, [])

  return (
    <React.Fragment>
      <Container>
        <CheckoutSection>
          <Label title="Resumen de tu compra:" marginBottom="24px" />
          {checkoutInfo?.ensurance === 'Sí' && (
            <ResumeItem>
              <span>- Seguro de viaje</span>
              <span>$200.00</span>
            </ResumeItem>
          )}
          <ResumeItem>
            <span>- Costo por asistentes</span>
            <span>
              $
              {(
                parseFloat(checkoutInfo?.experience?.personCost) *
                parseFloat(checkoutInfo?.assistants)
              ).toFixed(2)}
            </span>
          </ResumeItem>
          <Divider />
          <ResumeItem isTotal>
            <span>Total:</span>
            <span>${total.toFixed(2)}</span>
          </ResumeItem>
        </CheckoutSection>

        <CheckoutSection>
          <Label title="Método de pago:" marginBottom="24px" />
          <PaymentMethodsWrapper>
            <PaymentMethod checked={paymentMethod === 'card'} onClick={() => handleChange('card')}>
              <FlexWrapper>
                <FaCcVisa />
                <FaCcMastercard />
                <FaCcAmex />
              </FlexWrapper>
              <Radio
                label="Tarjeta de crédito o débito"
                value="card"
                id="payment-card"
                name="paymentMethod"
                checked={paymentMethod === 'card'}
                onChange={handleChange}
              />
            </PaymentMethod>
            {/* <PaymentMethod
            checked={paymentMethod === 'paypal'}
            onClick={() => handleChange('paypal')}
          >
            <FlexWrapper>
              <FaPaypal />
            </FlexWrapper>
            <Radio
              label="Paypal"
              value="paypal"
              checked={paymentMethod === 'paypal'}
              id="payment-paypal"
              onChange={handleChange}
              name="paymentMethod"
            />
          </PaymentMethod> */}
          </PaymentMethodsWrapper>
          <PaymentMethodProcess>
            {paymentMethod === 'paypal' ? (
              <FlexWrapper justify="center">
                {/* <PayPalButtons
                style={{
                  label: 'paypal',
                  tagline: true,
                  layout: 'horizontal',
                  height: 40,
                }}
                createOrder={createOrder}
                onApprove={onApprove}
                onError={onError}
              /> */}
              </FlexWrapper>
            ) : (
              <React.Fragment>
                <CardInput
                  value={cardInfo.cardNumber}
                  onChange={handleCardNumberChange}
                  onBlur={onBlurValidation}
                />
                <Input
                  onChange={handleCardInfoChange}
                  label="Nombre en Tarjeta"
                  name="cardName"
                  id="cardName"
                  value={cardInfo.cardName}
                  placeholder="John Snow"
                  onBlur={onBlurValidation}
                />
                <FlexWrapper>
                  <Input
                    onChange={handleCardInfoChange}
                    label="Mes"
                    name="cardMonth"
                    id="cardMonth"
                    value={cardInfo.cardMonth}
                    placeholder="08"
                    maxLength={2}
                    onBlur={onBlurValidation}
                  />
                  <Input
                    onChange={handleCardInfoChange}
                    label="Año"
                    name="cardYear"
                    id="cardYear"
                    value={cardInfo.cardYear}
                    placeholder="21"
                    maxLength={2}
                    onBlur={onBlurValidation}
                  />
                  <Input
                    onChange={handleCardInfoChange}
                    label="CVV"
                    name="cardCVV"
                    id="cardCVV"
                    maxLength={4}
                    value={cardInfo.cardCVV}
                    placeholder="123"
                    onBlur={onBlurValidation}
                  />
                </FlexWrapper>
              </React.Fragment>
            )}
          </PaymentMethodProcess>
          <FlexWrapper direction="column" align="flex-start">
            {paypalErrorMessage && <Error>{paypalErrorMessage}</Error>}
            {Object.values(errors).length > 0 &&
              Object.values(errors).map((error, index) => <Error key={index}>{error}</Error>)}
          </FlexWrapper>
        </CheckoutSection>
      </Container>
      <CheckoutFooter>
        <CheckoutButton onClick={() => onNextStep(0)}>
          <span>Atrás</span>
        </CheckoutButton>
        <CheckoutButton onClick={() => handleCreateToken()}>
          <span>Siguiente</span>
        </CheckoutButton>
      </CheckoutFooter>
    </React.Fragment>
  )
}

export default CheckoutStepTwo
