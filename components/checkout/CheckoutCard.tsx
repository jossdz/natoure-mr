import React from 'react'
import {
  CheckoutCardWrapper,
  CheckoutCardThumbnail,
  CheckoutText,
  CheckoutSubText,
  Dot,
} from '../../styles/checkout/Card'
import { IconWrapper, FlexWrapper } from '../../styles/checkout'
import { RiLeafFill, RiBookmarkLine, RiMapPinLine, RiHeart3Line, RiStarSFill } from 'react-icons/ri'

export const CheckoutCard = ({ experience }): JSX.Element => {
  return (
    <CheckoutCardWrapper>
      <CheckoutCardThumbnail image={experience && experience.mainPhoto[0]}>
        <FlexWrapper fluid align="flex-start">
          <FlexWrapper>
            <IconWrapper marginRight="8px" size="20px" svgColor="var(--green-natoure)">
              <RiLeafFill />
            </IconWrapper>
            <CheckoutText>4.0</CheckoutText>
          </FlexWrapper>
          <IconWrapper svgColor="white" size="20px">
            <RiBookmarkLine />
          </IconWrapper>
        </FlexWrapper>
      </CheckoutCardThumbnail>
      <FlexWrapper>
        <FlexWrapper>
          <IconWrapper marginRight="8px">
            <RiMapPinLine />
          </IconWrapper>

          <CheckoutSubText>Oaxaca</CheckoutSubText>
        </FlexWrapper>
        <FlexWrapper>
          26
          <IconWrapper marginLeft="8px">
            <RiHeart3Line />
          </IconWrapper>
        </FlexWrapper>
      </FlexWrapper>
      <FlexWrapper>
        <CheckoutText>{experience?.experienceName}</CheckoutText>

        <FlexWrapper>
          <IconWrapper marginRight="8px">
            <RiStarSFill />
          </IconWrapper>

          <CheckoutSubText>{experience?.rating}</CheckoutSubText>
        </FlexWrapper>
      </FlexWrapper>
      <FlexWrapper justify="flex-start">
        <CheckoutText>${experience?.personCost} por persona</CheckoutText>
        <Dot />
        <CheckoutText>Duración: </CheckoutText>
        <CheckoutSubText>
          {experience?.experienceTime} {experience?.daysHours}
        </CheckoutSubText>
      </FlexWrapper>
      {/* <FlexWrapper justify="flex-start">
        <CheckoutText>Costo de acceso al AIC: </CheckoutText>
        <CheckoutSubText>$100</CheckoutSubText>
      </FlexWrapper> */}
    </CheckoutCardWrapper>
  )
}
