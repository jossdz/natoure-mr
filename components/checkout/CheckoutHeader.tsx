import React, { ReactNode } from 'react'
import { CheckoutHeaderStyled } from '../../styles/checkout'

type CheckoutHeaderProps = {
  children: ReactNode
}

export const CheckoutHeader = ({ children }: CheckoutHeaderProps): JSX.Element => {
  return <CheckoutHeaderStyled>{children}</CheckoutHeaderStyled>
}
