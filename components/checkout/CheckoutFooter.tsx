import React, { ReactNode } from 'react'
import { CheckoutFooterStyled } from '../../styles/checkout'

type CheckoutFooterProps = {
  children: ReactNode
}

export const CheckoutFooter = ({ children }: CheckoutFooterProps): JSX.Element => {
  return <CheckoutFooterStyled>{children}</CheckoutFooterStyled>
}
