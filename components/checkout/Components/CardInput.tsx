import React from 'react'

import { InputWrapper, InputStyles } from '../../../styles/checkout/Inputs'
import { IconWrapper } from '../../../styles/checkout'
import {
  FaCcVisa,
  FaCcMastercard,
  FaCcAmex,
  FaCcDiscover,
  FaCcDinersClub,
  FaCcJcb,
  FaCreditCard,
} from 'react-icons/fa'
import OpenPayHelper from '../../../helpers/OpenPay'

type CardInputProps = {
  value?: string
  onChange: (e: React.ChangeEvent) => void
  onBlur: (e: React.ChangeEvent) => void
}

const cardBrands = {
  Mastercard: <FaCcMastercard />,
  Visa: <FaCcVisa />,
  'American Express': <FaCcAmex />,
  'Diners Club Carte Blanche': <FaCcDinersClub />,
  Discover: <FaCcDiscover />,
  JCB: <FaCcJcb />,
}

export const CardInput = ({ value, onChange, onBlur }: CardInputProps): JSX.Element => {
  const cardBrand = OpenPayHelper.getCardBrand(value)
  const CardBrandIcon = cardBrands[cardBrand]

  return (
    <InputWrapper>
      <label htmlFor="cardNumber">Número de tarjeta</label>
      <InputStyles>
        <IconWrapper marginRight="8px">{cardBrand ? CardBrandIcon : <FaCreditCard />}</IconWrapper>
        <input
          onChange={onChange}
          name="cardNumber"
          id="cardNumber"
          value={value}
          placeholder="4444 4444 4444 4444"
          maxLength={20}
          onBlur={onBlur}
        />
      </InputStyles>
    </InputWrapper>
  )
}
