import React from 'react'

import { InputWrapper, InputStyles } from '../../../styles/checkout/Inputs'

type InputProps = {
  value?: string
  label: string
  name: string
  id: string
  placeholder?: string
  maxLength?: number
  onChange: (e: React.ChangeEvent) => void
  onBlur: (e: React.ChangeEvent) => void
}

export const Input = ({
  value = '',
  label,
  name,
  id,
  placeholder,
  maxLength,
  onChange,
  onBlur,
}: InputProps): JSX.Element => {
  return (
    <InputWrapper>
      <label htmlFor={id}>{label}</label>
      <InputStyles>
        <input
          onChange={onChange}
          id={id}
          name={name}
          value={value}
          placeholder={placeholder}
          maxLength={maxLength}
          onBlur={onBlur}
        />
      </InputStyles>
    </InputWrapper>
  )
}
