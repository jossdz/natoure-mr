import React from 'react'
import Link from 'next/link'
import { AdminSectionTitle, GreenButton, MegaLabelLink, Divider } from '../../styles/admin'
import { FlexWrapper } from '../../styles/MapPage/styles'
import { FaPlus } from 'react-icons/fa'

type UserSectionNavProps = {
  route: string
}

const UserSectionNav = ({ route }: UserSectionNavProps): JSX.Element => {
  return (
    <React.Fragment>
      <FlexWrapper justify="flex-start" align="center">
        <AdminSectionTitle>Usuarios</AdminSectionTitle>
        <Link href="/admin/usuarios">
          <GreenButton>
            <FaPlus />
            Agregar
          </GreenButton>
        </Link>
      </FlexWrapper>
      <FlexWrapper justify="flex-start" align="center">
        <Link href="/admin/usuarios">
          <MegaLabelLink isActive={route === '/admin/usuarios'}>Natourista</MegaLabelLink>
        </Link>
        <Divider />
        <Link href="/admin/usuarios/anfitriones">
          <MegaLabelLink isActive={route === '/admin/usuarios/anfitriones'}>
            Anfitriones
          </MegaLabelLink>
        </Link>
        <Divider />
        <Link href="/admin/usuarios/administradores">
          <MegaLabelLink isActive={route === '/admin/usuarios/administradores'}>
            Administradores
          </MegaLabelLink>
        </Link>
      </FlexWrapper>
    </React.Fragment>
  )
}

export default UserSectionNav
