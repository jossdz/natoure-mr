/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/media-has-caption */
import React from 'react'
import Jumbo from '../styles/explore/Jumbo'
import HorizontalS from '../styles/general/HorizontalSlide'
import { MarginY } from '../styles/general'
import PagosC from '../styles/anfitrion/pagosCompleto'
import HostLayout from '../Layouts/Host'
import Container from '../styles/components/Container'
import Loading from '../styles/components/Loading'

const MisPagos = (): JSX.Element => {
  const [completados, setCompletados] = React.useState(true)
  const [pendientes, setPendientes] = React.useState(false)

  const handleCompletados = () => {
    setPendientes(false)
    setCompletados(true)
  }

  const handlePendientes = () => {
    setCompletados(false)
    setPendientes(true)
  }

  const renderCompletados = (
    <>
      <Jumbo>
        <HorizontalS>
          <button onClick={handleCompletados}>
            <span className="here">Completados</span>
          </button>
          <button onClick={handlePendientes}>Pendientes </button>
        </HorizontalS>
      </Jumbo>
      <PagosC
        image="https://upload.wikimedia.org/wikipedia/commons/9/95/Trajinera.JPG"
        title="Cosecha en Xochimilco"
        date="5 jun 2020"
        priceingreso="$0.00"
        total="$0.00"
      />
    </>
  )

  const renderPendientes = (
    <>
      <Jumbo>
        <HorizontalS>
          <button onClick={handleCompletados}>Completados</button>
          <button onClick={handlePendientes}>
            <span className="here">Pendientes</span>
          </button>
        </HorizontalS>
      </Jumbo>
      <PagosC
        image="https://upload.wikimedia.org/wikipedia/commons/9/95/Trajinera.JPG"
        title="Cosecha en Xochimilco"
        date="5 jun 2020"
        priceingreso="$0.00"
        total="$0.00"
      />
    </>
  )

  return (
    <MarginY>
      <Container>
        {completados ? renderCompletados : pendientes ? renderPendientes : <Loading />}
      </Container>
    </MarginY>
  )
}

MisPagos.Layout = HostLayout

export default MisPagos
