/* eslint-disable jsx-a11y/media-has-caption */
import React, { useEffect, useState } from 'react'
import { SectionHeading } from '../../styles/components'
import { MarginY } from '../../styles/general'
import Heading from '../../styles/experience/headingcard'
import ResumeExp from '../../styles/anfitrion/ResumeExp'
import Gallery from '../../styles/experience/Gallery'

import { MegaLabel } from '../../styles/landing'
import Resume from '../../styles/anfitrion/ResumeUs'
import FixedHead from '../../styles/anfitrion/FixedHead'
import Navbar from '../../components/layout/NavBar'
import { getOrganizationDetail } from '../api/api'
import { useRouter } from 'next/router'
import { FlexWrapper } from '../../styles/checkout'
import ExperienceCard from '../../components/Explore/components/ExperienceCard'
import { Container } from '../../styles/components'
import { AiOutlineEnvironment } from 'react-icons/ai'
import Location from '../../styles/experience/Location'

const anfitrion: React.FC = () => {
  const { query } = useRouter()
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [data, setData] = useState<any>({})

  useEffect(() => {
    if (query.id) {
      getOrganizationDetail(query.id).then(({ organization }) => {
        setData(organization)
      })
    }
  }, [query])
  return (
    <MarginY>
      <Navbar />

      <Heading
        back="/anfitriones"
        title={data?.organizationName}
        image={data?.mainPhoto && data.mainPhoto[0]}
        place={data?.address && data.address.address}
      />
      {/* <FixedHead
        rating={4.5}
        sustainable={data?.sustentabilityPromedy?.toFixed(1)}
        title=""
        likes={0}
        duration=""
        image=""
      /> */}
      {/* <HorizontalS>
        <TagCard />
      </HorizontalS> */}
      <ResumeExp
        title="¿Quiénes somos?"
        poblacion="Zapotecas"
        description={data?.organizationStory}
      />
      <hr></hr>

      <Resume
        title="¿Qué hacemos?"
        description="Nuestras actividades tienen 3 vertientes, la primera guiar, enseñar y apoyar a estudiantes, profesores e
        investigadores en sus actividades de aprendizaje sobre el sistema lacustre. La segunda, ofrecer experiencias en la
        agricultur tradicional chinampera con productores locales como guías en el aprendizaje de métodos agroecológicos y ecotecnias.
        La tercera vertiente
         se refiere al conocimiento de la zona lacustre mediante actividades cortas y lúdicas sobre su historia, cultura y biodiversidad."
        poblacion=""
      />

      <hr></hr>

      <SectionHeading title="Experiencias que brindamos" />
      <Container>
        <FlexWrapper wrap="wrap">
          {/* {data?.experiences?.map((item, index) => (
            <ExperienceCard key={index} {...item} />
          ))} */}
        </FlexWrapper>
      </Container>
      <Container>
        <Gallery photos={data?.photos} />
        <div style={{ margin: '48px 0', color: '#2c375a' }}>
          <MegaLabel>{data?.experienceName}</MegaLabel>
          <p
            style={{
              display: 'flex',
              alignItems: 'center',
              fontFamily: 'Montserrat',
              fontSize: '16px',
              fontWeight: 'bold',
              lineHeight: '1.5',
              letterSpacing: '-0.03px',
            }}
          >
            <AiOutlineEnvironment />
            <span>{data?.address && data.address.address}</span>
          </p>
        </div>
      </Container>
      <Container>
        <Location address={data?.address} addressDetails={data?.addressDetails} />
      </Container>
    </MarginY>
  )
}

export default anfitrion
