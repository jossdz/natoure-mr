/* eslint-disable jsx-a11y/media-has-caption */
import React, { useContext } from 'react'
import { AiOutlineEnvironment } from 'react-icons/ai'

import { MarginY, OnlyWeb, OnlyMobile } from '../../styles/general'
import { MegaLabel } from '../../styles/landing'
import Heading from '../../styles/experience/headingcard'
import ResumeExp from '../../styles/experience/ResumeExp'
import InterestCard from '../../styles/experience/interestCard'
import TagCard from '../../styles/experience/tags'
import Activities from '../../styles/experience/Activities'
import Include from '../../styles/experience/Include'
import Gallery from '../../styles/experience/Gallery'
import BestMonths from '../../styles/experience/BestMonths'
import Container from '../../styles/components/Container'
import Atributes from '../../styles/experience/Atributes'
import Location from '../../styles/experience/Location'
import Detailed from '../../styles/experience/Detailed'
import FloraFaunaCards from '../../styles/experience/FloraFauna'
import SustainCard from '../../styles/experience/SustainCard'
import { getExperience, getAllExperiences } from '../api/api'
import NavBar from '../../components/layout/NavBar'
import { AppContext } from '../../context/AppContext'

const ExperienceDetail = ({ data }): JSX.Element => {
  const { setCheckoutInfo } = useContext(AppContext)

  const handleReserve = () => {
    setCheckoutInfo((prev) => {
      return { ...prev, experience: data }
    })
  }

  return (
    <MarginY>
      <NavBar />
      <OnlyMobile>
        <Heading
          back="/explora"
          title={data?.experienceName}
          image={data?.mainPhoto && data.mainPhoto[0]}
          place={data?.address && data.address.address}
        />
      </OnlyMobile>
      <OnlyWeb>
        <Container>
          <Gallery photos={[...data?.mainPhoto, ...data?.photos]} />
          <div style={{ margin: '48px 0', color: '#2c375a' }}>
            <MegaLabel>{data?.experienceName}</MegaLabel>
            <p
              style={{
                display: 'flex',
                alignItems: 'center',
                fontFamily: 'Montserrat',
                fontSize: '16px',
                fontWeight: 'bold',
                lineHeight: '1.5',
                letterSpacing: '-0.03px',
              }}
            >
              <AiOutlineEnvironment />
              <span style={{ marginLeft: '4px' }}>{data?.address && data.address.address}</span>
            </p>
          </div>
        </Container>
      </OnlyWeb>

      <SustainCard
        price={data.personCost}
        duration={`${data.experienceTime} ${data?.daysHours}`}
        href="/checkout"
        onClick={handleReserve}
      />

      <Container>
        {data?.experienceMacros && data?.experienceMacros.length > 0 && (
          <TagCard margin="24px 0" tag={data?.experienceMacros} />
        )}
        <ResumeExp title="Resumen de la Experiencia" description={data?.experienceResume} />
        {data?.experienceActivities && data?.experienceActivities.length > 0 && (
          <Activities
            title="Actividades que se realizan en la experiencia"
            activities={data?.experienceActivities}
          />
        )}
        <Include included={data?.experienceIncludes} notIncluded={data?.experienceNotIncludes} />
      </Container>

      <OnlyMobile>
        <Gallery photos={[...data?.mainPhoto, ...data?.photos]} />
      </OnlyMobile>

      <Container>
        <InterestCard
          difficulty={data?.dificultLevel}
          age={data?.ageRange}
          duration={`${data.experienceTime} ${data?.daysHours}`}
          people={data?.maxPersons}
          language={data?.experienceLanguages}
          maxPersons={data?.maxPersons}
          sugestedClothing={data?.sugestedClothing}
        />
        {data?.bestMonths && data.bestMonths.length > 0 && (
          <BestMonths
            title="Mejores meses para realizar esta actividad"
            bestMonths={data?.bestMonths}
          />
        )}
        <Atributes title="Atributos de la Experiencia" attributes={data?.extraAttr} />
        <Location address={data?.address} addressDetails={data?.addressDetails} />
        {data?.experienceDays && data?.experienceDays.length > 0 && (
          <Detailed days={data?.experienceDays} />
        )}
        {(data?.flora && data?.flora.length > 0) ||
          (data?.fauna && data?.fauna.length > 0 && (
            <FloraFaunaCards flora={data?.flora} fauna={data?.fauna} />
          ))}
      </Container>
    </MarginY>
  )
}

export async function getStaticPaths() {
  const { experiences } = await getAllExperiences('')
  const paths = experiences.map((e) => `/experience/${e._id}`)
  return {
    paths,
    fallback: false,
  }
}

export async function getStaticProps({ params }) {
  const { experience } = await getExperience(params.id)
  return {
    props: {
      data: experience,
    },
  }
}

export default ExperienceDetail
