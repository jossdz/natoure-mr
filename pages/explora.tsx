import React, { useEffect, useState } from 'react'
import { AiOutlineEnvironment, AiFillAppstore } from 'react-icons/ai'

import Jumbo from '../styles/explore/Jumbo'
import HorizontalS from '../styles/general/HorizontalSlide'
import { MarginY } from '../styles/general'
import Loading from '../styles/components/Loading'
import FilterBar from '../styles/components/FilterBar'
import Gastronomia from '../styles/explore/Gastronomia'
import Hospedaje from '../styles/explore/Hospedaje'
import Experiencias from '../styles/explore/Experiencias'
import Productos from '../styles/explore/Productos'
import Container from '../styles/components/Container'
import MapButton from '../styles/components/MapButton'
import { Map } from '../components/MapPage'
import TravelerLayout from '../Layouts/Traveler'
import TagCard from '../styles/experience/tags'
import InfoCard from '../styles/components/InfoCard'
import {
  getAllExperiences,
  getAllPlates,
  getAllHotels,
  getAllRestaurants,
  getAllProducts,
} from './api/api'

import { useRouter } from 'next/router'

import { macros } from '../steppers/data'

const explora = (): JSX.Element => {
  const {
    query: { query: routeQuery },
  } = useRouter()

  const [category, setCategory] = useState('Experiencias')
  const [experiences, setExperiences] = useState([])
  const [restaurants, setRestaurants] = useState([])
  const [plates, setPlates] = useState([])
  const [lodgings, setLodgings] = useState([])
  const [products, setProducts] = useState([])
  const [macro, setMacro] = useState('')
  const [toggleMap, setToggleMap] = useState(false)
  const [inputQuery, setInputQuery] = useState<string>()
  const [isData, setIsData] = useState(false)

  const handleInputChange = (value) => {
    setInputQuery(value)
  }

  useEffect(() => {
    setInputQuery(routeQuery as string)
  }, [routeQuery])

  useEffect(() => {
    getAllPlates()
      .then(({ plates }) => {
        setPlates(plates)
      })
      .catch((err) => console.error(err))
    getAllRestaurants()
      .then(({ restaurants }) => {
        setRestaurants(restaurants)
      })
      .catch((err) => console.error(err))
    getAllHotels()
      .then(({ lodgings }) => {
        setLodgings(lodgings)
      })
      .catch((err) => console.error(err))
    getAllProducts()
      .then(({ article }) => {
        setProducts(article)
      })
      .catch((err) => console.error(err))
  }, [])

  useEffect(() => {
    let query = ''
    if (macro !== '')
      query = query === '' ? `?experienceMacros=${macro}` : `${query}&experienceMacros=${macro}`
    if (inputQuery)
      query =
        query === '' ? `?experienceName=${inputQuery}` : `${query}&experienceName=${inputQuery}`
    getAllExperiences(query)
      .then(({ experiences }) => {
        setIsData(true)
        setExperiences(experiences)
      })
      .catch((err) => console.error(err))
  }, [macro, inputQuery])

  const Experiences = () => (
    <React.Fragment>
      <TagCard
        margin="24px 0 40px 0"
        width="100%"
        activeTag={macro}
        tag={Object.keys(macros)}
        onClick={(item) => setMacro(item)}
      />
      <Experiencias experiences={experiences} />
    </React.Fragment>
  )

  const categories = {
    Experiencias: { component: <Experiences /> },
    Gastronomía: { component: <Gastronomia restaurants={restaurants} plates={plates} /> },
    Hospedaje: { component: <Hospedaje lodgings={lodgings} /> },
    Productos: { component: <Productos products={products} /> },
  }

  return !isData ? (
    <Loading />
  ) : (
    <MarginY>
      <Container>
        <Jumbo>
          <FilterBar
            placeholder="Busca por nombre…"
            value={inputQuery}
            onChange={handleInputChange}
          />
          <MapButton toggleMap={toggleMap} onClick={() => setToggleMap(!toggleMap)}>
            {toggleMap ? <AiFillAppstore /> : <AiOutlineEnvironment />}
          </MapButton>
          <HorizontalS>
            {Object.keys(categories).map((item, index) => (
              <button
                key={index}
                className={category === item ? 'here' : ''}
                onClick={() => setCategory(item)}
              >
                {item}
              </button>
            ))}
          </HorizontalS>
        </Jumbo>
        {!toggleMap && (
          <React.Fragment>
            {categories[category].component}
            <InfoCard>
              <div>
                Todas las experiencias que viviras con Natoure son <br />
                <b>experiencias únicas que ayudan a conservar el medio ambiente</b>
              </div>
            </InfoCard>
          </React.Fragment>
        )}
      </Container>
      {toggleMap && <Map isToggled={true} />}
    </MarginY>
  )
}

explora.Layout = TravelerLayout

export default explora
