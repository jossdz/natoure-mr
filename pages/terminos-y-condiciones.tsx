import React from 'react'
import styled from 'styled-components'

import { MarginY } from '../styles/general'
import Container from '../styles/components/Container'
import TravelerLayout from '../Layouts/Traveler'

const Advices = styled.div`
  font-size: 14px;
  li {
    margin: 1rem 0;
  }
  th,
  td {
    width: 50%;
    padding: 8px 16px;
    border: 1px solid var(--black);
  }
  .title {
    font-size: 20px;
    font-weight: bold;
    color: var(--dark-blue);
    margin: 32px 0;
  }
  @media (max-width: 1023px) {
    font-size: 12px;
    .title {
      display: none;
    }
  }
`

const tyc = (): JSX.Element => {
  return (
    <MarginY>
      <Container>
        <Advices>
          <p className="title">TÉRMINOS Y CONDICIONES</p>
          <p>
            Los presentes términos y condiciones de NATOURE S.A.P.I. DE C.V. (en adelante “NATOURE”)
            tienen como objetivo dar a conocer tanto para ti, como para nosotros, las condiciones
            generales de contratación bajo las cuales “NATOURE” prestará sus servicios, te sugerimos
            leer el documento completamente para que de esta manera te podamos brindar la mejor
            experiencia en “NATOURE”
            <p>Última actualización: 04 de enero de 2019</p>
          </p>
          <ol>
            <li>
              DEFINICIONES
              <p>
                Los Títulos alusivos que aparecen en los presentes Términos y Condiciones son
                utilizados únicamente como referencia, sin que de ellos se desprendan derechos u
                obligaciones Para un mejor entendimiento de los Términos y Condiciones presente, se
                emplearán las siguientes definiciones:
              </p>
              <ol>
                <li>
                  Usuario: Es cualquier persona ya sea física o moral de cualquier parte del mundo
                  que haga uso de la Plataforma “NATOURE” y cumpla con las obligaciones establecidas
                  en dicha plataforma
                </li>
                <li>
                  Plataforma NATOURE: Es un sitio web o aplicación de software que puede ser
                  instalado en dispositivos móviles, teléfonos inteligentes y/o cualquier
                  dispositivo electrónico, que consiste en un mercado comunitario que ayuda a
                  personas, empresas y organizaciones a crear,anunciar, descubrir y reservar
                  experiencias de aprendizaje y turismo sostenible únicos en todo el mundo
                </li>
                <li>
                  Viajero Natourista: Es aquella persona física o moral de cualquier parte del mundo
                  que se registra en la “PLATAFORMA NATOURE”Plataforma NATOURE con la finalidad de
                  contratar experiencias de viaje y de aprendizaje que brindan los “ANFITRIONES”
                </li>
                <li>
                  Anfitrión: Es aquella persona física o moral de cualquier parte del mundo que se
                  registra en la “PLATAFORMA NATOURE” para que sean publicadas y promocionadas las
                  “EXPERIENCIAS” que brindan y las mismas sean contratadas por los “VIAJERO
                  NATOURISTAS”, así como ofrecer y promocionar “PRODUCTOS” artesanales
                </li>
                <li>
                  Experiencia: Son una o varias actividades que ofrece el “ANFITRIÓN”, las cuales
                  pueden ser de tipo: aventura, naturaleza, rural y cultural, cuya finalidad
                  consiste en promover y conservar el patrimonio cultural y natural de áreas de
                  conservación en el territorio en que se encuentren. Dichas “EXPERIENCIAS” pueden
                  ser contratadas por el “VIAJERO NATOURISTA”
                </li>
                <li>
                  Embajador de Naturaleza: Es una persona física, moral o autoridad competente que
                  se registra en la Plataforma NATOURE y que por sus conocimientos técnicos o
                  científicos pueden administrar un “ÁREA DE IMPORTANCIA DE CONSERVACIÓN” y
                  compartir contenido e información de la misma a través de la plataforma
                </li>
                <li>
                  Métodos de Pago: Se refiere a las diferentes formas a través de las cuales el
                  “USUARIO” podrá realizar los pagos correspondientes por la contratación de las
                  “EXPERIENCIAS”
                </li>
                <li>
                  Área de Importancia de Conservación (AIC): Es un espacio natural o restaurado, el
                  cual se encuentra bajo una jurisdicción internacional, nacional, federal, estatal
                  (departamental), municipal y/o local, administrado de manera pública y/o privada,
                  en cogestión o de forma comunitaria. También, se considera Área de Importancia de
                  Conservación a todos los territorios indígenas y autónomos, así como territorios
                  que brindan conectividad para que los servicios y bienes ecosistémicos se
                  mantengan en el tiempo
                </li>
                <li>
                  Índice de Sostenibilidad (IDS): Es la medición de criterios sociales, económicos y
                  ambientales que se relacionan para medir la capacidad de sostenibilidad de una
                  comunidad en donde el “ANFITRIÓN” ofrece alguna “EXPERIENCIA”. Dicho índice se
                  obtiene como resultado de la suma y promedio de una autoevaluación de la comunidad
                  (capacidad), una evaluación del “VIAJERO NATOURISTA” (“EXPERIENCIA”) y una
                  evaluación realizada por “NATOURE”(impacto social)
                </li>
                <li>
                  Moneda del País del anuncio de la Experiencia: Se refiere a la moneda de curso
                  legal que corresponda al país en donde se situé la “EXPERIENCIA” o el anuncio de
                  la misma
                </li>
                <li>
                  Tipo de Cambio o Tasa de Cambio: Se refiere a las unidades de medida en la que se
                  calculará el monto por la prestación de servicio de la “EXPERIENCIA” , cuando la
                  misma se encuentre expresada en la Moneda del país del anuncio de la Experiencia
                </li>
                <li>
                  Natoure: Es la empresa encargada de administrar la Plataforma, cuya denominación
                  es “NATOURE” S.A.P.I. DE C.V. con domicilio en Calle Guillermo González Camarena
                  No. 1600, Piso 1, Oficina 1 G-H, Colonia Centro de Ciudad Santa Fe, Alcaldía de
                  Álvaro Obregón, Código Postal 01210, en la Ciudad de México, México
                </li>
              </ol>
            </li>
            <li>
              DESCRIPCION DE NUESTRO SERVICIO
              <p>
                La plataforma denominada “NATOURE” es un sitio web o aplicación de software que
                puede ser instalado en dispositivos móviles, teléfonos inteligentes o cualquier
                dispositivo electrónico, que consiste en un mercado comunitario que ayuda a
                personas, empresas y organizaciones a crear, anunciar, descubrir y reservar
                experiencias de aprendizaje y turismo sostenible únicos en todo el mundo, con el fin
                de promover la preservación de Áreas de Importancia de Conservación. Así mismo, a
                través de la plataforma los usuarios pueden interactuar con el contenido educativo,
                para de ésta forma lograr un impacto social y ambiental positivo Para disfrutar de
                los beneficios y servicios que ofrece la “PLATAFORMA NATOURE”, toda persona deberá
                registrarse de acuerdo con lo establecido en los siguientes términos y condiciones
              </p>
              <p>
                Los servicios para los “VIAJEROS NATOURISTAS” de la “PLATAFORMA NATOURE”, incluyen
                las siguientes actividades:
              </p>
              <ol>
                <li>
                  Los “VIAJEROS NATOURISTAS” podrán contratar “EXPERIENCIAS” de cualquier tipo
                  (aventura, naturaleza, rural o cultural) y utilizar de manera educativa el uso de
                  la información, contenido y bondades a ser publicadas dentro de la plataforma, así
                  como conocer los “PRODUCTOS” que ofrecen los “ANFITRIONES”
                </li>
                <li>
                  Interactuar con los “USUARIOS” de la “PLATAFORMA NATOURE” sobre temas culturales,
                  naturales, entre otros.
                </li>
                <li>
                  Hacerse acreedor de promociones, ofertas y demás beneficios que brinde la
                  “PLATAFORMA NATOURE” de acuerdo con las políticas sobre cada una de las
                  promociones y/o ofertas
                </li>
                <li>
                  Contar con el respaldo “NATOURE” en la solución de conflictos que llegase a
                  presentarse con los “ANFITRIONES” o cualquier otro “USUARIO” de la “PLATAFORMA
                  NATOURE”, siempre según lo establecido en los siguientes Términos y Condiciones
                </li>
              </ol>
            </li>
            <li>
              Los servicios para los “ANFITRIONES” de la “PLATAFORMA NATOURE”,incluyen las
              siguientes actividades:
              <ol>
                <li>
                  Publicar, promocionar y ofrecer las “EXPERIENCIAS” que brindan, para que las
                  mismas puedan ser adquiridas por los “VIAJEROS NATOURISTAS”
                </li>
                <li>
                  Interactuar con los “USUARIOS” de la “PLATAFORMA NATOURE” sobre temas culturales,
                  naturales, entre otros
                </li>
                <li>
                  Contar con el respaldo “NATOURE” en la solución de conflictos que llegase a
                  presentarse con los “VIAJEROS NATOURISTAS” o cualquier otro “USUARIO” de la
                  “PLATAFORMA NATOURE”, siempre según lo establecido en los siguientes Términos y
                  Condiciones
                </li>
              </ol>
            </li>
            <li>
              Los servicios para los “EMBAJADORES DE NATURALEZA” de la “PLATAFORMA NATOURE”,
              incluyen las siguientes actividades:
              <ol>
                <li>
                  Una vez registrados, podrán subir, publicar y divulgar contenido informativo tanto
                  científico, técnico y cultural sobre Áreas de Importancia de Conservación
                </li>
                <li>
                  Interactuar con los “USUARIOS” de la “PLATAFORMA NATOURE” sobre temas culturales,
                  naturales, y cualquier otro relacionado con la conservación y promoción de Áreas
                  de Importancia de Conservación
                </li>
                <li>Invitar a “ANFITRIONES” a formar parte de la “PLATAFORMA NATOURE”</li>
              </ol>
            </li>
            <li>
              OBLIGACIONES DE “NATOURE”
              <ol>
                <li>
                  Realizar las actividades necesarias para el funcionamiento óptimo de la
                  “PLATAFORMA NATOURE”
                </li>
                <li>
                  Realizar el estudio y evaluación de las solicitudes de los “ANFITRIONES” para
                  poder ofrecer sus “EXPERIENCIAS” en la “PLATAFORMA NATOURE”
                </li>
                <li>
                  Realizar las actividades necesarias para brindar el servicio de intermediación de
                  pago entre los “VIAJEROS NATOURISTAS” y los “ANFITRIONES”
                </li>
                <li>
                  Realizar las mediciones de “ÍNDICE DE SOSTENIBILIDAD” tanto para los “VIAJEROS
                  NATOURISTAS” como hacia los “ANFITRIONES”
                </li>
                <li>
                  Realizar las confirmaciones de pago necesarias a los “VIAJEROS NATOURISTAS” para
                  que los “ANFITRIONES” puedan llevar a cabo las “EXPERIENCIAS”
                </li>
                <li>
                  Notificar las reservaciones de las “EXPERIENCIAS” a los “ANFITRIONES” en un plazo
                  máximo de 72 horas
                </li>
              </ol>
            </li>
            <li>
              OBLIGACIONES COMO VIAJERO NATOURISTA
              <p>
                Si eres un “VIAJERO NATOURISTA” de la “PLATAFORMA NATOURE” tienes las siguientes
                obligaciones:
              </p>
              <ol>
                <li>
                  Aceptar todas las normativas y políticas establecidas por “NATOURE” para el uso de
                  la “PLATAFORMA NATOURE” en el presente documento y/o cualquiera de sus
                  modificaciones
                </li>
                <li>
                  Contar con todas las capacidades legales para contratar de acuerdo a las leyes
                  mexicanas vigentes
                </li>
                <li>
                  Proporcionar información exacta, actualizada y completa que sea requerida por
                  “NATOURE” para registrarse en la “PLATAFORMA NATOURE”
                </li>
                <li>
                  No crear o registrar cuentas en la “PLATAFORMA NATOURE” con datos falsos a fin de
                  no poder comprobar su identidad
                </li>
                <li>
                  No prestar a un tercero los datos de la para que hagan uso “PLATAFORMA NATOURE” de
                  la misma a nombre del “VIAJERO NATOURISTA”
                </li>
                <li>
                  En caso de registrarse en la “PLATAFORMA NATOURE” como persona jurídica o moral,
                  garantiza tener la capacidad legal suficiente para formar parte de la “PLATAFORMA
                  NATOURE”
                </li>
                <li>
                  Actualizar frecuentemente la información o contenido de su perfil en la
                  “PLATAFORMA NATOURE”
                </li>
                <li>
                  No utilizar, copiar, adaptar, modificar, licenciar, distribuir, vender,
                  transferir, difundir, entre otras, los derechos de “NATOURE”, como lo son de
                  manera enunciativa más no limitativa: derechos de autor, marcas, patentes,
                  invenciones, avisos comerciales, logos o cualquier otro
                </li>
                <li>
                  Garantizar que es el único y exclusivo propietario del contenido que coloca en la
                  “PLATAFORMA NATOURE” o que cuenta con todos los derechos, licencias,
                  consentimientos y/o permisos sobre dicho contenido
                </li>
                <li>
                  En caso de contar con una membresía cumplir con el pago mensual de la misma
                  establecido por “NATOURE”, así como en caso de contratación el pago total de la
                  “EXPERIENCIA”
                </li>
                <li>
                  Responder por los daños y perjuicios que sus acciones pudieran causar durante y
                  después de llevarse a cabo la “EXPERIENCIA”
                </li>
                <li>
                  Respetar las indicaciones, normas y prohibiciones de cada una de las áreas de
                  importancia de conservación en donde se lleve a cabo la “EXPERIENCIA”
                </li>
                <li>
                  Cumplir con las políticas de reservación que se establecen en los presentes
                  Términos y Condiciones
                </li>
                <li>
                  Cumplir en todo momento con las políticas, requisitos e indicaciones brindadas por
                  el “ANFITRIÓN” para el debido desempeño de la “EXPERIENCIAS”
                </li>
                <li>
                  Informar al “ANFITRIÓN” de cualquier problema médico, físico, psicológico y demás
                  circunstancias que puedan afectar su capacidad, o la de cualquier huésped
                  adicional, para de esta manera participar de forma segura en la “EXPERIENCIA”
                </li>
                <li>
                  Cumplir con las leyes, normas, reglamentos y decretos vigentes aplicables sobre el
                  consumido de bebidas alcohólicas en las “EXPERIENCIAS”
                </li>
                <li>
                  No llevar a las “EXPERIENCIAS” contratadas, personas adicionales que no hayan sido
                  añadidas en la contratación
                </li>
                <li>
                  No realizar actividades o tener actitudes que vayan contra las buenas costumbres o
                  que inciten al odio, a la violencia o generen discriminación
                </li>
                <li>
                  En general cumplir con todas y cada una de las obligaciones y políticas que se
                  establezcan en los presentes Términos y Condiciones
                </li>
              </ol>
            </li>
            <li>
              OBLIGACIONES COMO ANFITRIÓN
              <p>
                Si eres un “ANFITRIÓN” de la “PLATAFORMA NATOURE” tienes las siguientes
                obligaciones:
              </p>
              <ol>
                <li>
                  Aceptar todas las normativas y políticas establecidas por “NATOURE” para el uso de
                  la “PLATAFORMA NATOURE” en el presente documento y/o cualquiera de sus
                  modificaciones
                </li>
                <li>
                  Contar con todas las capacidades legales para contratar de acuerdo a las leyes
                  mexicanas vigentes
                </li>
                <li>
                  Proporcionar información exacta, actualiza y completa que sea requerida por
                  “NATOURE” para registrarse en la “PLATAFORMA NATOURE”
                </li>
                <li>
                  No crear o registrar cuentas en la “PLATAFORMA NATOURE” con datos falsos a fin de
                  no poder comprobar su identidad
                </li>
                <li>
                  Proporcionar toda la información que sea necesaria y requerida por “NATOURE” para
                  realizar el estudio de viabilidad de la solicitud presentada
                </li>
                <li>Cumplir en tiempo y forma con la “EXPERIENCIA” contratada</li>
                <li>
                  No prestar a un tercero los datos de la “PLATAFORMA NATOURE” para que hagan uso de
                  la misma a nombre del “ANFITRIÓN”
                </li>
                <li>
                  Garantizar que prestará los servicios de “EXPERIENCIAS” contratadas por los
                  “VIAJEROS NATOURISTAS” de manera personal, por lo que se compromete a no
                  subcontratar a un tercero para la prestación de los servicios de “EXPERIENCIAS”
                </li>
                <li>
                  Deberá garantizar que el personal que asigne para llevar a cabo las “EXPERIENCIAS”
                  se encuentre debidamente capacitado
                </li>
                <li>
                  En caso de registrarse en la “PLATAFORMA NATOURE” como persona jurídica o moral,
                  garantiza tener la capacidad legal suficiente para formar parte de la “PLATAFORMA
                  NATOURE”
                </li>
                <li>
                  Actualizar frecuentemente la información o contenido de su perfil en la
                  “PLATAFORMA NATOURE”
                </li>
                <li>
                  No utilizar, copiar, adaptar, modificar, licenciar, distribuir, vender,
                  transferir, difundir, entre otras, los derechos de “NATOURE”, como lo son de
                  manera enunciativa más no limitativa: derechos de autor, marcas, patentes,
                  invenciones, avisos comerciales, logos o cualquier otro
                </li>
                <li>
                  Confirmar al “VIAJERO NATOURISTA” en un plazo no mayor a 72 horas, después de ser
                  notificado por “NATOURE” sobre una reservación, la disponibilidad para llevar a
                  cabo la “EXPERIENCIA” solicitada
                </li>
                <li>
                  Garantizar que es el único y exclusivo propietario del contenido que coloca en la
                  “PLATAFORMA NATOURE” o que cuenta con todos los derechos, licencias,
                  consentimientos y/o permisos sobre dicho contenido
                </li>
                <li>
                  Contar con todos los permisos, certificados, licencias y demás documentos
                  necesarios y requeridos por las autoridades correspondientes, leyes, normas y/o
                  reglamentos aplicables del lugar donde se llevarán a cabo las “EXPERIENCIAS”
                </li>
                <li>
                  No realizar publicaciones, ofertas y/o promociones falsas, ambiguas, engañosas que
                  creen confusión a los “USUARIOS” de la “PLATAFORMA NATOURE”
                </li>
                <li>
                  Emitir a nombre de “NATOURE” el respectivo comprobante fiscal por la cantidad que
                  corresponda por la prestación del servicio de “EXPERIENCIA” que contrate el
                  “VIAJERO NATOURISTA”, el cual debe cumplir con los requisitos fiscales que sean
                  exigidos por la legislación mexicana vigente
                </li>
                <li>
                  Dar a conocer a los “VIAJEROS NATOURISTAS” todas las medidas de seguridad
                  necesarias para llevar a cabo las “EXPERIENCIAS”, de manera enunciativa más no
                  limitativa, riesgos inherentes a la “EXPERIENCIA”, requisitos para participar en
                  la “EXPERIENCIA” como lo son edad mínima, aptitudes físicas y/o psicológicas,
                  equipos, indumentarias, así como cualquier otra medida de seguridad inherente para
                  llevar a cabo la “EXPERIENCIA”
                </li>
                <li>
                  Dar a conocer a los “VIAJEROS NATOURISTAS” los datos de contacto para que puedan
                  reportar alguna emergencia que pudiera llegarse a presentar en la “EXPERIENCIA”
                </li>
                <li>
                  No colocar en la información de perfil, publicaciones, promociones y demás
                  contenido de la “PLATAFORMA NATOURE” ningún tipo de datos de contacto como de
                  manera enunciativa más no limitativa: correo, teléfono, dirección, página web,
                  redes sociales, entre otro, que sirvan para establecer una comunicación directa
                  entre el “ANFITRIÓN” y el “VIAJERO NATOURISTA”
                </li>
                <li>
                  Responder por los daños y perjuicios que sus acciones pudiera causar durante y
                  después de llevarse a cabo la “EXPERIENCIA”
                </li>
                <li>
                  No realizar actividades o tener actitudes que vayan contra las buenas costumbres o
                  que inciten al odio, a la violencia o generen discriminación
                </li>
                <li>
                  Cumplir con las políticas de reservación que se establecen en los presentes
                  Términos y Condiciones
                </li>
                <li>
                  En general cumplir con todas y cada una de las obligaciones y políticas que se
                  establezcan en los presentes Términos y Condiciones
                </li>
              </ol>
            </li>
            <li>
              OBLIGACONES COMO EMBAJADOR DE NATURALEZA
              <p>
                Si eres un “EMBAJADOR DE NATURALEZA” de la “PLATAFORMA NATOURE” tienes las
                siguientes obligaciones:
              </p>
              <ol>
                <li>
                  Aceptar todas las normativas y políticas establecidas por “NATOURE” para el uso de
                  la “PLATAFORMA NATOURE” en el presente documento y/o cualquiera de sus
                  modificaciones
                </li>
                <li>
                  Contar con todas las capacidades legales para contratar de acuerdo a las leyes
                  mexicanas vigentes
                </li>
                <li>
                  Proporcionar información exacta, actualiza y completa que sea requerida por
                  “NATOURE” para registrarse en la “PLATAFORMA NATOURE”
                </li>
                <li>
                  No crear o registrar cuentas en la “PLATAFORMA NATOURE” con datos falsos a fin de
                  no poder comprobar su identidad
                </li>
                <li>
                  No prestar a un tercero los datos de la “PLATAFORMA NATOURE” para que hagan uso de
                  la misma a nombre del “EMBAJADOR DE NATURALEZA”
                </li>
                <li>
                  En caso de registrarse en la “PLATAFORMA NATOURE” con persona jurídica o moral,
                  garantiza tener la capacidad legal suficiente para formar parte de la “PLATAFORMA
                  NATOURE”
                </li>
                <li>
                  Actualizar frecuentemente la información o contenido de su perfil en la
                  “PLATAFORMA NATOURE”
                </li>
                <li>
                  No utilizar, copiar, adaptar, modificar, licenciar, distribuir, vender,
                  transferir, difundir, entre otras, los derechos de “NATOURE”, como lo son de
                  manera enunciativa más no limitativa: derechos de autor, marcas, patentes,
                  invenciones, avisos comerciales, logos o cualquier otro
                </li>
                <li>
                  Garantizar que es el único y exclusivo propietario del contenido que coloca en la
                  “PLATAFORMA NATOURE” o que cuenta con todos los derechos, licencias,
                  consentimientos y/o permisos sobre dicho contenido
                </li>
                <li>
                  Brindar información verídica y actualizada de las Áreas de Importancia de
                  Conservación
                </li>
                <li>
                  Colaborar con “NATOURE” de manera conjunta y sin fines de lucro sobre el llenado y
                  levantamiento de información sobre los aspectos más relevantes de las Áreas de
                  Importancia de Conservación
                </li>
                <li>
                  Colaborar con “NATOURE” de manera conjunta y sin fines de lucro sobre la
                  recolección de fondos y aportaciones económicas para el desarrollo de las Áreas de
                  Importancia de Conservación
                </li>
                <li>
                  Comunicar a los “USUARIOS” de la “PLATAFORMA NATOURE” sobre cualquier evento
                  extraordinario, caso fortuito o de fuerza mayor por la que no se puedan llevar a
                  cabo las “EXPERIENCIAS” en las Áreas de Importancia de Conservación
                </li>
                <li>
                  Informar sobre normas, reglamentos, prohibiciones que sean impuestas en las Áreas
                  de Importancia de Conservación
                </li>
                <li>
                  No realizar actividades o tener actitudes que vayan contra las buenas costumbres o
                  que inciten al odio, a la violencia o generen discriminación
                </li>
                <li>
                  En general cumplir con todas y cada una de las obligaciones y políticas que se
                  establezcan en los presentes Términos y Condiciones
                </li>
              </ol>
            </li>
            <li>
              POLÍTICAS DE RESERVACIÓN DE EXPERIENCIAS<p>Aplicable a los “VIAJEROS NATOURISTAS”:</p>
              <ol>
                <li>
                  Deberá cumplir con el registro de la cuenta en la “PLATAFORMA NATOURE”, así como
                  las demás obligaciones establecidas en los presentes Términos y Condiciones
                </li>
                <li>
                  Sin perjuicio del cumplimiento de los requisitos que establezca el “ANFITRIÓN” los
                  “VIAJEROS NATOURISTAS” pueden reservar una “EXPERIENCIA” disponible en la
                  “PLATAFORMA NATOURE”, siempre cumpliendo con las políticas establecidas en los
                  presentes términos y condiciones y/o su modificación
                </li>
                <li>
                  Antes de reservar una “EXPERIENCIA” se le presentarán todas las tarifas
                  aplicables, incluyendo los impuestos y comisiones correspondientes (en adelante
                  “TARIFA TOTAL”)
                </li>
                <li>
                  Una vez reservada la “EXPERIENCIA”, se obliga a pagar la “TARIFA TOTAL” por la
                  contratación de dicha “EXPERIENCIA”
                </li>
                <li>
                  En el supuesto de contratar una “EXPERIENCIA” ofrecida por el “ANFITRIÓN” en
                  nombre de personas adicionales, se obliga a asegurarse de que cada una de las
                  personas adicionales, cumple los requisitos establecidos por el “ANFITRIÓN”, así
                  como con las políticas establecidas en los presentes Términos y Condiciones. En
                  caso de que las personas adicionales sean menores de edad, deberá contar con los
                  permisos legales para poder realizar la “EXPERIENCIA” contratada
                </li>
                <li>
                  Deberá revisar detenidamente las especificaciones, características y requisitos
                  establecidos por el “ANFITRIÓN” para la “EXPERIENCIA” sobre la cual desee
                  contratar
                </li>
              </ol>
            </li>
            <li>
              Aplicables a los “ANFITRIONES”:
              <ol>
                <li>
                  Deberán cumplir con el registro de la cuenta en la “PLATAFORMA NATOURE”, enviando
                  el llenado de la información solicitada por “NATOURE”. Las “EXPERIENCIAS” que
                  ofrezcan, deberá cumplir en todo momento los más altos niveles de calidad de
                  servicios y el “ÍNDICE DE SOSTENIBILIDAD” que en este caso se auto evalúa al
                  iniciar en la “PLATAFORMA NATOURE”. “NATOURE” tiene total derecho a decidir, a su
                  exclusivo criterio, si el “ANFITRIÓN” puede o no publicar, ofrecer, anunciar en la
                  “PLATAFORMA NATOURE” sus “EXPERIENCIAS”
                </li>
                <li>
                  Una vez que haya recibido el correspondiente correo electrónico y/o mensaje por
                  parte de la “PLATAFORMA NATOURE” donde se le informa sobre la contratación de la
                  “EXPERIENCIA” por parte del “VIAJERO NATOURISTA”, el ”ANFITRIÓN” se obliga en un
                  plazo no mayor a setenta y dos (72) horas después de haber recibido dicho correo
                  electrónico, a comunicarse con el “VIAJERO NATOURISTA” a través de cualquier medio
                  que tenga a su disposición (entendiendo como estos: correo electrónico, mensajes
                  por la Aplicación, sitio web, número de teléfono, entre otros) con la finalidad de
                  confirmar la prestación del servicio de “EXPERIENCIA” y coordinar la logística
                  necesaria
                </li>
              </ol>
            </li>
            <li>
              Aplicables a los “VIAJEROS NATOURISTAS” y “ANFITRIONES”:
              <ol>
                <li>
                  Una vez confirmada la recepción de una reserva de “EXPERIENCIA”, se crea un
                  contrato legalmente vinculante entre el “VIAJERO NATOURISTA” y el “ANFITRIÓN”, los
                  cuales tendrán sus propios términos y condiciones y políticas. Eximiendo de
                  cualquier responsabilidad a “NATOURE” sobre algún tipo de reclamación o daños
                  ocasionados
                </li>
              </ol>
            </li>
            <li>
              CONTENIDO DE LA PLATAFORMA NATOURE
              <p>
                “NATOURE” podrá a su pleno criterio, permitir a los “USUARIOS” de la “PLATAFORMA
                NATOURE” que:
              </p>
              <ol>
                <li>
                  Creen, suban, publiquen, envíen, reciban y almacenen contenido, como de forma
                  enunciativa más no limitativa podrán ser fotos, textos, audios, videos y demás
                  información“USUARIOS”, así como cualquier contenido que “NATOURE” ponga a
                  disposición a través de sus sitios web y el contenido que se publique bajo
                  licencia o autorización de un tercero
                </li>
                <li>
                  Accedan y visualicen el contenido de los demás “USUARIOS”, así como cualquier
                  contenido que “NATOURE” ponga a disposición a través de sus sitios web y el
                  contenido que se publique bajo licencia o autorización de un tercero
                </li>
              </ol>
            </li>
            <li>
              Los “USUARIOS” de la “PLATAFORMA NATOURE” no publicarán, subirán, difundirán, enviarán
              ni trasmitirán ningún tipo de contenido que:
              <ol>
                <li>
                  Sea fraudulento, falso, engañoso que cause confusión a los demás “USUARIOS” de la
                  “PLATAFORMA NATOURE” ya sea por negligencia, imprudencia o intensión
                </li>
                <li>Sea difamatorio, injurioso, obsceno, pornográfico, vulgar u ofensivo</li>
                <li>
                  Fomente la discriminación, la intolerancia, el racismo, el odio, el acoso o el
                  daño contra cualquier persona o grupo de personas
                </li>
                <li>
                  Sea violento, amenazador o incite a la violencia o a realizar acciones que sean
                  amenazantes sobre cualquier persona
                </li>
                <li>Fomente el uso o actividades de sustancias ilícitas o nocivas para la salud</li>
                <li>
                  En general cualquier contenido que incumplan las políticas establecidas en la
                  “PLATAFORMA NATOURE”
                </li>
              </ol>
            </li>
            <li>
              PROPIEDAD DEL CONTENIDO DE LA PLATAFORMA NATOURE
              <p>
                Los “USUARIOS” de la “PLATAFORMA NATOURE” aceptan y declaran que es de total
                entendimiento que la “PLATAFORMA NATOURE”, así como los programas, bases de datos,
                redes, archivos, contenido de la misma están protegidos en su totalidad o en parte
                por derechos de autor y demás derechos establecidos en las leyes aplicables, es por
                ello, que aceptan y respetan que son propiedad exclusiva de “NATOURE” y/o de sus
                licenciantes o terceros otorgantes Derivado de lo anterior, los “USUARIOS”
                garantizan que no utilizarán, copiarán, adaptarán, modificarán, distribuirán,
                licenciarán, venderán, transferirán, exhibirán, difundirán ni explotarán la
                “PLATAFORMA NATOURE” así como el contenido de la misma, salvo sobre el contenido que
                sea de su propiedad, del que tenga autorización legal y escrita por parte de
                “NATOURE” para dichas acciones o conforme a lo expresamente permitido en los
                presentes Términos y Condiciones Así mismo, los “USUARIOS” se obligan a que el
                contenido que compartan o publiquen en la “PLATAFORMA NATUORE”, será de creación
                exclusiva y original de él mismo; obligándose a no infringir derechos de propiedad
                intelectual de terceros y a no divulgar propiedad intelectual respecto de la cual no
                tenga legitimación para ello Los “USUARIOS” conceden a “NATOURE” una licencia no
                exclusiva, internacional, libre de regalías, irrevocable (durante el tiempo que dure
                la relación con “NATOURE”), sublicenciable y transferible sobre el contenido que
                publique en la “PLATAFORMA NATOURE”, para que “NATOURE” pueda acceder, utilizar,
                almacenar, copiar, modificar, preparar obras derivadas, distribuir, publicar,
                transmitir, emitir en continuo, difundir y explotar de cualquier otro modo o manera
                dicho contenido, a fin de proporcionar y/o promocionar la “PLATAFORMA NATOURE” en
                cualquier otro medio Los “ANFITRIONES” autorizan a “NATOURE” para que a su exclusiva
                consideración contrate profesionales de las áreas de comunicación como fotógrafos,
                productores, camarógrafos, entre otros, para documentar las “EXPERIENCIAS”
                ofrecidas. Derivado de lo anterior, los “USUARIOS” reconoce y acepta que “NATOURE”
                tendrá derecho a utilizar el contenido documentado a fin de usarlo en campañas
                publicitarias, fines mercadotécnicos y/o cualquier otro fin que “NATOURE” considere
                necesario, sin que esto implique pago alguno, retribución o regalía
              </p>
            </li>
            <li>
              TARIFAS DE SERVICIO
              <p>
                El registro en la “PLATAFORMA NATOURE” para los “USUARIOS” es de manera gratuita.
                Existe cierto contenido, beneficios y “EXPERIENCIAS” exclusivas en la “PLATAFORMA
                NATOURE” para los cuales es necesario contar con una membresía, la cual podrá ser
                adquirida por el “VIAJERO NATOURISTA” y tendrá un costo mensual de $99.99 (noventa y
                nueve pesos 99/100 M.N.) o lo que corresponda al tipo de cambio de la moneda
                nacional En la contratación de una “EXPERIENCIA” por parte del “VIAJERO NATOURISTA”,
                “NATOURE” cobrará al “VIAJERO NATOURISTA” una comisión de hasta el dieciocho por
                ciento (18%), la cual ya se encuentra incluida en el valor de la experiencia
                publicada en la “PLATAFORMA NATOURE”
              </p>
            </li>
            <li>
              MÉTODOS DE PAGO
              <p>
                El “VIAJERO NATOURISTA” deberá hacer el pago en una sola exhibición y contará con
                los siguientes métodos para poder realizar los pagos de reserva y liquidación
                referente a las “EXPERIENCIAS”:
              </p>
              <ol>
                <li>
                  Cobro directo a través de nuestra “PLATAFORMA NATOURE” con tarjeta de debido,
                  tarjeta de crédito y/o Paypal
                </li>
                <li>
                  Mediante línea de captura la cual podrá ser pagadera a través de transferencias
                  bancarias, deposito en ventanilla bancaria, pago en comercios autorizados como
                  OXXO
                </li>
              </ol>
            </li>
            <li>
              En aquellos casos en los que “NATOURE” proporcione al “VIAJERO NATOURISTA” una
              referencia de pago, “NATOURE” no será responsable en caso de que dicha referencia de
              pago proporcionada por el “VIAJERO NATOURISTA” sea incorrecta, siendo el “VIAJERO
              NATOURISTA” el único responsable de cerciorarse que el pago se realice a la referencia
              de pago proporcionada por “NATOURE”. Todo pago que sea recibido con información
              incorrecta de conformidad con el presente apartado tendrá como consecuencia la
              cancelación de la Reservación sin responsabilidad para “NATOURE”. El “VIAJERO
              NATOURISTA” en este acto libera a “NATOURE” de toda responsabilidad generada por
              cualquier error en la coincidencia de la referencia de pago y/o datos al momento de
              realizar su pago, así como por pagos realizados fuera del tiempo establecido
            </li>
            <li>
              CÁLCULO DE PAGO PARA EL “ANFITRIÓN”
              <p>
                El monto a ser depositado a “EL ANFITRIÓN” por la contratación de alguna
                “EXPERIENCIA” ofrecida, será el total de la aplicación de la siguiente formula:
                Monto a ser depositado a “EL ANFITRIÓN “ = (Monto aportado por el “VIAJERO
                NATOURISTA”) – (Comisión a favor de “NATOURE”) – (IVA para facturación) La
                transferencia de los fondos al “ANFITRIÓN” por parte de “NATOURE” se procesará
                dentro de los seis (6) días hábiles siguientes a la fecha en la que se llevó a cabo
                la “EXPERIENCIA” por parte del “ANFITRIÓN” a la cuenta bancaria que haya
                proporcionado a “NATOURE”.
                <p>
                  <small>
                    La fórmula establecida en el presente numeral, podrá modificarse de acuerdo a
                    las políticas y requisitos fiscales aplicables para cada “ANFITRIÓN”, según lo
                    establecido en las leyes fiscales mexicanas vigentes
                  </small>
                </p>
              </p>
            </li>
            <li>
              POLÍTICAS DE CANCELACION Y CAMBIO DE FECHAS DE LAS EXPERIENCIAS
              <p>
                Los “VIAJEROS NATOURISTAS” y “ANFITRIONES” aceptan y están de acuerdo en que
                “NATOURE” no acepta la cancelación de “EXPERIENCIAS” previamente adquiridas por el
                “VIAJERO NATOURISTA” o en su caso ofrecidas por el “ANFITRIÓN”, salvo por lo
                establecido en los dos siguientes párrafos
              </p>
              <p>
                El “ANFITRIÓN” únicamente podrá cancelar la “EXPERIENCIA” contratada, por causa de
                casos fortuitos o de fuerza mayor, como pueden ser condiciones meteorológicas o
                desastres naturales, que los mismos impidan poder llevar a cabo la “EXPERIENCIA”,
                por lo que sí así sucediera deberá enviar en los casos que aplique, la prueba
                documentada que conlleva a dicha cancelación
              </p>
              <p>
                En caso de que el “VIAJERO NATOURISTA” llegase a cancelar la “EXPERIENCIA” sin que
                se trate de caso fortuito o fuerza mayor, se aplicarán las políticas de reembolso
                establecidas en los presentes términos y condiciones
              </p>
              <p>
                De igual forma para solicitar la devolución, se deberá acreditar fehacientemente
                haber realizado el pago correspondiente por la Reservación de la “EXPERIENCIA” a
                “NATOURE”. Si la “EXPERINCIA” cancelada fue adquirida a través de nuestro Sitio en
                Internet, utilizando una tarjeta de crédito o débito, el pago de la devolución, a la
                que se hace referencia en el párrafo inmediato anterior, se procesará sólo a la
                misma tarjeta de crédito o débito con la que se compró la Reservación de la
                “EXPERIENCIA”. El tiempo en el que se verá reflejado la devolución aquí señalada
                dependerá de la institución bancaria emisora de la tarjeta
              </p>
              <p>
                El “VIAJERO NATOURISTA” solo podrá modificar por una (1) sola vez la fecha en la que
                se llevará a cabo la “EXPERIENCIA”, debiendo avisar al “ANFITRIÓN” para que este
                último indique si cuenta o no con disponibilidad para el cambio de fecha solicitado
                o sugiera una fecha para dicho cambio. Así mismo, el “VIAJERO NATOURISTA” se
                encuentra en total entendimiento de que las tarifas de la “EXPERIENCIA” pueden
                variar por distintos factores, por lo que en caso de llevarse a cabo lo establecido
                en el presente párrafo, el “VIAJERO NATOURISTA” cubrirá las diferencias en el monto
              </p>
              <p>
                En caso de que el “ANFITRIÓN” no llegase a contar con disponibilidad para el cambio
                de fecha solicitado por el “VIAJERO NATOURISTA” o este último no esté de acuerdo con
                la fecha sugerida por el “ANFITRIÓN” se aplicará lo establecido en las políticas de
                reembolso de los presentes Términos y Condiciones
              </p>
              <p>
                Cabe aclarar que las “EXPERIENCIAS” contratadas por los “USUARIOS” son
                intransferibles, por lo que, en caso de que el “VIAJERO NATOURISTA” no aproveche las
                “EXPERIENCIAS” contratadas en los términos pactados por cualquier motivo, podrá
                únicamente solicitar cambio de fecha de la “EXPERIENCIA” siempre que el “ANFITRIÓN”
                pueda y se encuentre en disponibilidad de modificar dicha fecha, en caso de no
                realizar no se aplicará nuestra política de reembolso
              </p>
            </li>
            <li>
              POLÍTICAS DE REEMBOLSO
              <p>
                Entendemos que existen situaciones que pueden hacer que la prestación de las
                “EXPERIENCIAS” se vuelvan complicadas o imposibles, por caso fortuito o fuerza mayor
                tanto por parte del “VIAJERO NATOURISTA” como del “ANFITRIÓN”, por lo que ofrecemos
                la posibilidad hacer una reprogramación del servicio o en caso de que sea imposible
                lo anterior existe la posibilidad de realizar la devolución a los “VIAJEROS
                NATOURISTAS” del cien por ciento (100%) del monto que pagó por las “EXPERIENCIAS”
                menos gastos administrativos, de acuerdo con las políticas de cancelación
                establecidas en los presentes Términos y Condiciones
              </p>
              <p>
                En caso de cancelación por parte del “ANFITRIÓN” sin que se trate de caso fortuito o
                fuerza mayor, le será devuelto al “VIAJERO NATOURISTA” el cien por ciento (100%) del
                monto pagado por la “EXPERIENCIA”, obligándose el “ANFITRIÓN” a cubrir los gastos
                administrativos generados por la reservación de la “EXPERIENCIA”, los cuales le
                serán descontados de la siguiente “EXPERIENCIA” que le haya sido contratada
                “NATOURE” procederá a aplicar sus políticas de reembolso en caso de cancelación por
                parte del “VIAJERO NATOURISTA” de la siguiente manera:
              </p>
              <table>
                <thead>
                  <tr>
                    <th>Antelación a la “EXPERIENCIA”</th>
                    <th>REEMBOLSO APLICABLE</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      Dentro de las (24) veinticuatro horas posteriores a la generación de la
                      Reservación de la “EXPERIENCIA”, (siempre y cuando no se contrate la misma
                      faltando de uno (1) a cinco (5) días naturales para llevarse a cabo dicha
                      “EXPERIENCIA”)
                    </td>
                    <td>
                      100% por el monto total pagado, menos los gastos de operación generados de la
                      contratación
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Faltando entre (10) diez o más días naturales previos a la realización de la
                      “EXPERIENCIA”
                    </td>
                    <td>
                      70% por el monto total pagado, menos los gastos de operación generados de la
                      contratación
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Faltando entre (6) seis días a (9) nueve días naturales previos a la
                      realización de la “EXPERIENCIA”
                    </td>
                    <td>
                      50% por el monto total pagado, menos los gastos de operación generados de la
                      contratación
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Faltando entre (1) un día a (5) cinco días naturales previos a la realización
                      de la “EXPERIENCIA
                    </td>
                    <td>0%</td>
                  </tr>
                </tbody>
              </table>
            </li>
            <li>
              El monto que resulte sobre el reembolso podrá tomarse como saldo a favor del “VIAJERO
              NATOURISTA” para contratar otra “EXPERIENCIA” El término para realizar dicho reembolso
              es de 10 (diez) días hábiles posteriores en que se haya realizado la cancelación.
            </li>
            <li>
              PROMOCIÓN DE LA PLATAFORMA NATOURE
              <p>
                Con la finalidad de promocionar la “PLATAFORMA NATOURE” y aumentar la exposición de
                los anuncios de los “ANFITRIONES” hacia “VIAJEROS NATOURISTAS” potenciales, el
                contenido de la “PLATAFORMA NATOURE” podrá ser exhibido en otros sitios web,
                aplicaciones para tabletas, celulares y demás dispositivos móviles, correo
                electrónico y publicidad en línea o no en línea
              </p>
              <p>
                La “PLATAFORMA NATOURE” podrá contener enlaces a sitios web de terceros; dichos
                servicios de terceros podrán estar sujetos a distintos Términos y Condiciones, así
                como a distintas políticas de privacidad. “NATOURE” no es responsable sobre el
                contenido, información, anuncios, servicios, entre otros, ofrecidos en los sitios
                web de terceros. Los enlaces a dichos sitios web no constituyen o crean ninguna
                garantía o aval por parte de “NATOURE”
              </p>
              <p>
                Para poder alcanzar una mayor red de “USUARIOS” de la “PLATAFORMA NATOURE”, es
                posible que el contenido de dicha plataforma sea traducido total o parcialmente a
                otros idiomas. Derivado de lo anterior, “NATOURE” no puede garantizar la exactitud o
                calidad de dichas traducciones; la “PLATAFORMA NATOURE” puede contener traducciones
                impulsadas por “Google” y este último no garantiza las traducciones expresas o
                implícitas, así como exactitud y confiabilidad en dichas traducciones. Es por ello,
                que los “USUARIOS” son responsables de revisar y comprobar la exactitud de dichas
                traducciones relacionadas con el contenido, servicios y/o “EXPERIENCIAS” publicadas
                en la “PLATAFORMA NATOURE” eximiendo de cual responsabilidad legal a “NATOURE”
              </p>
            </li>
            <li>
              RESTRICCIONES DE LA PLATAFORMA
              <p>
                Los “USUARIOS” para poder acceder a la “PLATAFORMA NATOURE” deberán ser mayores de
                dieciocho (18) años, por lo que se eximen a “NATOURE” de cualquier responsabilidad
                en caso de infringir las políticas de restricciones Los terceros no podrán acceder a
                la “PLATAFORMA NATOURE” con los datos de acceso del “USUARIO” ni hacer uso de la
                “PLATAFORMA NATOURE” en nombre del “USUARIO”
              </p>
            </li>
            <li>
              RESPONSABILIDAD
              <p>
                “NATOURE” se exime de cualquier responsabilidad en el caso de que el “ANFITRIÓN” no
                cuente con los permisos, certificados, registros y demás documentos necesarios y
                exigibles por las leyes, normas, reglamentos y/o decretos gubernamentales, estatales
                y municipales del lugar donde se llevará a cabo las “EXPERIENCIAS” ofrecidas
              </p>
              <p>
                Es posible que algunos servicios que ofrece el “ANFITRIÓN” generen riesgo de
                enfermedades, lesiones, incapacidades y cualquier otro tipo de riesgo, así como
                condiciones físicas y psicológicas que deben tener el “VIAJERO NATOURISTA” y este
                último asume libre y voluntariamente tales riesgos y condiciones, por lo que exime a
                “NATOURE” de cualquier responsabilidad por los hechos que pudiesen generarse antes,
                durante y después de su participación en la “EXPERIENCIA” contratadas a el
                “ANFITRIÓN”
              </p>
              <p>
                En caso de que el “VIAJERO NATOURISTA” lleve un menor de edad a la “EXPERIENCIA”,
                este asume total responsabilidad y supervisión sobre dicho menor a lo largo de todo
                la “EXPERIENCIA”, respondiendo por cualquier daño, pérdida, lesión o hasta
                fallecimiento que pudiese ocasionarse. Así mismo, deberá contar con todos los
                permisos legales que se llegasen a requerir por leyes, normas, reglamentos, derechos
                o autoridades correspondientes para que el menor pueda participar en la
                “EXPERIENCIA” de acuerdo con el país o ciudad donde se llevará a cabo la misma
              </p>
              <p>
                El “ANFITRIÓN” es totalmente responsable de los anuncios, servicios y “EXPERIENCIAS”
                que ofrece a través de la “PLATAFORMA NATOURE”. Cuando los “VIAJEROS NATOURISTAS”
                realizan una reservación, suscriben directamente con el “ANFITRIÓN” un contrato de
                prestación de servicios. Derivado de lo anterior, “NATOURE” se exime de cualquier
                tipo de responsabilidad sobre la prestación del servicio de “EXPERIENCIA” por parte
                del “ANFITRIÓN” toda vez que “NATOURE” no actúa como patrono, agente, empleado del
                “ANFITRIÓN”. “NATOURE” será responsable únicamente sobre los términos de pago tanto
                por parte del “VIAJERO NATOURISTA” como de “NATOURE” al “ANFITRIÓN” realizados
                dentro de la plataforma, respetando lo establecido en las políticas de los presentes
                términos y condiciones. Así mismo, toda vez que “NATOURE” es un punto de encuentro
                entre los “USUARIOS” y no participa en las operaciones que se realizan entre ellos,
                el “ANFITRIÓN” será el único responsable por todas las obligaciones y cargas
                impositivas que correspondan por la publicación y promoción de sus “PRODUCTOS”, sin
                que pudiera imputársele a “NATOURE” algún tipo de responsabilidad por
                incumplimientos
              </p>
            </li>
            <li>
              EVALUACIONES Y COMENTARIOS
              <p>
                Dentro de un plazo no mayor a diez (10) días hábiles contados a partir de llevarse a
                cabo la “EXPERIENCIA”, tanto el “VIAJERO NATOURISTA” como el “ANFITRIÓN” deberán
                dejar un comentario público y enviar una evaluación la cual se reflejará en
                estrellas. Así mismo, los resultados de las evaluaciones se traducirán en “ÍNDICE DE
                SOSTENIBILIDAD”
              </p>
              <p>
                Las evaluaciones y comentarios por parte del “VIAJERO NATOURISTA” y el “ANFITRIÓN”
                deberán ser justas, veraces y objetivas, es por ello que las mismas no podrán tener
                lenguajes ofensivos, injuriosos o en contra de las buenas costumbres Los comentarios
                y evaluaciones son exclusivamente personales de cada uno de los “USUARIOS” de la
                “PLATAFORMA NATOURE”, por lo que no se entenderán como evaluaciones o comentarios de
                parte de “NATOURE”
              </p>
              <p>
                El “VIAJERO NATOURISTA” y el “ANFITRIÓN” podrán presentar quejas por algún
                inconveniente en el servicio de “EXPERIENCIA”; “NATOURE” podrá intervenir y ayudar
                en la solución de dichos conflictos siempre dentro de lo establecido en las
                políticas de resolución de conflictos de los siguientes Términos y Condiciones
              </p>
            </li>
            <li>
              POLÍTICAS DE CANCELACIÓN DE CUENTA
              <p>
                “NATOURE” podrá, a su pleno derecho y sin previo aviso, retirar o desactivar el
                acceso a cualquier “USUARIO” de la “PLATAFORMA NATOURE” cuando se considere que
                incumple con cualquiera de las políticas establecidas en los presentes Términos y
                Condiciones o que por algún modo se consideren contra las buenas costumbres y sea
                dañino para los demás “USUARIOS”
              </p>
              <p>
                Así mismo, si los “USUARIOS” llegasen a infringir derechos de propiedad intelectual
                de terceros, “NATOURE” podrá a su discreción cancelar la cuenta. De igual manera,
                los “USUARIOS” podrán ser acreedores de las sanciones legales correspondientes por
                dicho incumplimiento
              </p>
              <p>
                Derivado de lo anterior, “NATOURE” podrá rescindir de la relación jurídica existente
                en los presentes términos y condiciones, de manera inmediata y sin previo aviso,
                cuando:
              </p>
              <ol>
                <li>
                  Haya incumplido sustancialmente sus obligaciones en virtud de las políticas
                  establecidas en el presente documento, los términos de pago y demás políticas
                </li>
                <li>
                  Haya vulnerado alguna ley, normativa o derechos de terceros que le sean de
                  aplicación
                </li>
                <li>
                  “NATOURE” estime de buena fe que tal acción resulta razonablemente necesaria para
                  proteger la seguridad personal o los bienes materiales de ”NATOURE”, sus
                  “USUARIOS” o terceros
                </li>
              </ol>
            </li>
            <li>
              De igual forma, podrá ser cancelada la cuenta del “ANFITRIÓN” cuando este incumpla en
              dos ocasiones con las políticas de cancelación establecidas en los presentes términos
              y condiciones De ser aplicadas las políticas de cancelación de cuenta, no tendrá
              derechos a que se restablezca la cuenta en la “PLATAFORMA NATOURE”, así mismo, no
              podrá registrar una nueva cuenta en la “PLATAFORMA NATOURE” ni hacer uso de la
              “PLATAFORMA NATOURE”
            </li>
            <li>
              REDONDEO Y CONVERSIÓN MONETARIA
              <p>
                “NATOURE” podrá, a su exclusivo criterio, redondear al alza o a la baja los importes
                pagaderos por los “USUARIOS” por la contratación de las “EXPERIENCIAS” a la unidad
                de base funcional entera más cercana en función del proceso de conversión de divisa
                del dólar americano a la moneda local aplicable. La conversión monetaria se
                calculará en hasta tres (3) puntos porcentuales por encima a lo establecido por el
                Banco Central de cada País
              </p>
            </li>
            <li>
              ACTURACIÓN
              <p>
                El “VIAJERO NATOURISTA” podrá requerir su factura por la contratación de la
                “EXPERIENCIA” que brinda el “ANFITRIÓN” al correo electrónico facturas@natoure.info
                adjuntando la orden de pago correspondiente y datos fiscales correspondientes. El
                departamento de contabilidad tendrá un plazo de 72 horas para enviar dicha factura
                al correo electrónico proporcionado por el “VIAJERO NATOURISTA”
              </p>
              <p>
                Las facturas se realizarán o emitirán hasta el día treinta (30) del mes
                correspondiente en el que se realizó la reserva
              </p>
              <p>
                El “ANFITRIÓN” deberá emitir a nombre de “NATOURE” su respectivo comprobante fiscal
                por la cantidad que corresponda por la prestación del servicio de “EXPERIENCIA” que
                contrate el “VIAJERO NATOURISTA”. Dicho comprobante deberá cumplir con los
                requisitos fiscales que impongan las leyes mexicanas vigentes para cada caso en
                particular
              </p>
              <p>
                El “ANFITRIÓN” deberá emitir dicho comprobante fiscal al momento de darse inicio a
                la “EXPERIENCIA” contratada por el “VIAJERO NATOURISTA”
              </p>
            </li>
            <li>
              IMPUESTOS
              <p>
                Es posible que la contratación de servicios, anuncios y/o “EXPERIENCIAS” publicadas
                en la “PLATAFORMA NATOURE” generen impuestos de acuerdo a las normas, leyes, códigos
                financieros, reglamentos fiscales tanto federales como estatales aplicables en cada
                caso. Es por ello que cada uno de los “USUARIOS” de la “PLATAFORMA NATOURE” asume la
                responsabilidad de cumplir con sus obligaciones fiscales con relación a los
                servicios o por cualquier otro concepto derivado de las relaciones jurídicas.
              </p>
            </li>
            <li>
              RESOLUCIÓN DE CONFLICTO
              <p>
                “NATOURE” podrá ayudar a facilitar la resolución de disputas, conflictos e
                inconvenientes que puedan presentarse entre los “USUARIOS” de la “PLATAFORMA
                NATOURE” siempre que considere que está entre sus posibilidades y sin infringir
                ninguna normativa legal aplicable, no teniendo obligación alguna en la resolución de
                conflictos entre los “USUARIOS”; es por ello, que cabe señalar que “NATOURE” no
                tiene control ni garantiza:
              </p>
              <ol>
                <li>
                  La existencia, calidad, seguridad, idoneidad o legalidad de ningún tipo de
                  anuncio, servicio y/o “EXPERIENCIA” que ofrece el “ANFITRIÓN”
                </li>
                <li>
                  La veracidad o exactitud de las descripciones de ningún anuncio, así como de las
                  evaluaciones, comentarios y demás contenido del “VIAJERO NATOURISTA”
                </li>
                <li>La conducta de ningún “VIAJERO NATOURISTA” o tercero</li>
              </ol>
            </li>
            <li>
              RELACIÓN JURÍDICA
              <p>
                El uso de la “PLATAFORMA NATOURE” no crea entre “NATOURE” y los “USUARIOS” de dicha
                plataforma ningún tipo de relación jurídica como, empleado, patrono, agente, socio
                comercial, encargado, prestador de servicio. Es por ello, que los “USUARIOS” se
                comprometen a actuar exclusivamente en su propio nombre e interés y no en nombre ni
                en interés de “NATOURE” “NATOURE” no avala a ningún “USUARIO”. Toda referencia a un
                “USUARIO” verificado (u otro término similar que se utilice) únicamente indica que
                ese “USUARIO” ha completado el proceso de verificación o identificación
                correspondientes. Este tipo de descripción no supone una garantía, aval, respaldo,
                certificado por parte de “NATOURE” acerca de algún “USUARIO”
              </p>
            </li>
            <li>
              ACUERDO ÚNICO
              <p>
                Salvo por el hecho de que puedan ser complementados por un documento referenciado e
                incorporado al presente o por políticas adicionales de “NATOURE”, directrices, y
                normas, o los términos aplicables a un producto, función, servicio u oferta
                concretos, estos Términos y Condiciones y el Aviso de Privacidad de “NATOURE”
                constituyen el acuerdo completo y exclusivo entre “NATOURE” y los “USUARIOS” de la
                “PLATAFORMA NATOURE”
              </p>
            </li>
            <li>
              CESIÓN
              <p>
                Los “USUARIOS” no podrán ceder ni transferir estos Términos, ni por imperativo legal
                ni de ninguna otra manera, sin el previo consentimiento por escrito de “NATOURE”.
                Todo intento por su parte de ceder o transferir estos Términos sin haber recabado
                dicho consentimiento será nulo y carecerá de validez. “NATOURE” podrá ceder o
                transferir estos Términos a su exclusivo criterio y sin restricción alguna. Sin
                perjuicio de lo anterior, estos términos obligarán y redundarán en beneficio de
                “NATOURE”, sus sucesores y cesionarios autorizados
              </p>
            </li>
            <li>
              MODIFICACIONES
              <p>
                “NATOURE” se reserva el derecho de modificar o actualizar los presentes Términos y
                Condiciones o el que haya puesto a su disposición en cualquier momento, según sea
                necesario o conveniente, por ejemplo, para cumplir con cambios a la legislación o
                cumplir con políticas internas o nuevos requerimientos para la prestación u
                ofrecimiento de nuestros servicios. “NATOURE” pondrá a su disposición los Términos y
                Condiciones actualizados dentro de este sitio web sección Términos y Condiciones e
                indicará la fecha de última actualización del presente aviso
              </p>
            </li>
            <li>
              NOTIFICACIONES
              <p>
                Toda notificación o demás comunicaciones permitidas o requeridas en virtud del
                presente documento, incluidas las relativas a sus modificaciones, se realizarán a
                través del correo electrónico, mediante notificación en la “PLATAFORMA NATOURE” o
                por mensaje incluido SMS y/o Chat
              </p>
            </li>
            <li>
              PROTECCIÓN DE DATOS PERSONALES
              <p>
                Para mayor información acerca del tratamiento y protección de sus Datos Personales
                los “USUARIOS” deberán ingresar al Aviso de Privacidad contenido en la siguiente
                dirección https://natoure.org/politica-de-privacidad
              </p>
            </li>
            <li>
              LEGISLACIÓN APLICABLE Y JURISDICCIÓN
              <p>
                Al hacer uso de la “PLATAFORMA NATOURE”, aceptas que te sometes a la jurisdicción de
                las leyes y tribunales del fuero común competentes en la Ciudad de México, México;
                por lo que renuncias expresamente a cualquier otra jurisdicción que pudiere
                corresponderte en razón de tu domicilio presente o futuro, o por cuestiones de
                nacionalidad
              </p>
            </li>
            <li>
              CONTACTO CON NATOURE
              <p>
                Si tiene alguna pregunta sobre estos Términos, puede ponerse en contacto con
                “NATOURE” a través del correo conectate@natoure.org
              </p>
            </li>
          </ol>
        </Advices>
      </Container>
    </MarginY>
  )
}

tyc.Layout = TravelerLayout

export default tyc
