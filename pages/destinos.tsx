import React, { useEffect, useState } from 'react'
import { AiOutlineEnvironment, AiFillAppstore } from 'react-icons/ai'
import Link from 'next/link'
import { useRouter } from 'next/router'

import Jumbo from '../styles/explore/Jumbo'
import HorizontalS from '../styles/general/HorizontalSlide'
import { MarginY } from '../styles/general'
import FilterBar from '../styles/components/FilterBar'
import Container from '../styles/components/Container'
import MapButton from '../styles/components/MapButton'
import { Map } from '../components/MapPage'
import TravelerLayout from '../Layouts/Traveler'
import { getAllDestinations } from './api/destinations'
import AreasNaturales from '../components/Destinos/AreasNaturales'
import TagCard from '../styles/experience/tags'
import DestinosSostenibles from '../components/Destinos/DestinosSostenibles'
import { macros } from '../steppers/data'

const categories = ['Áreas Naturales', 'Destinos Sostenibles', 'Rutas Turísticas']

const destinos = (): JSX.Element => {
  const router = useRouter()
  const currentRoute = router.pathname
  const [destinos, setDestinos] = useState([])
  const [search, setSearch] = useState('')
  const [macro, setMacro] = useState('')
  const [toggleMap, setToggleMap] = useState(false)

  useEffect(() => {
    getAllDestinations({ search, macro })
      .then(({ destinations }) => {
        setDestinos(destinations)
      })
      .catch((err) => console.error(err))
  }, [search, macro])

  const renderCategory = () => {
    return <AreasNaturales data={destinos} />
    // if (currentRoute === 'destination') return <DestinosSostenibles data={destinos} />
  }

  return (
    <MarginY>
      <Container>
        <Jumbo>
          <FilterBar placeholder="Busca por ubicación, nombre…" />
          <MapButton toggleMap={toggleMap} onClick={() => setToggleMap(!toggleMap)}>
            {toggleMap ? <AiFillAppstore /> : <AiOutlineEnvironment />}
          </MapButton>
          <HorizontalS>
            {categories.map((item, index) => (
              <button
                style={{ cursor: 'pointer' }}
                key={index}
                // className={category === item ? 'here' : ''}
                // onClick={() => handleCategory(item)}
              >
                {item}
              </button>
            ))}
          </HorizontalS>
        </Jumbo>
        {!toggleMap && (
          <React.Fragment>
            <TagCard
              margin="24px 0 64px 0"
              width="100%"
              tag={Object.keys(macros)}
              activeTag={macro}
              onClick={(item) => setMacro(item)}
            />
            {renderCategory()}
          </React.Fragment>
        )}
      </Container>
      {toggleMap && <Map isToggled={true} />}
    </MarginY>
  )
}

destinos.Layout = TravelerLayout

export default destinos
