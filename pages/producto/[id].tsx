/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable jsx-a11y/media-has-caption */
import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import { RiLeafFill, RiMapPinFill } from 'react-icons/ri'

import { MarginY, OnlyWeb, OnlyMobile } from '../../styles/general'
import { MegaLabel } from '../../styles/landing'
import NavBar from '../../components/layout/NavBar'
import Heading from '../../styles/experience/headingcard'
import TagCard from '../../styles/experience/tags'
import Gallery from '../../styles/experience/Gallery'
import Container from '../../styles/components/Container'
import { getProduct } from '../api/api'
import { FlexWrapper } from '../../styles/checkout'

const producto: React.FC = () => {
  const { query } = useRouter()
  const [data, setData] = useState<any>({})

  useEffect(() => {
    const { id } = query
    if (id) {
      getProduct(id)
        .then(({ article }) => {
          setData(article)
        })
        .catch((err) => console.error(err))
    }
  }, [query])

  const Info = () => (
    <React.Fragment>
      <FlexWrapper justify="flex-start" style={{ marginBottom: 16 }}>
        <RiMapPinFill style={{ marginRight: 4 }} />
        {data?.orgId && data?.orgId.orgState}
      </FlexWrapper>
      <FlexWrapper justify="flex-start" style={{ marginBottom: 24 }}>
        <RiLeafFill style={{ marginRight: 4 }} />
        Sostenibilidad {data?.orgId && data?.orgId.sustentabilityPromedy}
      </FlexWrapper>
      {data?.articuloTags && data?.articuloTags.length > 0 && (
        <div style={{ width: '100%', borderTop: '1px solid var(--light-grey)' }}>
          <p>Características del producto</p>
          <TagCard width="100%" margin="24px 0" tag={data?.articuloTags} />
        </div>
      )}
    </React.Fragment>
  )

  return (
    <MarginY style={{ fontWeight: 'bold', color: '#2c375a' }}>
      <NavBar />
      <OnlyMobile>
        <Heading
          back="/explora"
          title={data?.articuloName}
          image={data?.articuloCoverPhoto && data?.articuloCoverPhoto[0]}
        />
        <Container style={{ marginTop: 16 }}>
          <Info />
        </Container>
      </OnlyMobile>

      <OnlyWeb>
        <Container>
          <FlexWrapper align="flex-start">
            <div style={{ width: '55%' }}>
              <Gallery photos={data?.articuloCoverPhoto && data?.articuloCoverPhoto} />
              <Gallery photos={data?.articuloCoverPhoto} isSecondary />
            </div>
            <div style={{ margin: '48px 0', width: '40%' }}>
              <MegaLabel>{data?.articuloName}</MegaLabel>
              <Info />
            </div>
          </FlexWrapper>
        </Container>
      </OnlyWeb>

      <OnlyMobile>
        <Gallery photos={data?.articuloCoverPhoto} />
      </OnlyMobile>
    </MarginY>
  )
}

export default producto
