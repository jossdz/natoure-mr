import React, { useEffect, useState } from 'react'
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles'
import Button from '../styles/general/Button'
import TextField from '@material-ui/core/TextField'
import Modal from '@material-ui/core/Modal'
import Jumbo from '../styles/onboarding/Jumbo'
import Global from '../styles/onboarding/Global'
import Radio from '@material-ui/core/Radio'
import PhoneInput from 'react-phone-number-input'
import { postSignup, postLogin } from './api/api'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormControl from '@material-ui/core/FormControl'
import FormLabel from '@material-ui/core/FormLabel'
import { Container } from '../styles/components'
import { useRouter } from 'next/router'

/** Reset password flow inports*/
import { requestResetPassword } from './api/resetpassword'
import { SuccessAlert, ErrorAlert, PhoneWrapper } from '../styles/general'
/** END Reset password flow inports*/

function getModalStyle() {
  const top = 50
  const left = 50
  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
    maxWidth: '400px',
  }
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
    },
    button: {
      marginRight: theme.spacing(1),
    },
    instructions: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
    h2: {
      textAlign: 'center',
    },
    buttonmodal: {
      width: '100%',
      marginBottom: '20px',
      border: '1px solid',
      fontWeight: 'normal',
      fontSize: '12px',
    },
    flexModal: {
      display: 'flex',
      justifyContent: 'space-between',
      fontSize: '13px',
      color: 'gray',
      marginTop: '10px',
      marginBottom: '10px',
    },
    buttonmodal2: {
      width: '100%',
      marginBottom: '20px',
      border: '1px solid',
      background: 'white',
      fontWeight: 'normal',
      fontSize: '12px',
      color: 'black',
    },
    buttonLink: {
      textDecoration: 'underline gray',
      border: 'none',
      backgroundColor: 'white',
      marginTop: '10px',
    },
    marginauto: {
      marginRight: '10px',
    },
    bold: {
      fontWeight: 'bold',
    },
    paper: {
      position: 'absolute',
      marginLeft: 'auto',
      marginRight: 'auto',
      marginBottom: 'auto',
      marginTop: 'auto',
      width: '70%',
      height: 'auto',
      borderRadius: 16,
      backgroundColor: theme.palette.background.paper,
      border: '1px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
  })
)

function getSteps() {
  return ['', '', '']
}

export default function CustomizedSteppers() {
  const { push } = useRouter()
  const classes = useStyles()
  const [activeStep, setActiveStep] = React.useState(0)
  const [open, setOpen] = React.useState(false)
  const [modalStyle] = React.useState(getModalStyle)
  const [renderRegister, setRegister] = React.useState(false)
  const [renderForgot, setForgot] = React.useState(false)
  const [renderValidate, setValidate] = React.useState(false)
  const steps = getSteps()
  const [selectedValue, setSelectedValue] = React.useState('')
  const [form, setForm] = React.useState({})
  const [emailError, setEmailError] = React.useState(false)
  const [emailRes, setEmailRes] = React.useState(false)
  const [passError, setPassError] = React.useState(false)
  const [passRes, setPassRes] = React.useState(false)
  const [phoneNumber, setPhoneNumber] = React.useState(false)

  // eslint-disable-next-line no-useless-escape
  const emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  // eslint-disable-next-line no-useless-escape
  const passPattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/
  const [disable, setDisable] = React.useState(false)

  useEffect(() => {
    const session = localStorage.getItem('ID')
    if (session) push('/inicio')
  }, [])

  const handleInputs = (e) => {
    e.persist()
    if (e.target.name == 'telefoneNumber') {
      setForm((prevState) => ({
        ...prevState,
        [e.target.name]: phoneNumber,
      }))
    } else {
      setForm((prevState) => ({
        ...prevState,
        [e.target.name]: e.target.value,
      }))
    }
    if (e.target.name === 'email') setEmailError(!e.target.value.match(emailPattern))
    if (e.target.name === 'password') setPassError(!e.target.value.match(passPattern))
  }

  const handleSignUp = async () => {
    const response = await postSignup(form)
    handleReset()
    setDisable(false)
  }

  const disableAndSignup = () => {
    setDisable(true)
    handleSignUp()
  }

  const handleLogin = async () => {
    try {
      const response = await postLogin(form)
      localStorage.setItem('ID', response.user._id)
      localStorage.setItem('NatoureUser', JSON.stringify(response.user))
      if (response.user.anfitrion) {
        if (response.user && !response.user.myOrganization) {
          push('/form-organizacion')
        } else {
          localStorage.setItem('organizationid', response.user.myOrganization)
          push('/misproductos')
        }
      } else {
        push('/inicio')
      }
    } catch (err) {
      setEmailRes(true)
      setPassRes(true)
      setPassError(false)
    }
  }

  const handleChange = (event) => {
    setSelectedValue(event.target.value)
  }

  const handleReset = async () => {
    await setForgot(false)
    await setRegister(false)
    await handleClose()
    setForm({})
  }

  const handleOpen = () => {
    setForgot(false)
    setRegister(false)
    setOpen(true)
  }

  const handleNext = () => {
    activeStep === 2 ? handleOpen() : setActiveStep((prevActiveStep) => prevActiveStep + 1)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const handleRegister = () => {
    setRegister(true)
  }

  const handleForgot = () => {
    setForgot(true)
  }

  /** RESET PASSWORD LOGIC */

  const [emailInfo, setEmailInfo] = useState()
  const [requestStatus, setRequestStatus] = useState<string>('')
  const [success, setSuccess] = useState('')
  const [error, setError] = useState('')

  const handleForgotEmail = (e) => {
    const { value } = e.target
    setEmailInfo(value)
  }

  const makeResetRequest = () => {
    setRequestStatus('loading')
    setSuccess('')
    setError('')
    requestResetPassword(emailInfo)
      .then(({ msg, email }) => {
        setRequestStatus('finished')
        setSuccess(`${msg} a ${email}`)
        setTimeout(() => {
          handleOpen()
        }, 1500)
      })
      .catch(({ data }) => {
        setRequestStatus('finished')
        setError(data.msg)
      })
  }

  /** END RESET PASSWORD LOGIC */

  const stepsData = [
    {
      image: '/assets/one.jpg',
      title: 'Explora',
      description: (
        <p className="description">
          Los lugares más espectaculares de Latinoamérica, las especies que existen, así como{' '}
          <span>actividades únicas</span> que cambiarán tu forma de viajar.
        </p>
      ),
      buttonText: 'Siguiente',
      dots: 'https://i.postimg.cc/rsmjn6vz/Stepper1.png',
    },
    {
      image: '/assets/two.JPG',
      title: 'Conectate',
      description: (
        <p className="description">
          con comunidades y personas que te guiarán a <span>aventuras inolvidables</span>
        </p>
      ),
      buttonText: 'Siguiente',
      dots: 'https://i.postimg.cc/QN1yH53y/Stepper2.png',
    },
    {
      image: '/assets/three.jpeg',
      title: 'Conserva',
      description: (
        <p className="description">
          la biodiversidad en <span>Áreas protegidas</span> donde cada uno de tus viajes apoyará
          este fin
        </p>
      ),
      buttonText: 'Comienza tu experiencia',
      dots: 'https://i.postimg.cc/W3R7vWhb/Stepper-3.png',
    },
  ]

  const body = (
    <div style={modalStyle} className={classes.paper}>
      <h2 id="simple-modal-title" className={classes.h2}>
        Inicia Sesión
      </h2>
      <div id="simple-modal-description" className={classes.h2}>
        <TextField
          id="standard-full-width"
          style={{ margin: 8 }}
          placeholder="Correo Electronico"
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
          onChange={handleInputs}
          name="email"
          error={emailError}
          helperText={emailError ? 'Esto no parece un correo' : false}
        />
        <TextField
          id="standard-full-width"
          style={{ margin: 8 }}
          placeholder="Password"
          type="password"
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
          onChange={handleInputs}
          name="password"
          error={passError ? passError : passRes ? passRes : null}
          helperText={
            passError
              ? 'Recuerda que contraseña debe llevar un mayuscula y un numero'
              : passRes
              ? 'El correo o la contraseña no son correctos'
              : false
          }
        />
        <div className={classes.flexModal}>
          <button className={classes.buttonLink} onClick={handleRegister}>
            Registrate
          </button>
          <button className={classes.buttonLink} onClick={handleForgot}>
            Olvidaste tu contraseña?
          </button>
        </div>
        <Button btnType="primary" onClick={handleLogin}>
          Ingresar
        </Button>
      </div>
    </div>
  )

  const register = (
    <div style={modalStyle} className={classes.paper}>
      <h2 id="simple-modal-title" className={classes.h2}>
        Regístrate
      </h2>
      <div id="simple-modal-description" className={classes.h2}>
        <TextField
          id="standard-full-width"
          style={{ margin: 8 }}
          placeholder="Nombre"
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
          onChange={handleInputs}
          name="name"
        />
        <TextField
          id="standard-full-width"
          style={{ margin: 8 }}
          placeholder="Apellido"
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
          onChange={handleInputs}
          name="last_name"
        />
        <PhoneWrapper>
          <PhoneInput
            defaultCountry="MX"
            placeholder="Teléfono"
            value={phoneNumber}
            name="telefoneNumber"
            onChange={setPhoneNumber}
            onBlur={handleInputs}
          />
        </PhoneWrapper>
        <TextField
          id="standard-full-width"
          style={{ margin: 8 }}
          placeholder="E-mail"
          type="email"
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
          onChange={handleInputs}
          name="email"
          error={emailError}
          helperText={emailError ? 'Esto no parece un correo' : false}
        />
        <TextField
          id="standard-full-width"
          style={{ margin: 8 }}
          placeholder="Contraseña"
          type="password"
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
          onChange={handleInputs}
          name="password"
          error={passError}
          helperText={
            passError ? 'Recuerda que contraseña debe llevar un mayuscula y un numero' : false
          }
        />
        <div className={classes.flexModal}>
          <FormControl component="fieldset">
            <FormLabel component="legend">Género</FormLabel>
            <RadioGroup
              aria-label="gender"
              name="gender"
              value={selectedValue}
              onChange={(e) => {
                handleInputs(e)
                handleChange(e)
              }}
            >
              <FormControlLabel value="Female" control={<Radio color="primary" />} label="Mujer" />
              <FormControlLabel value="Male" control={<Radio color="primary" />} label="Hombre" />
              <FormControlLabel value="Other" control={<Radio color="primary" />} label="Otro" />
            </RadioGroup>
          </FormControl>
        </div>
        <Button btnType="primary" onClick={disableAndSignup} disabled={disable}>
          Registrarse
        </Button>
        <div>
          <button className={classes.buttonLink} onClick={handleReset}>
            Ya tienes cuenta? Inicia Sesión
          </button>
        </div>
      </div>
    </div>
  )

  const forgot = (
    <div style={modalStyle} className={classes.paper}>
      <h2 id="simple-modal-title" className={classes.h2}>
        Recuperar Contraseña
      </h2>
      <div id="simple-modal-description" className={classes.h2}>
        Ingresa el correo electrónico con el que te registraste y te enviaremos instrucciones para
        recuperar tu contraseña
        <TextField
          id="standard-full-width"
          style={{ margin: 8 }}
          placeholder="E-mail"
          type="email"
          fullWidth
          margin="normal"
          required
          value={emailInfo}
          autoComplete="off"
          onChange={handleForgotEmail}
          helperText="Por favor vuelve a escribir tu correo antes de enviar la solicitud"
          InputLabelProps={{
            shrink: true,
          }}
        />
        <Button
          style={{ marginBottom: '16px' }}
          disabled={requestStatus === 'loading'}
          btnType="primary"
          onClick={makeResetRequest}
        >
          {requestStatus === 'loading' ? 'Enviando...' : 'Enviar'}
        </Button>
      </div>
      {success && <SuccessAlert>{success}</SuccessAlert>}
      {error && <ErrorAlert>{error}</ErrorAlert>}
    </div>
  )

  const validate = (
    <div style={modalStyle} className={classes.paper}>
      <h2 id="simple-modal-title" className={classes.h2}>
        Validación
      </h2>
      <p id="simple-modal-description" className={classes.h2}>
        Hemos enviado un código al correo: <br />
        <br />
        <span className="valemail">ejemplo@gmail.com</span>
        <br />
        <br />
        Por favor ingresalo para verificar tu cuenta.
        <TextField
          id="standard-full-width"
          style={{ margin: 8 }}
          placeholder="0-0-0-0"
          type="number"
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
        />
        <Button btnType="primary">Validar</Button>
      </p>
    </div>
  )

  return (
    <>
      <div>
        <div>
          <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
          >
            {renderRegister ? register : renderForgot ? forgot : renderValidate ? validate : body}
          </Modal>
          {activeStep !== steps.length && (
            <Global>
              <Jumbo bgImg={stepsData[activeStep].image}>
                <Container>
                  <img width="140px" height="34px" src="/assets/logo-white.png" alt="Natoure" />
                  <div>
                    <p className="title">{stepsData[activeStep].title}</p>
                    {stepsData[activeStep].description}
                    <Button btnType="primary" color="primary" onClick={handleNext}>
                      {stepsData[activeStep].buttonText}
                    </Button>
                    <img className="dots" src={stepsData[activeStep].dots} alt="Stepper DOTS" />
                  </div>
                </Container>
              </Jumbo>
            </Global>
          )}
        </div>
      </div>
    </>
  )
}
