import React, { useEffect, useState } from 'react'
import { AiOutlineEnvironment, AiFillAppstore } from 'react-icons/ai'

import HorizontalS from '../styles/general/HorizontalSlide'
import { MarginY } from '../styles/general'
import FilterBar from '../styles/components/FilterBar'
import Jumbo from '../styles/explore/Jumbo'
import { Map } from '../components/MapPage'
import MapButton from '../styles/components/MapButton'
import Container from '../styles/components/Container'
import Loading from '../styles/components/Loading'
import TravelerLayout from '../Layouts/Traveler'
import { Reservation } from '../components/travels'
import { getReservations } from './api/reservations'

const tabs = ['Próximos viajes', 'Historial']

const travels = (): JSX.Element => {
  const [activeTab, setActiveTab] = useState('Próximos viajes')
  const [reservations, setReservations] = useState([])
  const [status, setStatus] = useState<'' | 'fetching' | 'finished'>('')
  const [data, setData] = useState([])
  const [toggleMap, setToggleMap] = useState(false)

  const dateInPast = (pastDate) => (pastDate < new Date() ? true : false)

  useEffect(() => {
    setStatus('fetching')
    getReservations()
      .then(({ reservations }) => {
        setStatus('finished')
        setReservations(reservations)
      })
      .catch((err) => {
        setStatus('finished')
        console.log(err)
      })
  }, [])

  useEffect(() => {
    const filtered = reservations.filter((item) => {
      if (activeTab === 'Historial') {
        return dateInPast(new Date(item.checkin)) || item.status === 'Cancelada'
      } else {
        return !dateInPast(new Date(item.checkin)) && item.status !== 'Cancelada'
      }
    })

    setData(
      filtered
        .sort((a, b) => new Date(a.checkin).getDate() - new Date(b.checkin).getDate())
        .sort((a, b) => new Date(a.checkin).getMonth() - new Date(b.checkin).getMonth())
    )
  }, [activeTab, reservations])

  return (
    <MarginY>
      <Container>
        <Jumbo>
          <FilterBar placeholder="Busca por ubicación, nombre…" />
          <MapButton toggleMap={toggleMap} onClick={() => setToggleMap(!toggleMap)}>
            {toggleMap ? <AiFillAppstore /> : <AiOutlineEnvironment />}
          </MapButton>
          <HorizontalS>
            {tabs.map((item, index) => (
              <button key={index} onClick={() => setActiveTab(item)}>
                <span className={activeTab === item ? 'here' : ''}>{item}</span>
              </button>
            ))}
          </HorizontalS>
        </Jumbo>
        {!toggleMap && (
          <>
            {data.length > 0 &&
              status === 'finished' &&
              data.map((item, index) => <Reservation key={index} {...item} />)}
            {status === 'finished' && data.length === 0 && (
              <h2>Aún no tienes reservaciones para mostrar</h2>
            )}
            {status === 'fetching' && <Loading />}
          </>
        )}
      </Container>
      {toggleMap && <Map isToggled={true} />}
    </MarginY>
  )
}

travels.Layout = TravelerLayout

export default travels
