import React, { useEffect } from 'react'
import { useRouter } from 'next/router'

import { Step1, Step2, Step3, Step4, Step5, Step6, Step7 } from '../steppers/experiencia'

import { ButtonText, Heading, Modal, SectionHeading } from '../styles/forms'

import { Container, Loading } from '../styles/components'

import { editExperience, getExperience } from '../pages/api/api'

import useStepper from '../hooks/useStepper'

const EditExperiencia = (): JSX.Element => {
  const { push } = useRouter()
  const {
    data,
    setData,
    status,
    setStatus,
    currentStep,
    onStepChange,
    onSubmit,
    onBackStep,
  } = useStepper(editExperience, true)

  useEffect(() => {
    const id = localStorage.getItem('experienceID')

    getExperience(id)
      .then(({ experience }) => {
        setData(experience)
      })
      .catch(() => {
        setStatus('error')
      })
  }, [])

  const steps = {
    1: {
      render: Step1,
      name: 'Tu experiencia',
      heading: 'Edita tu experiencia',
    },
    2: {
      render: Step2,
      name: 'Detalles de la experiencia',
      heading: 'Edita los detalles de tu experiencia',
    },
    3: {
      render: Step3,
      name: 'Detalles complementarios',
      heading: 'Edita los detalles de tu experiencia',
    },
    4: {
      render: Step4,
      name: 'Ubicación',
      heading: 'Edita dónde se ubica la experiencia',
    },
    5: {
      render: Step5,
      name: 'Calendario',
      heading: 'Edita las fechas para tu experiencia',
    },
    6: {
      render: Step6,
      name: 'Fotos y videos',
      heading: 'Edita las fotos y videos de la experiencia',
    },
    7: {
      render: Step7,
      name: 'Confirmación',
      heading: 'Confirma tus datos',
    },
  }

  const Component = steps[currentStep].render

  return (
    <React.Fragment>
      <Heading
        title="Edita tu experiencia"
        stepName={steps[currentStep].name}
        step={currentStep}
        total={7}
        onBackStep={onBackStep}
      />
      <Container>
        <SectionHeading title={steps[currentStep].heading} />
        {currentStep === 7 ? (
          <Component onStepChange={onStepChange} onSubmit={onSubmit} data={data} />
        ) : (
          <Component onStepChange={onStepChange} data={data} />
        )}
        <ButtonText
          style={{ display: currentStep === 1 && 'none', marginBottom: 96 }}
          onClick={onBackStep}
        />
        {status === 'fetching' && <Loading />}
        {status === 'finished' && (
          <Modal
            name="¡Listo! Tu experiencia ha sido actualizada"
            description="La información de tu experiencia será publicada."
            onClick={() => push('/misproductos')}
          />
        )}
        {status === 'error' && (
          <Modal
            name="¡Lo sentimos! Ha ocurrido un error"
            description="No fue posible editar tu experiencia."
            onClick={() => push('/misproductos')}
            error
          />
        )}
      </Container>
    </React.Fragment>
  )
}

export default EditExperiencia
