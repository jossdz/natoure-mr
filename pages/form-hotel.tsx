import React from 'react'
import { useRouter } from 'next/router'

import {
  Button,
  Checkbox,
  Chips,
  Heading,
  Input,
  Label,
  Modal,
  SectionHeading,
  SectionText,
  Select,
  Upload,
  SmallText,
} from '../styles/forms'

import { hotelExtraAttr, hotelTags, hotelType, hotelAmenities, currency } from '../steppers/data'

import { Container, Loading } from '../styles/components'

import { postHotel } from './api/api'

import useForm from '../hooks/useForm'

const FormHotel = (): JSX.Element => {
  const { push } = useRouter()
  const { validForm, status, type, handleChange, handleClick } = useForm(postHotel, false)

  return (
    <React.Fragment>
      <Heading title="Registra tu hospedaje" step={1} simple />
      <Container>
        <SectionHeading title="Comparte algunos datos sobre el hospedaje que ofreces" />
        <SectionText
          style={{ marginBottom: 64 }}
          title="Rellena los siguientes campos con información acerca del hospedaje, así los viajeros podrán conocerlo mejor."
        />
        <Label title="Nombre del hospedaje" />
        <Input name="hotelName" placeholder="Ingresa el nombre" onChange={handleChange} />
        <Label title="Elige el tipo de hospedaje que ofrecen" />
        <Select name="hotelType" options={hotelType} onChange={handleChange} />
        {type !== 'default' && (
          <React.Fragment>
            <Label title={`¿Qué capacidad tiene este tipo de ${type.toLowerCase()}?`} />
            <div
              style={{
                display: 'flex',
                width: '100%',
                maxWidth: '85vw',
                margin: '0 auto 64px auto',
                alignItems: 'center',
              }}
            >
              <Input
                style={{ width: 40 }}
                adjust
                name="capacity"
                placeholder="2"
                onChange={handleChange}
              />
              <SmallText black title="Personas" style={{ margin: '0 12px' }} />
            </div>
            <Label title={`¿Cuantas ${type.toLowerCase()} de esta capacidad tienes?`} />
            <div
              style={{
                display: 'flex',
                width: '100%',
                maxWidth: '85vw',
                margin: '0 auto 64px auto',
                alignItems: 'center',
              }}
            >
              <Input
                style={{ width: 40 }}
                adjust
                name="quantity"
                placeholder="2"
                onChange={handleChange}
              />
              <SmallText black title={type} style={{ margin: '0 12px' }} />
            </div>
            <Label title={`Ingresa el precio por ${type.toLowerCase()} por noche`} />
            <div style={{ display: 'flex', width: '100%', maxWidth: '85vw', margin: '0 auto' }}>
              <Input
                style={{ width: 48, margin: '0 18px 64px 0' }}
                adjust
                name="price"
                placeholder="$ 1,590"
                onChange={handleChange}
              />
              <Select
                style={{ width: '100%' }}
                name="currency"
                options={currency}
                onChange={handleChange}
              />
            </div>
          </React.Fragment>
        )}
        <Label title="Ingresa las amenidades que incluye tu hospedaje" />
        <Chips name="hotelAmenities" options={hotelAmenities} onChange={handleChange} />
        <Label title="Atributos extra de tu hospedaje" />
        <Checkbox name="hotelExtraAttr" options={hotelExtraAttr} onChange={handleChange} />
        <Label title="Elige etiquetas que mejor se relacionen con tu hospedaje" optional />
        <Chips name="hotelTags" options={hotelTags} onChange={handleChange} optional />
        <Label title="Sube tu imagen de portada" />
        <Upload name="hotelPhotosCover" onChange={handleChange} onlyPhoto />
        <Label title="Sube más fotos para mostrar en tu galería" />
        <Upload name="hotelPhotosGallery" onChange={handleChange} />
        <Button unable={!validForm} onClick={handleClick} text="Guardar y continuar" />
      </Container>
      {status === 'fetching' && <Loading />}
      {status === 'finished' && (
        <Modal
          name="¡Listo! Tu hospedaje ha sido registrado"
          description="La información de tu hospedaje será publicada."
          onClick={() => push('/misproductos')}
        />
      )}
      {status === 'error' && (
        <Modal
          name="¡Lo sentimos! Ha ocurrido un error"
          description="No fue posible publicar tu hospedaje."
          onClick={() => push('/misproductos')}
          error
        />
      )}
    </React.Fragment>
  )
}

export default FormHotel
