/* eslint-disable prefer-const */
import React, { useEffect } from 'react'
import { useRouter } from 'next/router'

import { Step1, Step2, Step3, Step4, Step5, Step6 } from '../steppers/organizacion'

import { ButtonText, Heading, Modal, SectionHeading } from '../styles/forms'

import { Container, Loading } from '../styles/components'

import { editOrganization, getOrganization } from './api/api'

import useStepper from '../hooks/useStepper'

const EditOrganizacion = (): JSX.Element => {
  const { push } = useRouter()
  const {
    data,
    setData,
    status,
    setStatus,
    currentStep,
    onStepChange,
    onSubmit,
    onBackStep,
  } = useStepper(editOrganization, true)

  useEffect(() => {
    const userId = localStorage.getItem('ID')
    getOrganization(userId)
      .then((organization) => {
        setData(organization)
      })
      .catch(() => {
        setStatus('error')
      })
  }, [])

  const steps = {
    1: {
      render: Step1,
      name: 'Datos principales',
      heading: 'Edita tus datos',
    },
    2: {
      render: Step2,
      name: 'Ubicación',
      heading: 'Edita dónde están ubicados',
    },
    3: {
      render: Step3,
      name: 'Datos de contacto',
      heading: 'Edita tus datos de contacto',
    },
    4: {
      render: Step4,
      name: 'Sostenibilidad',
      heading: 'Edita tu nivel de compromiso socioambiental',
    },
    5: {
      render: Step5,
      name: 'Fotos y videos',
      heading: 'Edita tus fotos y videos',
    },
    6: {
      render: Step6,
      name: 'Confirmación',
      heading: 'Confirma tus cambios',
    },
  }

  const Component = steps[currentStep].render

  return (
    <React.Fragment>
      <Heading
        title="Edita tu organización"
        stepName={steps[currentStep].name}
        step={currentStep}
        total={6}
        onBackStep={onBackStep}
      />
      <Container>
        <SectionHeading title={steps[currentStep].heading} />
        {currentStep === 6 ? (
          <Component onStepChange={onStepChange} onSubmit={onSubmit} data={data} />
        ) : (
          <Component onStepChange={onStepChange} data={data} />
        )}
        <ButtonText
          style={{ display: currentStep === 1 && 'none', marginBottom: 96 }}
          onClick={onBackStep}
        />
        {status === 'fetching' && <Loading />}
        {status === 'finished' && (
          <Modal
            name="¡Listo! Tu organización ha sido actualizada"
            description="La información de tu organización será publicada."
            onClick={() => push('/misproductos')}
          />
        )}
        {status === 'error' && (
          <Modal
            name="¡Lo sentimos! Ha ocurrido un error"
            description="No fue posible editar tu organización."
            onClick={() => push('/misproductos')}
            error
          />
        )}
      </Container>
    </React.Fragment>
  )
}

export default EditOrganizacion
