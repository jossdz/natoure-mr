/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable jsx-a11y/media-has-caption */
import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import { RiLeafFill, RiMapPinFill } from 'react-icons/ri'

import HorizontalS from '../../styles/general/HorizontalSlide'
import { MarginY, OnlyWeb, OnlyMobile } from '../../styles/general'
import { MegaLabel } from '../../styles/landing'
import NavBar from '../../components/layout/NavBar'
import Heading from '../../styles/experience/headingcard'
import TagCard from '../../styles/experience/tags'
import Gallery from '../../styles/experience/Gallery'
import PlateCard from '../../components/Explore/components/PlateCard'
import Container from '../../styles/components/Container'
import { getRestaurant } from '../api/api'
import { FlexWrapper } from '../../styles/checkout'

const restaurant: React.FC = () => {
  const { query } = useRouter()
  const [data, setData] = useState<any>({})

  useEffect(() => {
    const { id } = query
    if (id) {
      getRestaurant(id)
        .then(({ restaurant }) => {
          setData(restaurant)
        })
        .catch((err) => console.error(err))
    }
  }, [query])

  const Info = () => (
    <React.Fragment>
      <FlexWrapper justify="flex-start" style={{ marginBottom: 16 }}>
        <RiMapPinFill style={{ marginRight: 4 }} />
        {data?.orgId && data?.orgId.orgState}
      </FlexWrapper>
      <FlexWrapper justify="flex-start" style={{ marginBottom: 24 }}>
        <RiLeafFill style={{ marginRight: 4 }} />
        Sostenibilidad {data?.orgId && data?.orgId.sustentabilityPromedy}
      </FlexWrapper>
      {data?.restaurantTags && data?.restaurantTags.length > 0 && (
        <div style={{ width: '100%', borderTop: '1px solid var(--light-grey)' }}>
          <p>Características del restaurante</p>
          <TagCard width="100%" margin="24px 0" tag={data?.restaurantTags} />
        </div>
      )}
    </React.Fragment>
  )

  return (
    <MarginY style={{ fontWeight: 'bold', color: '#2c375a' }}>
      <NavBar />
      <OnlyMobile>
        <Heading
          back="/explora"
          title={data?.restaurantName}
          image={data?.restaurantCoverPhotos && data?.restaurantCoverPhotos[0]}
        />
        <Container style={{ marginTop: 16 }}>
          <Info />
        </Container>
      </OnlyMobile>

      <OnlyWeb>
        <Container>
          <FlexWrapper align="flex-start">
            <div style={{ width: '55%' }}>
              <Gallery photos={data?.restaurantCoverPhotos && data?.restaurantCoverPhotos} />
              <Gallery photos={data?.restaurantGalleryPhotos} isSecondary />
            </div>
            <div style={{ margin: '48px 0', width: '40%' }}>
              <MegaLabel>{data?.restaurantName}</MegaLabel>
              <Info />
            </div>
          </FlexWrapper>
        </Container>
      </OnlyWeb>

      <OnlyMobile>
        <Gallery photos={data?.restaurantGalleryPhotos} />
      </OnlyMobile>

      {data?.plates && data?.plates.length > 0 && (
        <Container>
          <p>Nuestros platillos</p>
          <HorizontalS>
            {data.plates.map((item, index) => (
              <PlateCard key={index} {...item} />
            ))}
          </HorizontalS>
        </Container>
      )}
    </MarginY>
  )
}

export default restaurant
