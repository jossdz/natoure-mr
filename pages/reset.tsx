import React, { useState, useEffect } from 'react'
import { FlexWrapper } from '../styles/checkout'
import { useRouter } from 'next/router'
import {
  ResetPasswordSection,
  FormWrapper,
  Subtitle,
  HidePasswordBtn,
} from '../styles/resetPassword/resetPassword'
import TextField from '@material-ui/core/TextField'
import Button from '../styles/general/Button'
import { updateCredentials } from './api/resetpassword'
import { SuccessAlert } from '../styles/general'

type ResetPassword = {
  password?: string
  confirmPassword?: string
}

const ResetPassword = (): JSX.Element => {
  const { query, push } = useRouter()
  const [form, setForm] = useState<ResetPassword>()
  const [hasError, setHasError] = useState(false)
  const [showPassword, setShowPassword] = useState(false)
  const [requestStatus, setRequestStatus] = useState<string>()
  const [success, setSuccess] = useState<string>()
  const [canSubmit, setCanSubmit] = useState(false)

  const handleChange = (e) => {
    const { name, value } = e.target
    setForm((prev) => ({ ...prev, [name]: value }))
  }
  const validatePassword = () => {
    const hasError = form.password !== form.confirmPassword
    setHasError(hasError)
  }
  const handleClickShowPassword = () => setShowPassword((prev) => !prev)

  const makeResetRequest = () => {
    setRequestStatus('loading')
    const credentials = { password: form.password, token: query.token }
    updateCredentials(credentials)
      .then(({ msg }) => {
        setSuccess(msg)
        setRequestStatus('finished')
        setTimeout(() => {
          push('/')
        }, 1000)
      })
      .catch((err) => {
        console.log(err)
        setRequestStatus('finished')
      })
  }

  useEffect(() => {
    const isNotSubmitted = requestStatus !== 'loading' && requestStatus !== 'finished'
    const hasValidValues = form?.password && form?.confirmPassword && !hasError
    setCanSubmit(isNotSubmitted && hasValidValues)
  }, [requestStatus, form, hasError])

  return (
    <React.Fragment>
      <ResetPasswordSection>
        <FormWrapper>
          <h2>Recuperar Contraseña</h2>
          <Subtitle>Ingresa tu nueva contraseña</Subtitle>
          <FlexWrapper direction="column">
            <TextField
              id="standard-full-width"
              style={{ margin: 8 }}
              placeholder="Nueva contraseña"
              fullWidth
              margin="normal"
              type={showPassword ? 'text' : 'password'}
              InputLabelProps={{
                shrink: true,
              }}
              onChange={handleChange}
              name="password"
              error={hasError}
              helperText={hasError ? 'Las contraseñas no coinciden' : false}
            />
            <TextField
              id="standard-full-width"
              style={{ margin: 8 }}
              placeholder="Confirma tu contraseña"
              fullWidth
              margin="normal"
              type={showPassword ? 'text' : 'password'}
              InputLabelProps={{
                shrink: true,
              }}
              onChange={handleChange}
              name="confirmPassword"
              error={hasError}
              onBlur={validatePassword}
              helperText={hasError ? 'Las contraseñas no coinciden' : false}
            />
            <HidePasswordBtn onClick={handleClickShowPassword}>
              {showPassword ? 'Ocultar' : 'Mostrar'} contraseña
            </HidePasswordBtn>
            <Button disabled={!canSubmit} btnType="primary" onClick={makeResetRequest}>
              {requestStatus === 'loading'
                ? 'Actualizando...'
                : requestStatus === 'finished'
                ? 'Actualizado'
                : 'Actualizar contraseña'}
            </Button>
          </FlexWrapper>
          {success && <SuccessAlert>{success}</SuccessAlert>}
        </FormWrapper>
      </ResetPasswordSection>
    </React.Fragment>
  )
}
export default ResetPassword
