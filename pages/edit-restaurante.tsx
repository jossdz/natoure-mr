import React from 'react'
import { useRouter } from 'next/router'

import {
  Button,
  Chips,
  Heading,
  Input,
  Label,
  Modal,
  SectionHeading,
  SectionText,
  Upload,
} from '../styles/forms'

import { restaurantTags } from '../steppers/data'

import { Container, Loading } from '../styles/components'

import { editRestaurant, getRestaurant } from './api/api'

import useForm from '../hooks/useForm'

const EditRestaurante = (): JSX.Element => {
  const { push } = useRouter()
  const { validForm, status, item, handleChange, handleClick } = useForm(
    editRestaurant,
    true,
    'restID',
    getRestaurant
  )

  const data = item && item.restaurant

  return (
    <React.Fragment>
      <Heading title="Edita tu restaurante" step={1} simple />
      <Container>
        <SectionHeading title="Comparte algunos datos sobre tu restaurante" />
        <SectionText
          style={{ marginBottom: 64 }}
          title="Rellena los siguientes campos con información acerca de tu restaurante, así los viajeros podrán conocerlo mejor."
        />
        <Label title="Nombre de tu restaurante" />
        <Input
          name="restaurantName"
          placeholder="Ingresa el nombre"
          onChange={handleChange}
          defaultValue={data && data.restaurantName}
        />
        <Label title="Elige etiquetas que mejor se relacionen con tu restaurante" optional />
        <Chips
          name="restaurantTags"
          options={restaurantTags}
          onChange={handleChange}
          optional
          defaultValue={data && data.restaurantTags}
        />
        <Label title="Sube algunas fotos de tu restaurante" />
        <Upload
          name="restaurantCoverPhotos"
          onChange={handleChange}
          defaultValue={data ? data.restaurantCoverPhotos : []}
        />
        <Label title="Sube más fotos para mostrar en tu galería" />
        <Upload
          name="restaurantGalleryPhotos"
          onChange={handleChange}
          defaultValue={data ? data.restaurantGalleryPhotos : []}
        />
        <Button unable={!validForm} onClick={handleClick} text="Guardar y continuar" />
      </Container>
      {status === 'fetching' && <Loading />}
      {status === 'finished' && (
        <Modal
          name="¡Listo! Tu restaurante ha sido actualizado"
          description="La información de tu restaurante será publicada."
          onClick={() => push('/misproductos')}
        />
      )}
      {status === 'error' && (
        <Modal
          name="¡Lo sentimos! Ha ocurrido un error"
          description="No fue posible editar tu restaurante."
          onClick={() => push('/misproductos')}
          error
        />
      )}
    </React.Fragment>
  )
}

export default EditRestaurante
