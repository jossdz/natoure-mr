import axios from 'axios'

const isProduction = process.env.NODE_ENV === 'production'
const baseURL = isProduction ? 'https://natoure-api.herokuapp.com' : 'http://localhost:3000'

export const getAllNaturalAreas = () => {
  return axios
    .get(`${baseURL}/api/naturalAreas`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const createNaturalArea = (data) => {
  return axios
    .post(`${baseURL}/api/naturalAreas`, data, {
      withCredentials: true,
    })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
