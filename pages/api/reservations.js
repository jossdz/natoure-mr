import axios from 'axios'
axios.defaults.withCredentials = true

const isProduction = process.env.NODE_ENV === 'production'
const baseURL = isProduction ? 'https://natoure-api.herokuapp.com' : 'http://localhost:3000'

export const getReservations = (experienceId) => {
  return axios
    .get(`${baseURL}/api/reservation${experienceId ? `?by=experience&exp=${experienceId}` : ''}`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const createReservation = (data) => {
  return axios
    .post(`${baseURL}/api/reservation`, data)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const getReservationById = (experienceId) => {
  return axios
    .get(`${baseURL}/api/reservation/${experienceId}`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const updateReservation = (experienceId, data) => {
  return axios
    .patch(`${baseURL}/api/reservation/${experienceId}`, data)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const deleteReservation = (experienceId) => {
  return axios
    .delete(`${baseURL}/api/reservation/${experienceId}`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
