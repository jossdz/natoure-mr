import axios from 'axios'
axios.defaults.withCredentials = true

const isProduction = process.env.NODE_ENV === 'production'
const baseURL = isProduction ? 'https://natoure-api.herokuapp.com' : 'http://localhost:3000'

export const verifyAdmin = (token) => {
  return axios
    .get(`${baseURL}/api/admin/verify`, {
      headers: {
        Authorization: token,
      },
    })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const getAdmins = () => {
  return axios
    .get(`${baseURL}/api/admin`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
