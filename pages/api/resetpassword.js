import axios from 'axios'
axios.defaults.withCredentials = true

const isProduction = process.env.NODE_ENV === 'production'
const baseURL = isProduction ? 'https://natoure-api.herokuapp.com' : 'http://localhost:3000'

export const requestResetPassword = (email) => {
  return axios
    .post(`${baseURL}/auth/forgot`, { email })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const updateCredentials = (credentials) => {
  return axios
    .post(`${baseURL}/auth/updateCredentials`, credentials)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
