/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import axios from 'axios'

const isProduction = process.env.NODE_ENV === 'production'
const baseURL = isProduction ? 'https://natoure-api.herokuapp.com' : 'http://localhost:3000'

export const uploadMedia = (media) => {
  return axios
    .post(`${baseURL}/api/upload`, media)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const postSignup = (form) => {
  return axios
    .post(`${baseURL}/auth/api/signup`, form, { withCredentials: true })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const postLogin = (form) => {
  return axios
    .post(`${baseURL}/auth/api/login`, form, { withCredentials: true })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const postOrganizacion = (form) => {
  const userID = localStorage.getItem('ID')
  return axios
    .post(`${baseURL}/organization/api/create-organization`, form, {
      headers: { id: userID, withCredentials: true },
    })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const getOrganization = (user_id) => {
  return axios
    .post(`${baseURL}/organization/api/my-organization`, { user_id, withCredentials: true })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const getOrganizationDetail = (id) => {
  return axios
    .get(`${baseURL}/host/api/detail-organization/${id}`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const getAllOrganizations = () => {
  return axios
    .get(`${baseURL}/host/api/all-organizations`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const editOrganization = (data, id, organizationID) => {
  return axios
    .post(`${baseURL}/organization/api/edit-organization/${id}`, data, {
      headers: { organizationid: organizationID, withCredentials: true },
    })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const postBankAccount = (form, orgId) => {
  return axios
    .post(`${baseURL}/account/api/create-account`, form, {
      headers: { organizationid: orgId, withCredentials: true },
    })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const getBankAccount = (orgId) => {
  return axios
    .get(`${baseURL}/account/api/org-accounts`, {
      headers: { organizationid: orgId, withCredentials: true },
    })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const editBankAccount = (accId) => {
  return axios
    .patch(`${baseURL}/account/api/update`, {
      headers: { accountid: accId, withCredentials: true },
    })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const postFiscaInfo = (form, orgId) => {
  return axios
    .post(`${baseURL}/fiscal/api/create-fiscal`, form, {
      headers: { organizationid: orgId, withCredentials: true },
    })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const getFiscalInfo = (orgId) => {
  return axios
    .get(`${baseURL}/fiscal/api/org-fiscal`, {
      headers: { organizationid: orgId, withCredentials: true },
    })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const editFiscalInfo = (fisId) => {
  return axios
    .patch(`${baseURL}/fiscal/api/update`, {
      headers: { accountid: fisId, withCredentials: true },
    })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

//NEW ROUTES - EXPERIENCE
export const postExperience = (form) => {
  return axios
    .post(`${baseURL}/api/experience`, form, { withCredentials: true })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
export const getAllExperiences = (query) => {
  return axios
    .get(`${baseURL}/api/experience${query}`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
export const getExperience = (id) => {
  return axios
    .get(`${baseURL}/api/experience/${id}`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
export const editExperience = (data, id, organizationID) => {
  return axios
    .patch(`${baseURL}/api/experience/${id}?org=${organizationID}`, data, { withCredentials: true })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

//NEW ROUTES - PLATE
export const postPlate = (form) => {
  return axios
    .post(`${baseURL}/api/plate`, form, { withCredentials: true })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
export const getAllPlates = (query = '') => {
  return axios
    .get(`${baseURL}/api/plate${query}`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
export const getPlate = (id) => {
  return axios
    .get(`${baseURL}/api/plate/${id}`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
export const editPlate = (data, id, organizationID) => {
  return axios
    .patch(`${baseURL}/api/plate/${id}?org=${organizationID}`, data, { withCredentials: true })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

//NEW ROUTES - RESTAURANT
export const postRestaurant = (form) => {
  return axios
    .post(`${baseURL}/api/restaurant`, form, { withCredentials: true })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
export const getAllRestaurants = (query = '') => {
  return axios
    .get(`${baseURL}/api/restaurant${query}`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
export const getRestaurant = (id) => {
  return axios
    .get(`${baseURL}/api/restaurant/${id}`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
export const editRestaurant = (data, id, organizationID) => {
  return axios
    .patch(`${baseURL}/api/restaurant/${id}?org=${organizationID}`, data, { withCredentials: true })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

//NEW ROUTES - HOTEL
export const postHotel = (form) => {
  return axios
    .post(`${baseURL}/api/lodging`, form, { withCredentials: true })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
export const getAllHotels = (query = '') => {
  return axios
    .get(`${baseURL}/api/lodging${query}`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
export const getHotel = (id) => {
  return axios
    .get(`${baseURL}/api/lodging/${id}`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
export const editHotel = (data, id, organizationID) => {
  return axios
    .patch(`${baseURL}/api/lodging/${id}?org=${organizationID}`, data, { withCredentials: true })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

//NEW ROUTES - PRODUCT
export const postProduct = (form) => {
  return axios
    .post(`${baseURL}/api/article`, form, { withCredentials: true })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
export const getAllProducts = (query = '') => {
  return axios
    .get(`${baseURL}/api/article${query}`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
export const getProduct = (id) => {
  return axios
    .get(`${baseURL}/api/article/${id}`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
export const editProduct = (data, id, organizationID) => {
  return axios
    .patch(`${baseURL}/api/article/${id}?org=${organizationID}`, data, { withCredentials: true })
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
