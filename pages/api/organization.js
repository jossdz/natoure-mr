import axios from 'axios'
axios.defaults.withCredentials = true

const isProduction = process.env.NODE_ENV === 'production'
const baseURL = isProduction ? 'https://natoure-api.herokuapp.com' : 'http://localhost:3000'

export const getReservationsByOrg = (organizationId) => {
  return axios
    .get(`${baseURL}/organization/${organizationId}/reservations`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
