import axios from 'axios'
axios.defaults.withCredentials = true

const isProduction = process.env.NODE_ENV === 'production'
const baseURL = isProduction ? 'https://natoure-api.herokuapp.com' : 'http://localhost:3000'

// Destinos

export const postDestino = (form) => {
  return axios
    .post(`${baseURL}/api/destinations`, form)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const getAllDestinations = (params) => {
  const queryString = `?${Object.keys(params)
    .filter((key) => params[key] !== '')
    .map((key) => key + '=' + params[key])
    .join('&')}`

  return axios
    .get(`${baseURL}/api/destinations/${queryString}`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

// export const getDestinationById = ({ id }) => {
//   return axios
//     .get(`${baseURL}/api/destinations/${id}`)
//     .then(({ data }) => data)
//     .catch(({ response }) => {
//       throw response
//     })
// }
