import axios from 'axios'
import { objectToQueryString } from '../../utils'

axios.defaults.withCredentials = true

const isProduction = process.env.NODE_ENV === 'production'
const baseURL = isProduction ? 'https://natoure-api.herokuapp.com' : 'http://localhost:3000'

// NaturalAreas

// export const postDestino = (form, orgId) => {
//   return axios
//     .post(`${baseURL}/destinations`, form)
//     .then(({ data }) => data)
//     .catch(({ response }) => {
//       throw response
//     })
// }

export const getAll = (params) => {
  const queryString = objectToQueryString({ params })
  return axios
    .get(`${baseURL}/api/naturalAreas/${queryString}`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}

export const getOneById = ({ id }) => {
  return axios
    .get(`${baseURL}/api/naturalAreas/${id}`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
