import axios from 'axios'
axios.defaults.withCredentials = true

const isProduction = process.env.NODE_ENV === 'production'
const baseURL = isProduction ? 'https://natoure-api.herokuapp.com' : 'http://localhost:3000'

export const getUsers = (params = {}) => {
  const queryString = `?${Object.keys(params)
    .filter((key) => params[key] !== '')
    .map((key) => key + '=' + params[key])
    .join('&')}`
  return axios
    .get(`${baseURL}/api/users${queryString}`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      throw response
    })
}
