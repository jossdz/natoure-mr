import React, { useState, useEffect } from 'react'
import { AiOutlineEnvironment, AiFillAppstore } from 'react-icons/ai'
import Link from 'next/link'

import TravelerLayout from '../Layouts/Traveler'
import {
  HostSection,
  HorizontalScroller,
  MainHostCard,
  MainHostInfo,
  MainHostInfoTitle,
  MainHostInfoDescription,
  HostStoryImg,
  HostStoryInfo,
  HostStoryMiniImage,
} from '../styles/anfitriones'
import { Label } from '../styles/forms'
import { HostSecondCard } from '../components/Host/components/HostSecondCard'
import FilterBar from '../styles/components/FilterBar'
import Jumbo from '../styles/explore/Jumbo'
import MapButton from '../styles/components/MapButton'
import { Map } from '../components/MapPage'
import Container from '../styles/components/Container'
import { getAllOrganizations } from '../pages/api/api'
import { FlexWrapper } from '../styles/checkout'
import { MarginY, OnlyWeb } from '../styles/general'
import HorizontalS from '../styles/general/HorizontalSlide'

const HostsPage = (): JSX.Element => {
  const [section, setSection] = useState<string>('host')
  const handleMenu = (section) => setSection(section)
  const [organizations, setOrganizations] = useState([])
  const [moreSustainable, setMoreSustainable] = useState([])
  const [toggleMap, setToggleMap] = useState(false)

  useEffect(() => {
    getAllOrganizations()
      .then(({ allOrganizations }) => {
        setOrganizations(allOrganizations)
        const sorted = allOrganizations
          .sort((a, b) => b.sustentabilityPromedy - a.sustentabilityPromedy)
          .slice(0, 4)
        setMoreSustainable(sorted)
      })
      .catch((err) => console.error(err))
  }, [])
  return (
    <MarginY>
      <Container>
        <Jumbo>
          <FilterBar placeholder="Busca por ubicación, nombre…" />
          <MapButton toggleMap={toggleMap} onClick={() => setToggleMap(!toggleMap)}>
            {toggleMap ? <AiFillAppstore /> : <AiOutlineEnvironment />}
          </MapButton>
          <HorizontalS>
            <button onClick={() => handleMenu('host')} className={section === 'host' ? 'here' : ''}>
              Anfitriones
            </button>
            <button
              onClick={() => handleMenu('products')}
              className={section === 'products' ? 'here' : ''}
            >
              Productos
            </button>
          </HorizontalS>
        </Jumbo>
        {!toggleMap && (
          <>
            <OnlyWeb>
              <HostSection>
                <Label title="Conoce a tus anfitriones" />
                <FlexWrapper>
                  <HostStoryImg bg="https://res.cloudinary.com/natoure/raw/upload/v1620242810/organizations/mcvscmrxbslr8z7rp1bv.jpg" />
                  <HostStoryInfo>
                    <h2 className="h2">Descubre las historias de nuestros anfitriones</h2>
                    <div>
                      Vive experiencias de viaje únicas que impulsan economías locales, generan
                      conservación de la naturaleza y frenan el Cambio Climático, trabajadores
                      humanitarios y de emergencias, para que puedan estar cerca de los pacientes y
                      permanecer a una distancia segura de sus familiares.
                    </div>
                    <div className="divider"></div>
                  </HostStoryInfo>
                </FlexWrapper>
                <FlexWrapper>
                  <HostStoryMiniImage bg="https://images.unsplash.com/photo-1501855901885-8b29fa615daf?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1336&q=80" />
                  <HostStoryMiniImage bg="https://images.unsplash.com/photo-1626233127008-5431435e7a57?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1498&q=80" />
                  <HostStoryMiniImage bg="https://images.unsplash.com/photo-1518105779142-d975f22f1b0a?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1500&q=80" />
                  <HostStoryMiniImage bg="https://images.unsplash.com/photo-1521216774850-01bc1c5fe0da?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1500&q=80" />
                  <HostStoryMiniImage bg="https://images.unsplash.com/photo-1584208632661-f8db31bb6489?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1787&q=80" />
                </FlexWrapper>
              </HostSection>
            </OnlyWeb>

            <HostSection>
              <Label title="Conoce a tus anfitriones" />
              <HorizontalScroller>
                {organizations.length > 0 &&
                  organizations.map((item) => (
                    <Link key={Math.random()} href={`/anfitrion/${item._id}`}>
                      <MainHostCard bg={item.mainPhoto[0]}>
                        <MainHostInfo>
                          <MainHostInfoTitle>
                            {item.organizationName || 'Nombre no disponible'}
                          </MainHostInfoTitle>
                          <MainHostInfoDescription>
                            {item.organizationStory || 'Descripción no disponible'}
                          </MainHostInfoDescription>
                        </MainHostInfo>
                      </MainHostCard>
                    </Link>
                  ))}
              </HorizontalScroller>
            </HostSection>
            <HostSection>
              <Label title="Los más sostenibles" />
              <FlexWrapper wrap="wrap">
                {moreSustainable.map((item, index) => (
                  <HostSecondCard
                    key={index}
                    image={item.mainPhoto[0]}
                    name={item.organizationName}
                    id={item._id}
                    address={item.orgState}
                  />
                ))}
              </FlexWrapper>
            </HostSection>
          </>
        )}
      </Container>
      {toggleMap && <Map isToggled={true} />}
    </MarginY>
  )
}

HostsPage.Layout = TravelerLayout

export default HostsPage
