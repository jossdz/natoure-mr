import React, { useEffect, useState } from 'react'

import Jumbo from '../styles/explore/Jumbo'
import HorizontalS from '../styles/general/HorizontalSlide'
import { MarginY } from '../styles/general'
import AddProduct from '../styles/anfitrion/addProduct'
import Container from '../styles/components/Container'
import Loading from '../styles/components/Loading'
import HostLayout from '../Layouts/Host'
import ProductsWrapper from '../components/common/ProductsWrapper'
import ExperienceCard from '../styles/anfitrion/ExperienceCard'
import PlateCard from '../styles/anfitrion/PlateCard'
import RestaurantCard from '../styles/anfitrion/RestaurantCard'
import HotelCard from '../styles/anfitrion/HotelCard'
import ProductCard from '../styles/anfitrion/ProductCard'

import {
  getAllExperiences,
  getOrganization,
  getAllHotels,
  getAllRestaurants,
  getAllPlates,
  getAllProducts,
} from './api/api'

const MisProductos = (): JSX.Element => {
  const [isData, setIsData] = useState(false)
  const [experiences, setExperiences] = useState([])
  const [restaurants, setRestaurants] = useState([])
  const [plates, setPlates] = useState([])
  const [lodgings, setLodgings] = useState([])
  const [products, setProducts] = useState([])
  const [category, setCategory] = useState('Experiencias')

  useEffect(() => {
    const userID = localStorage.getItem('ID')

    getOrganization(userID)
      .then((organization) => {
        localStorage.setItem('organizationid', organization._id)
        const { _id } = organization
        const query = `?org=${_id}`

        getAllExperiences(query)
          .then(({ experiences }) => {
            setIsData(true)
            setExperiences(experiences)
          })
          .catch((err) => console.error(err))
        getAllPlates()
          .then(({ plates }) => {
            setPlates(plates)
          })
          .catch((err) => console.error(err))
        getAllRestaurants(query)
          .then(({ restaurants }) => {
            setRestaurants(restaurants)
          })
          .catch((err) => console.error(err))
        getAllHotels(query)
          .then(({ lodgings }) => {
            setLodgings(lodgings)
          })
          .catch((err) => console.error(err))
        getAllProducts(query)
          .then(({ article }) => {
            setProducts(article)
          })
          .catch((err) => console.error(err))
      })
      .catch((err) => console.error(err))
  }, [])

  const categories = {
    Experiencias: {
      link: '/form-experiencia',
      add: 'una nueva experiencia',
      products: experiences,
      card: ExperienceCard,
    },
    Platillos: {
      link: '/form-platillo',
      add: 'un nuevo platillo',
      products: plates,
      card: PlateCard,
    },
    Restaurantes: {
      link: '/form-restaurant',
      add: 'un nuevo restaurante',
      products: restaurants,
      card: RestaurantCard,
    },
    Hospedaje: {
      link: '/form-hotel',
      add: 'un nuevo hotel',
      products: lodgings,
      card: HotelCard,
    },
    Productos: {
      link: '/form-producto',
      add: 'un nuevo producto',
      products: products,
      card: ProductCard,
    },
  }

  return !isData ? (
    <Loading />
  ) : (
    <MarginY>
      <Container>
        <Jumbo>
          <HorizontalS>
            {Object.keys(categories).map((item, index) => (
              <button
                key={index}
                className={category === item ? 'here' : ''}
                onClick={() => setCategory(item)}
              >
                {item}
              </button>
            ))}
          </HorizontalS>
        </Jumbo>
        <AddProduct ruta={categories[category].link} product={categories[category].add} />
        <ProductsWrapper
          products={categories[category].products}
          Card={categories[category].card}
        />
      </Container>
    </MarginY>
  )
}

MisProductos.Layout = HostLayout

export default MisProductos
