/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/media-has-caption */
import React from 'react'

import { SectionHeading } from '../styles/components'
import { MarginY } from '../styles/general'

import HeadingCert from '../styles/anfitrion/HeadingCert'
import AddingFiles from '../styles/anfitrion/addingFiles'
import SaveandBack from '../styles/anfitrion/saveandback'

const Certificados: React.FC = () => {
  const Profile = () => (
    <>
      <HeadingCert title="" description="" poblacion="" />

      <SectionHeading title="¿Tienes alguna certificación, reconocimiento o distintivo que desees agregar?" />

      <AddingFiles name="" description="" poblacion="" title="Certificaciones" />

      <AddingFiles title="Reconocimientos" name="" description="" poblacion="" />

      <AddingFiles title="Distintivos" name="" description="" poblacion="" />

      <SaveandBack title="" name="" description="" poblacion="" />
    </>
  )

  return (
    <MarginY>
      <Profile />
    </MarginY>
  )
}

export default Certificados
