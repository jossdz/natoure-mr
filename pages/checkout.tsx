import React, { useState } from 'react'
import { CheckoutHeaderClose, CheckoutBody, CheckoutButton } from '../styles/checkout'
import {
  CheckoutFooter,
  CheckoutHeader,
  CheckoutStepOne,
  CheckoutStepTree,
} from '../components/checkout'
import { IoIosClose } from 'react-icons/io'
import { useRouter } from 'next/router'

import dynamic from 'next/dynamic'

const DynamicComponent = dynamic(import('../components/checkout/CheckoutSteps/CheckoutStepTwo'), {
  ssr: false,
})

const Checkout = (): JSX.Element => {
  const [step, setStep] = useState(0)
  const { back } = useRouter()
  const handleSteps = (step) => setStep(step)
  return (
    <React.Fragment>
      <CheckoutHeader>
        {step === 2 ? 'Comparte si tienes algún requerimiento especial' : 'Confirma tu compra'}
        <CheckoutHeaderClose onClick={() => back()}>
          <IoIosClose />
        </CheckoutHeaderClose>
      </CheckoutHeader>
      <CheckoutBody>
        {step === 0 ? (
          <CheckoutStepOne onNextStep={handleSteps} />
        ) : step === 1 ? (
          <DynamicComponent onNextStep={handleSteps} />
        ) : (
          <CheckoutStepTree onNextStep={handleSteps} />
        )}
      </CheckoutBody>
    </React.Fragment>
  )
}

export default Checkout
