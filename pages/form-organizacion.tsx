import React from 'react'
import { useRouter } from 'next/router'

import { Step1, Step2, Step3, Step4, Step5, Step6 } from '../steppers/organizacion'

import { ButtonText, Heading, Modal, SectionHeading } from '../styles/forms'

import { Container, Loading } from '../styles/components'

import { postOrganizacion } from './api/api'

import useStepper from '../hooks/useStepper'

const FormOrganizacion = (): JSX.Element => {
  const { push } = useRouter()
  const { currentStep, status, onStepChange, onSubmit, onBackStep } = useStepper(
    postOrganizacion,
    false
  )

  const steps = {
    1: {
      render: Step1,
      name: 'Datos principales',
      heading: 'Registra tus datos',
    },
    2: {
      render: Step2,
      name: 'Ubicación',
      heading: 'Registra dónde están ubicados',
    },
    3: {
      render: Step3,
      name: 'Datos de contacto',
      heading: 'Registra tus datos de contacto',
    },
    4: {
      render: Step4,
      name: 'Sostenibilidad',
      heading: 'Ahora conozcamos tu nivel de compromiso socioambiental',
    },
    5: {
      render: Step5,
      name: 'Fotos y videos',
      heading: 'Sube tus fotos y videos',
    },
    6: {
      render: Step6,
      name: 'Confirmación',
      heading: 'Confirma tus datos',
    },
  }

  const Component = steps[currentStep].render

  return (
    <React.Fragment>
      <Heading
        title="Registra tu organización"
        stepName={steps[currentStep].name}
        step={currentStep}
        total={6}
        onBackStep={onBackStep}
      />
      <Container>
        <SectionHeading title={steps[currentStep].heading} />
        {currentStep === 6 ? (
          <Component onStepChange={onStepChange} onSubmit={onSubmit} />
        ) : (
          <Component onStepChange={onStepChange} />
        )}
        <ButtonText
          style={{ display: currentStep === 1 && 'none', marginBottom: 96 }}
          onClick={onBackStep}
        />
      </Container>
      {status === 'fetching' && <Loading />}
      {status === 'finished' && (
        <Modal
          name="¡Muchas gracias por compartir los datos de tu organización!"
          description="La información será revisada por el equipo de Natoure antes de ser publicada."
          onClick={() => push('/misproductos')}
        />
      )}
      {status === 'error' && (
        <Modal
          name="¡Lo sentimos! Ha ocurrido un error"
          description="No fue posible publicar tu organización."
          onClick={() => push('/misproductos')}
          error
        />
      )}
    </React.Fragment>
  )
}

export default FormOrganizacion
