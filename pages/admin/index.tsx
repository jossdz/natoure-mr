import React, { useState } from 'react'
import { AdminLoginWrapper, AdminLogin } from '../../styles/admin'
import TextField from '@material-ui/core/TextField'
import Button from '../../styles/general/Button'
import { useRouter } from 'next/router'
import { postLogin } from '../api/api'
// import { verifyAdmin } from '../api/admin'
// import { GetServerSideProps } from 'next'

const AdminIndex = (): JSX.Element => {
  const { push } = useRouter()
  const [data, setData] = useState({})
  const [status, setStatus] = useState<'' | 'fetching' | 'finished'>('')

  const handleInputs = (e) => {
    const { name, value } = e.target
    setData((prev) => ({ ...prev, [name]: value }))
  }
  const handleLogin = () => {
    setStatus('fetching')
    postLogin(data)
      .then(({ user }) => {
        setStatus('finished')
        localStorage.setItem('NatoureUser', JSON.stringify(user))
        push('/admin/usuarios')
      })
      .catch((err) => {
        setStatus('finished')
        console.log(err)
      })
  }
  return (
    <AdminLoginWrapper>
      <AdminLogin>
        <img src="/assets/icon-blue.png" alt="natour-logo" />
        <h2>Admin login</h2>
        <TextField
          id="admin-email"
          style={{ margin: 8 }}
          placeholder="Correo electrónico"
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
          onChange={handleInputs}
          name="email"
          type="email"
          required
        />
        <TextField
          id="admin-password"
          style={{ marginBottom: 24 }}
          placeholder="Contraseña"
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
          onChange={handleInputs}
          name="password"
          type="password"
          required
        />
        <Button btnType="primary" onClick={handleLogin} disabled={status === 'fetching'}>
          {status === 'fetching' ? 'Ingresando...' : 'Ingresar'}
        </Button>
      </AdminLogin>
    </AdminLoginWrapper>
  )
}

// export const getServerSideProps: GetServerSideProps = async (context) => {
//   const token = context.req.cookies?.token || null
//   if (token) {
//     const { isAdmin, name } = await verifyAdmin(token)
//     if (name === 'JsonWebTokenError' || name === 'TokenExpiredError') return { props: {} }
//     if (isAdmin) {
//       return {
//         redirect: {
//           destination: '/admin/usuarios',
//           permanent: false,
//         },
//       }
//     }
//   }

//   return { props: {} }
// }
export default AdminIndex
