import React, { useEffect, useState } from 'react'
import AdminLayout from '../../../Layouts/Admin'
import { Table } from '../../../components/common/Table'
import { getAdmins } from '../../api/admin'
import es from 'date-fns/locale/es'
import format from 'date-fns/format'
import { useRouter } from 'next/router'
import { UserSectionNav } from '../../../components/admin'
// import { verifyAdmin } from '../../api/admin'
// import { GetServerSideProps } from 'next'

const AdminList = (): JSX.Element => {
  const { route } = useRouter()
  const [users, setUsers] = useState([])

  useEffect(() => {
    getAdmins()
      .then(({ admins }) => {
        setUsers(admins)
      })
      .catch((err) => console.log(err))
  }, [])

  return (
    <React.Fragment>
      <UserSectionNav route={route} />
      <React.Fragment>
        <Table customGridTemplate="repeat(3, 1fr) repeat(2, 0.5fr)" rowLg>
          <Table.Header>
            <Table.HeaderCell>Nombre</Table.HeaderCell>
            <Table.HeaderCell>Correo</Table.HeaderCell>
            <Table.HeaderCell>Télefono</Table.HeaderCell>
            <Table.HeaderCell>Pais</Table.HeaderCell>
            <Table.HeaderCell>Fecha de Registro</Table.HeaderCell>
          </Table.Header>
          <Table.Body>
            {users.map((adminObj) => (
              <Table.Row key={adminObj.user._id}>
                <Table.Cell>
                  {adminObj.user.name || adminObj.user.last_name
                    ? `${adminObj.user.name} ${adminObj.user.last_name}`
                    : 'No disponible'}
                </Table.Cell>
                <Table.Cell>{adminObj.user.email}</Table.Cell>
                <Table.Cell>{adminObj.user.telefoneNumber || 'No disponible'}</Table.Cell>
                <Table.Cell>{adminObj.user.country || 'No disponible'}</Table.Cell>
                <Table.Cell>
                  {format(new Date(adminObj.user.createdAt), 'P', { locale: es })}
                </Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
      </React.Fragment>
    </React.Fragment>
  )
}

// export const getServerSideProps: GetServerSideProps = async (context) => {
//   const token = context.req.cookies?.token || null
//   if (token) {
//     const { isAdmin, name } = await verifyAdmin(token)
//     if (name === 'JsonWebTokenError' || name === 'TokenExpiredError') {
//       return {
//         redirect: {
//           destination: '/admin',
//           permanent: false,
//         },
//       }
//     }

//     if (!isAdmin) {
//       return {
//         redirect: {
//           destination: '/inicio',
//           permanent: false,
//         },
//       }
//     }

//     if (isAdmin) {
//       return { props: {} }
//     }
//   }

//   return {
//     redirect: {
//       destination: '/inicio',
//       permanent: false,
//     },
//   }
// }

AdminList.Layout = AdminLayout
export default AdminList
