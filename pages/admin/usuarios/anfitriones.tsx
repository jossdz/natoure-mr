import React, { useEffect, useState } from 'react'
import AdminLayout from '../../../Layouts/Admin'
import { useRouter } from 'next/router'
import { Table } from '../../../components/common/Table'
import { getUsers } from '../../api/users'
import es from 'date-fns/locale/es'
import format from 'date-fns/format'
import { UserSectionNav } from '../../../components/admin'
// import { verifyAdmin } from '../../api/admin'
// import { GetServerSideProps } from 'next'

const HostList = (): JSX.Element => {
  const { route } = useRouter()
  const [users, setUsers] = useState([])

  useEffect(() => {
    getUsers({ anfitrion: true })
      .then(({ users }) => {
        setUsers(users)
      })
      .catch((err) => console.log(err))
  }, [])

  return (
    <React.Fragment>
      <UserSectionNav route={route} />
      <React.Fragment>
        <Table customGridTemplate="repeat(3, 1fr) repeat(2, 0.5fr)" rowLg>
          <Table.Header>
            <Table.HeaderCell>Nombre</Table.HeaderCell>
            <Table.HeaderCell>Correo</Table.HeaderCell>
            <Table.HeaderCell>Télefono</Table.HeaderCell>
            <Table.HeaderCell>Pais</Table.HeaderCell>
            <Table.HeaderCell>Fecha de Registro</Table.HeaderCell>
          </Table.Header>
          <Table.Body>
            {users.map((user) => (
              <Table.Row key={user._id}>
                <Table.Cell>
                  {user.name || user.last_name ? `${user.name} ${user.last_name}` : 'No disponible'}
                </Table.Cell>
                <Table.Cell>{user.email}</Table.Cell>
                <Table.Cell>{user.telefoneNumber || 'No disponible'}</Table.Cell>
                <Table.Cell>{user.country || 'No disponible'}</Table.Cell>
                <Table.Cell>{format(new Date(user.createdAt), 'P', { locale: es })}</Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
      </React.Fragment>
    </React.Fragment>
  )
}

// export const getServerSideProps: GetServerSideProps = async (context) => {
//   const token = context.req.cookies?.token || null
//   if (token) {
//     const { isAdmin, name } = await verifyAdmin(token)
//     if (name === 'JsonWebTokenError' || name === 'TokenExpiredError') {
//       return {
//         redirect: {
//           destination: '/admin',
//           permanent: false,
//         },
//       }
//     }

//     if (!isAdmin) {
//       return {
//         redirect: {
//           destination: '/inicio',
//           permanent: false,
//         },
//       }
//     }

//     if (isAdmin) {
//       return { props: {} }
//     }
//   }

//   return {
//     redirect: {
//       destination: '/inicio',
//       permanent: false,
//     },
//   }
// }

HostList.Layout = AdminLayout
export default HostList
