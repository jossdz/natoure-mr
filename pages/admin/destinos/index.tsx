import React, { useEffect, useState } from 'react'
import AdminLayout from '../../../Layouts/Admin'
import { AdminSectionTitle, GreenButton } from '../../../styles/admin'
import { FlexWrapper } from '../../../styles/MapPage/styles'
import { FaPlus } from 'react-icons/fa'
import { useRouter } from 'next/router'
import { Table } from '../../../components/common/Table'
import { getAllDestinations } from '../../api/destinations'
import { MegaLabel } from '../../../styles/landing'

const Destinos = (): JSX.Element => {
  const { push } = useRouter()
  const [destinations, setDestinations] = useState([])
  const [status, setStatus] = useState<'' | 'fetching' | 'finished'>('')

  useEffect(() => {
    setStatus('fetching')
    getAllDestinations({})
      .then(({ destinations }) => {
        setStatus('finished')
        setDestinations(destinations)
      })
      .catch((err) => {
        setStatus('finished')
        console.log(err)
      })
  }, [])
  return (
    <React.Fragment>
      <FlexWrapper justify="flex-start" align="center">
        <AdminSectionTitle>Destinos sostenibles</AdminSectionTitle>
        <GreenButton onClick={() => push('/admin/destinos/nuevo')}>
          <FaPlus />
          Agregar
        </GreenButton>
      </FlexWrapper>
      <React.Fragment>
        {destinations.length > 0 && status === 'finished' && (
          <Table customGridTemplate="1fr 1fr 0.5fr 1fr" rowLg>
            <Table.Header>
              <Table.HeaderCell>Nombre del destino</Table.HeaderCell>
              <Table.HeaderCell>Pais</Table.HeaderCell>
              <Table.HeaderCell>Estado</Table.HeaderCell>
              <Table.HeaderCell>Tipo de destino</Table.HeaderCell>
            </Table.Header>
            <Table.Body>
              {destinations.map((destination) => (
                <Table.Row key={destination._id}>
                  <Table.Cell>{destination.name}</Table.Cell>
                  <Table.Cell>{destination.location.country || 'No disponible'}</Table.Cell>
                  <Table.Cell>{destination.location.state || 'No disponible'}</Table.Cell>
                  <Table.Cell>
                    {destination.destinationType.reduce((acc, item) => {
                      return `${acc} ${item}`
                    }, '')}
                  </Table.Cell>
                </Table.Row>
              ))}
            </Table.Body>
          </Table>
        )}
        {destinations.length === 0 && status === 'finished' && (
          <MegaLabel>No hay destinos para mostrar por el momento</MegaLabel>
        )}
      </React.Fragment>
    </React.Fragment>
  )
}

Destinos.Layout = AdminLayout

export default Destinos
