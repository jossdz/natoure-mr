import React, { useState, useRef, useEffect } from 'react'
import AdminLayout from '../../../Layouts/Admin'
import { AdminSectionTitle } from '../../../styles/admin'
import { FlexWrapper } from '../../../styles/MapPage/styles'
import { Button, Chips, Disclaimer, Input, Label, Select, Upload } from '../../../styles/forms'
import DatePicker, { registerLocale } from 'react-datepicker'
import es from 'date-fns/locale/es'
import format from 'date-fns/format'
import styled from 'styled-components'
import mapboxgl from 'mapbox-gl'
import Map, { MapWrapper } from '../../../styles/forms/Map'
import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder'
import { useRouter } from 'next/router'
import { Container } from '../../../styles/components'
import { createNaturalArea } from '../../api/areas'

registerLocale('es', es)

mapboxgl.accessToken =
  'pk.eyJ1IjoibWx6eiIsImEiOiJjandrNmVzNzUwNWZjNGFqdGcwNmJ2ZWhpIn0.ybY6wnAtJwj-Tq0c46sW6A'

const Wrapper = styled.button`
  position: relative;
  width: 100%;
  max-width: 85vw;
  margin: 0 auto;
  font-family: 'Montserrat', sans-serif;
  height: 44px;
  padding: 12px 18px;
  border-radius: 8px;
  border: solid 1px var(--grey);
  background-color: var(--white);
  text-align: left;
  color: var(--dark-grey);
  overflow-x: scroll;
  ::-webkit-scrollbar {
    display: none;
  }
`

const SecondaryButton = styled.button`
  height: 40px;
  min-width: 122px;
  max-width: 360px;
  border: 1px solid var(--green-natoure);
  background-color: transparent;
  border-radius: 8px;
  color: var(--green-natoure);
  font-family: 'Montserrat', sans-serif;
  font-weight: 600;
  font-size: 16px;
  letter-spacing: 0.5px;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 32px;
  :not(:only-child) {
    margin: 0 16px 32px;
  }
`

type PropsCal = {
  onClick?: () => void
  value?: string
}

type RefType = number

const NewArea = (): JSX.Element => {
  const { push } = useRouter()

  const [date, setDate] = useState('')
  const [isInvalidForm, setIsInvalidForm] = useState(false)
  const [form, setForm] = useState({ location: {} })
  const [status, setStatus] = useState<'' | 'fetching' | 'finished'>('')
  const [error, setError] = useState({})
  const node = useRef(null)
  const node2 = useRef(null)
  const node3 = useRef(null)
  const node4 = useRef(null)

  const addressInput = useRef(null)

  useEffect(() => {
    const isInvalid = Object.values(form).some(
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      (value: any) => value === undefined || value === '' || value.optional === false
    )
    setIsInvalidForm(isInvalid)
  }, [form])

  useEffect(() => {
    const fixedLocation = [-99.1412, 19.4352]

    const map = new mapboxgl.Map({
      container: node.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: fixedLocation,
      zoom: 5,
    })

    const geocoder = new MapboxGeocoder({
      accessToken: mapboxgl.accessToken,
      mapboxgl: mapboxgl,
      marker: false,
      language: 'es',
      placeholder: 'Ingresa la dirección',
    })

    if (document.getElementById('geocoder').innerHTML === '')
      addressInput.current.appendChild(geocoder.onAdd(map))

    geocoder.on('result', ({ result }) => {
      const marker = new mapboxgl.Marker({ draggable: true }).setLngLat(result.center).addTo(map)
      marker.on('dragend', (e) => {
        const lngLat = e.target.getLngLat()
        setForm((prev) => ({
          ...prev,
          location: {
            ...prev.location,
            coordinates: [lngLat.lng, lngLat.lat],
            address: result.place_name,
          },
        }))
      })

      setForm((prev) => ({
        ...prev,
        location: { ...prev.location, coordinates: result.center, address: result.place_name },
      }))
    })

    const map2 = new mapboxgl.Map({
      container: node2.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: fixedLocation,
      zoom: 5,
    })

    const map3 = new mapboxgl.Map({
      container: node3.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: fixedLocation,
      zoom: 5,
    })

    const map4 = new mapboxgl.Map({
      container: node4.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: fixedLocation,
      zoom: 5,
    })

    const geocoder2 = new MapboxGeocoder({
      accessToken: mapboxgl.accessToken,
      mapboxgl: mapboxgl,
      language: 'es',
      placeholder: 'Buscar',
    })

    const geocoder3 = new MapboxGeocoder({
      accessToken: mapboxgl.accessToken,
      mapboxgl: mapboxgl,
      language: 'es',
      placeholder: 'Buscar',
    })

    const geocoder4 = new MapboxGeocoder({
      accessToken: mapboxgl.accessToken,
      mapboxgl: mapboxgl,
      language: 'es',
      placeholder: 'Buscar',
    })

    map2.addControl(geocoder2)

    map3.addControl(geocoder3)

    map4.addControl(geocoder4)

    geocoder2.on('result', ({ result }) => {
      setForm((prev) => ({
        ...prev,
        closestCity: { coordinates: result.center, address: result.place_name },
      }))
    })

    geocoder3.on('result', ({ result }) => {
      setForm((prev) => ({
        ...prev,
        closestAirport: { coordinates: result.center, address: result.place_name },
      }))
    })

    geocoder4.on('result', ({ result }) => {
      setForm((prev) => ({
        ...prev,
        closesBusStation: { coordinates: result.center, address: result.place_name },
      }))
    })
  }, [])

  const handleChange = (e) => {
    const arrayField = ['predominantEcosystems', 'conservationType']
    const value = arrayField.includes(e.name)
      ? e.value
      : Array.isArray(e.value)
      ? e.value[0]
      : e.value
      ? e.value
      : e
    setForm((prev) => {
      if (e.name === 'country' || e.name === 'state')
        return { ...prev, location: { ...prev.location, [e.name]: value } }
      return { ...prev, [e.name]: value }
    })
  }

  const handleSubmit = async () => {
    setStatus('fetching')
    const cleanedData = await Object.keys(form).reduce((acc, key) => {
      return form[key].optional ? { ...acc, [key]: '' } : { ...acc, [key]: form[key] }
    }, {})
    createNaturalArea(cleanedData)
      .then(() => {
        setStatus('finished')
        push('/admin/areas')
      })
      .catch((err) => {
        setStatus('finished')
        setError(err)
      })
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const CalendarInput = React.forwardRef<RefType, PropsCal>(({ onClick }, ref: any) => (
    <Wrapper onClick={onClick} ref={ref}>
      {date === '' ? (
        'Selecciona una fecha'
      ) : (
        <span style={{ paddingRight: 8, color: '#1c1c1c' }}>{date}</span>
      )}
    </Wrapper>
  ))
  CalendarInput.displayName = 'CalendarInput'

  return (
    <React.Fragment>
      <FlexWrapper justify="flex-start" align="center">
        <AdminSectionTitle>Nueva área natural</AdminSectionTitle>
      </FlexWrapper>
      <Container>
        <Disclaimer
          bold="Registra los datos principales "
          text="para dar de alta tu AIC. Podrás complementar tu información más adelante."
          style={{ marginTop: 48, marginBottom: 48 }}
        />
        <Label title="Foto de portada" />
        <Upload name="coverMedia" onlyPhoto onChange={handleChange} />

        <Label title="Logo del área" />
        <Upload name="logo" onlyPhoto onChange={handleChange} />

        <Label title="Pais donde se encuentra el área" />
        <Select
          name="country"
          onChange={handleChange}
          options={['México']}
          style={{ marginBottom: 40 }}
        />

        <Label title="Estado donde se encuentra el área" />
        <Select
          name="state"
          onChange={handleChange}
          options={['CDMX']}
          style={{ marginBottom: 40 }}
        />

        <Label title="Área de importancia de conservación" />
        <Input name="name" onChange={handleChange} placeholder="Ingresa el nombre del área" />

        <Label title="Tipo de Área de Conservación" />
        <Chips
          name="conservationType"
          onChange={handleChange}
          options={[
            'Parques Nacionales',
            'Reservas Biológicas',
            'Monumentos Nacionales',
            'Refugios Nacionales de Vida Silvestre',
            'Humedales',
            'Reservas Forestales',
            'Zonas Protectoras',
            'Corredor Biológico',
          ]}
        />

        <Label title="Fecha de creación del área" />
        <div style={{ width: '100%', maxWidth: '85vw', margin: '0 auto 40px auto' }}>
          <DatePicker
            onChange={(date) => {
              const formated = format(date, 'dd/MM/yyyy')
              setDate(formated)
              setForm((prev) => ({
                ...prev,
                creationDate: date,
              }))
            }}
            locale="es"
            customInput={<CalendarInput />}
          />
        </div>

        <Label title="¿Quién administra el área?" />
        <Input name="adminName" placeholder="Ingresa el nombre" onChange={handleChange} />

        <Label title="Correo electrónico del administrador" />
        <Input name="adminEmail" placeholder="Ingresa el correo" onChange={handleChange} />

        <Label title="Acrónimo del administrador del área" />
        <Input name="adminShortName" placeholder="Ingresa el nombre" onChange={handleChange} />

        <Label title="Sitio web del área" optional />
        <Input name="website" optional onChange={handleChange} placeholder="www.tusitio.com" />

        <Label title="Ecosistemas predominantes" />
        <Chips
          name="predominantEcosystems"
          onChange={handleChange}
          options={['Bosques', 'Matorrales', 'Herbazales', 'Tundra', 'Desierto']}
        />

        <Label title="¿Dónde esta ubicada tu AIC?" />
        <MapWrapper>
          <div id="geocoder" className="geocoder" ref={addressInput}></div>
          <Map ref={node} />
        </MapWrapper>
        <Label title="Describe cómo llegar (asumiendo que un viajero va por su cuenta)" />
        <Disclaimer
          style={{ marginTop: 12, marginBottom: 24 }}
          text="Recuerda que muchos de los participantes en tu experiencia no conocen el lugar, sé lo más claro posible con las indicaciones. Menciona avenidas principales o lugares conocidos que los ayuden a ubicarse mejor."
        />
        <Input
          name="addressDetails"
          onChange={handleChange}
          placeholder="Máximo 1000 caracteres"
          textarea
        />

        <Label title="¿Cuál es la localidad más cercana?" />
        <div style={{ width: '100%', maxWidth: '85vw', margin: '0 auto 64px auto' }}>
          <Map ref={node2} />
        </div>
        <Label title="¿Cuál es el aeropuerto más cercano?" />
        <div style={{ width: '100%', maxWidth: '85vw', margin: '0 auto 64px auto' }}>
          <Map ref={node3} />
        </div>
        <Label title="¿Cuál es la terminal de bus más cercana?" />
        <div style={{ width: '100%', maxWidth: '85vw', margin: '0 auto 64px auto' }}>
          <Map ref={node4} />
        </div>
        <FlexWrapper justify="center" align="center">
          <SecondaryButton onClick={() => push('/admin/areas')}>Cancelar</SecondaryButton>
          <Button
            unable={isInvalidForm}
            disabled={isInvalidForm}
            text="Agregar"
            onClick={handleSubmit}
          />
        </FlexWrapper>
      </Container>
    </React.Fragment>
  )
}

NewArea.Layout = AdminLayout

export default NewArea
