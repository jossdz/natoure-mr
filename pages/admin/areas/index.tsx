import React, { useEffect, useState } from 'react'
import AdminLayout from '../../../Layouts/Admin'
import { AdminSectionTitle, GreenButton } from '../../../styles/admin'
import { FlexWrapper } from '../../../styles/MapPage/styles'
import { FaPlus } from 'react-icons/fa'
import { useRouter } from 'next/router'
import { Table } from '../../../components/common/Table'
import { getAllNaturalAreas } from '../../api/areas'
import { MegaLabel } from '../../../styles/landing'

const Areas = (): JSX.Element => {
  const { push } = useRouter()
  const [areas, setAreas] = useState([])
  const [status, setStatus] = useState<'' | 'fetching' | 'finished'>('')

  useEffect(() => {
    setStatus('fetching')
    getAllNaturalAreas()
      .then(({ naturalAreas }) => {
        setStatus('finished')
        setAreas(naturalAreas)
      })
      .catch((err) => {
        setStatus('finished')
        console.log(err)
      })
  }, [])
  return (
    <React.Fragment>
      <FlexWrapper justify="flex-start" align="center">
        <AdminSectionTitle>Áreas Naturales</AdminSectionTitle>
        <GreenButton onClick={() => push('/admin/areas/nueva')}>
          <FaPlus />
          Agregar
        </GreenButton>
      </FlexWrapper>
      <React.Fragment>
        {areas.length > 0 && status === 'finished' && (
          <Table customGridTemplate="1fr 1fr 0.5fr 1fr" rowLg>
            <Table.Header>
              <Table.HeaderCell>Nombre de área</Table.HeaderCell>
              <Table.HeaderCell>Administrador</Table.HeaderCell>
              <Table.HeaderCell>Pais</Table.HeaderCell>
              <Table.HeaderCell>Tipo de área</Table.HeaderCell>
            </Table.Header>
            <Table.Body>
              {areas.map((area) => (
                <Table.Row key={area._id}>
                  <Table.Cell>{area.name}</Table.Cell>
                  <Table.Cell>{area.adminName || 'No disponible'}</Table.Cell>
                  <Table.Cell>{area.location.country || 'No disponible'}</Table.Cell>
                  <Table.Cell>
                    {area.predominantEcosystems.reduce((acc, item) => {
                      return `${acc} ${item}`
                    }, '')}
                  </Table.Cell>
                </Table.Row>
              ))}
            </Table.Body>
          </Table>
        )}
        {areas.length === 0 && status === 'finished' && (
          <MegaLabel>No hay áreas para mostrar por el momento</MegaLabel>
        )}
      </React.Fragment>
    </React.Fragment>
  )
}

Areas.Layout = AdminLayout

export default Areas
