import React from 'react'
import { useRouter } from 'next/router'

import {
  Button,
  Chips,
  Heading,
  Input,
  Label,
  Modal,
  Upload,
  SectionText,
  SectionHeading,
} from '../styles/forms'

import { articleTags, unityTags } from '../steppers/data'

import { Container, Loading } from '../styles/components'

import { postProduct } from './api/api'

import useForm from '../hooks/useForm'

const FormProducto = (): JSX.Element => {
  const { push } = useRouter()
  const { validForm, status, handleChange, handleClick } = useForm(postProduct, false)

  return (
    <React.Fragment>
      <Heading title="Registra tu artículo" step={1} simple />
      <Container>
        <SectionHeading title="Comparte algunos datos sobre tu artículo" />
        <SectionText
          style={{ marginBottom: 64 }}
          title="Rellena los siguientes campos con información acerca de tu artículo, así los viajeros podrán conocerlo mejor."
        />
        <Label title="Sube tu imagen de portada" />
        <Upload name="articuloCoverPhoto" onChange={handleChange} />
        <Label title="Sube más fotos para mostrar en tu galería" />
        <Upload name="articuloPhotosGalery" onChange={handleChange} />
        <Label title="Titulo del artículo" />
        <Input name="articuloName" placeholder="Ingresa el nombre" onChange={handleChange} />
        <Label title="Elige etiquetas que mejor se relacionen con tu artículo" optional />
        <Chips name="articuloTags" options={articleTags} onChange={handleChange} optional />
        <Label title="Stock disponible" />
        <Input name="stock" placeholder="0" onChange={handleChange} />
        <Label title="Unidad" />
        <Chips name="unity" options={unityTags} onChange={handleChange} />
        <Button unable={!validForm} onClick={handleClick} text="Guardar y publicar" />
      </Container>
      {status === 'fetching' && <Loading />}
      {status === 'finished' && (
        <Modal
          name="¡Listo! Tu producto ha sido registrado"
          description="La información de tu producto será publicada."
          onClick={() => push('/misproductos')}
        />
      )}
      {status === 'error' && (
        <Modal
          name="¡Lo sentimos! Ha ocurrido un error"
          description="No fue posible publicar tu producto."
          onClick={() => push('/misproductos')}
          error
        />
      )}
    </React.Fragment>
  )
}

export default FormProducto
