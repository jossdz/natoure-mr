/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/media-has-caption */
import React, { useEffect, useState } from 'react'
import Jumbo from '../styles/explore/Jumbo'
import HorizontalS from '../styles/general/HorizontalSlide'
import { MarginY } from '../styles/general'
import PaymentCards from '../styles/profile/paymentMethods'
import AddPaymentCards from '../styles/profile/AddPayment'
import ProfileCard from '../styles/profile/ProfileData'
import Preference from '../styles/profile/Preference'
import Configuracion from '../styles/profile/Configuracion'
import TravelerLayout from '../Layouts/Traveler'
import { Loading } from '../styles/components'

const categories = ['Datos de mi perfil', 'Mis preferencias', 'Configuracion']

const profile = (): JSX.Element => {
  const [category, setCategory] = useState('Datos de mi perfil')

  useEffect(() => {
    let location = window.location.hash
      .replace(/-/, ' ')
      .replace(/#/, '')
      .replace(/%C3%AD/, 'í')
    location = location.charAt(0).toUpperCase() + location.slice(1)

    location !== '' && setCategory(location)
  }, [])

  const profile = (
    <ProfileCard
      image="https://dev-to-uploads.s3.amazonaws.com/uploads/user/profile_image/92592/ec3c6a02-49f8-42c6-b754-858b61defab6.jpeg"
      person="Jose Correa"
      sustain={4.5}
      description=""
    />
  )

  const payment = (
    <>
      <HorizontalS shadow>
        <PaymentCards
          metodos={[
            {
              name: 'Paypal',
              image: 'https://logos-marcas.com/wp-content/uploads/2020/04/PayPal-emblema.jpg',
            },
            {
              name: 'MasterCard',
              image:
                'https://upload.wikimedia.org/wikipedia/commons/7/72/MasterCard_early_1990s_logo.png',
            },
            {
              name: 'Visa',
              image: 'https://tentulogo.com/wp-content/uploads/VISA-FB.jpg',
            },
          ]}
        />
      </HorizontalS>
      <AddPaymentCards
        addpayments={[
          {
            name: 'Tarjeta de Credito',
          },
          {
            name: 'Paypal',
          },
        ]}
      />
    </>
  )

  const preferences = <Preference description="" person="" image="" sustain={0} />

  const config = <Configuracion description="" person="" image="" sustain={0}></Configuracion>

  return (
    <MarginY>
      {/* <Jumbo>
        <HorizontalS>
          {categories.map((item, index) => (
            <button
              key={index}
              className={category === item ? 'here' : ''}
              onClick={() => setCategory(item)}
            >
              {item}
            </button>
          ))}
        </HorizontalS>
      </Jumbo> */}
      {category === 'Datos de mi perfil' ? (
        profile
      ) : category === 'Mis preferencias' ? (
        preferences
      ) : category === 'Configuración' ? (
        config
      ) : (
        <Loading />
      )}
    </MarginY>
  )
}

profile.Layout = TravelerLayout

export default profile
