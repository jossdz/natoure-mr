/* eslint-disable jsx-a11y/media-has-caption */
import React, { useEffect, useState } from 'react'
import Jumbo from '../styles/landing/Jumbo'
import HorizontalS from '../styles/general/HorizontalSlide'
import {
  CategorySection,
  InclusiveSec,
  CategoryItem,
  MegaLabel,
  DoubleCard,
  EcoRate,
  Logos,
  SpotifyPlaylists,
  WebLink,
} from '../styles/landing'
import {
  CardWide,
  SectionHeading,
  SearchBar,
  CardDiscoverMap,
  Container,
  SustainableCards,
} from '../styles/components'
import { OutlineBtn, Box, Divider, MarginY, OnlyWeb, OnlyMobile } from '../styles/general'
import SustainableCardsU from '../styles/components/Cards/SustainableCardU'
import ExperienceCard from '../components/Explore/components/ExperienceCard'
import GastronomyCard from '../components/Explore/components/RestaurantCard'

import {
  FaHiking,
  FaMortarPestle,
  FaPalette,
  FaSeedling,
  FaHandHoldingHeart,
  FaTree,
  FaDrumstickBite,
} from 'react-icons/fa'
import TravelerLayout from '../Layouts/Traveler'
import { useRouter } from 'next/router'

import { getAllExperiences, getAllRestaurants } from './api/api'

const mainpage = (): JSX.Element => {
  const [experiencesArr, setExperiencesArr] = useState([])
  const [platesArr, setPlatesArr] = useState([])
  const [query, setQuery] = useState('')
  const { push } = useRouter()

  useEffect(() => {
    getAllExperiences('')
      .then(({ experiences }) => {
        setExperiencesArr(experiences)
      })
      .catch((err) => console.error(err))

    getAllRestaurants()
      .then(({ allRestaurants }) => {
        setPlatesArr(allRestaurants)
      })
      .catch((err) => console.error(err))
  }, [])

  const handleQuery = (value) => {
    setQuery(value)
  }

  return (
    <MarginY>
      <Jumbo>
        <Container>
          <div className="main-text">
            <strong>Explora</strong>
            <p>experiencias de viaje únicas</p>
          </div>
          <div className="web-text">
            <strong>VIVE LAS MEJORES EXPERIENCIAS</strong>
            <p>
              <b>Conviertete en Anfitrión</b> y comparte <br /> las mejores experiencias sostenibles
            </p>
          </div>
          <SearchBar
            onClick={() => {
              push(`/explora?query=${query}`)
            }}
            onChange={handleQuery}
            value={query}
            placeholder="¿A dónde quieres viajar?"
          />
        </Container>
      </Jumbo>

      <CategorySection>
        <Container>
          <MegaLabel onlyWeb>Explora diferentes categorías de experiencias</MegaLabel>
        </Container>
        <Container>
          <CategoryItem color="#5ddfbf" image="/assets/naturaleza.jpg">
            <FaTree />
            <p>Naturaleza</p>
          </CategoryItem>
          <CategoryItem color="#f7b500" image="/assets/rural.jpg">
            <FaMortarPestle />
            <p>Rural</p>
          </CategoryItem>
          <CategoryItem color="#5d8edf" image="/assets/aventura.jpg">
            <FaHiking />
            <p>Aventura</p>
          </CategoryItem>
          <CategoryItem color="#fa4775" image="/assets/biocultural.jpg">
            <FaPalette />
            <p>Biocultural</p>
          </CategoryItem>
          <CategoryItem color="#e1cd68" image="/assets/regenerativo.jpeg">
            <FaHandHoldingHeart />
            <p>Regenerativo</p>
          </CategoryItem>
          <CategoryItem color="#ae5ddf" image="/assets/agroturismo.jpeg">
            <FaSeedling />
            <p>Agroturismo</p>
          </CategoryItem>
          <CategoryItem color="#e9b3c1" image="/assets/rutas.jpeg">
            <FaDrumstickBite />
            <p>Rutas gastronómicas</p>
          </CategoryItem>
        </Container>
        <p>#turismosostenible</p>
      </CategorySection>

      <OnlyMobile>
        <iframe
          width="100%"
          height="420"
          src="https://www.youtube.com/embed/XhUG31v-t8U"
          title="YouTube video player"
          frameBorder="0"
        ></iframe>
        <Container>
          <SectionHeading title="Impulsando un turismo incluyente" />
          <InclusiveSec>
            <article>
              <img
                src="https://www.zonadocs.mx/wp-content/uploads/2020/07/Copia-de-Copia-de-Dise%C3%B1os-portada-8-7.jpg"
                alt="cap"
              />
              <p>
                Mayor participación activa y <b>liderazgo de mujeres</b>
              </p>
            </article>
            <article>
              <img
                src="https://www.entornoturistico.com/wp-content/uploads/2019/03/Wixarikas-Huicholes-660x330.jpg"
                alt="cap"
              />
              <p>
                Integración de <b>pueblos indígenas y comunidades locales</b>
              </p>
            </article>
            <article>
              <img
                src="https://espaciohogar.com/wp-content/uploads/2020/12/silla-ruedas-600x393.jpg"
                alt="cap"
              />
              <p>
                <b>Personas con discapacidad</b>
              </p>
            </article>
            <article>
              <img
                src="https://www.redadultomayor.org/wp-content/uploads/2019/07/2560_3000.jpeg"
                alt="cap"
              />
              <p>
                <b>Adultos mayores</b>
              </p>
            </article>
            <article>
              <img
                src="https://media.vogue.mx/photos/5dcef9ddb9285b0009b3ad97/master/w_2251,c_limit/muxes-en-portada-de-revista-vogue-mexico.jpg"
                alt="cap"
              />
              <p>
                <b>LGBTT+</b>
              </p>
            </article>
          </InclusiveSec>
        </Container>
        <Container>
          <SectionHeading title="Experiencias populares" linkTitle="Ver todas" link="/explora" />
        </Container>
        <HorizontalS shadow paddingLeft>
          <CardWide
            name="Cosecha en Xochimilco"
            sustainLevel={4.2}
            likes={24}
            location="CDMX"
            duration="2h"
            rating={4.5}
            price="1233"
            distance="2km"
            image="https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Trajinera_en_canal_Nativitas.jpg/1200px-Trajinera_en_canal_Nativitas.jpg"
          />
          <CardWide
            name="Cosecha en Xochimilco"
            sustainLevel={4.2}
            likes={24}
            location="CDMX"
            duration="2h"
            rating={4.5}
            price="1233"
            distance="2km"
            image="https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Trajinera_en_canal_Nativitas.jpg/1200px-Trajinera_en_canal_Nativitas.jpg"
          />
          <CardWide
            name="Cosecha en Xochimilco"
            sustainLevel={4.2}
            likes={24}
            location="CDMX"
            duration="2h"
            rating={4.5}
            price="1233"
            distance="2km"
            image="https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Trajinera_en_canal_Nativitas.jpg/1200px-Trajinera_en_canal_Nativitas.jpg"
          />
        </HorizontalS>
        <CardDiscoverMap />
        <Container>
          <SectionHeading title="Vive experiencias nativas" linkTitle="Ver todas" link="/explora" />
        </Container>
        {/* <HorizontalS paddingLeft>
          <ExperienceCards
            experiences={[
              {
                title: 'Recorrido por el sendero',
                place: 'Guanajuato',
                duration: '2 horas',
                rating: 4.5,
                image: 'https://i.postimg.cc/hGvb7FTP/Screenshot-2021-01-14-at-15-26-25.png',
                likes: 23,
                price: 50,
                sustainable: 4.0,
              },
              {
                title: 'Conociendo las tradiciones',
                place: 'Oaxaca',
                duration: '2 horas',
                rating: 4.5,
                image: 'https://i.postimg.cc/RZDJyWJ4/oaxaca.jpg',
                likes: 23,
                price: 50,
                sustainable: 4.7,
              },
              {
                title: 'Conociendo las tradiciones',
                place: 'Oaxaca',
                duration: '2 horas',
                rating: 4.5,
                image: 'https://i.postimg.cc/RZDJyWJ4/oaxaca.jpg',
                likes: 23,
                price: 50,
                sustainable: 4.7,
              },
              {
                title: 'Conociendo las tradiciones',
                place: 'Oaxaca',
                duration: '2 horas',
                rating: 4.5,
                image: 'https://i.postimg.cc/RZDJyWJ4/oaxaca.jpg',
                likes: 23,
                price: 50,
                sustainable: 4.7,
              },
            ]}
          />
        </HorizontalS> */}
        <Container>
          <SectionHeading
            title="Gastronomia Sostenible"
            linkTitle="Ver todas"
            link="/explora#gastronomía"
          />
        </Container>
        <HorizontalS paddingLeft>
          <CardWide
            name="Gastronomia Sostenible"
            sustainLevel={4.2}
            likes={24}
            location="CDMX"
            duration="2h"
            rating={4.5}
            price="1233"
            distance="2km"
            image="https://i.postimg.cc/T1M492Jc/Gastronomia-2-Img.png"
          />
        </HorizontalS>
      </OnlyMobile>

      <OnlyWeb>
        <Container>
          <DoubleCard
            title="Conoce a nuestros anfitriones"
            description="Vive experiencias de viaje únicas que impulsan economías locales, generan conservación de la naturaleza y frenan el Cambio Climático,  trabajadores humanitarios y de emergencias, para que puedan estar cerca de los pacientes y permanecer a una distancia segura de sus familiares."
            image="/assets/anfitriones.jpg"
            linkText="Conecta con nuestros anfitriones"
            href="/anfitriones"
          />
          <MegaLabel withLink>Conoce nuestras experiencias</MegaLabel>
          <WebLink />
          <HorizontalS>
            {experiencesArr?.length > 0 ? (
              experiencesArr.map((item, index) => <ExperienceCard key={index} {...item} />)
            ) : (
              <h2>No hay información para mostrar</h2>
            )}
          </HorizontalS>
          <DoubleCard
            title="Descubre las mejores experiencias gastronómicas"
            description="Vive experiencias de viaje únicas que impulsan economías locales, generan conservación de la naturaleza y frenan el Cambio Climático,  trabajadores humanitarios y de emergencias, para que puedan estar cerca de los pacientes y permanecer a una distancia segura de sus familiares."
            image="/assets/gastronomia.jpg"
            linkText="Conoce todas nuestras experiencias gastronómicas"
            href="/explora"
            reverse
          />
          <MegaLabel withLink>Estos destinos sostenibles te sorprenderán</MegaLabel>
          <WebLink />
          <HorizontalS>
            <SustainableCards
              experiences={[
                {
                  name: 'Calakmul',
                  image: 'https://i.postimg.cc/qh3KYGGm/Calakmul-Img.png',
                  link: '',
                  spell: ['Calakmul'],
                },
                {
                  name: 'El Pinacate',
                  image: 'https://i.postimg.cc/1V0qR52w/Pinacate-Img.jpg',
                  link: '',
                  spell: ['El', 'Pinacate'],
                },
                {
                  name: 'Reserva de la Mariposa Monarca',
                  image: 'https://i.postimg.cc/tYy6SWn5/Reserva-de-la-mariposa-monarca-Img.jpg',
                  link: '',
                  spell: ['tulum'],
                },
                {
                  name: 'Iztacihuatl-Popocatepetl',
                  image: 'https://i.postimg.cc/dZcCKTHJ/iztacihuatl-Popocatepetl-Img.jpg',
                  link: '/',
                  spell: ['Iztacihuatl-', 'Popocatepetl'],
                },
                {
                  name: 'Calakmul',
                  image: 'https://i.postimg.cc/qh3KYGGm/Calakmul-Img.png',
                  link: '',
                  spell: ['Calakmul'],
                },
                {
                  name: 'El Pinacate',
                  image: 'https://i.postimg.cc/1V0qR52w/Pinacate-Img.jpg',
                  link: '',
                  spell: ['El', 'Pinacate'],
                },
              ]}
            />
          </HorizontalS>
        </Container>
        <CardDiscoverMap />
        <Container>
          <MegaLabel withLink>Conoce la gastronomía local</MegaLabel>
          <WebLink />
          <HorizontalS>
            {platesArr?.length > 0 ? (
              platesArr.map((item, index) => <GastronomyCard key={index} {...item} />)
            ) : (
              <h2>No hay información para mostrar</h2>
            )}
          </HorizontalS>
        </Container>
        <EcoRate />
        <Container>
          <MegaLabel>Escucha nuestras mejores playlists para viajar</MegaLabel>
          <SpotifyPlaylists />
          <MegaLabel>Nuestros reconocimientos</MegaLabel>
          <Logos
            images={[
              '/assets/sectur.png',
              '/assets/buen-socio.png',
              '/assets/rainforest.png',
              '/assets/onu.png',
              '/assets/worldbank.png',
              '/assets/cop.png',
              '/assets/ecofilm.png',
              '/assets/enpact.png',
              '/assets/gix.png',
              '/assets/launchpad.png',
              '/assets/makesense.png',
              '/assets/momentum.png',
              '/assets/most.png',
              '/assets/unwto.png',
            ]}
          />
        </Container>
      </OnlyWeb>
    </MarginY>
  )
}

mainpage.Layout = TravelerLayout

export default mainpage
