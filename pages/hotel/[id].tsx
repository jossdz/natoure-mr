/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable jsx-a11y/media-has-caption */
import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import { RiLeafFill, RiMapPinFill, RiHotelBedFill } from 'react-icons/ri'

import { MarginY, OnlyWeb, OnlyMobile } from '../../styles/general'
import { MegaLabel } from '../../styles/landing'
import NavBar from '../../components/layout/NavBar'
import Heading from '../../styles/experience/headingcard'
import TagCard from '../../styles/experience/tags'
import Gallery from '../../styles/experience/Gallery'
import Container from '../../styles/components/Container'
import Atributes from '../../styles/experience/Atributes'
import { getHotel } from '../api/api'
import { FlexWrapper } from '../../styles/checkout'

const hotel: React.FC = () => {
  const { query } = useRouter()
  const [data, setData] = useState<any>({})

  useEffect(() => {
    const { id } = query
    if (id) {
      getHotel(id)
        .then(({ lodging }) => {
          setData(lodging)
        })
        .catch((err) => console.error(err))
    }
  }, [query])

  const Info = () => (
    <React.Fragment>
      <FlexWrapper justify="flex-start" style={{ marginBottom: 16 }}>
        <RiMapPinFill style={{ marginRight: 4 }} />
        {data?.orgId && data?.orgId.orgState}
      </FlexWrapper>
      <FlexWrapper justify="flex-start" style={{ marginBottom: 24 }}>
        <RiLeafFill style={{ marginRight: 4 }} />
        Sostenibilidad {data?.orgId && data?.orgId.sustentabilityPromedy}
      </FlexWrapper>
      <FlexWrapper justify="flex-start" style={{ marginBottom: 16 }}>
        <RiHotelBedFill style={{ marginRight: 4 }} />
        Tipo de alojamiento: {data?.hotelType}
      </FlexWrapper>
      {data?.hotelTags && data?.hotelTags.length > 0 && (
        <div style={{ width: '100%', borderTop: '1px solid var(--light-grey)' }}>
          <p>Características del hotel</p>
          <TagCard width="100%" margin="24px 0" tag={data?.hotelTags} />
        </div>
      )}
      <Atributes title="Atributos del hospedaje" attributes={data?.hotelExtraAttr} />
      <Atributes title="Amenidades" attributes={data?.hotelAmenities} />
    </React.Fragment>
  )

  return (
    <MarginY style={{ fontWeight: 'bold', color: '#2c375a' }}>
      <NavBar />
      <OnlyMobile>
        <Heading
          back="/explora"
          title={data?.hotelName}
          image={data?.hotelPhotosCover && data?.hotelPhotosCover[0]}
        />
        <Container style={{ marginTop: 16 }}>
          <Info />
        </Container>
      </OnlyMobile>

      <OnlyWeb>
        <Container>
          <FlexWrapper align="flex-start">
            <div style={{ width: '55%' }}>
              <Gallery photos={data?.hotelPhotosCover && data?.hotelPhotosCover} />
              <Gallery photos={data?.hotelPhotosGallery} isSecondary />
            </div>
            <div style={{ margin: '48px 0', width: '40%' }}>
              <MegaLabel>{data?.hotelName}</MegaLabel>
              <Info />
            </div>
          </FlexWrapper>
        </Container>
      </OnlyWeb>

      <OnlyMobile>
        <Gallery photos={data?.hotelPhotosGallery} />
      </OnlyMobile>
    </MarginY>
  )
}

export default hotel
