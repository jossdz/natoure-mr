import React from 'react'
import { Map } from '../components/MapPage'

const MapPage = (): JSX.Element => {
  return <Map />
}

export default MapPage
