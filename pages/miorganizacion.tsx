/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/media-has-caption */
import React, { useEffect } from 'react'
import Jumbo from '../styles/explore/Jumbo'
import HorizontalS from '../styles/general/HorizontalSlide'
import { SectionHeading, Loading } from '../styles/components'
import { MarginY } from '../styles/general'
import ProfileCard from '../styles/anfitrion/ProfileData'
import Configuracion from '../styles/profile/Configuracion'
import Resume from '../styles/anfitrion/ResumeUs'
import AddingCard from '../styles/anfitrion/addingCard'
import { getOrganization } from '../pages/api/api'
import { StatsSection } from '../components/organization'
import HostLayout from '../Layouts/Host'
import Container from '../styles/components/Container'

export type Organization = {
  activityTypes?: Array<any>
  address?: {
    address?: string
    coords?: Array<any>
  }
  organizationPhotos?: string
  organizationName?: string
  organizationStory?: string
  mainPhoto?: Array<any>
}

const Miorganizacion = (): JSX.Element => {
  const [renderProfile, setProfile] = React.useState(true)
  const [renderStats, setStats] = React.useState(false)
  const [renderPreferences, setPreferences] = React.useState(false)
  const [renderConfig, setConfig] = React.useState(false)
  const [myOrganization, setMyOrganization] = React.useState<Organization>({})
  const [isData, setIsData] = React.useState(false)

  useEffect(() => {
    const userID = localStorage.getItem('ID')
    getOrganization(userID)
      .then((organization) => {
        setMyOrganization(organization)
        setIsData(true)
      })
      .catch((err) => err)
  }, [])

  const handleProfile = () => {
    setStats(false)
    setPreferences(false)
    setConfig(false)
    setProfile(true)
  }

  const handleStats = () => {
    setProfile(false)
    setPreferences(false)
    setConfig(false)
    setStats(true)
  }

  const handleConfig = () => {
    setProfile(false)
    setStats(false)
    setPreferences(false)
    setConfig(true)
  }

  const OrgLayout: React.FC = ({ children }) => (
    <React.Fragment>
      <Jumbo>
        <HorizontalS>
          <p onClick={handleProfile}>
            {renderProfile ? (
              <span className="here">Datos de mi Perfil</span>
            ) : (
              'Datos de mi Perfil'
            )}
          </p>
          <p onClick={handleStats}>
            {renderStats ? <span className="here">Mis Estadisticas</span> : 'Mis Estadisticas'}
          </p>
          <p onClick={handleConfig}>
            {renderConfig ? <span className="here">Configuracion</span> : 'Configuracion'}
          </p>
        </HorizontalS>
      </Jumbo>
      {children}
    </React.Fragment>
  )

  const profile = (
    <>
      {!isData ? (
        <Loading />
      ) : (
        <>
          <ProfileCard
            image={'mainPhoto' in myOrganization && myOrganization.mainPhoto[0]}
            person={myOrganization.organizationName}
            sustain={4.5}
            description=""
          />
          <SectionHeading title="Tu descripción" />
          <Resume title="" description={myOrganization.organizationStory} poblacion="" />
          <AddingCard
            title="Tus Datos Bancarios / Fiscales"
            name="Datos Bancarios"
            description=""
            poblacion=""
          />
          <AddingCard
            title="Tus Certificaciones / Reconocimientos"
            name="certificaciones"
            description=""
            poblacion=""
          />
        </>
      )}
    </>
  )

  const config = <Configuracion description="" person="" image="" sustain={0}></Configuracion>

  return (
    <MarginY>
      <Container>
        <OrgLayout>
          {renderProfile ? (
            profile
          ) : renderStats ? (
            <StatsSection />
          ) : renderConfig ? (
            config
          ) : (
            <Loading />
          )}
        </OrgLayout>
      </Container>
    </MarginY>
  )
}

Miorganizacion.Layout = HostLayout

export default Miorganizacion
