import React from 'react'
import styled from 'styled-components'

import { MarginY } from '../styles/general'
import Container from '../styles/components/Container'
import TravelerLayout from '../Layouts/Traveler'

const Advices = styled.div`
  font-size: 14px;
  li {
    margin: 1rem 0;
  }
  th,
  td {
    width: 50%;
    padding: 8px 16px;
    border: 1px solid var(--black);
  }
  .title {
    font-size: 20px;
    font-weight: bold;
    color: var(--dark-blue);
    margin: 32px 0;
  }
  @media (max-width: 1023px) {
    font-size: 12px;
    .title {
      display: none;
    }
  }
`

const aviso = (): JSX.Element => {
  return (
    <MarginY>
      <Container>
        <Advices>
          <p className="title">AVISO DE PRIVACIDAD</p>
          <p>
            Tiene por objeto informarle acerca de cómo tratamos sus Datos Personales. En Natoure
            estamos preocupados por protegerte como usuario, por ello hacemos de tu conocimiento
            nuestras políticas de privacidad las cuales salvaguardan en todo momento tu información
            así como el manejo adecuado de la misma.
          </p>
          <p>Válida desde el 04 de diciembre de 2018.</p>
          <p>
            Para “NATOURE” S.A.P.I. de C.V., con domicilio en Calle Guillermo González Camarena No.
            1600, Piso 1, Oficina 1 G-H, Colonia Centro de Ciudad Santa Fe, Alcaldía de Álvaro
            Obregón, Código Postal 01210, en la Ciudad de México, México (en adelante “NATOURE”), el
            tratamiento legítimo, controlado e informado de sus datos personales es de vital
            importancia, por lo que, pone a su disposición el presente Aviso de Privacidad, en aras
            de que como titular de los datos personales, se encuentre facultado a ejercer sus
            derechos a la autodeterminación informativa
          </p>
          <p>
            El presente Aviso de Privacidad tiene como objeto informarle sobre el tratamiento que se
            les dará a sus datos personales cuando los mismos sean recabados, utilizados,
            almacenados y/o transferidos por “NATOURE”, de conformidad con las disposiciones de la
            Ley Federal de Protección de Datos Personales en Posesión de los Particulares, su
            Reglamento, Lineamientos y demás disposiciones normativas vigentes (en adelante la
            “Ley”)
          </p>
          <ol>
            <li>
              DEFINICIONES
              <ul>
                <li>
                  Aviso de privacidad: Es un documento generado por el responsable que se pone a
                  disposición del titular ya sea de forma física, electrónica o en cualquier
                  formato, previo al tratamiento de sus datos personales.
                </li>
                <li>
                  Datos personales: Es cualquier información concerniente a una persona física
                  identificada o identificable. Se considera que una persona es identificable cuando
                  su identidad pueda determinarse directa o indirectamente a través de cualquier
                  información.
                </li>
                <li>
                  Datos personales sensibles: Son aquellos que se refieran a la esfera más íntima de
                  su titular, o cuya utilización indebida pueda dar origen a discriminación o
                  conlleve un riesgo grave para éste.
                </li>
                <li>
                  Derechos ARCO: Es el derecho de acceso, rectificación, cancelación y oposición al
                  tratamiento de datos personales.
                </li>
                <li>
                  Responsable: Es una persona física o moral de carácter privado que decide sobre el
                  tratamiento de datos personales.
                </li>
                <li>
                  Titular: Es la persona física a quien corresponden los datos personales, es decir,
                  el dueño de los datos.
                </li>
                <li>
                  Encargado: Es la persona física o moral que sola o conjuntamente con otras trate
                  datos personales por cuenta del responsable, como consecuencia de la existencia de
                  una relación jurídica que le vincula con el mismo y lo delimita el ámbito de su
                  actuación para la prestación de un servicio.
                </li>
                <li>
                  Tratamiento: Consiste en todo manejo que se le dé a los datos personales por
                  cualquier medio, pudiendo ser: la obtención, el uso, que abarca cualquier acción,
                  manejo, aprovechamiento, transferencia o disposición de datos personales, la
                  divulgación y el almacenamiento.
                </li>
                <li>
                  Plataforma NATOURE: Es un sitio web o aplicaciones para celulares, tablets y demás
                  dispositivos móviles, que consiste en un mercado comunitario que ayuda a personas,
                  empresas y organizaciones a crear, anunciar, descubrir y reservar experiencias de
                  viajes y aprendizaje únicos en todo el mundo.
                </li>
                <li>
                  Usuario: Es aquella persona física o moral que se registra en la Plataforma
                  NATOURE con la finalidad de contratar experiencias de viaje y de aprendizaje que
                  brindan los “ANFITRIONES”.
                </li>
                <li>
                  Viajero Natourista: Es aquella persona física o moral de cualquier parte del mundo
                  que se registra en la Plataforma NATOURE con la finalidad de contratar
                  experiencias de viaje y de aprendizaje que brindan los “ANFITRIONES”
                </li>
                <li>
                  Anfitrión: Es aquella persona física o moral de cualquier parte del mundo que se
                  registra en la Plataforma NATOURE para que sean publicadas y promocionadas las
                  “EXPERIENCIAS” que brindan y las mismas sean contratadas por los “VIAJEROS
                  NATOURISTAS”, así como ofrecer y promocionar “PRODUCTOS” artesanales
                </li>
                <li>
                  Experiencias: Son una o varias actividades que ofrece el “ANFITRIÓN”, las cuales
                  pueden ser de tipo: aventura, naturaleza, rural y cultural, cuya finalidad
                  consiste en promover y conservar el patrimonio cultural y natural de áreas de
                  conservación en el territorio en que se encuentren. Dichas “EXPERIENCIAS” pueden
                  ser contratadas por el “VIAJERO NATOURISTA”
                </li>
                <li>
                  Embajador de Naturaleza: Es una persona física, moral o autoridad competente que
                  se registra en la Plataforma NATOURE y que por sus conocimientos técnicos o
                  científicos pueden administrar un área de conservación y compartir contenido e
                  información de la misma a través de la plataforma.
                </li>
              </ul>
            </li>
            <li>
              DATOS PERSONALES QUE RECABAMOS
              <p>
                Los datos personales que recabamos de usted, los cuales podrán ser de carácter
                personales y/o sensibles, son necesarios para verificar, confirmar y validar su
                identidad; así como administrar y operar los servicios que solicita o contrata con
                nosotros. “NATOURE” recabará los siguientes datos:
              </p>
              <ul>
                <li>
                  Si usted crea un perfil como “USUARIO”, “ANFITRIÓN” o “EMBAJADOR DE NATURALEZA” en
                  nuestra plataforma, los datos personales que podremos recabar son: nombre
                  completo, domicilio, domicilio fiscal, teléfono, Registro Federal de
                  Contribuyentes (RFC), fecha de nacimiento, edad, fotografía, documentos de
                  identificación, forma de contacto preferida, correo electrónico, teléfono, firma
                  autógrafa y/o electrónica, cuentas de contacto en redes sociales, reconocimiento
                  facial y fotos. Usted puede proporcionar de forma opcional información adicional
                  como parte de su perfil de “NATOURE” (como su sexo, idioma(s) preferido(s), ciudad
                  y una descripción personal). Algunos de estos datos, según se indique en los
                  ajustes de su cuenta, forman parte de su página de perfil pública y serán visibles
                  públicamente.
                </li>
                <li>
                  Si usted ofrece una “EXPERIENCIA”, los datos personales que podremos recabar son:
                  nombre completo, domicilio, domicilio fiscal, teléfono, Registro Federal de
                  Contribuyentes (RFC), fecha de nacimiento, edad, fotografía, documentos de
                  identificación, forma de contacto preferida, correo electrónico, teléfono, firma
                  autógrafa y/o electrónica, cuentas de contacto en redes sociales, reconocimiento
                  facial, fotos, puesto de trabajo, experiencia profesional, domicilio laboral,
                  teléfono y fax del trabajo, nombre del empleador, área o departamento, ocupación,
                  correo electrónico de trabajo, móvil de trabajo, certificados, reconocimientos,
                  referencia bancaria CLABE y número de cuentas, datos de facturación.
                </li>
                <li>
                  Si usted contrata alguna “EXPERIENCIA”, los datos personales que podremos recabar
                  son: nombre completo, domicilio, domicilio fiscal, teléfono, Registro Federal de
                  Contribuyentes (RFC), fecha de nacimiento, edad, fotografía, documentos de
                  identificación, forma de contacto preferida, correo electrónico, teléfono, firmas,
                  correo electrónico, cuentas de contacto en redes sociales, reconocimiento facial y
                  fotos.
                </li>
                <li>
                  Si usted realiza un pago a través de nuestra plataforma, los datos personales que
                  podremos recabar son: nombre completo, domicilio, domicilio fiscal, teléfono,
                  Registro Federal de Contribuyentes (RFC), fecha de nacimiento, edad, fotografía,
                  documentos de identificación, forma de contacto preferida, correo electrónico,
                  teléfono, firmas, correo electrónico, cuentas de contacto en redes sociales,
                  referencia bancaria CLABE y número de cuentas, datos de facturación, número de
                  tarjeta de crédito o débito, identificadores de cuentas bancarias y/o conceptos de
                  operaciones.
                </li>
              </ul>
            </li>
            <li>
              Le informamos que, para cumplir con las finalidades previstas en este aviso de
              privacidad, podrán ser recabados datos sensibles, como aquellos que refieren a,
              nacionalidad, datos migratorios, origen étnico o racial, costumbres y tradiciones, los
              cuales serán recabados para llevar a cabo las finalidades primarias señaladas en el
              presente aviso de privacidad Así mismo, “NATOURE” conoce la importancia que tiene la
              protección y privacidad de los datos personales de los menores de edad, por lo que
              “NATOURE” no obtiene, usa, divulga o almacena información relacionada con menores de
              edad, sin el previo consentimiento de los padres y/o tutores. El único dato personal
              que “NATOURE” podrá recabar de menores de edad es: nombre completo
            </li>
            <li>
              FINALIDADES DEL TRATAMIENTO DE SUS DATOS PERSONALES
              <p>
                Sus datos personales y/o datos personales sensibles y/o financieros podrán
                utilizarse para diversas finalidades, dependiendo del caso en particular para el que
                sean proporcionados o recabados, siempre acorde con el presente Aviso de Privacidad
                o sus actualizaciones que, en su momento, se pongan a su disposición para cumplir
                con las siguientes finalidades primarias:
              </p>
              <ul>
                <li>Brindarle los servicios que nos solicitan;</li>
                <li>
                  Vincular las cuentas de usuario de la plataforma con otras cuentas de la misma
                  plataforma;
                </li>
                <li>Brindarle comunicación con otros miembros de la plataforma;</li>
                <li>Realizar el cobro de los servicios solicitados;</li>
                <li>
                  Elaborar la facturación electrónica derivada de los servicios que se ofrecen y
                  brindan;
                </li>
                <li>
                  Verificación y comprobación de su identidad en caso de pagar con tarjeta de
                  crédito y/o débito;
                </li>
                <li>Medio de contacto.</li>
              </ul>
            </li>
            <li>
              <p>
                Para las finalidades señaladas en este aviso de privacidad, podemos recabar sus
                datos personales de distintas formas: cuando usted nos los proporciona
                personalmente; automáticamente a partir del uso de la Plataforma NATOURE Asimismo,
                “NATOURE” podrá utilizar sus datos personales para las siguientes finalidades
                secundarias: (i) evaluar la calidad del servicio que le brindamos; (ii) realizar
                prospección comercial; (iii) con fines mercadotécnicos y/o publicitarios, pudiendo
                al efecto ser transferidos a prestadores de servicios publicitarios con quien se
                tenga alguna relación jurídica con el objeto de proporcionar dichos servicios; (iv)
                a operadores de servicios que tengan a su cargo las plataformas; (v) podremos
                contactarlo y hacerle llegar comunicaciones mediante el envío de mensajes SMS,
                correos electrónicos y llamadas telefónicas; (vi) realizar encuestas; (vii)
                participar en programas de lealtad; (viii) entrega de premios y otras promociones;
                (ix) registrarlo en concursos y eventos; (x) atender su solicitud de derechos ARCO
                (Acceso, Rectificación, Cancelación y Oposición)
              </p>
              <p>
                En caso de que no desee que sus datos personales se utilicen para estos fines
                secundarios, indíquelo mediante un correo electrónico dirigido a la siguiente
                dirección ayuda@natoure.info, o hágalo de nuestro conocimiento al momento en que se
                ponga a su disposición el presente aviso de privacidad. Usted podrá cambiar de
                opción en cualquier momento
              </p>
            </li>
            <li>
              TRANSFERENCIA DE DATOS PERSONALES
              <p>
                “NATOURE” transferirá los datos personales del Titular a favor de terceros para las
                siguientes finalidades:
              </p>
              <ul>
                <li>
                  Plataformas de cobro a efecto realizar el cobro de los servicios que se contraten
                  mediante la plataforma de NATOURE;
                </li>
                <li>
                  A personas físicas o morales afiliadas y/o relacionadas corporativamente con
                  “NATOURE”;
                </li>
                <li>
                  A las personas morales o cualquier entidad que formen parte del mismo grupo
                  económico de control al que pertenece “NATOURE”;
                </li>
                <li>
                  A compañías afiliadas o no afiliadas, organizaciones no gubernamentales, así como
                  autoridades competentes nacionales y/o internacionales que asisten, apoyan o
                  coadyuvan a “NATOURE”;
                </li>
                <li>
                  Asimismo, para el caso de que usted haya aceptado las finalidades secundarias del
                  presente aviso de privacidad, sus datos personales podrán ser transferidos a
                  empresas cuya actividad preponderante sea la de brindar servicios comerciales,
                  mercadotécnicos, publicitarios y/o de prospección comercial mediante diversas
                  plataformas de comunicación, siempre con apego a las disposiciones del presente
                  aviso de privacidad y la Ley.
                </li>
              </ul>
            </li>
            <li>
              <p>
                El Titular acepta y reconoce que en caso de no manifestar su oposición para que sus
                datos sean transferidos, se entenderá que ha otorgado su pleno consentimiento para
                ello “NATOURE” podrá transferir sus datos personales sin que medie su previo
                consentimiento cuando se encuentre en alguno de los supuestos del artículo 37 de la
                Ley Federal de Protección de Datos Personales en Posesión de los Particulares
              </p>
              <p>
                No obstante, lo anterior y, en caso de que se presenten vulneraciones de seguridad
                ocurridas en cualquier fase del tratamiento, que afecten de forma significativa los
                derechos patrimoniales o morales de los Titulares, éstos serán informados por correo
                electrónico de forma inmediata, a fin de que estos últimos puedan tomar las medidas
                correspondientes a la defensa de sus derechos, deslindando de cualquier
                responsabilidad a “NATOURE”, si la vulneración no es imputable a ésta
              </p>
            </li>
            <li>
              USO DE COOKIES Y WEB BEACONS
              <p>
                “NATOURE” utilizará “cookies” y “web beacons” junto con la Plataforma para obtener
                información. Asimismo, podremos permitir que nuestros socios comerciales instalen
                cookies en su dispositivo. Las “cookies” son archivos de datos que se almacenan en
                el disco duro del equipo de cómputo o de su dispositivo electrónico al navegar en un
                sitio de internet especifico, el cual permite intercambiar información de estado
                entre dicho sitio y el navegador del usuario. Las “web beacons” son una imagen
                visible u oculta insertada dentro de un sitio web o correo electrónico, que se
                utiliza para monitorear el comportamiento del usuario en estos medios “NATOURE”
                podrá obtener a través del uso que usted haga de la Plataforma por medio de los
                cookies o “web beacons” información técnica como:
              </p>
              <ul>
                <li>Su dirección de protocolo de internet “Dirección IP”;</li>
                <li>Su sistema operativo;</li>
                <li>Su tipo de navegador;</li>
                <li>Su identificación de sesión y autentificación;</li>
                <li>Preferencias del usuario;</li>
                <li>La dirección de un sitio web de referencia;</li>
                <li>La ruta que usted sigue durante su recorrido por nuestros sitios web;</li>
                <li>
                  en el caso del uso del correo electrónico la asociación de los datos anteriores
                  con el destinatario.
                </li>
              </ul>
            </li>
            <li>
              <p>
                El Titular puede configurar su navegador para que no acepte automáticamente
                “cookies” y “web beacons”. Sin embargo, debe tener en cuenta que sin las “cookies”
                es posible que determinadas partes de la Plataforma no funcionen o no lo hagan como
                está previsto
              </p>
              <p>
                “NATOURE” utiliza las “cookies” y “web beacons” para las siguientes finalidades:
              </p>
              <ul>
                <li>
                  Permitir, agilizar y racionalizar el funcionamiento de la Plataforma NATOURE
                </li>
                <li>
                  Simplificar el acceso a la Plataforma y su utilización por parte de usted para
                  hacerlos más sencillos;
                </li>
                <li>
                  Vigilar y analizar el rendimiento, el funcionamiento y la efectividad de la
                  Plataforma, de modo que podamos mejorarla y optimizarla;
                </li>
                <li>
                  Mostrarle el contenido que sea más relevante para usted (lo que podrá incluir
                  anuncios); y
                </li>
                <li>Para la detección y prevención del fraude.</li>
              </ul>
            </li>

            <li>
              <p>
                Le informamos que nuestra Política de Privacidad regulará el tratamiento que
                llevemos a cabo de los datos que obtengamos a través de las “cookies” en nuestra
                Plataforma, pero no regulará ni responderá por el uso de “cookies” de terceros
              </p>
              <p>
                Para lo anterior, “NATOURE” le informa que en todo momento pueden deshabilitar el
                uso de estos mecanismos, de acuerdo a las instrucciones que cada empresa propietaria
                de los browsers (navegador o visor de Internet) tiene implementado para activar y
                desactivar las citadas “Cookies”
              </p>
            </li>
            <li>
              ENLACES A SITIOS WEB DE TERCEROS
              <p>
                Nuestros sitios web pueden contener, para su conveniencia, enlaces a otros sitios
                web que no pertenecen a “NATOURE”. “NATOURE” desconoce acerca de las Políticas de
                Privacidad y Avisos de Privacidad de dichos sitios web, por lo que no garantiza ni
                se hace responsable por el contenido en dichos enlaces ni el tratamiento de datos
                personales que se lleven a cabo en los mismos. Lo exhortamos a que lea
                cuidadosamente la Política y Avisos de Privacidad de cada uno de los sitios que
                pudieran estar vinculados desde nuestros sitios web
              </p>
            </li>
            <li>
              EJERCICIO DE LOS DERECHOS ARCO
              <p>
                Si desea acceder al contenido de sus datos personales, rectificarlos, cancelarlos u
                oponerse y/o revocar el consentimiento que nos han otorgado, puede hacerlo de
                nuestro conocimiento mediante un correo electrónico a la dirección
                ayuda@natoure.info, a fin de que dejemos hacer uso de ellos, indicando:
              </p>
              <ul>
                <li>Nombre del titular;</li>
                <li>
                  Domicilio de titular o dirección de correo electrónico para comunicar respuesta a
                  la solicitud;
                </li>
                <li>
                  Documentos que acrediten identidad o personalidad para presentar la solicitud;
                </li>
                <li>
                  Descripción detallada de datos personales sobre los que se pretende ejercer algún
                  derecho ARCO;
                </li>
                <li>
                  Cualquier otro elemento que permita la localización de los datos personales y
                  atención a la solicitud.
                </li>
              </ul>
            </li>
            <li>
              La respuesta a su solicitud se llevará a cabo dentro de los 20 días hábiles siguientes
              contados a partir de la fecha en que la misma haya sido recibida. “NATOURE” podrá
              ampliar éste plazo hasta por 20 días hábiles más, cuando el caso lo amerite, previa
              notificación de esto a usted. La resolución adoptada por “NATOURE” será comunicada a
              usted a través del medio que nos haya proporcionado en su solicitud de ejercicio de
              derechos ARCO, y en el supuesto que “NATOURE” no cuente con sus Datos Personales, se
              lo informaremos por los medios a través de los cuáles realizó la solicitud
            </li>
            <li>
              MEDIDAS DE SEGURIDAD
              <p>
                “NATOURE” ha implementado y mantiene las medidas de seguridad, técnicas,
                administrativas y físicas, necesarias para proteger sus datos personales y evitar el
                daño, pérdida, alteración, destrucción o el uso, acceso o tratamiento no autorizado
              </p>
            </li>
            <li>
              ACTUALIZACIÓN Y/O MODIFICACIÓN AL AVISO DE PRIVACIDAD
              <p>
                “NATOURE” se reserva el derecho de modificar o actualizar el presente Aviso de
                Privacidad o el que haya puesto a su disposición en cualquier momento, según sea
                necesario o conveniente, por ejemplo, para cumplir con cambios a la legislación o
                cumplir con políticas internas o nuevos requerimientos para la prestación u
                ofrecimiento de nuestros servicios. “NATOURE” pondrá a su disposición el Aviso de
                Privacidad actualizado dentro de este sitio web sección aviso de privacidad e
                indicará la fecha de última actualización del presente aviso
              </p>
            </li>
            <li>
              MEDIO DE CONTACTO
              <p>
                Para cualquier duda o aclaración respecto al Aviso de Privacidad favor de
                comunicarse por medio de correo electrónico a la siguiente dirección:
                ayuda@natoure.info, y con gusto atenderemos su solicitud
              </p>
            </li>
          </ol>
        </Advices>
      </Container>
    </MarginY>
  )
}

aviso.Layout = TravelerLayout

export default aviso
