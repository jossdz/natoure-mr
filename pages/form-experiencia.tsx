import React from 'react'
import { useRouter } from 'next/router'

import { Step1, Step2, Step3, Step4, Step5, Step6, Step7 } from '../steppers/experiencia'

import { ButtonText, Heading, Modal, SectionHeading } from '../styles/forms'

import { Container, Loading } from '../styles/components'

import { postExperience } from '../pages/api/api'

import useStepper from '../hooks/useStepper'

const FormExperiencia = (): JSX.Element => {
  const { push } = useRouter()
  const { currentStep, status, onStepChange, onSubmit, onBackStep } = useStepper(
    postExperience,
    false,
    true
  )

  const steps = {
    1: {
      render: Step1,
      name: 'Tu experiencia',
      heading: 'Comienza a crear tu experiencia',
    },
    2: {
      render: Step2,
      name: 'Detalles de la experiencia',
      heading: 'Define los detalles de tu experiencia',
    },
    3: {
      render: Step3,
      name: 'Detalles complementarios',
      heading: 'Define los detalles de tu experiencia',
    },
    4: {
      render: Step4,
      name: 'Ubicación',
      heading: 'Registra dónde se ubica la experiencia',
    },
    5: {
      render: Step5,
      name: 'Calendario',
      heading: 'Registra las fechas para tu experiencia',
    },
    6: {
      render: Step6,
      name: 'Fotos y videos',
      heading: 'Sube fotos y videos de la experiencia',
    },
    7: {
      render: Step7,
      name: 'Confirmación',
      heading: 'Confirma tus datos',
    },
  }

  const Component = steps[currentStep].render

  return (
    <React.Fragment>
      <Heading
        title="Registra tu experiencia"
        stepName={steps[currentStep].name}
        step={currentStep}
        total={7}
        onBackStep={onBackStep}
      />
      <Container>
        <SectionHeading title={steps[currentStep].heading} />
        {currentStep === 7 ? (
          <Component onStepChange={onStepChange} onSubmit={onSubmit} />
        ) : (
          <Component onStepChange={onStepChange} />
        )}
        <ButtonText
          style={{ display: currentStep === 1 && 'none', marginBottom: 96 }}
          onClick={onBackStep}
        />
      </Container>
      {status === 'fetching' && <Loading />}
      {status === 'finished' && (
        <Modal
          name="¡Listo! Tu experiencia ha sido registrada"
          description="La información de tu experiencia será publicada."
          onClick={() => push('/misproductos')}
        />
      )}
      {status === 'error' && (
        <Modal
          name="¡Lo sentimos! Ha ocurrido un error"
          description="No fue posible publicar tu experiencia."
          onClick={() => push('/misproductos')}
          error
        />
      )}
    </React.Fragment>
  )
}

export default FormExperiencia
