import React, { Component } from 'react'
import { PayPalScriptProvider } from '@paypal/react-paypal-js'
import Head from 'next/head'
import GlobalStyles from '../styles/global-styles'
import DefaultLayout from '../Layouts/Default'
import AppContextWrapper from '../context/AppContext'

interface IProps {
  Component: React.FunctionComponent & { Layout?: typeof Component }
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  pageProps: any
}

// eslint-disable-next-line prettier/prettier
export default function App({ Component, pageProps }: IProps): JSX.Element {
  const Layout = Component.Layout || DefaultLayout

  return (
    <AppContextWrapper>
      <PayPalScriptProvider
        options={{ 'client-id': process.env.NEXT_PUBLIC_PAYPAL_CLIENT_ID, currency: 'MXN' }}
      >
        <Head>
          <title>Natoure</title>
          <link href="/icons/icon-192x192.png" rel="icon" type="image/png" sizes="192x192" />
          <link rel="apple-touch-icon" href="/apple-icon.png"></link>
          <script src="https://unpkg.com/react-phone-number-input@3.x/bundle/react-phone-number-input.js"></script>
          <script src="https://unpkg.com/react-phone-number-input@3.x/bundle/react-phone-number-input-max.js"></script>
          <script src="https://unpkg.com/react-phone-number-input@3.x/bundle/react-phone-number-input-mobile.js"></script>
          <script type="text/javascript" src="https://js.openpay.mx/openpay.v1.min.js"></script>
          <script
            type="text/javascript"
            src="https://resources.openpay.mx/lib/openpay-data-js/1.2.38/openpay-data.v1.min.js"
          ></script>
        </Head>
        <Layout>
          <GlobalStyles />
          <Component {...pageProps} />
        </Layout>
      </PayPalScriptProvider>
    </AppContextWrapper>
  )
}
