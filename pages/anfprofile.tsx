/* eslint-disable jsx-a11y/media-has-caption */
import React from 'react'
import { SectionHeading } from '../styles/components'
import { MarginY } from '../styles/general'
import Resumen from '../styles/anfitrion/Resumen'
import Discover from '../styles/restaurants/discover'
import Gallery from '../styles/restaurants/Gallery'
import Messages from '../styles/anfitrion/messages'
import HostLayout from '../Layouts/Host'
import Container from '../styles/components/Container'

const anfprofile = (): JSX.Element => {
  return (
    <MarginY>
      <Container>
        <Resumen
          mes="Mayo-Junio"
          ganancia="$18.000 m.n"
          reservaciones={7}
          ranking={13}
          sostenibilidad="4"
          servicio="4.5"
          title=""
          description=""
        />
        <SectionHeading title="Tus últimos mensajes" />
        <Messages
          title=""
          description=""
          mes=""
          ganancia=""
          reservaciones={0}
          ranking={0}
          sostenibilidad=""
          servicio=""
        />
        <SectionHeading title="Descubre lo que hay para tí" />
        <Discover title="" description="" poblacion="" />
        <SectionHeading title="Mejora tus productos con estos tips" />
        <Gallery title="" title2="" description="" poblacion="" />
      </Container>
    </MarginY>
  )
}

anfprofile.Layout = HostLayout

export default anfprofile
