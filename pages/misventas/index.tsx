import React, { useState, useEffect } from 'react'
import HostLayout from '../../Layouts/Host'
import { Reservation } from '../../components/sales'
import Jumbo from '../../styles/explore/Jumbo'
import HorizontalS from '../../styles/general/HorizontalSlide'
import Container from '../../styles/components/Container'
import { MarginY } from '../../styles/general'
import { getReservationsByOrg } from '../api/organization'
import Loading from '../../styles/components/Loading'

const tabs = ['Pendientes', 'Confirmadas', 'Finalizadas', 'Canceladas']

const Misventas = (): JSX.Element => {
  const [activeTab, setActiveTab] = useState('Pendientes')
  const [reservations, setReservations] = useState([])
  const [status, setStatus] = useState<'' | 'fetching' | 'finished'>('')
  const [data, setData] = useState([])

  const dateInPast = (pastDate) => (pastDate < new Date() ? true : false)

  useEffect(() => {
    setStatus('fetching')
    const organizationid = localStorage.getItem('organizationid')
    if (organizationid) {
      getReservationsByOrg(organizationid)
        .then(({ reservations }) => {
          setStatus('finished')
          setReservations(reservations)
        })
        .catch((err) => {
          setStatus('finished')
          console.error(err)
        })
    }
  }, [])

  useEffect(() => {
    const filtered = reservations.filter((item) => {
      if (dateInPast(new Date(item.checkin)) && item.status !== 'Cancelada') {
        item.status = 'Finalizada'
      }
      return activeTab.match(item.status)
    })
    setData(filtered)
  }, [activeTab, reservations])

  return (
    <MarginY>
      <Container>
        <Jumbo>
          <HorizontalS>
            {tabs.map((item, index) => (
              <button key={index} onClick={() => setActiveTab(item)}>
                <span className={activeTab === item ? 'here' : ''}>{item}</span>
              </button>
            ))}
          </HorizontalS>
        </Jumbo>
        {data.length > 0 &&
          status === 'finished' &&
          data.map((item, index) => <Reservation key={index} {...item} />)}
        {status === 'finished' && data.length === 0 && (
          <h2>Aún no tienes reservaciones para mostrar</h2>
        )}
        {status === 'fetching' && <Loading />}
      </Container>
    </MarginY>
  )
}

Misventas.Layout = HostLayout

export default Misventas
