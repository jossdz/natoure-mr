import React, { useEffect, useState } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'

import { MarginY } from '../../styles/general'
import Container from '../../styles/components/Container'
import TravelerLayout from '../../Layouts/Traveler'
import { getOneById } from '../api/naturalAreas'
import Detail from '../../components/Destinos/Detail'

const initialNaturalArea = {}

const naturalAreasPage = (): JSX.Element => {
  const router = useRouter()
  const { id: naturalAreaID } = router.query
  const [naturalArea, setNaturalArea] = useState(initialNaturalArea)

  useEffect(() => {
    if (!naturalAreaID) return
    getOneById({ id: naturalAreaID }).then((data) => {
      setNaturalArea(data.naturalArea)
    })
  }, [naturalAreaID])

  return (
    <MarginY>
      <Container>
        <Detail data={naturalArea} />
      </Container>
    </MarginY>
  )
}

naturalAreasPage.Layout = TravelerLayout

export default naturalAreasPage
