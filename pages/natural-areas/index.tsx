import React, { useEffect, useState } from 'react'
import { AiOutlineEnvironment, AiFillAppstore } from 'react-icons/ai'
import Link from 'next/link'
import { useRouter } from 'next/router'

import Jumbo from '../../styles/explore/Jumbo'
import HorizontalS from '../../styles/general/HorizontalSlide'
import { MarginY } from '../../styles/general'
import FilterBar from '../../styles/components/FilterBar'
import Container from '../../styles/components/Container'
import MapButton from '../../styles/components/MapButton'
import { Map } from '../../components/MapPage'
import TravelerLayout from '../../Layouts/Traveler'
import { getAll } from '../api/naturalAreas'
import AreasNaturales from '../../components/Destinos/AreasNaturales'
import TagCard from '../../styles/experience/tags'
import { macros } from '../../steppers/data'

const categories = [
  {
    name: 'Áreas Naturales',
    route: 'natural-areas',
  },
  {
    name: 'Destinos Sostenibles',
    route: 'sustainable-destinations',
  },
]

const naturalAreasPage = (): JSX.Element => {
  const router = useRouter()
  const currentRoute = router.pathname
  const [naturalAreas, setNaturalAreas] = useState([])
  const [search, setSearch] = useState('')
  const [macro, setMacro] = useState('')
  const [toggleMap, setToggleMap] = useState(false)

  useEffect(() => {
    getAll({ search, macros: macro })
      .then((data) => {
        setNaturalAreas(data.naturalAreas)
      })
      .catch((err) => console.error(err))
  }, [search, macro])

  return (
    <MarginY>
      <Container>
        <Jumbo>
          <FilterBar placeholder="Busca por ubicación, nombre…" />
          <MapButton toggleMap={toggleMap} onClick={() => setToggleMap(!toggleMap)}>
            {toggleMap ? <AiFillAppstore /> : <AiOutlineEnvironment />}
          </MapButton>
          <HorizontalS>
            {categories.map((item, index) => (
              <button
                style={{ cursor: 'pointer' }}
                key={index}
                className={currentRoute === `/${item.route}` ? 'here' : ''}
                onClick={() => router.push(item.route)}
              >
                {item.name}
              </button>
            ))}
          </HorizontalS>
        </Jumbo>
        {!toggleMap && (
          <>
            <TagCard
              margin="24px 0 64px 0"
              width="100%"
              tag={Object.keys(macros)}
              activeTag={macro}
              onClick={(item) => setMacro(item)}
            />
            <AreasNaturales data={naturalAreas} />
          </>
        )}
      </Container>
      {toggleMap && <Map isToggled={true} />}
    </MarginY>
  )
}

naturalAreasPage.Layout = TravelerLayout

export default naturalAreasPage
