import React from 'react'
import { useRouter } from 'next/router'

import {
  Button,
  Chips,
  Heading,
  Input,
  Label,
  Modal,
  SectionHeading,
  SectionText,
  Upload,
} from '../styles/forms'

import { restaurantTags } from '../steppers/data'

import { Container, Loading } from '../styles/components'

import { postRestaurant } from './api/api'

import useForm from '../hooks/useForm'

const FormRestaurante = (): JSX.Element => {
  const { push } = useRouter()
  const { validForm, status, handleChange, handleClick } = useForm(postRestaurant, false)

  return (
    <React.Fragment>
      <Heading title="Registra tu restaurante" step={1} simple />
      <Container>
        <SectionHeading title="Comparte algunos datos sobre tu restaurante" />
        <SectionText
          style={{ marginBottom: 64 }}
          title="Rellena los siguientes campos con información acerca de tu restaurante, así los viajeros podrán conocerlo mejor."
        />
        <Label title="Nombre de tu restaurante" />
        <Input name="restaurantName" placeholder="Ingresa el nombre" onChange={handleChange} />
        <Label title="Elige etiquetas que mejor se relacionen con tu restaurante" optional />
        <Chips name="restaurantTags" options={restaurantTags} onChange={handleChange} optional />
        <Label title="Sube algunas fotos de tu restaurante" />
        <Upload name="restaurantCoverPhotos" onChange={handleChange} />
        <Label title="Sube más fotos para mostrar en tu galería" />
        <Upload name="restaurantGalleryPhotos" onChange={handleChange} />
        <Button unable={!validForm} onClick={handleClick} text="Guardar y continuar" />
      </Container>
      {status === 'fetching' && <Loading />}
      {status === 'finished' && (
        <Modal
          name="¡Listo! Tu restaurante ha sido registrado"
          description="La información de tu restaurante será publicada."
          onClick={() => push('/misproductos')}
        />
      )}
      {status === 'error' && (
        <Modal
          name="¡Lo sentimos! Ha ocurrido un error"
          description="No fue posible publicar tu restaurante."
          onClick={() => push('/misproductos')}
          error
        />
      )}
    </React.Fragment>
  )
}

export default FormRestaurante
