import React from 'react'
import { useRouter } from 'next/router'

import {
  Button,
  Chips,
  Heading,
  Input,
  Label,
  Modal,
  Upload,
  SectionText,
  SectionHeading,
} from '../styles/forms'

import { articleTags, unityTags } from '../steppers/data'

import { Container, Loading } from '../styles/components'

import { editProduct, getProduct } from './api/api'

import useForm from '../hooks/useForm'

const EditProducto = (): JSX.Element => {
  const { push } = useRouter()
  const { validForm, status, item, handleChange, handleClick } = useForm(
    editProduct,
    true,
    'articleID',
    getProduct
  )

  const data = item && item.article

  return (
    <React.Fragment>
      <Heading title="Edita tu artículo" step={1} simple />
      <Container>
        <SectionHeading title="Comparte algunos datos sobre tu artículo" />
        <SectionText
          style={{ marginBottom: 64 }}
          title="Rellena los siguientes campos con información acerca de tu artículo, así los viajeros podrán conocerlo mejor."
        />
        <Label title="Sube tu imagen de portada" />
        <Upload
          name="articuloCoverPhoto"
          onChange={handleChange}
          defaultValue={data && data.articuloCoverPhoto}
        />
        <Label title="Sube más fotos para mostrar en tu galería" />
        <Upload
          name="articuloPhotosGalery"
          onChange={handleChange}
          defaultValue={data && data.articuloPhotosGalery}
        />
        <Label title="Titulo del artículo" />
        <Input
          name="articuloName"
          placeholder="Ingresa el nombre"
          onChange={handleChange}
          defaultValue={data && data.articuloName}
        />
        <Label title="Elige etiquetas que mejor se relacionen con tu artículo" optional />
        <Chips
          name="articuloTags"
          options={articleTags}
          onChange={handleChange}
          optional
          defaultValue={data && data.articuloTags}
        />
        <Label title="Stock disponible" />
        <Input
          name="stock"
          placeholder="0"
          onChange={handleChange}
          defaultValue={data && data.stock.toString()}
        />
        <Label title="Unidad" />
        <Chips
          name="unity"
          options={unityTags}
          onChange={handleChange}
          defaultValue={data && data.unity}
        />
        <Button unable={!validForm} onClick={handleClick} text="Guardar y publicar" />
      </Container>
      {status === 'fetching' && <Loading />}
      {status === 'finished' && (
        <Modal
          name="¡Listo! Tu producto ha sido actualizado"
          description="La información de tu producto será publicada."
          onClick={() => push('/misproductos')}
        />
      )}
      {status === 'error' && (
        <Modal
          name="¡Lo sentimos! Ha ocurrido un error"
          description="No fue posible editar tu producto."
          onClick={() => push('/misproductos')}
          error
        />
      )}
    </React.Fragment>
  )
}

export default EditProducto
