import React from 'react'
import { useRouter } from 'next/router'

import {
  Button,
  Chips,
  Heading,
  Input,
  Label,
  Modal,
  Upload,
  SectionText,
  SectionHeading,
} from '../styles/forms'

import { restaurantTags } from '../steppers/data'

import { Container, Loading } from '../styles/components'

import { postPlate } from './api/api'

import useForm from '../hooks/useForm'

const FormPlatillo = (): JSX.Element => {
  const { push } = useRouter()
  const { validForm, status, handleChange, handleClick } = useForm(postPlate, false)

  return (
    <React.Fragment>
      <Heading title="Registra tu platillo / especialidad" step={1} simple />
      <Container>
        <SectionHeading title="Comparte algunos datos sobre tu platillo" />
        <SectionText
          style={{ marginBottom: 64 }}
          title="Rellena los siguientes campos con información acerca de tu platillo, así los viajeros podrán conocerlo mejor."
        />
        <Label title="Sube tu imagen de portada" />
        <Upload name="platilloCoverPhoto" onChange={handleChange} />
        <Label title="Sube más fotos para mostrar en tu galeria" />
        <Upload name="platillosPhotosGaleri" onChange={handleChange} />
        <Label title="Titulo del platillo" />
        <Input name="platilloName" placeholder="Ingresa el nombre" onChange={handleChange} />
        <Label title="Elige etiquetas que mejor se relacionen con tu platillo" optional />
        <Chips name="restaurantTags" options={restaurantTags} onChange={handleChange} optional />
        <Button unable={!validForm} onClick={handleClick} text="Guardar y publicar" />
      </Container>
      {status === 'fetching' && <Loading />}
      {status === 'finished' && (
        <Modal
          name="¡Listo! Tu platillo ha sido registrado"
          description="La información de tu platillo será publicada."
          onClick={() => push('/misproductos')}
        />
      )}
      {status === 'error' && (
        <Modal
          name="¡Lo sentimos! Ha ocurrido un error"
          description="No fue posible publicar tu platillo."
          onClick={() => push('/misproductos')}
          error
        />
      )}
    </React.Fragment>
  )
}

export default FormPlatillo
