/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { useEffect, useState } from 'react'

const useStepper = (endpoint, edit, experience = false) => {
  const [data, setData] = useState({})
  const [currentStep, setCurrentStep] = useState(1)
  const [body, setBody] = useState(null)
  const [status, setStatus] = useState('')

  const onStepChange = (stepData, step) => {
    setData((prevState) => {
      return {
        ...prevState,
        ...stepData,
      }
    })
    setCurrentStep(step)
  }

  const onSubmit = async () => {
    setStatus('fetching')
    const orgId = localStorage.getItem('organizationid')
    const { _id, ...cleanedData } = data
    const action = edit
      ? endpoint(cleanedData, _id, orgId)
      : experience
      ? endpoint({ ...data, orgId })
      : endpoint(data, orgId)
    action
      .then(() => {
        setStatus('finished')
      })
      .catch(() => {
        setStatus('error')
      })
  }

  const onBackStep = () => {
    setCurrentStep(currentStep - 1)
  }

  useEffect(() => {
    if (typeof window !== 'undefined') {
      window.scroll({ top: 0 })
    }
  }, [currentStep])

  useEffect(() => {
    if (typeof window !== 'undefined') setBody(document.getElementsByTagName('body')[0])
  }, [])

  useEffect(() => {
    if (body !== null) body.style.overflow = status === 'finished' ? 'hidden' : 'auto'
  }, [body, status])

  return {
    data,
    setData,
    status,
    setStatus,
    currentStep,
    onStepChange,
    onSubmit,
    onBackStep,
  }
}

export default useStepper
