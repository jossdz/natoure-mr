/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { useEffect, useState } from 'react'

const useForm = (endpoint, edit, id, endpointEdit) => {
  const [body, setBody] = useState(null)
  const [form, setForm] = useState({})
  const [item, setItem] = useState(null)
  const [validForm, setValidForm] = useState(false)
  const [status, setStatus] = useState('')
  const [type, setType] = useState('default')

  const handleChange = (e) => {
    if (e.name === 'hotelType') setType(e.value)
    setForm((prevState) => {
      return {
        ...prevState,
        [e.name]: e,
      }
    })
  }

  const handleClick = () => {
    const orgId = localStorage.getItem('organizationid')
    const itemId = localStorage.getItem(id)
    window.scroll({ top: 0 })
    setStatus('fetching')
    const data = Object.keys(form).reduce((acc, key) => {
      if (form[key].valid) {
        return { ...acc, [key]: form[key].value }
      } else {
        return { ...acc }
      }
    }, {})
    const action = edit ? endpoint({ data }, itemId, orgId) : endpoint({ ...data, orgId })
    action
      .then(() => {
        setStatus('finished')
      })
      .catch(() => {
        setStatus('error')
      })
  }

  useEffect(() => {
    const requiredFields = Object.values(form).filter((item) => !item.optional)
    const isValid = requiredFields.every((e) => e.valid)
    setValidForm(isValid)
  }, [form])

  useEffect(() => {
    const orgId = localStorage.getItem('organizationid')
    const itemId = localStorage.getItem(id)
    if (typeof window !== 'undefined') setBody(document.getElementsByTagName('body')[0])
    if (edit) {
      orgId
        ? endpointEdit(itemId)
            .then((response) => {
              setItem(response)
            })
            .catch(() => {
              setStatus('error')
            })
        : setStatus('error')
    }
  }, [])

  useEffect(() => {
    if (body !== null) body.style.overflow = status === 'finished' ? 'hidden' : 'auto'
  }, [body, status])

  return { validForm, status, type, item, handleChange, handleClick }
}

export default useForm
