import React, { useEffect, useState } from 'react'
import { ProfileBar } from '../../styles/explore'
import NavBar from '../../components/layout/NavBar'
import Footer from '../../components/layout/Footer'
import ProfileDrawer from '../../styles/components/ProfileDrawer'
import { LayoutContainerStyled } from '../../styles/Layouts'
import { OnlyWeb } from '../../styles/general'

import { useRouter } from 'next/router'

const getTitle = (pathname) => {
  switch (pathname) {
    case '/anfitriones':
      return 'Anfitriones'
    case '/explora':
      return 'Explora'
    case '/misviajes':
      return 'Mis viajes'
    case '/profile':
      return 'Perfil'
    case '/inicio':
      return 'Cambia tu forma de viajar, sé el cambio'
    case '/terminos-y-condiciones':
      return 'Términos y condiciones'
    case '/aviso-de-privacidad':
      return 'Aviso de privacidad'
  }
}

const TravelerLayout: React.FC = ({ children }) => {
  const { pathname } = useRouter()
  const pageTitle = getTitle(pathname)
  const mainpage = pathname === '/inicio'
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [userInfo, setUserInfo] = useState<any>()

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem('NatoureUser'))
    setUserInfo(user)
  }, [])

  return (
    <React.Fragment>
      <NavBar
        name={`${userInfo?.name} ${userInfo?.last_name}`}
        anfitrion={userInfo?.anfitrion}
        photo="/assets/icon-blue.png"
      />
      <LayoutContainerStyled>
        <ProfileBar mainpage={mainpage}>
          <div>
            <img src={mainpage ? '/assets/icon-white.png' : '/assets/icon-blue.png'} alt="logo" />
            <p>{pageTitle}</p>
          </div>
          <ProfileDrawer
            name={`${userInfo?.name} ${userInfo?.last_name}`}
            photo="/assets/icon-blue.png"
            anfitrion={userInfo?.anfitrion}
          />
        </ProfileBar>
        {children}
      </LayoutContainerStyled>
      <OnlyWeb>
        <Footer />
      </OnlyWeb>
    </React.Fragment>
  )
}

export default TravelerLayout
