import React, { useState } from 'react'
import SideNavbar from '../../components/common/SideNavbar'
import AdminContentSection from '../../components/layout/AdminContentSection'

const AdminLayout: React.FC = ({ children }) => {
  const [isOpen, setIsOpen] = useState(false)
  const handleExpand = () => setIsOpen((prev) => !prev)
  return (
    <React.Fragment>
      <SideNavbar isOpen={isOpen} handleExpand={handleExpand} />
      <AdminContentSection isOpen={isOpen}>{children}</AdminContentSection>
    </React.Fragment>
  )
}

export default AdminLayout
