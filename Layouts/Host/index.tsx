import React, { useEffect } from 'react'
import NavBar from '../../components/layout/NavBar'
import Footer from '../../components/layout/Footer'
import ProfileDrawer from '../../styles/components/ProfileDrawer'
import { ProfileBar } from '../../styles/explore'
import { getOrganization } from '../../pages/api/api'
import { Organization } from '../../pages/miorganizacion'
import { LayoutContainerStyled } from '../../styles/Layouts'
import { useRouter } from 'next/router'
import { OnlyWeb } from '../../styles/general'

const getTitle = (pathname) => {
  switch (pathname) {
    case '/misventas':
      return 'Mis ventas'
    case '/mispagos':
      return 'Mis pagos'
    case '/misproductos':
      return 'Mis productos'
    case '/miorganizacion':
      return 'Mi organización'
    case '/anfprofile':
      return 'Mi perfil'
  }
}

const HostLayout: React.FC = ({ children }): JSX.Element => {
  const [myOrganization, setMyOrganization] = React.useState<Organization>({})
  const { pathname } = useRouter()
  const pageTitle = getTitle(pathname)

  useEffect(() => {
    const userID = localStorage.getItem('ID')
    getOrganization(userID)
      .then((organization) => {
        //localStorage.setItem('organizationid', organization._id)
        setMyOrganization(organization)
      })
      .catch((err) => err)
  }, [])

  return (
    <React.Fragment>
      <NavBar
        anfitrion
        name={myOrganization.organizationName}
        photo={'mainPhoto' in myOrganization && myOrganization.mainPhoto[0]}
      />
      <LayoutContainerStyled>
        <ProfileBar>
          <div>
            <img src="/assets/icon-blue.png" alt="logo" />
            <p>{pageTitle}</p>
          </div>
          <ProfileDrawer
            anfitrion
            name={myOrganization.organizationName}
            photo={'mainPhoto' in myOrganization && myOrganization.mainPhoto[0]}
          />
        </ProfileBar>
        {children}
      </LayoutContainerStyled>
      <OnlyWeb>
        <Footer />
      </OnlyWeb>
    </React.Fragment>
  )
}

export default HostLayout
