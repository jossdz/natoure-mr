import React, { ReactNode } from 'react'

/**
 *
 * At the time this file was created there was no default layout defined, update this file in case this has changed.
 *
 **/

type DefaultLayoutProps = { children: ReactNode }

const DefaultLayout = ({ children }: DefaultLayoutProps): JSX.Element => {
  return <React.Fragment>{children}</React.Fragment>
}

export default DefaultLayout
