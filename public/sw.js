if (!self.define) {
  const e = (e) => {
      'require' !== e && (e += '.js')
      let c = Promise.resolve()
      return (
        a[e] ||
          (c = new Promise(async (c) => {
            if ('document' in self) {
              const a = document.createElement('script')
              ;(a.src = e), document.head.appendChild(a), (a.onload = c)
            } else importScripts(e), c()
          })),
        c.then(() => {
          if (!a[e]) throw new Error(`Module ${e} didn’t register its module`)
          return a[e]
        })
      )
    },
    c = (c, a) => {
      Promise.all(c.map(e)).then((e) => a(1 === e.length ? e[0] : e))
    },
    a = { require: Promise.resolve(c) }
  self.define = (c, s, n) => {
    a[c] ||
      (a[c] = Promise.resolve().then(() => {
        let a = {}
        const i = { uri: location.origin + c.slice(1) }
        return Promise.all(
          s.map((c) => {
            switch (c) {
              case 'exports':
                return a
              case 'module':
                return i
              default:
                return e(c)
            }
          })
        ).then((e) => {
          const c = n(...e)
          return a.default || (a.default = c), a
        })
      }))
  }
}
define('./sw.js', ['./workbox-c2b5e142'], function (e) {
  'use strict'
  importScripts(),
    e.skipWaiting(),
    e.clientsClaim(),
    e.precacheAndRoute(
      [
        {
          url: '/_next/static/GtivHNh5mocDHTSWDKyye/_buildManifest.js',
          revision: '9e78fbd2ee6d8e15c7eed6320dc82ef7',
        },
        {
          url: '/_next/static/GtivHNh5mocDHTSWDKyye/_ssgManifest.js',
          revision: 'abee47769bf307639ace4945f9cfd4ff',
        },
        {
          url: '/_next/static/chunks/0c428ae2.a41cd219111550998b2d.js',
          revision: 'c9b63385f3b6225296c0109822776c31',
        },
        {
          url: '/_next/static/chunks/1a48c3c1.91487c9d3ebd31ea7d4b.js',
          revision: 'b1dce0c6b9ca0fc93e99b009f5515966',
        },
        {
          url: '/_next/static/chunks/1bfc9850.f35737368290ae5eb4cc.js',
          revision: '7e0ffaa457ee7701ce8a3b8e46cedfd8',
        },
        {
          url: '/_next/static/chunks/20029f67.1b0e3e3df37e5834c657.js',
          revision: '38982cecf97131ae76439aa8e1e34c74',
        },
        {
          url: '/_next/static/chunks/252f366e.56a6aa30861ec9717f11.js',
          revision: 'd421ce7f15db1e1ff4214aba6142e6d8',
        },
        {
          url: '/_next/static/chunks/2c796e83.4f0595fd3d5b97b1ceb6.js',
          revision: '50479be81a1e743f18cb042b1212398e',
        },
        {
          url:
            '/_next/static/chunks/33f88775012d9f0f44eff7b968d8f8d14c08879d.6cf313c6bcda6a71a083.js',
          revision: '9ff0129f57fa20b4bacf2d5869cabd3c',
        },
        {
          url: '/_next/static/chunks/3d4c4fcf.961f6f113e189903bbd7.js',
          revision: '9e051af06ec6e2bd79642dadea5b2eac',
        },
        {
          url:
            '/_next/static/chunks/41dd71df43638009ec2b58a7045cb00c34f7c21a.85de8d76e106c8a7789a.js',
          revision: '954741ee4ebff0252d576d60f431f3d8',
        },
        {
          url:
            '/_next/static/chunks/478d00323462734010ebb096567c71f3b617caad.24252c32d4c8fd21765e.js',
          revision: 'd9941fa8f3206df059332a10c5c0fb87',
        },
        {
          url:
            '/_next/static/chunks/4aa11c1960d18e4dc395991ee09b7e55708fcc58.e274b5867947321bb5e6.js',
          revision: '2953ca47fb004cfa8645dfa96a628c50',
        },
        {
          url:
            '/_next/static/chunks/5b087ea951588d017f565111e3e5f3bd0bd82932.70830a03fe47a873135e.js',
          revision: 'b3b0547504f28a1ae6b79aeebef46ed7',
        },
        {
          url:
            '/_next/static/chunks/5bf780ef43e3821316558704efa830aa302395aa.ab585d7e21bff393e529.js',
          revision: 'a19202ebadd85e0c893f0e2ff2e8f9e3',
        },
        {
          url:
            '/_next/static/chunks/62b53ab1e6a51fd3fae5ac4d0a9ad9e2624f011c.aff2bdae1434e8b8cca0.js',
          revision: '9a04ee641cb2c9e0f603bb9840986c3b',
        },
        {
          url:
            '/_next/static/chunks/6ad20935b0e9a828eec9576675532189ed86dda7.947bd994a23ce450f8be.js',
          revision: '14be2b6b3698a48127b6c175f8313318',
        },
        {
          url:
            '/_next/static/chunks/7473f2b225cfd202ad5ed048b577b1e91ac5071a.22c1a203ed79df0809a0.js',
          revision: '154e90adf1623f5463d4980a39852deb',
        },
        {
          url: '/_next/static/chunks/78e521c3.861e7fe46474198c1bb1.js',
          revision: '75b94d9c453a9f5b8e2d03a4a7e04a02',
        },
        {
          url:
            '/_next/static/chunks/976fc723e56f210bb1c0faf010e8ec1177f01a25.f85d475a6dbf33e176f7.js',
          revision: 'f9858c08441ddc89c0aa231636d71f62',
        },
        {
          url: '/_next/static/chunks/ae51ba48.78c84ef6e9f6f55abb7b.js',
          revision: '922c90c0612f92822896ccca8a37fcd0',
        },
        {
          url:
            '/_next/static/chunks/bc25e990ad9aafe3654710194cfb0d41d0be0e68.c8699127cf5ac6d488f9.js',
          revision: '28830e1980ca7cfd4871757194365a30',
        },
        {
          url: '/_next/static/chunks/d64684d8.5061107879c48a5b5cd0.js',
          revision: '7364217c20c0bf20eff717865ecf028e',
        },
        {
          url: '/_next/static/chunks/d7eeaac4.d0b234ef9cebe1a9fc20.js',
          revision: 'd514213b5cbddb4b974ff7e02753ac0a',
        },
        {
          url: '/_next/static/chunks/de71a805.db6608e19b3c457b0b2c.js',
          revision: '9c87549b95abc1bdac42ef65f5a4bf6b',
        },
        {
          url:
            '/_next/static/chunks/e6b336ce813c188d0cb78ea12897a06edf1dcd19.02cde346229ccfaa1586.js',
          revision: '018c80710af907366a44969a3f8257cf',
        },
        {
          url:
            '/_next/static/chunks/f3be7840d9fbaf4b9496c4301781ae474c8261be.5d8d690818990149a5f3.js',
          revision: 'ec7f69e02065f23a74ea39ec61e5048e',
        },
        {
          url: '/_next/static/chunks/framework.18a81529ca607127d8ce.js',
          revision: 'eb43b99838b43eb618bfe5a78ab7985a',
        },
        {
          url: '/_next/static/chunks/main-27b20065d01d85cbef01.js',
          revision: '12eb3f26e1db532b075d491b0effac22',
        },
        {
          url: '/_next/static/chunks/pages/_app-2afacdadeedbd985a913.js',
          revision: 'c856c221f141ba65e8966a840724a73f',
        },
        {
          url: '/_next/static/chunks/pages/_error-7ebcd95c78f88db13856.js',
          revision: '2d32555ca01752a7fba226141806fde5',
        },
        {
          url: '/_next/static/chunks/pages/anfitrion-6653c6b1797945d6d354.js',
          revision: 'd901c89ef35615f8c5748562d2a06deb',
        },
        {
          url: '/_next/static/chunks/pages/anfprofile-38ca0a9b1f22feb6cf1d.js',
          revision: 'a1e3a2799cc0ef61c4f7e40b10c1aee3',
        },
        {
          url: '/_next/static/chunks/pages/certificados-55750dba6228e3228087.js',
          revision: '6be645f8f49086768600d3be76358a33',
        },
        {
          url: '/_next/static/chunks/pages/destinos-5d58d03626f4178c6124.js',
          revision: 'a1f703d6d6dbdfcea5d1c54f01f046dd',
        },
        {
          url: '/_next/static/chunks/pages/experience-aab4e4879439125d81ed.js',
          revision: '471f3dfe22e163157daa7ac65f3e893d',
        },
        {
          url: '/_next/static/chunks/pages/explora-1b4f84054d3984ed1bd9.js',
          revision: 'bca7a2851e765bb061c24699ff603de8',
        },
        {
          url: '/_next/static/chunks/pages/form-experiencia-ae65d4b3cad6ea255564.js',
          revision: '2b6dc9ca06164bdb272b994056c0c68b',
        },
        {
          url: '/_next/static/chunks/pages/form-hotel-e069128f9ae6f2dc0dfa.js',
          revision: '1d8007aeac14dd43226635754e358257',
        },
        {
          url: '/_next/static/chunks/pages/form-organizacion-e09fd5900e8d3fec9f97.js',
          revision: '86dfb40c67a1cde559c3cd70e4c6fc3e',
        },
        {
          url: '/_next/static/chunks/pages/form-restaurante-f34aca9dd670d61379ed.js',
          revision: '7a68a61f60a722b94ca307146d4f45f5',
        },
        {
          url: '/_next/static/chunks/pages/hospedaje-7e89d2973ebc81392992.js',
          revision: '072ec510029fac84c7555006c52bcc23',
        },
        {
          url: '/_next/static/chunks/pages/index-e4328df78a550e5b9dd0.js',
          revision: '28217f413f6887a86a9eac65cc86115f',
        },
        {
          url: '/_next/static/chunks/pages/mainpage-c79ddb5d1ef6b1e8eba9.js',
          revision: '86dcc77122afeb9aaa2ac6bfa7e6eaea',
        },
        {
          url: '/_next/static/chunks/pages/miorganizacion-d60dc41aec2aa2c218e1.js',
          revision: '0eb0c783b9fe35de1dc674b189c27bea',
        },
        {
          url: '/_next/static/chunks/pages/mispagos-08eba6cee0dbe392df0a.js',
          revision: 'a889bab4c1791c3b5d1b5c9bad188974',
        },
        {
          url: '/_next/static/chunks/pages/misproductos-752b29276e65a5e1d4f7.js',
          revision: 'cb126d5cb0111025ea1f2504c1d35faa',
        },
        {
          url: '/_next/static/chunks/pages/pagosDetails-f7770fcae897f41fa066.js',
          revision: 'f9f9e15b288af9785860d3c0a64464c8',
        },
        {
          url: '/_next/static/chunks/pages/profile-ce712b6cc1e13b792a88.js',
          revision: '911d6720ce97302d2b22b02892ca7e3b',
        },
        {
          url: '/_next/static/chunks/pages/pueblomagico-7f0866fd0f62aab3b1cb.js',
          revision: '41297593cce7fc7b381140f2db532f5a',
        },
        {
          url: '/_next/static/chunks/pages/restaurant-fa5fbc40f672973789ad.js',
          revision: '9f29e30e7ab8040ea721efa31fc0b8dd',
        },
        {
          url: '/_next/static/chunks/pages/travels-43621b19f16d744f6d10.js',
          revision: 'cbc486d115ef73168bcac4bd1e5cf848',
        },
        {
          url: '/_next/static/chunks/pages/vexperience-593b50dc4e35b15baf6e.js',
          revision: 'badf5afceb3866b4c5c73c84d4bd5ab8',
        },
        {
          url: '/_next/static/chunks/polyfills-aa9972cc48f4dc66be42.js',
          revision: '55b3c0b6cb723d75145e44e6c1162d6b',
        },
        {
          url: '/_next/static/chunks/webpack-e067438c4cf4ef2ef178.js',
          revision: '8c19f623e8389f11131a054a7e17ff95',
        },
        { url: '/icons/avatar.png', revision: 'e6886e7b8f2fe98a2358ca9ba528630a' },
        { url: '/icons/hojas.png', revision: '70555ada1b64e21cbd5a27a2e053fecc' },
        { url: '/icons/icon-192x192.png', revision: '3480dfeea9a6cc4fd13c54f52b11d298' },
        { url: '/icons/icon-256x256.png', revision: '873971722f4a698fbc0a024e87ec6abd' },
        { url: '/icons/icon-384x384.png', revision: 'b79661b929f1641443253252b69e11cd' },
        { url: '/icons/icon-512x512.png', revision: '919c36fe564a4abb816b5c55c625d96d' },
        { url: '/icons/preview1.png', revision: '9f9a235410403219b9e1060f366681f9' },
        { url: '/icons/preview2.png', revision: 'd06f17478f33d856831d1ab444ab4512' },
        { url: '/icons/preview3.png', revision: '04476243d86a6beb8a3b93b313c281a4' },
        { url: '/icons/promo.jpg', revision: '0f2d2e248ca50a9e186d6c73f9c46ffb' },
        { url: '/icons/tips.png', revision: '9f528fef43672f64bf3823034d153eba' },
        { url: '/manifest.json', revision: '2c2eec7794014ac08439a3ba23ff1a18' },
      ],
      { ignoreURLParametersMatching: [] }
    ),
    e.cleanupOutdatedCaches(),
    e.registerRoute(
      '/',
      new e.NetworkFirst({
        cacheName: 'start-url',
        plugins: [
          new e.ExpirationPlugin({ maxEntries: 1, maxAgeSeconds: 86400, purgeOnQuotaError: !0 }),
        ],
      }),
      'GET'
    ),
    e.registerRoute(
      /^https:\/\/fonts\.(?:googleapis|gstatic)\.com\/.*/i,
      new e.CacheFirst({
        cacheName: 'google-fonts',
        plugins: [
          new e.ExpirationPlugin({ maxEntries: 4, maxAgeSeconds: 31536e3, purgeOnQuotaError: !0 }),
        ],
      }),
      'GET'
    ),
    e.registerRoute(
      /\.(?:eot|otf|ttc|ttf|woff|woff2|font.css)$/i,
      new e.StaleWhileRevalidate({
        cacheName: 'static-font-assets',
        plugins: [
          new e.ExpirationPlugin({ maxEntries: 4, maxAgeSeconds: 604800, purgeOnQuotaError: !0 }),
        ],
      }),
      'GET'
    ),
    e.registerRoute(
      /\.(?:jpg|jpeg|gif|png|svg|ico|webp)$/i,
      new e.StaleWhileRevalidate({
        cacheName: 'static-image-assets',
        plugins: [
          new e.ExpirationPlugin({ maxEntries: 64, maxAgeSeconds: 86400, purgeOnQuotaError: !0 }),
        ],
      }),
      'GET'
    ),
    e.registerRoute(
      /\.(?:js)$/i,
      new e.StaleWhileRevalidate({
        cacheName: 'static-js-assets',
        plugins: [
          new e.ExpirationPlugin({ maxEntries: 32, maxAgeSeconds: 86400, purgeOnQuotaError: !0 }),
        ],
      }),
      'GET'
    ),
    e.registerRoute(
      /\.(?:css|less)$/i,
      new e.StaleWhileRevalidate({
        cacheName: 'static-style-assets',
        plugins: [
          new e.ExpirationPlugin({ maxEntries: 32, maxAgeSeconds: 86400, purgeOnQuotaError: !0 }),
        ],
      }),
      'GET'
    ),
    e.registerRoute(
      /\.(?:json|xml|csv)$/i,
      new e.NetworkFirst({
        cacheName: 'static-data-assets',
        plugins: [
          new e.ExpirationPlugin({ maxEntries: 32, maxAgeSeconds: 86400, purgeOnQuotaError: !0 }),
        ],
      }),
      'GET'
    ),
    e.registerRoute(
      /\/api\/.*$/i,
      new e.NetworkFirst({
        cacheName: 'apis',
        networkTimeoutSeconds: 10,
        plugins: [
          new e.ExpirationPlugin({ maxEntries: 16, maxAgeSeconds: 86400, purgeOnQuotaError: !0 }),
        ],
      }),
      'GET'
    ),
    e.registerRoute(
      /.*/i,
      new e.NetworkFirst({
        cacheName: 'others',
        networkTimeoutSeconds: 10,
        plugins: [
          new e.ExpirationPlugin({ maxEntries: 32, maxAgeSeconds: 86400, purgeOnQuotaError: !0 }),
        ],
      }),
      'GET'
    )
})
