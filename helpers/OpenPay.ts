declare global {
  interface Window {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    OpenPay: any
  }
}

type tokenizeParams = {
  cardNumber: string
  cardName: string
  cardMonth: string
  cardYear: string
  cardCVV: string
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  address?: any
}

const isDevelopment = process.env.NODE_ENV === 'development'

const OpenPayHelper = {
  initOpenPay: (): void => {
    window.OpenPay.setId(process.env.NEXT_PUBLIC_OPENPAY_MERCHANT_ID)
    window.OpenPay.setApiKey(process.env.NEXT_PUBLIC_OPENPAY_PUBLIC_API_KEY)
    window.OpenPay.setSandboxMode(isDevelopment)
  },
  generateDeviceId: (): void => {
    return window.OpenPay.deviceData.setup()
  },
  getCardBrand: (cardNumber: string): string => {
    return window.OpenPay.card.cardType(cardNumber)
  },
  validateCardNumber: (cardNumber: string): boolean => {
    return window.OpenPay.card.validateCardNumber(cardNumber)
  },
  validateCVC: (cvc: string): boolean => {
    return window.OpenPay.card.validateCVC(cvc)
  },
  validateExpirationDate: (expiryMonth: string, expiryYear: string): boolean => {
    return window.OpenPay.card.validateExpiry(expiryMonth, `20${expiryYear}`)
  },
  tokenize: (
    { cardNumber, cardName, cardMonth, cardYear, cardCVV, address }: tokenizeParams,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    successCallback: (obj: any) => void,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    errorCallback: (obj: any) => void
  ): void => {
    const tokenParams = {
      card_number: cardNumber,
      holder_name: cardName,
      expiration_year: cardYear,
      expiration_month: cardMonth,
      cvv2: cardCVV,
      address,
    }
    window.OpenPay.token.create(tokenParams, successCallback, errorCallback)
  },
}

export default OpenPayHelper
